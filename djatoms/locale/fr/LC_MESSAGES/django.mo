��          |      �          V   !  \   x     �     �            ,   -     Z  '   w     �  )   �  A  �  T     T   i     �     �  	   �     �  &   �       4   '  	   \  ,   f                                          	      
           %(count)s %(name)s was added successfully. %(count)s %(name)s were added successfully. %(count)s %(name)s was modified successfully. %(count)s %(name)s were modified successfully. ADMIN BUTTON Enregistrer ADMIN BUTTON Envoyer ADMIN BUTTON Soumettre ADMIN Téléverser ADMIN Téléverser des données compressées ADMIN Tératothèque - Admin Delete selected %(verbose_name_plural)s Show all Successfully deleted %(count)d %(items)s. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 %(count)s %(name)s ajouté avec succès ! %(count)s %(name)s ajoutés avec succès ! %(count)s %(name)s modifié avec succès. %(count)s %(name)s modifiés avec succès. Enregistrer Envoyer Soumettre Téléverser Téléverser des données compressées Tératothèque - Admin Supprimer les %(verbose_name_plural)s sélectionnés Voir tout %(count)d %(items)s supprimés avec succès. 
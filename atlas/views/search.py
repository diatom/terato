from datetime import datetime as dt
from datetime import timedelta

from django.contrib.postgres.aggregates import StringAgg
from django.db.models import CharField, F, Q, Value
from django.db.models.functions import Concat, ExtractDay, Greatest, Least, NullIf
from django.http import JsonResponse

from ..models import DiatomChildren, Inventory, Station, TaxonCode


def search_taxon(request):

    if request.GET.get("term"):

        taxon = request.GET.get("term").split(",")[0]

        if request.GET.get("pk") and taxon.isdigit():
            object_list = TaxonCode.objects.filter(taxon__id=int(taxon)).select_related(
                "taxon"
            )
        else:
            object_list = TaxonCode.objects.filter(
                Q(taxon__taxon_name__istartswith=taxon) | Q(code__istartswith=taxon)
            ).select_related("taxon")

        object_list = (
            TaxonCode.objects.filter(taxon__id__in=object_list.values("taxon__id"))
            .filter(taxon__freshwater_diatom=True)
            .filter(taxon__is_taxon=True)
            .values("taxon")
            .distinct()
            .annotate(
                label=Concat(
                    F("taxon__taxon_name"),
                    NullIf(
                        Concat(
                            Value(", "),
                            StringAgg(
                                "code",
                                delimiter=", ",
                                distinct=True,
                                ordering="-code",
                            ),
                            output_field=CharField(),
                        ),
                        Value(", "),
                    ),
                ),
                value=F("taxon__id"),
            )
            .order_by("taxon__taxon_name")
            .values("label", "value")
        )
        return JsonResponse(list(object_list), safe=False)
    return JsonResponse([], safe=False)


def search_station(request):

    if request.GET.get("term"):
        station = request.GET.get("term")

        if request.GET.get("pk") and station.isdigit():
            object_list = Station.objects.filter(pk=int(station))
        else:
            object_list = Station.objects.filter(
                Q(station_name__icontains=station) | Q(commune__istartswith=station)
            ).distinct()

        object_list = object_list.annotate(
            label=Concat(
                F("station_name"),
                NullIf(
                    Concat(Value(", "), F("commune")),
                    Value(", "),
                ),
            ),
            value=F("station_id"),
        ).values("label", "value")

        return JsonResponse(list(object_list), safe=False)
    return JsonResponse([], safe=False)


def search_diatom(request):

    if request.GET.get("taxon") and request.GET.get("health"):
        num_per_page = 10

        taxon = request.GET.get("taxon")
        health = request.GET.get("health")

        try:
            page = int(request.GET.get("page", 1))
        except ValueError:
            page = 1

        if page < 0:
            offset = ((-1) * page - 1) * num_per_page
            qs = (
                DiatomChildren.objects.filter(ancestor=taxon)
                .filter(health=health)
                .order_by("upload_date")[offset : offset + num_per_page]  # noqa: 203
                .values_list("diatom", "taxon")
            )
        else:
            offset = (page - 1) * num_per_page
            qs = (
                DiatomChildren.objects.filter(ancestor=taxon)
                .filter(health=health)
                .order_by("-upload_date")[offset : offset + num_per_page]  # noqa: 203
                .values_list("diatom", "taxon")
            )

        results = [{"id": obj[0], "taxon": obj[1]} for obj in qs]

        return JsonResponse(results, safe=False)
    return JsonResponse([], safe=False)


def search_inventory(request):
    if request.GET.get("date", None) and request.GET.get("station", None):

        try:
            date = dt.strptime(request.GET.get("date"), "%Y-%m-%d")
            station = request.GET.get("station")

            inventory = (
                Inventory.objects.filter(
                    Q(station=station)
                    & Q(inventory_type=1)
                    & Q(inventory_date__lte=date + timedelta(15))
                    & Q(inventory_date__gte=date - timedelta(60))
                )
                .annotate(
                    ecart=ExtractDay(
                        Greatest(date, F("inventory_date"))
                        - Least(date, F("inventory_date"))
                    )
                )
                .order_by("ecart")
            )

            if inventory:
                return JsonResponse(
                    {
                        "inventory": inventory[0].inventory_id,
                        "date": inventory[0].inventory_date.date(),
                    }
                )
        except ValueError:
            # happens if date can't be stripped
            return JsonResponse([], safe=False)

        return JsonResponse({"inventory": -1})
    return JsonResponse([], safe=False)

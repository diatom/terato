from django.contrib.auth import get_user_model
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from tests import RedirectionTestMixin, TestCase
from tests.utils import DEFAULT_EMAIL, DEFAULT_PWD

User = get_user_model()


class TestLoggedOutRedirects(RedirectionTestMixin, TestCase):

    fixtures = ["users"]

    def setUp(self):
        super().setUp()

        u = User.objects.get(email=DEFAULT_EMAIL)

        uid = urlsafe_base64_encode(force_bytes(u.pk))
        token = "whatever"

        self.expected = {
            ("profile",): ("login",),
            ("password_change",): ("login",),
            ("password_reset",): (None,),
            ("password_reset_done",): (None,),
            ("password_reset_confirm", uid, token): (None,),
            ("login",): (None,),
            ("logout",): ("login",),
            ("register",): (None,),
            ("register_done",): (None,),
            ("register_confirm", uid, token): (None,),
            ("delete",): ("login",),
        }


class TestLoggedInRedirects(RedirectionTestMixin, TestCase):

    fixtures = ["users"]

    def setUp(self):
        super().setUp()

        u = User.objects.get(email=DEFAULT_EMAIL)

        self.client.login(username=DEFAULT_EMAIL, password=DEFAULT_PWD)

        uid = urlsafe_base64_encode(force_bytes(u.pk))
        token = "whatever"

        self.expected = {
            ("profile",): (None,),
            ("password_change",): (None,),
            ("password_reset",): (None,),
            ("password_reset_done",): (None,),
            ("password_reset_confirm", uid, token): (None,),
            ("login",): ("profile",),
            ("logout",): ("profile",),
            ("register",): ("profile",),
            ("register_done",): ("profile",),
            ("register_confirm", uid, token): ("profile",),
            ("delete",): (None,),
        }

    def tearDown(self):
        self.client.logout()

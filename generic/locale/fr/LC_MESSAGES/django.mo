��    I      d  a   �      0  �   1    �  �  �  �   �  $  �  E  �  7  	  �  A  �  �  �   �  �  A  �  �  z   �  !  :    \  �   n  h  b  x  �  �  D   =  !#  4  _$  �   �,  �   -  �   �-  t   7.     �.     �.     �.     �.     /      /     </     Q/  	   j/  .   t/  $   �/  &   �/     �/     �/     0  @   (0  /   i0  .   �0  "   �0     �0     1     "1      ;1  !   \1     ~1  #   �1  $   �1  #   �1  #   2     /2     M2     \2     q2     �2     �2  !   �2  "   �2     �2     3     43  &   P3     w3  ,   �3     �3     �3     �3  &   �3  A  !4  �   c5    (6  �  /8  �   �:  $  �;  �  �<  7  y?  �  �@  �  iB  �   �C  �  �D  �  fF  z   /H    �H    �I  �   �K  �  �L  x  �N  �  P  =  �R  5  T  �   T\  �   �\  �   �]  �   o^  
   _     _     )_  
   8_  
   C_     N_  
   `_     k_  $   z_  $   �_     �_     �_     �_     `     '`  5   7`  Q   m`  Q   �`  )   a  (   ;a  �   da  �   3b  �   �b  d   Cc  v   �c  ,  d  0  Lf  �  }g    @j  �  _k  D   [o     �o     �o     �o     �o  ,   �o     !p     ;p  2   Pp  I   �p     �p     �p      �p     q     "q     2q     ?q               8   I   .   -                                         E   G                   2                 7      )         5       @              =       !   ;   3   ?       1   %       A       	   &   /   0          <   B   (   >      ,      '   4           $              +      #   6             F   
   9   C   "   *   D                    :   H        
                    <li>SIREN : 180070039</li>
                    <li>Code APE : 7219Z</li>
                    <li>Numéro de TVA intracommunautaire : FR 57 1800700039</li>
                     
                    Autres crédits :
                    <ul>
                        <li>
                            L'apparence de ce site utilise une version customisée de <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>.
                        </li>
                        <li>
                            Ce site utilise les fonctionnalités de <a href="https://www.djangoproject.com/" target="_blank">Django</a>.
                        </li>
                    </ul>
                     
                    Ce site a pour vocation de permettre à la communauté scientifique étudiant les diatomées de partager des photos de diatomées, déformées ou non, ainsi que les
                    conditions environnementales associées à ces observations. Les objectifs sont :
                    <ul>
                        <li>
                            Constituer une iconographie de référence, accessible à tous, pouvant servir d'appui à identifier une diatomée comme déformée ou non ;
                        </li>
                        <li>Utiliser les données collectées pour étudier statistiquement les déformations des diatomées.</li>
                    </ul>
                     
                    Ce site n'emploie pas de cookies, outre ceux strictement nécessaires à son fonctionnement pour la connexion des utilisateurs et la sécurité de leurs contributions.
                     
                    Cette page concerne les conditions d'utilisation du site Internet.
                    Elle précise les droits et devoirs respectifs des utilisateurs du site Internet et de l'éditeur et hébergeur de ce dernier, défini comme l'unité EABX d'INRAE.
                     
                    Crédits des icônes :
                    <ul>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/france" title="france icônes">Drapeau de la France créé par Freepik - Flaticon</a>
                        </li>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/royaume-uni" title="royaume-uni icônes">Drapeau du Royaume-Uni créé par Freepik - Flaticon</a>
                        </li>
                    </ul>
                     
                    Des données personnelles sont recueillies auprès des utilisateurs remplissant le formulaire d'inscription. Les informations relatives au traitement de ces données personnelles sont disponibles dans nos <a href="%(link_cgu)s">conditions générales d'utilisation</a>.
                     
                    Deux catégories d'utilisateurs sont à distinguer, en fonction de leur inscription ou non. Les utilisateurs non inscrits ne peuvent que consulter le site,
                    sans possibilité d'y ajouter du contenu. Leurs droits et devoirs, ainsi que les droits et devoirs de l'éditeur envers eux, se limitent aux principes élémentaires régissant l'Internet et la propriété intellectuelle.
                     
                    En vertu de ce principe de confiance, l'éditeur et INRAE déclinent toute responsabilité en cas d'ajout de contenu inexact, inapproprié ou illégal. Néanmoins, afin de limiter les désagréments pour les autres utilisateurs, l'éditeur s'engage à assurer de façon régulière la modification ou la suppression, le cas échéant, de tels contenus.
                     
                    Hormis le contenu des contributions, l'éditeur se réserve tout droit de propriété intellectuelle sur le contenu actuel et à venir de ce site.
                     
                    INRAE est un établissement public à caractère scientifique et technologique.
                    Il a son siège au 147, rue de l’université – 75338 Paris cedex 07.
                    Ses statuts sont publiés dans le Code rural et de la pêche maritime (articles R831-1 et suivants).
                    Il est représenté par son président-directeur général, M. Philippe MAUGUIN.
                     
                    L'ajout de données sur la plateforme ne modifie en rien la propriété intellectuelle de ces données, qui restent donc la propriété du contributeur, de son institution ou du tiers dont il a obtenu la permission d'effectuer l'ajout. La photographie est ajoutée par le contributeur sous la licence CC-BY-NC, qui en autorise le partage et l'utilisation à des fins non commerciales, sous condition d'attribution.
                     
                    L'hébergement est assuré par l'unité EABX, 50 avenue de Verdun, 33612 Cestas.
                     
                    La plateforme Terato fonctionne sur un principe de confiance. Les utilisateurs s'engagent à n'ajouter que des informations à leurs connaissances exactes. En cas d'erreur, la possibilité de modifier ou de supprimer leurs ajouts leur est ouverte.
                     
                    La possibilité de contribuer à la plateforme est garantie à tous les utilisateurs inscrits dès lors qu'ils sont connectés. L'éditeur s'engage à mettre tout en oeuvre pour assurer le maintien et la sécurité du site.
                    Néanmoins, des interruptions de service pourront avoir lieu en cas de mise à jour, de panne ou de force majeure. Si une interruption prévisible devait s'étendre au-delà de 48 heures, les utilisateurs inscrits en seront prévenus par mail.
                     
                    Le site Plateforme Terato est édité par l'unité EABX (Ecosystèmes aquatiques et changements globaux) d'INRAE, l'Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement.
                     
                    Les données personnelles collectées ne font l'objet d'aucun autre usage, et ne sont transmises à aucun tiers. Elles sont conservées pour la durée totale de leur inscription. A tout moment, les utilisateurs ont la possibilité de voir leurs données rectifiées ou effacées en contactant l'administrateur du site.
                     
                    Les photos ajoutées par les contributeurs de la plateforme, ainsi que les informations attenantes, relèvent de leur responsabilité exclusive.
                    INRAE ne saurait être tenu responsable de tout contenu erroné ou inapproprié, comme le précisent nos <a href="%(link_cgu)s">conditions générales d'utilisation</a>.
                     
                    Les utilisateurs s'engagent à n'ajouter que des photos de diatomées dont ils, ou l'institution qu'ils représentent et à laquelle ils déclarent appartenir, détiennent la propriété intellectuelle, ou dont ils obtenu du détenteur de la propriété intellectuelle la permission expresse d'effectuer cet ajout.
                    L'éditeur et INRAE ne seraient en aucun cas être tenus responsables si un contributeur venait à ajouter des données sans en avoir le droit ou l'autorisation. En cas de litige, néanmoins, l'éditeur s'engage à supprimer de bonne foi les données concernées, à la demande du contributeur ou du véritable détenteur de la propriété intellectuelle.
                     
                    Les utilisateurs sont considérés comme inscrits après remplissage du <a href="%(register_link)s">formulaire d'inscription</a>, au terme duquel une case à cocher obligatoirement vaut pour
                    acceptation des présentes conditions générales d'utilisation.
                     
                    Lors de l'inscription, les utilisateurs renseignent leurs noms, prénoms et adresse mail. Ils peuvent également renseigner l'institution à laquelle ils appartiennent. Ces données personnelles ne sont utilisées que pour les finalités suivantes, dont la base légale est précisée entre parenthèses :
                    <ul>
                        <li>
                            Le nom, prénom et institution d'appartenance sont affichés sur chaque contribution. Cette identification est reconnue nécessaire à garantir la qualité scientifique de la plateforme (intérêt légitime) ;
                        </li>
                        <li>
                            L'adresse mail est utilisée pour la connexion, de même nécessaire à la qualité scientifique de la plateforme (intérêt légitime) ;
                        </li>
                        <li>
                            L'adresse mail pourra aussi être utilisée pour contacter les utilisateurs dans les cas suivants (consentement) :
                            <ul>
                                <li>Pour les prévenir d'une interruption de service de plus de 48 heures ;</li>
                                <li>
                                    Pour leur communiquer, à leur demande, un lien de renouvellement de leur mot de passe, en cas d'oubli de ce dernier ;
                                </li>
                                <li>Pour leur signaler un manquement ou abus dans une de leurs contributions ;</li>
                                <li>
                                    Pour leur proposer de devenir modérateur du site, s'ils remplissent les conditions évoquées dans la section Règlement des contributions.
                                </li>
                                <li>
                                    Pour leur signaler une modification des présentes conditions générales d'utilisation.
                                </li>
                            </ul>
                        </li>
                    </ul>
                     
    La page demandée n'existe pas, ou a été déplacée. Vérifiez l'URL, ou reprenez votre navigation depuis l'accueil.
     
    Une de vos actions a généré une mauvaise requête vers notre serveur. Réessayez, ou reprenez votre navigation depuis l'accueil.
     
    Une erreur est survenue de notre côté. Nous la corrigerons dès que possible. Dans l'attente, vous pouvez revenir à la page d'accueil.
     
    Vous n'avez pas accès à cette ressource. Vérifiez l'URL, ou reprenez votre navigation depuis l'accueil.
     400 TITLE Erreur 400 400 TITLE Requête invalide 403 TITLE Accès refusé 403 TITLE Erreur 403 404 TITLE Erreur 404 404 TITLE Page non trouvée 500 TITLE Erreur 500 500 TITLE Erreur interne CGU TITLE CGU TITLE Inscription et données personnelles CGU TITLE Propriété intellectuelle CGU TITLE Règlement des contributions HEADER Ajouter HEADER Consulter HOME ALT Photo diatomée HOME CHART Cause probable des tératologies dans la littérature HOME CHART Probabilité d'occurrence Fragilaria HOME CHART Probabilité d'occurrence Nitzschia HOME CHART Source causes probables HOME CHART Source probabilités HOME CONTENT Bienvenue HOME CONTENT Financement HOME CONTENT Objectifs Collecter HOME CONTENT Objectifs Constituer HOME CONTENT Objectifs Utiliser HOME CONTENT Pourquoi tératologies HOME CONTENT Que sont les diatomées HOME CONTENT Terato bioindication 1 HOME CONTENT Terato bioindication 2 HOME CONTENT Terato typologie HOME NAME Home HOME TITLE Bienvenue HOME TITLE Financement HOME TITLE Objectifs HOME TITLE Plus d’infos HOME TITLE Pourquoi tératologies HOME TITLE Que sont les diatomées HOME TITLE Références HOME TITLE Terato bioindication HOME TITLE Terato typologie LEGAL CONTENT Numéro de téléphone : LEGAL TITLE LEGAL TITLE Cookies et données personnelles LEGAL TITLE Crédits LEGAL TITLE Editeur du site LEGAL TITLE Hébergement LEGAL TITLE Propriété intellectuelle Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
                    <li>SIREN : 180070039</li>
                    <li>Code APE : 7219Z</li>
                    <li>Numéro de TVA intracommunautaire : FR 57 1800700039</li>
                     
                    Autres crédits :
                    <ul>
                        <li>
                            L'apparence de ce site utilise une version customisée de <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>.
                        </li>
                        <li>
                            Ce site utilise les fonctionnalités de <a href="https://www.djangoproject.com/" target="_blank">Django</a>.
                        </li>
                    </ul>
                     
                    Ce site a pour vocation de permettre à la communauté scientifique étudiant les diatomées de partager des photos de diatomées, déformées ou non, ainsi que les
                    conditions environnementales associées à ces observations. Les objectifs sont :
                    <ul>
                        <li>
                            Constituer une iconographie de référence, accessible à tous, pouvant servir d'appui à identifier une diatomée comme déformée ou non ;
                        </li>
                        <li>Utiliser les données collectées pour étudier statistiquement les déformations des diatomées.</li>
                    </ul>
                     
                    Ce site n'emploie pas de cookies, outre ceux strictement nécessaires à son fonctionnement pour la connexion des utilisateurs et la sécurité de leurs contributions.
                     
                    Cette page concerne les conditions d'utilisation du site Internet.
                    Elle précise les droits et devoirs respectifs des utilisateurs du site Internet et de l'éditeur et hébergeur de ce dernier, défini comme l'unité EABX d'INRAE.
                     
                    Crédits des icônes :
                    <ul>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/france"
                               title="france icônes">Drapeau de la France créé par Freepik - Flaticon</a>
                        </li>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/royaume-uni"
                               title="royaume-uni icônes">Drapeau du Royaume-Uni créé par Freepik - Flaticon</a>
                        </li>
                    </ul>
                     
                    Des données personnelles sont recueillies auprès des utilisateurs remplissant le formulaire d'inscription. Les informations relatives au traitement de ces données personnelles sont disponibles dans nos <a href="%(link_cgu)s">conditions générales d'utilisation</a>.
                     
                    Deux catégories d'utilisateurs sont à distinguer, en fonction de leur inscription ou non. Les utilisateurs non inscrits ne peuvent que consulter le site,
                    sans possibilité d'y ajouter du contenu. Leurs droits et devoirs, ainsi que les droits et devoirs de l'éditeur envers eux, se limitent aux principes élémentaires régissant l'Internet et la propriété intellectuelle.
                     
                    En vertu de ce principe de confiance, l'éditeur et INRAE déclinent toute responsabilité en cas d'ajout de contenu inexact, inapproprié ou illégal. Néanmoins, afin de limiter les désagréments pour les autres utilisateurs, l'éditeur s'engage à assurer de façon régulière la modification ou la suppression, le cas échéant, de tels contenus.
                     
                    Hormis le contenu des contributions, l'éditeur se réserve tout droit de propriété intellectuelle sur le contenu actuel et à venir de ce site.
                     
                    INRAE est un établissement public à caractère scientifique et technologique.
                    Il a son siège au 147, rue de l’université – 75338 Paris cedex 07.
                    Ses statuts sont publiés dans le Code rural et de la pêche maritime (articles R831-1 et suivants).
                    Il est représenté par son président-directeur général, M. Philippe MAUGUIN.
                     
                    L'ajout de données sur la plateforme ne modifie en rien la propriété intellectuelle de ces données, qui restent donc la propriété du contributeur, de son institution ou du tiers dont il a obtenu la permission d'effectuer l'ajout. La photographie est ajoutée par le contributeur sous la licence CC-BY-NC, qui en autorise le partage et l'utilisation à des fins non commerciales, sous condition d'attribution.
                     
                    L'hébergement est assuré par l'unité EABX, 50 avenue de Verdun, 33612 Cestas.
                     
                    La Tératothèque fonctionne sur un principe de confiance. Les utilisateurs s'engagent à n'ajouter que des informations à leurs connaissances exactes. En cas d'erreur, la possibilité de modifier ou de supprimer leurs ajouts leur est ouverte.
                     
                    La possibilité de contribuer à la plateforme est garantie à tous les utilisateurs inscrits dès lors qu'ils sont connectés. L'éditeur s'engage à mettre tout en œuvre pour assurer le maintien et la sécurité du site.
                    Néanmoins, des interruptions de service pourront avoir lieu en cas de mise à jour, de panne ou de force majeure. Si une interruption prévisible devait s'étendre au-delà de 48 heures, les utilisateurs inscrits en seront prévenus par mail.
                     
                    La Tératothèque est un site édité par l'unité EABX (Ecosystèmes aquatiques et changements globaux) d'INRAE, l'Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement.
                     
                    Les données personnelles collectées ne font l'objet d'aucun autre usage, et ne sont transmises à aucun tiers. Elles sont conservées pour la durée totale de leur inscription. A tout moment, les utilisateurs ont la possibilité de voir leurs données rectifiées ou effacées en contactant l'administrateur du site. Le lien pour contacter ce dernier est disponible en bas de page sur tout le site.
                     
                    Les photos ajoutées par les contributeurs de la plateforme, ainsi que les informations attenantes, relèvent de leur responsabilité exclusive.
                    INRAE ne saurait être tenu responsable de tout contenu erroné ou inapproprié, comme le précisent nos <a href="%(link_cgu)s">conditions générales d'utilisation</a>.
                     
                    Les utilisateurs s'engagent à n'ajouter que des photos de diatomées dont ils, ou l'institution qu'ils représentent et à laquelle ils déclarent appartenir, détiennent la propriété intellectuelle, ou dont ils obtenu du détenteur de la propriété intellectuelle la permission expresse d'effectuer cet ajout.
                    L'éditeur et INRAE ne seraient en aucun cas être tenus responsables si un contributeur venait à ajouter des données sans en avoir le droit ou l'autorisation. En cas de litige, néanmoins, l'éditeur s'engage à supprimer de bonne foi les données concernées, à la demande du contributeur ou du véritable détenteur de la propriété intellectuelle.
                     
                    Les utilisateurs sont considérés comme inscrits après remplissage du <a href="%(register_link)s">formulaire d'inscription</a>, au terme duquel une case à cocher obligatoirement vaut pour
                    acceptation des présentes conditions générales d'utilisation.
                     
                    Lors de l'inscription, les utilisateurs renseignent leurs noms, prénoms et adresse mail. Ils peuvent également renseigner l'institution à laquelle ils appartiennent. Ces données personnelles ne sont utilisées que pour les finalités suivantes, dont la base légale est précisée entre parenthèses :
                    <ul>
                        <li>
                            Le nom, prénom et institution d'appartenance sont affichés sur chaque contribution. Cette identification est reconnue nécessaire à garantir la qualité scientifique de la plateforme (intérêt légitime) ;
                        </li>
                        <li>
                            L'adresse mail est utilisée pour la connexion, de même nécessaire à la qualité scientifique de la plateforme (intérêt légitime) ;
                        </li>
                        <li>
                            L'adresse mail pourra aussi être utilisée pour contacter les utilisateurs dans les cas suivants (consentement) :
                            <ul>
                                <li>Pour les prévenir d'une interruption de service de plus de 48 heures ;</li>
                                <li>
                                    Pour leur communiquer, à leur demande, un lien de renouvellement de leur mot de passe, en cas d'oubli de ce dernier ;
                                </li>
                                <li>Pour leur signaler un manquement ou abus dans une de leurs contributions ;</li>
                                <li>
                                    Pour leur proposer de devenir modérateur du site, s'ils remplissent les conditions évoquées dans la section Règlement des contributions ;
                                </li>
                                <li>
                                    Pour leur signaler une modification des présentes conditions générales d'utilisation.
                                </li>
                            </ul>
                        </li>
                    </ul>
                     
                        La page demandée n'existe pas, ou a été déplacée. Vérifiez l'URL, ou reprenez votre navigation depuis l'accueil.
                         
                        Une de vos actions a généré une mauvaise requête vers notre serveur. Réessayez, ou reprenez votre navigation depuis l'accueil.
                         
                        Une erreur est survenue de notre côté. Nous la corrigerons dès que possible. Dans l'attente, vous pouvez revenir à la page d'accueil.
                         
                        Vous n'avez pas accès à cette ressource. Vérifiez l'URL, ou reprenez votre navigation depuis l'accueil.
                         Erreur 400 Requête invalide Accès refusé Erreur 403 Erreur 404 Page non trouvée Erreur 500 Erreur interne Conditions Générales d'Utilisation Inscription et données personnelles Propriété intellectuelle Règlement des contributions Ajouter une diatomée Consulter le recueil Photo diatomée Cause probable des tératologies dans la littérature Probabilité d'occurrence des formes normales (FCAP) et tératologiques (FCAT) de Probabilité d'occurrence des formes normales (NPAL) et tératologiques (NPTR) de Source : adapté de Falasco et al. (2021) Source : chiffres de Coste et al. (2009) Cette interface interactive et collaborative vise à documenter les tératologies des diatomées, en recueillant les photos de spécimens déformés et les conditions de milieu associées à leur présence. Ce projet est financé par l’Office Français de la Biodiversité (OFB) au travers de la programmation Aquaref 2022-2024 (Thème B). <b>Collecter</b> des images de diatomées déformées, accompagnées de leurs données environnementales, par une approche collaborative <b>Constituer</b> une iconographie de référence pour l’identification des diatomées déformées <b>Utiliser</b> ce jeu de données pour établir des corrélations entre déformations et facteurs de stress associés Diverses études indiquent que les déformations du frustule des diatomées sont caractéristiques de stress toxique. Sur le corpus de publications mentionnant des diatomées déformées (issu de <a href='https://doi.org/10.1016/j.ecolind.2017.06.048' target='_blank'>Lavoie et al. 2017</a> et <a href='https://doi.org/10.1007/s10750-021-04540-x' target='_blank'>Falasco et al. 2021</a>), la présence de contaminants toxiques (barres rouges ci-contre) est en effet préférentiellement mentionnée comme cause probable de l’apparition des tératologies. Les diatomées, algues microscopiques unicellulaires omniprésentes dans les milieux aquatiques, sont utilisées comme bioindicateurs de qualité des eaux des rivières et des lacs. Les espèces de diatomées sont identifiables à la forme et à l’ornementation de leur squelette siliceux, le frustule. Face au constat de l’augmentation de fréquence des anomalies en conditions d’exposition toxique, certains indices diatomiques considèrent l’abondance relative des diatomées tératologiques comme un critère déclassant de qualité du milieu (par exemple l’IBD, <a href='https://doi.org/10.1016/j.ecolind.2008.06.003' target='_blank'>Coste et al. 2009</a>, graphique ci-dessous), en attribuant aux spécimens déformés un profil spécifique.En l’absence de prise en compte du pourcentage de déformations, les notes d’indice peuvent être surestimées (<a href='https://doi.org/10.1007/978-3-030-39212-3_4' target='_blank'>Olenici et al. 2020</a>) et ainsi surévaluer la qualité du milieu. Malgré leur pertinence, les déformations sont généralement peu renseignées, en raison notamment de l’insuffisance de documentation concernant les formes anormales existantes et de la difficulté pour les opérateurs à établir la limite entre normalité et déformation subtile. Les déformations concernent l’altération du contour des individus (perte de symétrie notamment) ou de l’ornementation du frustule. La tendance à la déformation, et l’aptitude à manifester l’un ou l’autre des types d’anomalies du frustule, semblent dépendante des espèces (<a href='https://doi.org/10.1007/s10750-021-04540-x' target='_blank'>Falasco et al. 2021</a>). Ainsi, <a href='https://doi.org/10.1016/j.ecolind.2017.06.048' target='_blank'>Lavoie et al. (2017)</a> suggéraient de considérer la dimension taxonomique en plus de la fréquence, toutes espèces confondues, de tératologies, pour le diagnostic de pollution toxique. Des travaux récents d’analyse morphométrique avancent également que la sévérité des déformations pourrait traduire l’intensité du niveau de pollution toxique (<a href='https://doi.org/10.1007/s10646-017-1830-3' target='_blank'>Olenici et al. 2017</a>, <a href='https://doi.org/10.1080/23818107.2018.1474800' target='_blank'>Cerisier et al. 2019</a>). La Tératothèque : références pour les diatomées tératologiques Bienvenue sur la Tératothèque Financement Objectifs du projet Plus d'informations Pourquoi s’intéresser aux tératologies ? Que sont les diatomées ? Références citées Bioindication et prise en compte des tératologies Tératologies : une question d’espèces, de typologie et de sévérité Numéro de téléphone : Mentions légales Cookies et données personnelles Crédits Editeur du site Hébergement Propriété intellectuelle 
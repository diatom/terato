from base64 import b64encode

from django.conf import settings
from django.contrib import admin
from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BTreeIndex
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _


class Health(models.Model):
    id = models.AutoField(primary_key=True, db_column="health_id")
    health_abb = models.CharField(
        max_length=1, db_comment="One-letter abbreviation of health status"
    )
    health_full = models.CharField(
        max_length=15, db_comment="Full name for health status (fallback)"
    )

    class Meta:
        db_table = 'terato"."health'
        db_table_comment = "Health status options for uploaded diatoms"
        managed = False

    def __str__(self):
        return self.health_full


class Diatom(models.Model):

    id = models.AutoField(primary_key=True, db_column="diatom_id")
    taxon = models.ForeignKey(
        "Taxon", models.DO_NOTHING, db_column="taxon_id", verbose_name=_("Taxon")
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("User")
    )
    health = models.ForeignKey(
        "Health", models.DO_NOTHING, db_column="health_id", verbose_name=_("Health")
    )
    inventory = models.ForeignKey(
        "Inventory",
        models.DO_NOTHING,
        null=True,
        blank=True,
        db_column="inventory_id",
        verbose_name=_("Inventory"),
    )
    station = models.ForeignKey(
        "Station",
        models.DO_NOTHING,
        null=True,
        blank=True,
        db_column="station_id",
        verbose_name=_("Station"),
    )
    sampling_date = models.DateTimeField(
        _("Sampling date"), null=True, blank=True, db_comment="Sampling date"
    )

    photo = models.BinaryField(db_comment="Entire image data", editable=True)
    photo_thumbnail = models.BinaryField(db_comment="Thumbnail image data")
    filename = models.CharField(
        _("Filename"),
        db_comment="Original name of image file",
        db_column="photo_filename",
    )
    height = models.IntegerField(db_comment="Height of image, in pixels")
    width = models.IntegerField(db_comment="Width of image, in pixels")
    scale = models.DecimalField(
        _("Scale"),
        max_digits=16,
        decimal_places=10,
        null=True,
        blank=True,
        db_comment="100px represent this much micrometers",
        help_text="100px represent this much micrometers",
    )
    upload_date = models.DateTimeField(
        _("Upload date"), auto_now=False, auto_now_add=True, db_comment="Upload date"
    )
    photo_author = models.CharField(
        _("Photo author"),
        null=True,
        blank=True,
        db_comment="Original author of the photo",
    )
    photo_doi = models.CharField(
        _("Photo DOI"),
        null=True,
        blank=True,
        db_comment="DOI of the publication where the photo first appeared",
    )

    def __str__(self):
        return self.filename

    def get_real_width(self):
        if self.scale:
            return round((10 * 100) / self.scale, 1)
        else:
            return None

    @admin.display(description=_("Thumbnail"))
    def thumbnail_tag(self):
        return mark_safe(
            '<img height=45 src="data: image/png; base64, %s" />'
            % (b64encode(self.photo).decode("utf8"))
        )

    class Meta:
        db_table = 'terato"."diatom'
        managed = False
        verbose_name = _("Diatom")
        verbose_name_plural = _("Diatoms")

        indexes = [
            BTreeIndex(fields=["taxon"], name="diatom_taxon_id_idx"),
            BTreeIndex(fields=["user"], name="diatom_user_id_idx"),
            BTreeIndex(fields=["health"], name="diatom_health_id_idx"),
        ]


class DiatomChildren(models.Model):

    diatom = models.ForeignKey(
        "Diatom",
        on_delete=models.DO_NOTHING,
        db_column="diatom_id",
        related_name="children",
    )
    health = models.IntegerField(db_column="health_id")
    taxon = models.CharField(db_column="taxon_name")
    ancestor = models.IntegerField(db_column="taxon_ancestor")
    upload_date = models.DateTimeField()

    class Meta:
        db_table = 'terato"."children_diatoms'
        managed = False

        indexes = [
            BTreeIndex(fields=["ancestor"], name="children_diatoms_ancestor_id_idx"),
            BTreeIndex(fields=["health"], name="children_diatoms_health_id_idx"),
        ]

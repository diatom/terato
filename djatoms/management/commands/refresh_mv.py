from django.core.management.base import BaseCommand
from django.db import connections
from psycopg2 import sql


class Command(BaseCommand):
    help = "Refresh a materialized view, given its name"

    def add_arguments(self, parser):
        parser.add_argument("table_name", type=str)

    def handle(self, *args, **options):

        with connections["default"].cursor() as cursor:
            instr = (
                sql.SQL("REFRESH MATERIALIZED VIEW CONCURRENTLY {};")
                .format(sql.Identifier(*options["table_name"].split(".")))
                .as_string(cursor.cursor)
            )
            cursor.execute(instr)

from django.contrib.gis.geos import Polygon
from django.db.models import Q
from djgeojson.views import GeoJSONLayerView


class LeafletLayerView(GeoJSONLayerView):

    with_modelname = False

    def setup(self, request, *args, **kwargs):
        self.request = request
        super().setup(request, *args, **kwargs)

    def get_queryset(self):

        qs = super().get_queryset()

        x0 = self.request.GET.get("x0")
        x1 = self.request.GET.get("x1")
        y0 = self.request.GET.get("y0")
        y1 = self.request.GET.get("y1")

        if x0 and x1 and y0 and y1:
            bounds = Polygon.from_bbox((x0, y0, x1, y1))
            qs = qs.filter(Q(geom__within=bounds))
        else:
            qs = qs[:10]
        return qs

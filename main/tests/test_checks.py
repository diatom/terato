from io import StringIO

from django.core.management import call_command
from django.core.management.base import SystemCheckError
from django.test import TestCase, override_settings

from main.models import DbVersion


class MainCheckTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        DbVersion.objects.create(version_name="0.0", version_date="2024-06-15")
        DbVersion.objects.create(version_name="0.1", version_date="2024-07-01")

    @override_settings(VERSION="")
    def test_no_version_in_settings(self):
        message = "(main.E001) Improperly configured."

        with self.assertRaisesMessage(SystemCheckError, message):
            call_command("check", "--deploy")

    @override_settings(VERSION="0.0.1")
    def test_outdated_version_in_settings(self):
        message = "(main.E002) Improperly configured."

        with self.assertRaisesMessage(SystemCheckError, message):
            call_command("check", "--deploy")

    @override_settings(VERSION="0.1.2")
    def test_correct_version_in_settings(self):
        stderr = StringIO()
        call_command("check", "--deploy", stderr=stderr)
        self.assertNotIn("main.E00", stderr.getvalue())

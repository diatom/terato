import "jquery-ui/ui/widgets/autocomplete";

import L from "leaflet";
import "leaflet/dist/images/marker-shadow.png";
import "leaflet.markercluster";

import { createMap } from "./leaflet";

/* global gettext */

export function validDisplay(el) {
  el.removeClass("c-border-danger").addClass("c-border-success");
}

export function invalidDisplay(el) {
  el.removeClass("c-border-success").addClass("c-border-danger");
}

export function preparePage(calc) {
  const geo_tick = JSON.parse(document.getElementById("geotick").textContent);

  var lang = window.location.href.split("/")[3];

  function simulateClick(tar) {
    var event = new MouseEvent("click", {
      view: window,
      bubbles: true,
      cancelable: true,
    });
    tar.dispatchEvent(event);
  }

  // Common logic for abling and disabling "next" buttons
  var observers = {};
  function observerCallback(node) {
    var num = parseInt(node.id.split("-")[1]);

    if (node.getElementsByClassName("c-border-danger").length == 0) {
      $("#btn-next-" + num).removeClass("disabled");
      $("#step-" + (num + 1) + "-tab").removeClass("disabled");
    } else {
      $("#btn-next-" + num).addClass("disabled");
      for (let i = 4; i > num; i--) {
        $("#step-" + i + "-tab").addClass("disabled");
      }
    }
  }

  for (let i = 1; i < 5; i++) {
    // making sure clicking on next and prev has the same effect as clicking on corresponding tab
    $("#btn-prev-" + i).on("click", function () {
      simulateClick($("#step-" + (i - 1) + "-tab").get(0));
    });
    if (i < 4) {
      $("#btn-next-" + i).on("click", function () {
        simulateClick($("#step-" + (i + 1) + "-tab").get(0));
      });
    }

    // make "next" available only if there is no column with the border-danger class in corresponding tab
    observers[i] = new MutationObserver(() => {
      observerCallback(document.getElementById("step-" + i));
    });
    observers[i].observe(document.getElementById("step-" + i), {
      attributes: true,
      subtree: true,
      attributeFilter: ["class"],
    });
  }

  // disabling "Enter" in input fields to avoid validation
  $(":input:not(:button)").on("keydown", function (event) {
    if (event.key == "Enter") {
      event.preventDefault();
    }
  });

  // Logic for step 2
  $("#step-3-tab").on("click", function () {
    if (calc.ratio) {
      $("#id_scale").val(
        Math.round(
          (($("#id_scale_human").val() * 100) /
            (Math.hypot(calc.lenX, calc.lenY) / calc.ratio)) *
            10000
        ) / 10000
      );
    }
  });

  // Logic for step 3
  // More on taxon autocomplete
  var observer_taxon = new MutationObserver(() => {
    if ($("input[name=taxon]").val() != "") {
      validDisplay($("#col-taxon"));
    } else {
      invalidDisplay($("#col-taxon"));
    }
  });
  observer_taxon.observe(document.getElementsByName("taxon")[0], {
    attributes: true,
    attributeFilter: ["value"],
  });

  $("input[name=taxon_label]").on("change keyup focusout", function () {
    if ($("input[name=taxon_label]").val().includes("abnormal form")) {
      $("#id_health").val("2");
      $("#id_health option[value='']").prop("disabled", true);
      $("#id_health option[value='1']").prop("disabled", true);
      $("#id_health option[value='3']").prop("disabled", true);
    } else {
      $("#id_health option[value='']").prop("disabled", false);
      $("#id_health option[value='1']").prop("disabled", false);
      $("#id_health option[value='3']").prop("disabled", false);
    }
  });

  $("#id_health").on("change", () => {
    if ($("#id_health").val()) {
      validDisplay($("#col-status"));
    } else {
      invalidDisplay($("#col-status"));
    }
  });

  // Logic for step 4
  // Station autocomplete

  var observer_station = new MutationObserver(() => {
    if (
      $("input[name=station]").val() != "" &&
      $("input[name=station]").val() != -1
    ) {
      findSelected();
    }
    update_inventory();
  });
  observer_station.observe(document.getElementsByName("station")[0], {
    attributes: true,
    attributeFilter: ["value"],
  });

  // Inventory from date and Station
  function update_inventory() {
    if (
      $("#id_sampling_date").val() &&
      $("input[name=station]").val() &&
      $("input[name=station]").val() != -1
    ) {
      $.ajax({
        url: "/" + lang + "/search/inventory",
        data: {
          date: $("#id_sampling_date").val(),
          station: $("input[name=station]").val(),
        },
        dataType: "json",
      }).done(function (inventory) {
        if (inventory["inventory"] == -1) {
          $("#id_inventory").val("");
          $("#row_inventory").attr("hidden", true);
        } else {
          $("#date-container").text(
            inventory["date"].split("-").reverse().join("/")
          );
          $("#row_inventory").removeAttr("hidden");
          $("#radioInventoryYes").on("change", function () {
            if ($(this).is(":checked")) {
              $("#id_inventory").val(inventory["inventory"]);
            }
          });
          $("#radioInventoryNo").on("change", function () {
            if ($(this).is(":checked")) {
              $("#id_inventory").val("");
            }
          });
        }
      });
    }
  }

  // call update_inventory when date and station fields change
  $("#id_sampling_date").on("change", update_inventory);

  // Map logic (step 4)

  // Initialisation de la carte

  function pointToLayer(feature, latlng) {
    var div = document.createElement("div");
    div.innerHTML += `<b>${gettext("AJOUT CARTE Station name")}</b> : `;
    div.innerHTML += feature.properties.station_name;
    div.innerHTML += "<br>";
    if (feature.properties.commune) {
      div.innerHTML += `<b>${gettext("AJOUT CARTE Commune")}</b> : `;
      div.innerHTML += feature.properties.commune;
      div.innerHTML += "<br>";
    }
    if (feature.properties.station_code) {
      div.innerHTML += `<b>${gettext("AJOUT CARTE Code Sandre")}</b> : `;
      div.innerHTML += feature.properties.station_code;
      div.innerHTML += "<br>";
    }

    var buttonDiv = document.createElement("div");
    buttonDiv.classList.add("text-center", "mt-2");

    var button = document.createElement("button");
    button.classList.add("btn", "btn-outline-success", "btn-xs");
    button.type = "button";
    button.id = "btn-select-" + feature.properties.station_id;
    button.innerHTML = "Sélectionner";
    button.addEventListener("click", function () {
      if ($("#known-station-switch").is(":checked")) {
        $("#known-station-switch").trigger("click");
      }
      $("input[name=station_label]").val(
        feature.properties.station_name + "," + feature.properties.commune
      );
      $("input[name=station]").val(feature.properties.station_id);
      $("input[name=station]").trigger("change");

      selectedFeature = drawSelected(feature.properties.station_id, latlng);
    });
    buttonDiv.appendChild(button);

    div.appendChild(buttonDiv);

    var html =
      "<img src='" + geo_tick + "' class='base-color' height=32 width=32>";
    var icon = L.divIcon({
      className: "d-flex justify-content-center",
      html: html,
      iconSize: [32, 32],
      iconAnchor: [16, 32],
      popupAnchor: [0, -25],
    });

    var m = new L.Marker(latlng, { icon: icon });
    m.bindPopup(div);
    return m;
  }

  function getSelected() {
    return $("input[name=station]").val();
  }

  var map = undefined;

  $.getJSON("https://geolocation-db.com/json/")
    .done(function (location) {
      map = createMap(
        getSelected,
        pointToLayer,
        9,
        7,
        location.latitude,
        location.longitude
      );
    })
    .fail(function () {
      map = createMap(getSelected, pointToLayer, 9, 7);
    });

  // Station sélectionnée
  const highlight = L.divIcon({
    className: "selected-icon",
    html:
      "<img src='" + geo_tick + "' class='selected-color' height=32 width=32>",
    iconSize: [32, 32],
    iconAnchor: [16, 32],
    popupAnchor: [0, -25],
  });

  var selecteddata = L.geoJSON(null, {
    pointToLayer: function (feature, latlng) {
      var m = new L.Marker(latlng, { icon: highlight });
      return m;
    },
  });

  // selecteddata change quand drawSelected est appelée
  function drawSelected(id, coords) {
    var selectedFeature = {
      type: "Feature",
      properties: {
        station_id: id,
      },
      geometry: {
        type: "Point",
        coordinates: [coords.lng, coords.lat],
      },
    };

    if (
      coords.lat.toString().includes(".") &&
      coords.lat.toString().split(".")[1].length > 5
    ) {
      $("#id_latitude").val(Math.round(coords.lat * 1e5) / 1e5);
    }
    if (
      coords.lng.toString().includes(".") &&
      coords.lng.toString().split(".")[1].length > 5
    ) {
      $("#id_longitude").val(Math.round(coords.lng * 1e5) / 1e5);
    }

    map.removeLayer(selecteddata);
    selecteddata.clearLayers();
    selecteddata.addData(selectedFeature);
    map.addLayer(selecteddata);
    selecteddata.bringToFront();
    return selectedFeature;
  }
  var selectedFeature;
  // dessine selected et zoom dessus
  function findSelected() {
    if ($("input[name=station]").val()) {
      $.ajax({
        url: "/stations_geojson",
        data: {
          arg: $("input[name=station]").val(),
        },
        success: function (data) {
          var feature = data.features[0];
          var coords = {
            lng: feature.geometry.coordinates[0],
            lat: feature.geometry.coordinates[1],
          };
          selectedFeature = drawSelected(
            feature.properties.station_id,
            coords,
            map
          );
          map.setView(coords, 13);
        },
      });
    }
  }
  findSelected();

  // Toutes stations

  $("#step-4-tab").on("click", function () {
    map.invalidateSize();
  });

  $("#id_latitude,#id_longitude").on("input", (e) => {
    //.log(e.target.value);
    e.target.value = e.target.value.replace(",", ".").replace(/[^0-9.-]+/g, "");
    if ($("#id_longitude").val() && $("#id_latitude").val()) {
      map.setView([$("#id_latitude").val(), $("#id_longitude").val()]);
      if ($("#known-station-switch").is(":checked")) {
        var new_station = {
          lat: $("#id_latitude").val(),
          lng: $("#id_longitude").val(),
        };
        selectedFeature = drawSelected(-1, new_station);
      }
    }
  });

  // Station add
  function click_station(e) {
    selectedFeature = drawSelected(-1, e.latlng);
  }

  $("#known-station-switch").on("change", function () {
    if ($("#known-station-switch").is(":checked")) {
      $("#row_inventory").attr("hidden", true);
      $("input[name=station_label]").val("");
      $("input[name=station]").val("");
      $("input[name=station_label]").on("focusout.complete", function () {
        $("input[name=station]").val(-1);
      });
      $("#add-station-explain").removeClass("d-none");
      selecteddata.clearLayers();
      map.on("click", click_station);
      L.DomUtil.addClass(map._container, "crosshair-cursor-enabled");
    } else {
      $("#add-station-explain").addClass("d-none");
      $("input[name=station_label]").off("focusout.complete");
      $("input[name=station]").val("");
      map.off("click", click_station);
      L.DomUtil.removeClass(map._container, "crosshair-cursor-enabled");

      if (typeof selectedFeature !== "undefined") {
        if (selectedFeature.properties.station_id < 0) {
          $("input[name=station_label]").val("");
          $("input[name=station]").val("");
          selecteddata.clearLayers();
        }
      }
    }
  });
}

// Logic for step 2
export function getMousePos(canvas, evt) {
  var clientX;
  var clientY;
  if (evt.type.startsWith("mouse")) {
    clientX = evt.clientX;
    clientY = evt.clientY;
  } else if (evt.type.startsWith("touch")) {
    clientX = evt.touches[0].clientX;
    clientY = evt.touches[0].clientY;
  }
  var rect = canvas.getBoundingClientRect();
  return {
    x: ((clientX - rect.left) / (rect.right - rect.left)) * canvas.width,
    y: ((clientY - rect.top) / (rect.bottom - rect.top)) * canvas.height,
  };
}

export function drawOnImage(image, first, coords, calc) {
  const canvasElement = document.getElementById("scale-canvas");
  const parent = canvasElement.parentElement;

  // the image passed as a parameter is rescaled in the canvas,
  // which is rescaled to fit perfectly
  var hRatio =
    image.width > parent.offsetWidth ? parent.offsetWidth / image.width : 1;
  var vRatio =
    image.height > parent.offsetHeight ? parent.offsetHeight / image.height : 1;
  var ratio = Math.min(hRatio, vRatio);

  canvasElement.width = image.width * ratio;
  canvasElement.height = image.height * ratio;
  parent.style.height = "auto";

  // context configuration
  const context = canvasElement.getContext("2d");
  context.lineWidth = 3;
  context.strokeStyle = "#ed6e6c";
  context.lineCap = "round";

  context.drawImage(
    image,
    0,
    0,
    image.width,
    image.height,
    0,
    0,
    image.width * ratio,
    image.height * ratio
  );

  if (first) {
    coords.startX = 0;
    coords.startY = 0.5 * image.height * ratio;
    coords.endX = image.width * ratio;
    coords.endY = 0.5 * image.height * ratio;

    calc.lenX = coords.startX - coords.endX;
    calc.lenY = coords.startY - coords.endY;
    calc.ratio = ratio;

    first = false;
  }

  if (coords) {
    context.beginPath();
    context.moveTo(coords.startX, coords.startY);
    context.lineTo(coords.endX, coords.endY);
    context.stroke();
    context.closePath();
  }

  // drawing above image
  let isDrawing;

  function startDrawing(e) {
    isDrawing = true;
    coords.startX = getMousePos(canvasElement, e).x;
    coords.startY = getMousePos(canvasElement, e).y;
  }

  function moveDrawing(e) {
    if (isDrawing) {
      context.clearRect(0, 0, canvasElement.width, canvasElement.height);
      context.drawImage(
        image,
        0,
        0,
        image.width,
        image.height,
        0,
        0,
        image.width * ratio,
        image.height * ratio
      );
      context.beginPath();
      context.moveTo(coords.startX, coords.startY);
      coords.endX = getMousePos(canvasElement, e).x;
      coords.endY = getMousePos(canvasElement, e).y;
      context.lineTo(coords.endX, coords.endY);
      context.stroke();
    }
  }

  function endDrawing() {
    isDrawing = false;
    context.closePath();

    var lenX = coords.startX - coords.endX;
    var lenY = coords.startY - coords.endY;

    validDisplay($("#col-scale"));
    $("#btn-next-2").removeClass("disabled");

    calc.ratio = ratio;
    calc.lenX = lenX;
    calc.lenY = lenY;
  }

  canvasElement.onmousedown = (e) => startDrawing(e);
  canvasElement.ontouchstart = (e) => startDrawing(e);

  canvasElement.onmousemove = (e) => moveDrawing(e);
  canvasElement.ontouchmove = (e) => moveDrawing(e);

  canvasElement.onmouseup = endDrawing;
  canvasElement.ontouchend = endDrawing;
}

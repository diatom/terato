-- diatom.identifier_type
INSERT INTO diatom.identifier_type(identifier_type_name)  VALUES ('omnidia');
INSERT INTO diatom.identifier_type(identifier_type_name)  VALUES ('sandre');


-- diatom.inventory_type
INSERT INTO diatom.inventory_type(inventory_type_id, inventory_type_name)  VALUES (1,'Floristic inventory');
INSERT INTO diatom.inventory_type(inventory_type_id, inventory_type_name)  VALUES (2,'ADNe inventory');


-- diatom.taxonomy_type
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Empire','Empire','Domain');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Kingdom','Règne','Kingdom');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Sub-kingdom','Sous-règne','Sub-kingdom');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Phylum','Embranchement','Division');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Class','Classe','Class');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Order','Ordre','Order');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Family','Famille','Family');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Gender','Genre','Genus');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Species','Espèce','Species');
INSERT INTO diatom.taxonomy_type(taxonomy_type_name,taxonomy_type_name_fr,taxonomy_type_name_en)  VALUES ('Infra-species','Infra-espèce','Infra-species');


-- terato.health
INSERT INTO terato.health(health_abb,health_full,health_full_fr,health_full_en)  VALUES ('S','Saine','Saine','Healthy');
INSERT INTO terato.health(health_abb,health_full,health_full_fr,health_full_en)  VALUES ('T','Térato','Tératologique','Teratological');
INSERT INTO terato.health(health_abb,health_full,health_full_fr,health_full_en)  VALUES ('I','Indeter','Indéterminée','Undetermined');


-- terato.defo_factor_cat
INSERT INTO terato.defo_factor_cat(defo_factor_cat_name,defo_factor_cat_name_fr,defo_factor_cat_name_en)  VALUES ('chimique','Facteurs chimiques','Chemical factors');
INSERT INTO terato.defo_factor_cat(defo_factor_cat_name,defo_factor_cat_name_fr,defo_factor_cat_name_en)  VALUES ('physique','Facteurs physiques','Physical factors');
INSERT INTO terato.defo_factor_cat(defo_factor_cat_name,defo_factor_cat_name_fr,defo_factor_cat_name_en)  VALUES ('biologique','Facteurs biologiques','Biological factors');


-- terato.defo_factor
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Acidité extrême','Acidité extrême','Extreme acidity',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='chimique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Manque de nutriments','Manque de nutriments','Lack of nutrients',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='chimique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Manque de silice','Manque de silice','Lack of silicium',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='chimique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Micropolluants organiques (pesticides, résidus pharmaceutiques...)','Micropolluants organiques (pesticides, résidus pharmaceutiques...)','Organic micropollutants (pesticides, pharmaceutical residues...)',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='chimique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Micropolluants inorganiques (métaux)','Micropolluants inorganiques (métaux)','Inorganic micropollutants (metals)',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='chimique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('UV','UV','UV',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='physique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Température','Température','Temperature',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='physique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Dessication','Dessication','Drying',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='physique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Vitesse du courant','Vitesse du courant','Flowing speed',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='physique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Environnements particuliers (e.g. sources)','Environnements particuliers (e.g. sources)','Peculiar environments (e.g. sources)',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='physique'));
INSERT INTO terato.defo_factor(defo_factor_name,defo_factor_name_fr,defo_factor_name_en,defo_factor_cat_id)  VALUES ('Surpopulation','Surpopulation','Overpopulation',(select defo_factor_cat_id from terato.defo_factor_cat where defo_factor_cat_name='biologique'));

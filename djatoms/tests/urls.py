from django.urls import path

from . import admin

urlpatterns = [
    path("admin/", admin.site.urls, name="admin"),
    path("admin_uploadable/", admin.site_uploadable.urls, name="admin_uploadable"),
    path(
        "admin_not_uploadable/",
        admin.site_not_uploadable.urls,
        name="admin_not_uploadable",
    ),
]

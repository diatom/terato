from tests import CSPTestMixin, TestCase


class CSPTestCase(CSPTestMixin, TestCase):

    fixtures = ["users"]

    def test_csp_all_other(self):
        self._csp_test_all_other([])

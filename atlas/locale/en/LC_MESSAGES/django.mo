��    b      ,  �   <      H  �   I     	  �   
    �
  �
                 0     E     V     t     �  !   �     �     �     �     �          ,     G     Y  #   o     �  '   �     �     �          *     H      f     �  '   �     �     �     �          +     H     \     h     �     �     �     �     �          1  $   J  $   o  -   �      �     �     �     �     �       	             "  	   +     5     Q     o     �     �     �  	   �     �     �     �                         ,     9     B     b     y     �     �     �     �     �     �        #         5   !   S      u   	   {   g   �   �   �   w   �!  9   �!  .   3"     b"  B  n"  �   �#     f$  �   �%  �  l&  	  J)     O2     o2     t2     {2  '   ~2     �2     �2     �2     �2     �2     �2     �2     �2     3     3  *   3  Z   C3  
   �3  q   �3  �   4  j   �4  +   5  B   A5  �   �5  y   6  ;   �6  m   �6  >   77  B   v7     �7     �7     �7     �7     �7  #   �7     8     #8  
   &8     18     ?8     F8     M8     T8  [   a8  2   �8     �8     9     9     9     9     &9  	   -9     79     @9  	   I9  *   S9  %   ~9     �9  �   �9     ::     X:  	   _:     i:     v:  
   �:     �:     �:     �:     �:     �:     �:     �:     ;  
   ;     &;     4;     ;;     R;  
   f;     q;     };     �;     �;  Q   �;     <  	   <  b   <  �   x<  �   =  /   �=  $   �=     �=        ,   V       [          >       M           K                 b   <          5                 Y   I       3   E   "            X      ]   
   B       *   4           %       (   Z       	   R                   ^       D   =   G   U   /   P       S   A       W   ;   _               9   @   a      J         O   $   0   ?   `          H      #              &          +          )   .               8      N   6      Q   1   '   2   :       7   L   !          -      C   \      F       T    
                                                Cette photo a été publiée <a href="https://doi.org/%(link)s" target="_blank">ici.</a>
                                             
                                                Un ajout localisé, sur %(count)s.
                                             
                                                %(count_located)s ajouts localisés, sur %(count)s.
                                             
                                    Nous avons trouvé un inventaire diatomique réalisé sur cette station le <span id="date-container"></span>. L'ajout provient-il de cet inventaire ?
                                     
                                Pour ajouter une nouvelle station :
                                <ul>
                                    <li>
                                        Commencez par indiquer des coordonnées. Pour ce faire, vous pouvez zoomer sur la carte et cliquer, ou bien directement
                                        entrer les coordonnées (format WGS84 / EPSG:4326) dans les champs Latitude et Longitude ci-dessous.
                                    </li>
                                    <li>
                                        Indiquez un nom pour votre station. Le format recommandé est : Le/la {nom de la rivière} à {nom de la commune}.
                                    </li>
                                </ul>
                                 
        <p>
            Cette section vous permet d'ajouter un groupe de photographies à la plateforme. Elle est pertinente pour les ajouts
            organisés de plusieurs diatomées.
        </p>
        <h2>Etape 1 : Verser les données</h2>
        <p>
            Vous trouverez ci-dessous un champ permettant de choisir un fichier compressé .zip. Ce fichier peut contenir autant
            de diatomées que vous le souhaitez, organisées comme vous le souhaitez. Un fichier .csv (séparateur : virgule, encodage : utf-8)
            donnera à l'application les informations nécessaires pour ajouter vos photos, grâce aux colonnes suivantes (toutes les autres colonnes seront ignorées) :
            <ul>
                <li><b>photo</b> : donne l'emplacement de la photo, relativement au fichier .csv ;</li>
                <li><b>taxon</b> : code OMNIDIA ou SANDRE du taxon ;</li>
                <li><b>health</b> : S pour Saine, T pour Tératologique, I pour Indéterminée ;</li>
                <li>scale : combien de micromètres représentent 100 pixels ;</li>
                <li>station : code SANDRE de la station de prélèvement ;</li>
                <li>sampling_date : date de prélèvement, au format "YYYY-MM-DD" ;</li>
                <li>photo_author : auteur à créditer de la photo ;</li>
                <li>photo_doi : DOI de la publication où cette photo est apparue pour la première fois.</li>
            </ul>
        </p>
        <h2>Etape 2 : Vérifier les ajouts</h2>
        <p>
            Si le fichier transmis est valide, vous verrez apparaître un récapitulatif des diatomées que vous vous apprêtez à ajouter.
            Vous pourrez y effectuer des corrections, compléter les manques, ajouter une station présente en base mais sans code SANDRE...
        </p>
        <p>
            En bas de ce tableau récapitulatif, vous trouverez deux boutons. Le bouton <b>Enregistrer</b> permet de sauvegarder vos modifications,
            sans ajouter les diatomées à la base de données. Le bouton <b>Soumettre</b> permet d'envoyer vos contributions : vos diatomées seront dès lors visibles sur le site.
            Par souci de lisibilité, vos photos sont groupées par centaine. Les deux boutons ne concernent que la centaine actuellement affichée.
        </p>
        <p>
            Vos photos sont sauvegardées : vous pouvez enregistrer vos modifications et y revenir plus tard, avant la soumission. Restez prudents : un redémarrage
            du serveur, ou autre souci technique, peut supprimer votre travail. Mieux vaut soumettre vos ajouts au fur et à mesure !
            Il n'est pas possible de transmettre un nouveau fichier tant que les diatomées précédentes n'ont pas été soumises ou supprimées.
        </p>
     AJOUT ALT Cropper AJOUT BUTTON Continuer AJOUT BUTTON Envoyer AJOUT BUTTON Non AJOUT BUTTON Nouvelle station AJOUT BUTTON Oui AJOUT BUTTON Retour AJOUT CHAMP Date de prélèvement AJOUT CHAMP ECHELLE AJOUT CHAMP Echelle AJOUT CHAMP Photo DOI AJOUT CHAMP Photo author AJOUT CHAMP Station label AJOUT CHAMP Statut térato AJOUT CHAMP Taxon AJOUT HELPER Collecte AJOUT HELPER Facteurs déformations AJOUT HELPER Fichier en cours AJOUT HELPER Propriété intellectuelle AJOUT HELPER Recadrage photo AJOUT HELPER Recherche station AJOUT HELPER Station AJOUT HELPER Sélection photo AJOUT HELPER Sélection taxon AJOUT HELPER Sélection échelle AJOUT MESSAGE Erreurs AJOUT MESSAGE Format non pris en charge AJOUT MSG Diatomée ajoutée AJOUT MSG Error in DOI AJOUT SECTION Echelle AJOUT SECTION Environnement AJOUT SECTION Identification AJOUT SECTION Photo AJOUT TITLE DELETE MSG Diatomée supprimée DIATOM CONTENT Ajoutée le DIATOM CONTENT Ajoutée par DIATOM CONTENT Prélevée le DIATOM CONTENT Revenir au taxon DIATOM CONTENT Signaler DIATOM CONTENT Statut terato DIATOM CONTENT Supprimer DIATOM CONTENT Utilisateur supprimé DIATOM CONTENT action irrréversible DIATOM CONTENT supprimer cette contribution ? DIATOM CONTENT sur la commune de DIATOM TITLE Diatom Diatoms Filename Health Inventory Latitude Locality Longitude MAP HELPER Zoom pour ajouts MAP HELPER Zoom pour stations MODIFY BUTTON Envoyer MODIFY HELPER Sélection photo MODIFY MSG Diatomée modifiée MODIFY TITLE Photo DOI Photo author Photo non disponible. River Sampling date Scale Station Station code Station name Stations TAXON BUTTON Taxons avec photos TAXON CONTENT No photo TAXON CONTENT Voir le détail TAXON INFO Alias TAXON INFO Auteur TAXON INFO Codes TAXON INFO Nb ajouts TAXON INFO Nom TAXON INFO Parent TAXON INFO Rang TAXON INFO Voir la carte des ajouts TAXON INFO Voir les synonymes TAXON MSG Redirection vers valide Taxon Thumbnail UPLOAD MSG For %(errors)s paths (total : %(total)s),                    no corresponding file was found UPLOAD MSG Found %(count)s files with invalid format.                 Accepted formats are .png, .jpg, .jpeg, .tif, .tiff                 and .bmp. UPLOAD MSG Found no file with a valid format. Accepted             formats are .png, .jpg, .jpeg, .tif, .tiff and .bmp. UPLOAD MSG More than one csv file provided during upload. UPLOAD MSG No csv file provided during upload. Upload date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                                This photo first appeared <a href="https://doi.org/%(link)s" target="_blank">here</a>.
                                             
                                                One located contribution, among %(count)s.
                                             
                                                %(count_located)s located contributions, among %(count)s.
                                             
                                We have found a diatomic inventory conducted at this site on a date close to the one you indicated, precisely on <span id="date-container"></span>. Does your sample come from this inventory?
     
                                To add a new site:
                                <ul>
                                    <li>
                                        Start by indicating its coordinates. To do so, you can zoom in on the map and click, or directly                                         enter the coordinates (WGS84 / EPSG:4326 format) in the Latitude and Longitude fields below.
                                    </li>
                                    <li>
                                        Give a name to your station. The recommended format is: The WATER_BODY_NAME at MUNICIPALITY_NAME.
                                    </li>
                                </ul>
                                 
        <p>
            This section allows you to upload a set of photographs to the website. This is relevant when dealing with quite a lot of well-organized diatoms.
        </p>
        <h2>Step 1: Upload data</h2>
        <p>
You'll find below a field to choose a .zip compressed file. This file may contain as many diatoms as you wish, organized as you wish. A .csv file (separator: comma, encoding: utf-8) will give the application all necessary information to add your photos, thanks to the following columns (any other column will be ignored):            <ul>
                <li><b>photo</b>: gives the path of a photograph, relative to the .csv file;</li>
                <li><b>taxon</b>: OMNIDIA or SANDRE code of the taxon;</li>
                <li><b>health</b>: S for Healthy (Saine), T for Teratological, I for Unknown (Indéterminée);</li>
                <li>scale: how many micrometers 100 pixels represent;</li>
                <li>station: SANDRE sampling station code;</li>
                <li>sampling_date: sampling date, format "YYYY-MM-DD";</li>
                <li>photo_author: author to be credited for the photograph;</li>
                <li>photo_doi: DOI of the publication where this photograph appeared for the first time.</li>
            </ul>
        </p>
        <h2>Step 2: Check the upload</h2>
        <p>
            If the uploaded zip file is validated, a summary of the diatoms you're adding will be displayed. You'll be able to review them, to add missing data, to fill a station with no SANDRE code...        </p>
        <p>
Below the summary table, you'll find two buttons. The <b>Save</b> button saves your modifications, without pushing your data to the database. The <b>Submit</b> button sends your uploads: your diatoms will then be displayed on the website. For readability, your photographs are grouped by hundreds. Both buttons only deal with the displayed hundred.        </p>
        <p>
Your photographs are saved: your can save your work and come back to it later, before submitting. Stay cautious: should the server restart, or any other technical problem occur, your work would be lost. Better submit bit by bit! It is not possible to load another file as long as the previous diatoms are not either submitted or deleted.        </p>
         Image cropper is not available. Next Upload No Not finding your sampling site? Add it! Yes Back Sampling date: Length in micrometers Scale DOI: Photo author: Sampling site name: This diatom is: Taxon: If possible, specify the date of sampling. (Optional) Check on the left the factors that you suspect may have caused the deformation. Uploading: Indicate the original author of the picture and, if relevant, the DOI of the first publication where it appeared. You may now crop the downloaded picture. By moving and resizing the window, frame the diatom of interest as close as possible. Scroll to zoom. The sampling location may be retrieved by typing its name or coordinates, or by using the interactive map. If possible, specify the sampling location. To begin, please select the diatom photography you wish to upload. Identify your diatom by its taxon. Start typing to see suggestions. Suggestions are available with complete name, SANDRE code or OMNIDIA code. Indicate a scale for your image. To this end, draw a line on your picture; then, choose what length this line represents. An error occurred during form validation. Please try again. This image format is not currently supported. Available formats are: .jpg, .jpeg, .png, .tif, .tiff and .bmp. Thank you! Your contribution has been added to the collection. The DOI you provided could not be validated, and has been ignored. Scale Environment Identification Picture Upload a picture This contribution has been deleted. Uploaded on by Sampled on Back to taxon Report Status Delete Deleted user Data will be irremediably lost. In the event of an error, you will have to upload it again. Are you sure you want to delete this contribution? in the municipality of Detail Diatom Diatoms Filename Status Inventory Latitude Locality Longitude Zoom in to display contributions locations Zoom in to display sampling locations Save Currently, modifying the image is not supported. If there is a problem with this image, please delete your contribution, and start a new upload. Your changes have been saved! Modify Photo DOI Photo author Picture not available. Water body Sampling date Scale Sampling place Station code Station name Sampling places Display only taxa with pictures No picture available. See detail Accepted name Author Code (Omnidia, Sandre) Uploaded pictures:  Latin name Parent name Rank Display contributions map Show synonymised names You've been redirected to the currently accepted name of the taxon you asked for. Taxon Thumbnail For %(errors)s paths (total: %(total)s), no corresponding file was found.
                         Found %(count)s files with invalid format. Accepted formats are 
                        .png, .jpg, .jpeg, .tif, .tiff and .bmp.
                         Found no file with a valid format. Accepted formats are 
                        .png, .jpg, .jpeg, .tif, .tiff and .bmp.
                         More than one .csv file provided during upload. No .csv file provided during upload. Upload date 
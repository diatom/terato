from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
)
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, ListView, TemplateView

from atlas.models import Diatom
from main.utils import refresh_materialized_view

from .forms import (
    AuthenticationForm,
    DeleteProfileForm,
    PasswordChangeForm,
    PasswordResetForm,
    SetPasswordForm,
    UserCreationForm,
)
from .models import User


class UsersViewMixin:
    template_name = "base_users.html"
    title = ""
    preform = None
    postform = None

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = self.title
        context["preform"] = self.preform
        context["postform"] = self.postform
        return context


class LoginView(UsersViewMixin, LoginView):
    template_name = "login.html"
    form_class = AuthenticationForm
    redirect_authenticated_user = True

    title = _("LOGIN Title")


class LogoutView(LogoutView):
    http_method_names = ["get", "post", "options"]
    next_page = reverse_lazy("users:login")

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("users:profile")
        else:
            return redirect("users:login")


class ProfileView(LoginRequiredMixin, ListView):
    model = Diatom
    template_name = "profile.html"
    paginate_by = 10
    ordering = "-upload_date"

    def get_queryset(self):
        queryset = (
            Diatom.objects.filter(user_id=self.request.user.id)
            .values("id", "taxon__taxon_name", "upload_date")
            .order_by(self.get_ordering())
        )
        return queryset


class RegistrationView(CreateView):
    form_class = UserCreationForm
    template_name = "register.html"
    success_url = reverse_lazy("users:register_done")

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse_lazy("users:profile"))
        return super().get(self.request, *args, **kwargs)

    def form_valid(self, form):

        user = form.save(commit=False)
        user.is_active = False
        user.save()

        current_site = get_current_site(self.request)
        message = render_to_string(
            "register_email.html",
            {
                "user": user,
                "domain": current_site.domain,
                "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                "token": default_token_generator.make_token(user),
            },
        )

        email = EmailMultiAlternatives(
            subject=_("Account activation"),
            body=strip_tags(message),
            to=[form.cleaned_data["email"]],
        )
        email.attach_alternative(message, "text/html")
        email.send()
        return redirect(self.success_url)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = _("REGISTER Title")
        context["col_classes"] = "col-md-8 col-lg-5"
        return context


class RegistrationDoneView(UsersViewMixin, TemplateView):
    title = _("REGISTER TITLE Email Sent")
    postform = _("REGISTER MSG Email sent")

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse_lazy("users:profile"))
        return super().get(self.request, *args, **kwargs)


class RegistrationConfirmView(TemplateView):
    template_name = "register_confirm.html"
    success_url = reverse_lazy("users:profile")

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect(reverse_lazy("users:profile"))

        uid = urlsafe_base64_decode(kwargs["uidb64"]).decode()
        user = User.objects.get(pk=uid)
        if default_token_generator.check_token(user, kwargs["token"]):
            user.is_active = True
            user.save()
            login(self.request, user)
            messages.success(self.request, _("REGISTER MSG Account activated"))
            return redirect(self.success_url)
        return self.render_to_response(
            {"validlink": False, "title": _("REGISTER TITLE Lien invalide")}
        )


class PasswordResetView(UsersViewMixin, PasswordResetView):
    email_template_name = "password_reset_email.html"
    html_email_template_name = "password_reset_email.html"
    success_url = reverse_lazy("users:password_reset_done")
    form_class = PasswordResetForm

    title = _("PASSWORD TITLE Forgotten Password")
    preform = _("PASSWORD MSG enter your mail")


class PasswordResetDoneView(UsersViewMixin, PasswordResetDoneView):
    title = _("PASSWORD TITLE Email Sent")
    preform = _("PASSWORD MSG Email sent")


class PasswordResetConfirmView(UsersViewMixin, PasswordResetConfirmView):
    model = User
    template_name = "password_reset_confirm.html"
    success_url = reverse_lazy("users:login")
    form_class = SetPasswordForm

    title = _("PASSWORD TITLE Reset")

    def form_valid(self, form):
        messages.success(self.request, _("PASSWORD MSG Password changed"))
        return super().form_valid(form)


class PasswordChangeView(UsersViewMixin, PasswordChangeView):
    form_class = PasswordChangeForm
    model = User
    success_url = reverse_lazy("users:profile")
    title = _("PASSWORD TITLE Change password")

    def form_valid(self, form):
        messages.success(self.request, _("PASSWORD MSG Password changed"))
        return super().form_valid(form)


class DeleteProfileView(LoginRequiredMixin, DeleteView):
    model = User
    form_class = DeleteProfileForm
    template_name = "delete.html"
    success_url = reverse_lazy("generic:home")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = _("DELETE Title")
        context["col_classes"] = "col-md-8 col-lg-6"
        return context

    def get_object(self):
        return self.request.user

    def form_valid(self, form):
        if form.cleaned_data["keep_diatom"] == "KEEP":
            self.request.user.is_active = False
            self.request.user.save()
            messages.warning(self.request, _("DELETE MSG Account inactive"))
            return redirect(self.success_url)
        elif form.cleaned_data["keep_diatom"] == "ANONYM":
            deleted_user = User.objects.get(first_name="Deleted")
            Diatom.objects.filter(user=self.request.user).update(user=deleted_user)
            messages.warning(self.request, _("DELETE MSG Account deleted"))
            return super().form_valid(form)
        else:
            s = super().form_valid(form)
            refresh_materialized_view("terato.children_diatoms")
            messages.warning(self.request, _("DELETE MSG All deleted"))
            return s

import L from "leaflet";
import "leaflet/dist/images/marker-shadow.png";
import "leaflet.markercluster";

import "leaflet/dist/leaflet.css";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";

export function createMap(
  arg,
  pointToLayer,
  initZoom,
  maxZoom,
  initLatitude = 46.4939,
  initLongitude = 2.6028
) {
  const link = JSON.parse(document.getElementById("data-url").textContent);

  if (arg instanceof Function) {
    arg = arg.call();
  }

  var osm = L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 19,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  });
  // Google Maps
  var google_maps = L.tileLayer(
    "https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
    {
      maxZoom: 19,
      subdomains: ["mt0", "mt1", "mt2", "mt3"],
      attribution: "&copy; Google Maps",
    }
  );
  var baseMaps = {
    OpenStreetMap: osm,
    "Google Maps": google_maps,
  };

  // Initialisation de la carte
  var map = L.map("map", {
    center: [initLatitude, initLongitude],
    zoom: initZoom,
    preferCanvas: true,
    layers: [osm],
    maxBounds: [(-90, -180), (90, 180)],
    minZoom: 4,
    worldCopyJump: true,
  });
  L.control.layers(baseMaps).addTo(map);
  L.control.scale({ position: "bottomright", imperial: false }).addTo(map);

  // Toutes stations
  const markers = L.markerClusterGroup({
    showCoverageOnHover: false,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var c = " marker-cluster-";

      if (markers[0].feature.properties.diatom__count) {
        var sum = 0;
        for (var i = 0; i < markers.length; i++) {
          sum += markers[i].feature.properties.diatom__count;
        }

        if (sum < 10) {
          c += "small";
        } else if (sum < 100) {
          c += "medium";
        } else {
          c += "large";
        }

        return new L.DivIcon({
          html:
            "<div><span>" +
            sum +
            ' <span aria-label="markers"></span>' +
            "</span></div>",
          className: "marker-cluster" + c,
          iconSize: new L.Point(40, 40),
        });
      } else {
        var childCount = cluster.getChildCount();

        if (childCount < 10) {
          c += "small";
        } else if (childCount < 100) {
          c += "medium";
        } else {
          c += "large";
        }

        return new L.DivIcon({
          html: "<div><span>" + childCount + "</span></div>",
          className: "marker-cluster" + c,
          iconSize: new L.Point(40, 40),
        });
      }
    },
  });

  var stationdata = L.geoJSON(null, {
    pointToLayer: pointToLayer,
  });
  markers.addLayer(stationdata);

  map.on("moveend", function () {
    if (map.getZoom() > maxZoom) {
      $("#map-overlay").addClass("d-none");
      $.ajax({
        url: link,
        data: {
          x0: map.getBounds().getWest(),
          x1: map.getBounds().getEast(),
          y0: map.getBounds().getNorth(),
          y1: map.getBounds().getSouth(),
          arg: arg,
        },
        success: function (data) {
          map.removeLayer(markers);
          markers.removeLayer(stationdata);
          stationdata.clearLayers();
          stationdata.addData(data);
          markers.addLayer(stationdata);
          map.addLayer(markers);
        },
      });
    } else {
      map.removeLayer(markers);
      $("#map-overlay").removeClass("d-none");
    }
  });

  return map;
}

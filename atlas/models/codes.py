from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BTreeIndex


class CodeType(models.Model):
    id = models.AutoField(primary_key=True, db_column="identifier_type_id")
    code_type_name = models.CharField(db_column="identifier_type_name")

    class Meta:
        db_table = 'diatom"."identifier_type'
        db_table_comment = "List of types of identifiers"
        managed = False

    def __str__(self):
        return self.code_type_name


class TaxonCode(models.Model):
    id = models.AutoField(primary_key=True, db_column="taxon_identifier_id")
    taxon = models.ForeignKey("Taxon", models.DO_NOTHING)
    code = models.CharField()
    code_type = models.ForeignKey(
        "CodeType", models.DO_NOTHING, db_column="identifier_type_id"
    )

    class Meta:
        db_table = 'diatom"."taxon_identifier'
        db_table_comment = "List of codes used to identifier a taxon"
        managed = False

        indexes = [
            BTreeIndex(
                fields=["code", "code_type"], name="taxon_identifier_type_code_idx"
            ),
        ]

    def __str__(self):
        return self.code

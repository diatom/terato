"""
Make sure that all views making create / update / delete operations on table
terato.diatom refresh the associated materialized view terato.children_diatoms
"""

from unittest.mock import Mock, patch

from django.contrib.auth import get_user_model
from django.contrib.messages.storage.fallback import FallbackStorage
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory
from django.urls import reverse

from djatoms.admin import admin_site
from tests import TestCase
from tests.utils import DEFAULT_EMAIL

from ..admin import DiatomAdmin
from ..models import Diatom
from ..views import DiatomDeleteView, ModifyFormView, UploadFormView

User = get_user_model()


class UserRefreshMVTestCase(TestCase):

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "station",
        "defo_factor_cat",
        "defo_factor",
        "diatom",
    ]

    @patch("subprocess.Popen")
    def _refresh_mv_test(
        self, view_class, name, mock_command, object_id=None, data=False
    ):

        view = view_class()
        request = RequestFactory()
        request.session = "session"
        request._messages = FallbackStorage(request)
        request.user = User.objects.get(email=DEFAULT_EMAIL)

        if object_id:
            request.get(reverse(name, args=[object_id]))
            view.setup(request, pk=object_id)
            view.object = view.get_object()
        else:
            request.get(reverse(name))
            view.setup(request)

        if data:
            with open("atlas/tests/grid.png", "rb") as img:
                files = {"photo": SimpleUploadedFile(img.name, img.read())}
                data = {
                    "taxon": 17,
                    "health": 1,
                    "inventory": "",
                    "station": 2,
                    "sampling_date": "2024-08-28",
                    "scale": 8.73,
                    "photo_author": "",
                    "photo_doi": "pas_un_vrai_doi",
                    "deformation_factors": ["4", "8"],
                }
            form = view.form_class(data, files)

        else:
            form = view.form_class()

        form.full_clean()
        view.form_valid(form)
        self.assertEqual(mock_command.call_count, 1)
        self.assertEqual(
            mock_command.call_args.args[0],
            ["python", "manage.py", "refresh_mv", "terato.children_diatoms"],
        )

    def test_modify_view(self):
        self._refresh_mv_test(
            ModifyFormView, "atlas:contrib:diatom_modify", object_id=1, data=True
        )

    def test_upload_view(self):
        self._refresh_mv_test(UploadFormView, "atlas:contrib:diatom_upload", data=True)

    def test_delete_view(self):
        self._refresh_mv_test(
            DiatomDeleteView, "atlas:contrib:diatom_delete", object_id=1
        )


class AdminRefreshMVTestCase(TestCase):

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "station",
        "defo_factor_cat",
        "defo_factor",
        "diatom",
    ]

    def setUp(self):
        self.request = RequestFactory()
        self.request.COOKIES = {}
        self.request.method = "POST"
        self.request.META = {}
        self.request.is_secure = Mock()
        self.request.path = ""
        self.request.POST = []

        self.model_admin = DiatomAdmin(Diatom, admin_site)

    @patch("subprocess.Popen")
    def _refresh_mv_test(self, view, post, mock_command, args=None):

        self.request.POST.append(post)
        getattr(self.model_admin, "%s_view" % view)(self.request, args)

        self.assertEqual(mock_command.call_count, 1)
        self.assertEqual(
            mock_command.call_args.args[0],
            ["python", "manage.py", "refresh_mv", "terato.children_diatoms"],
        )

    def test_delete_view(self):
        self._refresh_mv_test("delete", "_save", args=1)

    def test_changelist_view(self):
        # also called by uploadlist_view
        self._refresh_mv_test("changelist", "_save")

    def test_changeform_view(self):
        # parent function for change_view and add_view
        self._refresh_mv_test("changeform", "_save")

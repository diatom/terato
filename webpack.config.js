const path = require("path");
const webpack = require("webpack");
const glob = require("glob");
const BundleTracker = require("webpack-bundle-tracker");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const RemoveEmptyScriptsPlugin = require("webpack-remove-empty-scripts");
const { PurgeCSSPlugin } = require("purgecss-webpack-plugin");

require("dotenv").config({ path: ".env" });

const isDevelopment = (process.env.DEBUG || "false").toLowerCase() == "true";

const pluginRules = [
  // expose $ and jQuery to global scope.
  new webpack.ProvidePlugin({
    $: "jquery",
    jQuery: "jquery",
  }),
  new BundleTracker({
    filename: "webpack-stats.json",
    path: __dirname,
  }),
  new RemoveEmptyScriptsPlugin({ extensions: /\.(css.js)$/ }),
  new MiniCssExtractPlugin({
    filename: (pathData) => {
      return pathData.chunk.name.endsWith(".css")
        ? "css/[name]"
        : "css/[name].css";
    },
  }),
];

if (!isDevelopment) {
  pluginRules.push(
    new PurgeCSSPlugin({
      paths: glob.sync("./**/templates/**/*.html"),
      only: ["atlas"],
      safelist: [/^form-/, /^carousel-/, "c-border-success"],
    })
  );
}

const moduleRules = {
  rules: [
    {
      mimetype: "image/svg+xml",
      scheme: "data",
      type: "asset/resource",
      generator: {
        filename: "icons/[hash].svg",
      },
    },
    {
      mimetype: "image/png",
      scheme: "data",
      type: "asset/resource",
      generator: {
        filename: "img/[hash][ext]",
      },
    },
    {
      test: /\.(scss|css)$/i,
      use: [
        MiniCssExtractPlugin.loader,
        "css-loader",
        "resolve-url-loader",
        {
          loader: "sass-loader",
          options: { sourceMap: true },
        },
      ],
    },
    {
      test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
      type: "asset/resource",
      generator: {
        filename: "fonts/[name][ext]",
      },
    },
    {
      test: /\.(png|svg|jpg|jpeg|gif)$/i,
      type: "asset/resource",
      generator: {
        filename: "img/[name][ext]",
      },
    },
  ],
};

my_apps = [
  ["atlas", ["img"]],
  ["contact", ["img"]],
  ["djatoms", []],
  ["generic", ["img"]],
  ["mailbox", []],
  ["main", ["img", "css"]],
  ["users", []],
];

module.exports = [];

for (app of my_apps) {
  app_name = app[0];
  entries = glob
    .sync("./" + app_name + "/assets/js/**.js")
    .reduce(function (obj, el) {
      obj[path.parse(el).name] = el;
      return obj;
    }, {});

  if (app[1].length) {
    entries[app_name] = [];

    for (type of app[1]) {
      entries[app_name].push(
        ...glob.sync("./" + app_name + "/assets/" + type + "/*")
      );
    }
  }

  module.exports.push({
    context: path.resolve(),
    mode: isDevelopment ? "development" : "production",
    entry: entries,
    output: {
      path: path.resolve(process.env.WEBPACK_BUNDLE_DIR, app_name),
      pathinfo: isDevelopment ? true : false,
      filename: "js/[name].js",
    },
    plugins: pluginRules,
    module: moduleRules,
  });
}

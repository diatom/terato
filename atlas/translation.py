from modeltranslation.translator import TranslationOptions, translator

from .models import DefoFactor, DefoFactorCategory, Health, TaxonomicRank

DEFAULT_REQUIRED_LANGUAGES = ("fr", "en")


class TransDefoFactor(TranslationOptions):
    fields = ("name",)
    required_languages = DEFAULT_REQUIRED_LANGUAGES


class TransDefoFactorCategory(TranslationOptions):
    fields = ("category_name",)
    required_languages = DEFAULT_REQUIRED_LANGUAGES


class TransHealth(TranslationOptions):
    fields = ("health_full",)
    required_languages = DEFAULT_REQUIRED_LANGUAGES


class TransTaxonomicRank(TranslationOptions):
    fields = ("rank",)
    required_languages = DEFAULT_REQUIRED_LANGUAGES


translator.register(DefoFactor, TransDefoFactor)
translator.register(DefoFactorCategory, TransDefoFactorCategory)
translator.register(Health, TransHealth)
translator.register(TaxonomicRank, TransTaxonomicRank)

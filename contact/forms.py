from django import forms
from django.utils.translation import gettext_lazy as _

from atlas.models import Diatom


class ContactForm(forms.Form):
    class Meta:
        fields = ("first_name", "last_name", "email", "message")

    first_name = forms.CharField(max_length=50, label=_("Prénom"))
    last_name = forms.CharField(max_length=50, label=_("Nom"))
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea())


class FlagForm(forms.Form):
    class Meta:
        fields = ("diatom", "flag", "message")

    diatom = forms.ModelChoiceField(
        queryset=Diatom.objects.all(), widget=forms.HiddenInput()
    )
    flag = forms.ChoiceField(
        choices=[
            ("INDES", _("FLAG CHOICE Contenu inapproprié ou indésirable")),
            ("ERROR", _("FLAG CHOICE Contenu erroné ou approximatif")),
            ("PROPR", _("FLAG CHOICE Non-respect de la propriété intellectuelle")),
            ("OTHER", _("FLAG CHOICE Autre")),
        ],
        widget=forms.RadioSelect,
        label=_("FLAG MSG Pourquoi signaler cet ajout ?"),
    )
    message = forms.CharField(widget=forms.Textarea(), label=_("FLAG MSG Précisez :"))

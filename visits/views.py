from django.views.defaults import (
    ERROR_400_TEMPLATE_NAME,
    ERROR_403_TEMPLATE_NAME,
    ERROR_404_TEMPLATE_NAME,
    ERROR_500_TEMPLATE_NAME,
)
from django.views.defaults import bad_request as old_bad_request
from django.views.defaults import page_not_found as old_page_not_found
from django.views.defaults import permission_denied as old_permission_denied
from django.views.defaults import server_error as old_server_error

from .models import Error


def register_error(request, response):
    # make sure there's a session key, even for anonymous users
    if not request.session or not request.session.session_key:
        request.session.save()

    e = Error(
        path=request.path,
        error_code=response.status_code,
        traceback=response.reason_phrase,
        ua_string=request.headers.get("User-Agent", ""),
    )
    e.save()


# Rewrite default handlers to write error report, in case the error
# occurs before reaching the middleware


def page_not_found(request, exception, template_name=ERROR_404_TEMPLATE_NAME):
    response = old_page_not_found(request, exception, template_name)
    register_error(request, response)
    return response


def server_error(request, template_name=ERROR_500_TEMPLATE_NAME):
    response = old_server_error(request, template_name)
    register_error(request, response)
    return response


def bad_request(request, exception, template_name=ERROR_400_TEMPLATE_NAME):
    response = old_bad_request(request, exception, template_name)
    register_error(request, response)
    return response


def permission_denied(request, exception, template_name=ERROR_403_TEMPLATE_NAME):
    response = old_permission_denied(request, exception, template_name)
    register_error(request, response)
    return response


class ErrorFormViewMixin:

    """
    A mixin allowing to report form errors in database. To use with caution: all
    POSTed data will be sent to admin. Do NOT use with form views handling
    sensitive data and/or passwords (e.g. auth views).

    To be used with a FormView or any inheriting class.
    """

    def form_invalid(self, form):

        data = dict(self.request.POST)
        data.pop("csrfmiddlewaretoken")
        data.pop("password", None)

        e = Error(
            path=self.request.path,
            error_code=302,
            traceback=form.errors.as_data(),
            posted=str(data).encode(),
            ua_string=self.request.headers.get("User-Agent", ""),
        )
        e.save()
        return self.render_to_response(self.get_context_data(form=form))

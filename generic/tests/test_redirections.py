from django.test import TestCase

from tests import RedirectionTestMixin


class TestLoggedOutRedirects(RedirectionTestMixin, TestCase):
    def setUp(self):

        self.expected = {
            ("home",): (None,),
            ("cgu",): (None,),
            ("legals",): (None,),
        }


class TestLoggedInRedirects(RedirectionTestMixin, TestCase):
    def setUp(self):

        self.expected = {
            ("home",): (None,),
            ("cgu",): (None,),
            ("legals",): (None,),
        }

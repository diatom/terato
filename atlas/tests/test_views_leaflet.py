from django.test import RequestFactory

from djatoms.views import LeafletLayerView
from tests import TestCase

from ..models import Station, Taxon
from ..views import ContributionsLayerView, StationsLayerView


class StationsLayerViewTests(TestCase):

    fixtures = ["station"]

    def setUp(self):

        self.view = StationsLayerView()
        self.request = RequestFactory()
        self.view.setup(self.request)

    def test_get_queryset_coords_without_selected(self):
        # Should not make any difference compared to parent view
        self.request.GET = {"x0": 1, "x1": 2, "y0": 50, "y1": 51}

        qs = self.view.get_queryset()

        p_view = LeafletLayerView()
        p_view.model = Station
        p_view.setup(self.request)
        p_qs = p_view.get_queryset()

        self.assertEqual(set(qs), set(p_qs))

    def test_get_queryset_selected_without_coords(self):

        selected = 5
        self.request.GET = {"arg": selected}

        qs = self.view.get_queryset()

        self.assertEqual(qs[0], Station.objects.get(pk=selected))

    def test_get_queryset_without_selected_without_coords(self):

        self.request.GET = {}
        qs = self.view.get_queryset()
        self.assertFalse(qs.exists())

    def test_get_queryset_selected_with_coords(self):

        selected = 5
        self.request.GET = {"x0": 1, "x1": 2, "y0": 50, "y1": 51, "arg": selected}

        qs = self.view.get_queryset()

        # should yield as parent + selected
        p_view = LeafletLayerView()
        p_view.model = Station
        p_view.setup(self.request)
        p_qs = p_view.get_queryset()

        # Make sure that selected is not yielded by parent
        self.assertNotIn(Station.objects.get(pk=selected), p_qs)

        self.assertEqual(set(qs), set(p_qs | Station.objects.filter(pk=selected)))


class ContributionsLayerViewTests(TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):

        self.view = ContributionsLayerView()
        self.request = RequestFactory()
        self.view.setup(self.request)

    def test_get_queryset_coords_without_taxon(self):
        # Should not yield anything
        self.request.GET = {"x0": 1, "x1": 2, "y0": 50, "y1": 51}

        qs = self.view.get_queryset()
        self.assertFalse(qs.exists())

    def test_get_queryset_taxon_without_coords(self):

        for t in Taxon.objects.all():
            self.request.GET = {"arg": t.pk}

            qs = self.view.get_queryset()

            self.assertEqual(
                set(qs), set(Station.objects.filter(diatom__children__ancestor=t.pk))
            )

    def test_get_queryset_without_taxon_without_coords(self):

        self.request.GET = {}
        qs = self.view.get_queryset()
        self.assertFalse(qs.exists())

    def test_get_queryset_taxon_with_coords(self):

        for t in Taxon.objects.all():
            self.request.GET = {"x0": 1, "x1": 2, "y0": 50, "y1": 51, "arg": t.pk}

            qs = self.view.get_queryset()

            p_view = LeafletLayerView()
            p_view.model = Station
            p_view.setup(self.request)
            p_qs = p_view.get_queryset()

            self.assertEqual(
                set(qs),
                set(Station.objects.filter(diatom__children__ancestor=t.pk) & p_qs),
            )

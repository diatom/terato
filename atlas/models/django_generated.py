from django.contrib.gis.db import models

from .inventory import Inventory


class AnalysisPlace(models.Model):
    analysis_place_id = models.IntegerField(primary_key=True)
    analysis_place_name = models.CharField()
    analysis_place_code = models.CharField(
        blank=True, null=True, db_comment="Code attributed by the SANDRE"
    )

    class Meta:
        db_table = "analysis_place"
        db_table_comment = "Place of analysis (in situ, laboratory)"


class AnalysisRemark(models.Model):
    analysis_remark_id = models.AutoField(primary_key=True)
    analysis_remark_name = models.CharField()
    analysis_remark_code = models.CharField(
        blank=True, null=True, db_comment="Code attributed by the SANDRE"
    )

    class Meta:
        db_table = "analysis_remark"
        db_table_comment = "Remarks of the analysis, as quantification threshold"


class AnalyzedFraction(models.Model):
    analyzed_fraction_id = models.AutoField(primary_key=True)
    analyzed_fraction_name = models.CharField()
    analyzed_fraction_code = models.CharField(
        blank=True, null=True, db_comment="Code attributed by the SANDRE"
    )

    class Meta:
        db_table = "analyzed_fraction"
        db_table_comment = "List of analyzed fractions"


class InventoryPhysicoChemistry(models.Model):
    inventory = models.OneToOneField(
        Inventory, models.DO_NOTHING, primary_key=True
    )  # The composite primary key (inventory_id, physico_chemistry_id) found,
    # that is not supported. The first column is selected.
    physico_chemistry = models.ForeignKey("PhysicoChemistry", models.DO_NOTHING)

    class Meta:
        db_table = "inventory_physico_chemistry"
        unique_together = (("inventory", "physico_chemistry"),)
        db_table_comment = "List of related physico-chemical samplings"


class Parameter(models.Model):
    parameter_id = models.AutoField(primary_key=True)
    parameter_name = models.CharField()
    parameter_code = models.CharField(
        blank=True, null=True, db_comment="Code attributed by the SANDRE"
    )

    class Meta:
        db_table = "parameter"
        db_table_comment = "List of parameters"


class PhysicoChemistry(models.Model):
    physico_chemistry_id = models.AutoField(primary_key=True)
    physico_chemistry_date = models.DateTimeField(db_comment="Date of the measure")
    station = models.ForeignKey("Station", models.DO_NOTHING)

    class Meta:
        db_table = "physico_chemistry"
        db_table_comment = "List of physico-chemistry measures"


class PhysicoChemistryAnalysis(models.Model):
    physico_chemistry_analysis_id = models.AutoField(primary_key=True)
    physico_chemistry = models.ForeignKey("PhysicoChemistry", models.DO_NOTHING)
    value = models.FloatField(blank=True, null=True)
    parameter = models.ForeignKey("Parameter", models.DO_NOTHING)
    unit = models.ForeignKey("Unit", models.DO_NOTHING, blank=True, null=True)
    support = models.ForeignKey("Support", models.DO_NOTHING)
    analyzed_fraction = models.ForeignKey(
        "AnalyzedFraction", models.DO_NOTHING, blank=True, null=True
    )
    analysis_place = models.ForeignKey(
        "AnalysisPlace", models.DO_NOTHING, blank=True, null=True
    )
    analysis_remark = models.ForeignKey(
        "AnalysisRemark", models.DO_NOTHING, blank=True, null=True
    )

    class Meta:
        db_table = "physico_chemistry_analysis"
        db_table_comment = "List of values recorded"


class SpatialRefSys(models.Model):
    srid = models.IntegerField(primary_key=True)
    auth_name = models.CharField(max_length=256, blank=True, null=True)
    auth_srid = models.IntegerField(blank=True, null=True)
    srtext = models.CharField(max_length=2048, blank=True, null=True)
    proj4text = models.CharField(max_length=2048, blank=True, null=True)

    class Meta:
        db_table = "spatial_ref_sys"


class Support(models.Model):
    support_id = models.AutoField(primary_key=True)
    support_name = models.CharField()
    support_code = models.CharField(
        blank=True, null=True, db_comment="Code attributed by the SANDRE"
    )

    class Meta:
        db_table = "support"


class Unit(models.Model):
    unit_id = models.AutoField(primary_key=True)
    unit_name = models.CharField(db_comment="Name of the unit")
    symbol = models.CharField(
        blank=True, null=True, db_comment="Symbol used to represent the unit"
    )
    unit_code = models.CharField(
        blank=True, null=True, db_comment="Corresponding code attributed by the Sandre"
    )

    class Meta:
        db_table = "unit"
        db_table_comment = "Units used to analyse the physico-chemistry parameters"

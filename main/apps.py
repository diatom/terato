from django.apps import AppConfig
from django.conf import settings
from django.core.checks import Error, Tags, register


class MainConfig(AppConfig):
    name = "main"

    def ready(self):
        @register(Tags.compatibility, Tags.database, deploy=True)
        def version_check(app_configs, **kwargs):
            errors = []

            # Checking db version against code version

            from .models import DbVersion

            db_version = DbVersion.objects.latest("version_date").version_name

            code_version = getattr(settings, "VERSION", "")

            if code_version == "":
                errors.append(
                    Error(
                        msg="Improperly configured.",
                        hint="""
                            VERSION setting must be defined in settings,
                            and indicate current code version."
                        """,
                        id="main.E001",
                    )
                )

            elif ".".join(code_version.split(".")[:2]) != db_version:
                errors.append(
                    Error(
                        msg="Improperly configured.",
                        hint="""
                            Your database version ({}) does not match code
                            version ({}). Please run the appropriate codes
                            in the db folder to update your database.
                        """.format(
                            db_version, code_version
                        ),
                        id="main.E002",
                    )
                )
            return errors

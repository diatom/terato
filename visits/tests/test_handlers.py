from django.core.exceptions import (
    ObjectDoesNotExist,
    PermissionDenied,
    SuspiciousOperation,
)
from django.test import Client, modify_settings, override_settings
from django.urls import path
from django.urls.exceptions import Resolver404

from tests.testcases import TestCase
from visits.views import bad_request, page_not_found, permission_denied, server_error

from ..models import Error

handler400 = bad_request
handler403 = permission_denied
handler404 = page_not_found
handler500 = server_error


def error_view_404(request):
    raise Resolver404


def error_view_403(request):
    raise PermissionDenied


def error_view_500(request):
    raise ObjectDoesNotExist


def error_view_400(request):
    raise SuspiciousOperation


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": ["main/templates"],
        "APP_DIRS": False,
    },
]

urlpatterns = [
    path("400", error_view_400),
    path("403", error_view_403),
    path("404", error_view_404),
    path("500", error_view_500),
]


@override_settings(ROOT_URLCONF="visits.tests.test_handlers", TEMPLATES=TEMPLATES)
@modify_settings(MIDDLEWARE={"remove": "visits.middleware.VisitMiddleware"})
class HandlerErrorsTestCase(TestCase):
    def test_handler400_creates_error(self):
        self.client.get("/400")
        self.assertEqual(1, Error.objects.count())

    def test_handler403_creates_error(self):
        self.client.get("/403")
        self.assertEqual(1, Error.objects.count())

    def test_handler404_creates_error(self):
        self.client.get("/404")
        self.assertEqual(1, Error.objects.count())

    def test_handler500_creates_error(self):
        client = Client(raise_request_exception=False)
        client.get("/500")
        self.assertEqual(1, Error.objects.count())

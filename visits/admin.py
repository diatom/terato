from django.db.models import Count

from djatoms.admin import AggregatedModelAdmin, ExtendedModelAdmin, admin_site

from .models import Error, Request


class RequestAdmin(AggregatedModelAdmin):
    list_display = ("request_time__date", "session", "num_visited")
    list_filter = ("request_time",)

    fields = (
        ("request_time__date", "session"),
        "num_visited",
    )
    readonly_fields = (
        "request_time__date",
        "session",
        "num_visited",
    )
    ordering = ("-request_time__date",)

    def num_visited(self, obj):
        return obj.num_visited

    def request_time__date(self, obj):
        return obj.request_time__date

    num_visited.short_description = "Number of visited pages"
    request_time__date.short_description = "Date of visit"

    def get_queryset(self, request):
        """Will return an aggregated queryset."""

        qs = super().get_queryset(request)
        qs = qs.values("request_time__date", "session").annotate(
            num_visited=Count("path")
        )
        return qs


class ErrorAdmin(ExtendedModelAdmin):
    list_display = ("path", "error_code", "error_time", "user_agent")
    list_filter = ("error_time",)

    fields = (
        "error_time",
        "path",
        "error_code",
        "traceback",
        ("device", "os", "browser"),
        "post_decoded",
    )
    readonly_fields = (
        "error_time",
        "path",
        "error_code",
        "traceback",
        "browser",
        "device",
        "os",
        "post_decoded",
    )
    ordering = ("-error_time",)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin_site.register(Error, ErrorAdmin)
admin_site.register(Request, RequestAdmin)

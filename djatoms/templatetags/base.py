from django.contrib.admin.templatetags.base import InclusionAdminNode
from django.template.library import InclusionNode


class InclusionAdminNode(InclusionAdminNode):
    def render(self, context):
        opts = context["opts"]
        app_label = opts.app_label.lower()
        object_name = opts.object_name.lower()
        context.render_context[self] = context.template.engine.select_template(
            [
                "admin/%s/%s/%s" % (app_label, object_name, self.template_name),
                "admin/%s/%s" % (app_label, self.template_name),
                "djatoms/admin/%s" % self.template_name,
                "admin/%s" % self.template_name,
            ]
        )
        return InclusionNode.render(self, context)

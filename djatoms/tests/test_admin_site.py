from django.utils.translation import gettext as _

from tests import TestCase

from ..admin.actions import delete_selected
from ..admin.admin import admin_site


class MyAdminSiteTestCase(TestCase):
    def test_site_header(self):

        self.assertEqual(admin_site.site_header, _("ADMIN Tératothèque - Admin"))

    def test_site_actions(self):
        self.assertEqual(admin_site._actions["delete_selected"], delete_selected)

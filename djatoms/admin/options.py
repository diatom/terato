import copy
from functools import update_wrapper

from django.contrib import messages
from django.contrib.admin import ModelAdmin
from django.contrib.admin.utils import model_ngettext
from django.forms import ModelForm
from django.http import Http404, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils.decorators import method_decorator
from django.utils.translation import ngettext
from django.views.decorators.csrf import csrf_protect

from .forms import BaseUploadForm, UploadModelFormMixin
from .views.main import AggregatedChangeList

csrf_protect_m = method_decorator(csrf_protect)


class ExtendedModelAdmin(ModelAdmin):
    """
    Extends ModelAdmin with an uploadlist_view, which allows users to
    create multiple objects at once, from a well-written file.

    Many mechanisms are similar to those of ModelAdmin.changeview_list
    """

    uploadable = False
    upload_list_display = ()
    upload_list_editable = ()
    upload_list_template = None
    upload_form = BaseUploadForm
    upload_list_form = ModelForm
    uploading = False
    _upload_queryset = None

    class Media:
        css = {
            "all": ["djatoms/css/uploadlink.css"],
        }

    def __init__(self, *args, **kwargs):

        # assert that the provided form subclasses modelform
        assert issubclass(self.upload_list_form, ModelForm)

        class UploadModelForm(UploadModelFormMixin, self.upload_list_form):
            pass

        self._upload_list_form = UploadModelForm
        self._upload_list_editable = self.upload_list_editable
        self._upload_list_display = self.upload_list_display
        self._upload_list_display_links = None

        super().__init__(*args, **kwargs)

    def get_urls(self):

        if not self.uploadable:
            return super().get_urls()

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.opts.app_label, self.opts.model_name

        return [
            path(
                "upload/",
                wrap(self.upload_view),
                name="%s_%s_uploadlist" % info,
            ),
            *super().get_urls(),
        ]

    def upload_view(self, request):
        if self.uploadable:
            if request.FILES or self.uploading:
                return self.uploadlist_view(request)

            context = {
                **self.admin_site.each_context(request),
            }
            context["opts"] = opts = self.model._meta
            context["form"] = self.upload_form
            context["actions_on_bottom"] = True
            context["init"] = True

            app_label = opts.app_label.lower()
            object_name = opts.object_name.lower()
            template_name = "upload_list.html"

            return TemplateResponse(
                request,
                [
                    "admin/%s/%s/%s" % (app_label, object_name, template_name),
                    "admin/%s/%s" % (app_label, template_name),
                    "djatoms/admin/%s" % template_name,
                ],
                context=context,
            )
        raise Http404

    def _upload_get_changelist(self, request, **kwargs):
        from .views.main import UploadList

        return UploadList

    def _upload_get_queryset(self, request):
        if not self._upload_queryset:
            self._upload_queryset = self.get_upload_queryset(request)

            # make sure we've got ids, no big deal what they are
            for i in range(0, len(self._upload_queryset)):
                setattr(self._upload_queryset[i], "id", i + 1)
        return self._upload_queryset

    def get_upload_queryset(self, request):
        """
        Since the actual upload process highly depends on the model we want
        to upload data for, building a common method is impossible.

        Subclasses should override this method.
        """

        raise NotImplementedError(
            """
            Please define this method in ExtendedModelAdmin subclasses.
            """
        )

    def _upload_get_changelist_form(self, request, **kwargs):
        """
        Return a Form class for use in the Formset on the uploadlist page.
        """
        return super().get_changelist_form(
            request, form=self._upload_list_form, **kwargs
        )

    def uploadlist_view(self, request, extra_context=None):

        if not self.uploadable:
            raise Http404

        # set the uploading state
        self.uploading = True

        class UploadingModelAdmin(type(self)):
            def __init__(self, parent):
                for i in vars(parent):
                    setattr(self, i, parent.__getattribute__(i))

            def __getattribute__(self, name):
                try:
                    return ModelAdmin.__getattribute__(self, "_upload_" + name)
                except AttributeError:
                    return ModelAdmin.__getattribute__(self, name)

        c = UploadingModelAdmin(self)

        # case : save modifications without submitting yet
        if request.method == "POST" and "_save_tmp" in request.POST:
            FormSet = c.get_changelist_formset(request)
            modified_objects = c._get_list_editable_queryset(
                request, FormSet.get_default_prefix()
            )
            formset = FormSet(request.POST, request.FILES, queryset=modified_objects)
            if formset.is_valid():
                changecount = 0

                for form in formset.forms:
                    if form.has_changed():
                        obj = c._upload_queryset.get(pk=form.cleaned_data["id"])
                        for f, value in form.cleaned_data.items():
                            setattr(obj, f, value)
                        changecount += 1
                if changecount:
                    msg = ngettext(
                        "%(count)s %(name)s was modified successfully.",
                        "%(count)s %(name)s were modified successfully.",
                        changecount,
                    ) % {
                        "count": changecount,
                        "name": model_ngettext(c.opts, changecount),
                    }

                c.message_user(request, msg, messages.SUCCESS)
            return HttpResponseRedirect(request.get_full_path())

        # other cases : launch Django's machinery and catch the response
        response = c.changelist_view(request, extra_context)

        # if all objects have been deleted or added, quit uploading
        self._upload_queryset = c._upload_queryset
        if not self._upload_queryset:
            self.uploading = False
            if request.FILES:
                # can be the case if uploaded file was a fake
                request.FILES.pop("file")
            return self.upload_view(request)

        # patch response with correct template
        app_label = self.opts.app_label
        template_name = "upload_list.html"

        response.template_name = self.upload_list_template or [
            "admin/%s/%s/%s" % (app_label, self.opts.model_name, template_name),
            "admin/%s/%s" % (app_label, template_name),
            "djatoms/admin/%s" % template_name,
        ]
        return response

    def changelist_view(self, request, extra_context=None):
        s = super().changelist_view(request, extra_context)
        # Making sure a djatoms/admin template is considered before
        # a django/contrib/admin one
        if request.method == "GET":
            s.template_name.insert(2, "djatoms/admin/change_list.html")
        return s

    def _upload_save_form(self, request, form, change):

        form.save(commit=False)

        obj = self._upload_queryset.get(id=form.cleaned_data["id"])

        obj_fordb = copy.deepcopy(obj)
        for f, value in form.cleaned_data.items():
            setattr(obj_fordb, f, value)

        # reset ids and let the database define it (autofield)
        setattr(obj_fordb, "id", None)
        self._upload_queryset.remove(obj)

        return obj_fordb

    def _upload_message_user(
        self, request, message, level=messages.INFO, extra_tags="", fail_silently=False
    ):

        if "_save" in request.POST:
            # patching messages a posteriori
            changecount = int(str(message).split(" ")[0])
            message = ngettext(
                "%(count)s %(name)s was added successfully.",
                "%(count)s %(name)s were added successfully.",
                changecount,
            ) % {
                "count": changecount,
                "name": model_ngettext(self.opts, changecount),
            }

        super().message_user(request, message, level, extra_tags, fail_silently)


class AggregatedModelAdmin(ExtendedModelAdmin):

    """
    Overwrites methods so as to display aggregate tables in Django admin,
    with minimal effort.

    CRUD actions are deactivated, since there is no corresponding table
    in database.
    """

    list_select_related = []
    list_display_links = None  # don't display links : objects don't exist !

    def get_changelist(self, request, **kwargs):
        return AggregatedChangeList

    def get_actions(self, request):
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def change_view(self, request, object_id, form_url="", extra_context=None):
        """
        The 'change' admin view for this model.

        We override this to redirect back to the changelist
        """

        opts = self.model._meta
        url = reverse(
            "admin:{app}_{model}_changelist".format(
                app=opts.app_label,
                model=opts.model_name,
            )
        )
        return HttpResponseRedirect(url)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

import "bootstrap";

$(function () {
  $(function () {
    var current = location.pathname;
    $(".nav-link").each(function () {
      // if the current path is like this link, make it active
      if (current.indexOf($(this).attr("href")) !== -1) {
        $(this).addClass("active");
        $(this).attr("aria-current", "page");
      }
    });
  });
});

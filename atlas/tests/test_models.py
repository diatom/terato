from base64 import b64encode

from django.test import TestCase
from django.utils.html import mark_safe

from tests import ModelTestMixin

from ..models import (
    CodeType,
    DefoFactor,
    DefoFactorCategory,
    Diatom,
    Health,
    Projection,
    Station,
    Taxon,
    TaxonCode,
    TaxonomicRank,
)


class TaxonModelTests(ModelTestMixin, TestCase):

    """
    Testing custom methods and properties of atlas.Taxon model.

    These methods are written the Django way, we'll be testing
    them in a more Pythonic way
    """

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
    ]

    model = Taxon
    name_field = "taxon_name"
    maxDiff = None

    def _get_msg(self, name):
        return "Test failed for object {}. Some objects may remain untested.".format(
            name
        )

    def test_taxon_get_direct_children(self):

        for t in self.queryset:
            res = []
            for c in Taxon.objects.all():
                if (
                    c.taxon_parent == t
                    and (
                        c.taxonomic_rank.id >= 8
                        or c.id in Taxon.objects.values_list("taxon_parent", flat=True)
                    )
                    and c.taxonomic_rank.id == t.taxonomic_rank.id + 1
                ):
                    if not c.is_terato and c.id == c.taxon_alias.id:
                        res.append(c)
            self.assertEqual(
                res, list(t.get_direct_children), msg=self._get_msg(t.taxon_name)
            )

    def test_taxon_get_indirect_children(self):
        for t in self.queryset:
            res = []
            for c in Taxon.objects.all():
                if (
                    c.taxon_parent == t
                    and (
                        c.taxonomic_rank.id >= 8
                        or c.id in Taxon.objects.values_list("taxon_parent", flat=True)
                    )
                    and c.taxonomic_rank.id > t.taxonomic_rank.id + 1
                ):
                    if not c.is_terato and c.id == c.taxon_alias.id:
                        res.append(c)
            self.assertEqual(
                res, list(t.get_indirect_children), msg=self._get_msg(t.taxon_name)
            )

    def test_taxon_get_parent_taxonomy(self):

        for t in self.queryset:
            res = []

            if t.taxon_parent:
                parent = t.taxon_parent
                while parent.taxonomic_rank.id >= 4:
                    res.append(parent.id)
                    if not parent.taxon_parent:
                        break
                    parent = parent.taxon_parent
                res.reverse()

            self.assertEqual(
                res,
                [d["taxon_id"] for d in t.get_parent_taxonomy],
                msg=self._get_msg(t.taxon_name),
            )

    def test_taxon_get_aliases_with_codes(self):
        for t in self.queryset:

            res = []
            omnidia = []
            sandre = []

            for a in Taxon.objects.all():
                if a.taxon_alias == t and a != t:
                    res.append(a)

                    if list(a.taxoncode_set.all()):
                        omnidia += [
                            c.code for c in a.taxoncode_set.filter(code_type_id=1)
                        ]
                        sandre += [
                            c.code for c in a.taxoncode_set.filter(code_type_id=2)
                        ]

            self.assertEqual(
                res, list(t.get_aliases_with_codes), msg=self._get_msg(t.taxon_name)
            )
            if res:
                self.assertEqual(
                    omnidia,
                    [m.omnidia for m in t.get_aliases_with_codes if m.omnidia],
                    msg=self._get_msg(t.taxon_name),
                )
                self.assertEqual(
                    sandre,
                    [m.sandre for m in t.get_aliases_with_codes if m.sandre],
                    msg=self._get_msg(t.taxon_name),
                )

    def test_taxon_get_codes(self):
        for t in self.queryset:
            c = {}
            for co in TaxonCode.objects.filter(taxon_id=t.id).order_by(
                "code_type__code_type_name"
            ):
                c.setdefault(co.code_type.code_type_name, []).append(co.code)
            self.assertEqual(c, t.get_codes, msg=self._get_msg(t.taxon_name))


class CodeTypeModelTests(ModelTestMixin, TestCase):

    fixtures = ["identifier_type"]
    model = CodeType
    name_field = "code_type_name"


class TaxonCodeModelTests(ModelTestMixin, TestCase):

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
    ]

    model = TaxonCode
    name_field = "code"


class DefoFactorCategoryModelTests(ModelTestMixin, TestCase):

    fixtures = ["defo_factor_cat"]
    model = DefoFactorCategory
    name_field = "category_name"


class DefoFactorModelTests(ModelTestMixin, TestCase):

    fixtures = ["defo_factor_cat", "defo_factor"]
    model = DefoFactor
    name_field = "name"


class HealthModelTests(ModelTestMixin, TestCase):

    fixtures = ["health"]
    model = Health
    name_field = "health_full"


class DiatomModelTests(ModelTestMixin, TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    model = Diatom
    name_field = "filename"

    def test_get_real_width(self):
        for d in self.queryset:
            res = None
            if d.scale:
                res = round(10 * 100 / d.scale, 1)
            else:
                res = None
            self.assertEqual(res, d.get_real_width())

    def test_thumbnail_tag(self):
        for d in self.queryset:
            out = mark_safe(
                '<img height=45 src="data: image/png; base64, %s" />'
                % (b64encode(d.photo).decode("utf8"))
            )
            self.assertEqual(out, d.thumbnail_tag())


class RankModelTests(ModelTestMixin, TestCase):

    fixtures = ["taxonomy_type"]
    model = TaxonomicRank
    name_field = "rank"


class StationModelTests(ModelTestMixin, TestCase):

    fixtures = ["station"]
    model = Station
    name_field = "station_name"


class ProjectionModelTests(ModelTestMixin, TestCase):

    fixtures = ["projection"]
    model = Projection
    name_field = "projection_name"

import re

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site

# from django.contrib.auth.tokens import default_token_generator
from django.core import mail
from django.template.loader import render_to_string
from django.test import RequestFactory, SimpleTestCase
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_encode
from django.views.generic.base import ContextMixin, View

from atlas.models import Diatom
from tests import TestCase
from tests.utils import DEFAULT_EMAIL, DEFAULT_FNAME, DEFAULT_LNAME, DEFAULT_PWD

from ..forms import UserCreationForm
from ..views import DeleteProfileView, ProfileView, UsersViewMixin

User = get_user_model()
NEW_PWD = "testPwd789"

# See tests for base auth_views :
# https://github.com/django/django/blob/main/tests/auth_tests/test_views.py
# The following tests only test overridden funcitonalities


def base_get_link_in_mail(user, target):
    msg = mail.outbox[0].alternatives[0][0]
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token_regex = reverse("users:" + target, args=[uid, "e"])[:-2]
    token_regex += r"([A-Za-z0-9:\-]+)/"
    match = re.search(token_regex, msg)
    return reverse("users:" + target, args=[uid, match.group(1)])


class TestingViewClass(UsersViewMixin, ContextMixin, View):
    title = "a_title"
    preform = "a_preform"
    postform = "a_postform"


class TestUsersViewMixin(SimpleTestCase):
    def test_get_context_data(self):
        view = TestingViewClass()

        context = view.get_context_data()
        self.assertIn("title", context)
        self.assertIn("preform", context)
        self.assertIn("postform", context)


class TestRegistrationView(TestCase):
    def fill_registration_form(self):
        self.data = {
            "first_name": DEFAULT_FNAME,
            "last_name": DEFAULT_LNAME,
            "email": DEFAULT_EMAIL,
            "password1": DEFAULT_PWD,
            "password2": DEFAULT_PWD,
            "cgu": 1,
        }

        self.response = self.client.post(
            reverse("users:register"), data=self.data, follow=True
        )
        self.created = User.objects.get(email=self.data["email"])

    def test_get_non_authenticated(self):
        response = self.client.get(reverse("users:register"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("register.html")
        self.assertIsInstance(response.context["form"], UserCreationForm)

    def test_get_context_data(self):
        response = self.client.get(reverse("users:register"))
        self.assertIn("title", response.context)
        self.assertIn("col_classes", response.context)

    def test_form_valid_creates_inactive_user(self):

        self.fill_registration_form()

        self.assertEqual(self.created.first_name, self.data["first_name"])
        self.assertEqual(self.created.last_name, self.data["last_name"])
        self.assertEqual(self.created.email, self.data["email"])
        self.assertTrue(self.created.check_password(self.data["password1"]))
        self.assertFalse(self.created.is_active)

    def test_form_valid_sends_mail(self):

        self.fill_registration_form()

        self.assertEqual(len(mail.outbox), 1)

        sent = mail.outbox[0]

        current_site = get_current_site(self.response.wsgi_request)
        message = render_to_string(
            "register_email.html",
            {
                "user": self.created,
                "domain": current_site.domain,
                "uid": urlsafe_base64_encode(force_bytes(self.created.pk)),
                "token": default_token_generator.make_token(self.created),
            },
        )
        self.assertEqual(strip_tags(message), sent.body)
        self.assertEqual(sent.to, [self.created.email])

    def test_form_valid_redirects_to_done(self):

        self.fill_registration_form()
        self.assertEqual(self.response.status_code, 200)
        self.assertRedirects(self.response, reverse("users:register_done"))
        self.assertTemplateUsed("register_done.html")


class TestRegistrationConfirmView(TestCase):
    def setUp(self):
        super().setUp()
        self.data = {
            "first_name": DEFAULT_FNAME,
            "last_name": DEFAULT_LNAME,
            "email": DEFAULT_EMAIL,
            "password1": DEFAULT_PWD,
            "password2": DEFAULT_PWD,
            "cgu": 1,
        }

        self.response = self.client.post(
            reverse("users:register"), data=self.data, follow=True
        )
        self.created = User.objects.get(email=self.data["email"])

        self.link = base_get_link_in_mail(self.created, "register_confirm")

    def test_dispatch_activates_user(self):
        self.client.get(self.link)
        self.created.refresh_from_db(fields=["is_active"])
        self.assertTrue(self.created.is_active)

    def test_dispatch_redirects_to_profile(self):
        response = self.client.get(self.link)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse("users:profile"))
        self.assertTemplateUsed("profile.html")

    def test_dispatch_yields_success_message(self):
        response = self.client.get(self.link, follow=True)

        self.assertContains(response, "alert-success", 1)

    def test_dispatch_stale_link_fails(self):
        self.client.get(self.link)  # once
        self.client.logout()

        response = self.client.get(self.link)  # twice
        self.assertFalse(response.context["validlink"])
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("register_confirm.html")
        self.assertTemplateNotUsed("profile.html")


class TestPasswordResetConfirmView(TestCase):

    fixtures = ["users"]

    def test_form_valid_success_message(self):
        user = User.objects.get(email=DEFAULT_EMAIL)

        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = default_token_generator.make_token(user)

        response = self.client.get(
            reverse("users:password_reset_confirm", args=[uid, token]), follow=True
        )
        response = self.client.post(
            response.redirect_chain[0][0],
            data={"new_password1": NEW_PWD, "new_password2": NEW_PWD},
            follow=True,
        )
        self.assertContains(response, "alert-success")


class TestPasswordChangeView(TestCase):

    fixtures = ["users"]

    def test_form_valid_success_message(self):
        self.client.login(username=DEFAULT_EMAIL, password=DEFAULT_PWD)
        response = self.client.post(
            reverse("users:password_change"),
            data={
                "old_password": DEFAULT_PWD,
                "new_password1": NEW_PWD,
                "new_password2": NEW_PWD,
            },
            follow=True,
        )
        self.assertContains(response, "alert-success")


class TestProfileView(TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def test_get_queryset(self):
        user = User.objects.get(email=DEFAULT_EMAIL)

        request = RequestFactory().get(reverse("users:profile"))
        request.user = user
        view = ProfileView()
        view.setup(request)

        qs = (
            Diatom.objects.filter(user_id=user.id)
            .values("id", "taxon__taxon_name", "upload_date")
            .order_by("-upload_date")
        )
        self.assertEqual(list(qs), list(view.get_queryset()))


class TestDeleteProfileView(TestCase):

    fixtures = ["users"]

    def setUp(self):
        super().setUp()

        self.user = User.objects.get(email=DEFAULT_EMAIL)
        self.client.login(username=DEFAULT_EMAIL, password=DEFAULT_PWD)

    def test_get_context_data(self):
        response = self.client.get(reverse("users:delete"))
        self.assertIn("title", response.context)
        self.assertIn("col_classes", response.context)

    def test_get_object(self):
        request = RequestFactory().get(reverse("users:delete"))
        request.user = self.user
        view = DeleteProfileView()
        view.setup(request)

        self.assertEqual(self.user, view.get_object())

    def test_form_valid_keep(self):
        response = self.client.post(
            reverse("users:delete"), data={"keep_diatom": "KEEP"}, follow=True
        )
        self.user.refresh_from_db(fields=["is_active"])
        self.assertFalse(self.user.is_active)
        self.assertContains(response, "alert-warning")
        self.assertRedirects(response, reverse("generic:home"))

    def test_form_valid_anonym(self):
        initial_count = Diatom.objects.filter(user=self.user).count()

        response = self.client.post(
            reverse("users:delete"), data={"keep_diatom": "ANONYM"}, follow=True
        )

        deleted_user = User.objects.get(first_name="Deleted")
        self.assertEqual(
            initial_count, Diatom.objects.filter(user=deleted_user).count()
        )
        self.assertEqual(0, Diatom.objects.filter(user=self.user).count())
        self.assertContains(response, "alert-warning")
        self.assertRedirects(response, reverse("generic:home"))

    def test_form_valid_delete(self):
        response = self.client.post(
            reverse("users:delete"), data={"keep_diatom": "DELETE"}, follow=True
        )
        self.assertContains(response, "alert-warning")
        self.assertRedirects(response, reverse("generic:home"))

    def tearDown(self):
        self.client.logout()

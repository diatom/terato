import requests
from django.http import Http404, HttpResponse

from ..models import Diatom


def generate_image(request, diatom_id):
    try:
        diatom = Diatom.objects.get(pk=diatom_id)
        return HttpResponse(diatom.photo, content_type="image/PNG")
    except Diatom.DoesNotExist:
        raise Http404


def generate_thumbnail(request, diatom_id):
    try:
        thumbnail = Diatom.objects.get(pk=diatom_id).photo_thumbnail
        return HttpResponse(thumbnail, content_type="image/PNG")
    except Diatom.DoesNotExist:
        raise Http404


def check_doi(user_input):
    if "doi.org" not in user_input:
        user_input = "https://doi.org/" + user_input

    try:
        response = requests.head(user_input)
        if response.status_code == 302 and response.headers["location"]:
            return True
        elif response.status_code in [301, 404]:
            return False
        else:
            print(
                "Unexpected status code while checking \
                    the DOI given by the user: {}".format(
                    user_input
                )
            )
    except Exception:
        print(
            "An error occurred while checking the DOI given by the user: {}".format(
                user_input
            )
        )
        return False

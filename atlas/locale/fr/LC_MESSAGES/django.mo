��    b      ,  �   <      H  �   I     	  �   
    �
  �
                 0     E     V     t     �  !   �     �     �     �     �          ,     G     Y  #   o     �  '   �     �     �          *     H      f     �  '   �     �     �     �          +     H     \     h     �     �     �     �     �          1  $   J  $   o  -   �      �     �     �     �     �       	             "  	   +     5     Q     o     �     �     �  	   �     �     �     �                         ,     9     B     b     y     �     �     �     �     �     �        #         5   !   S      u   	   {   g   �   �   �   w   �!  9   �!  .   3"     b"  A  n"  �   �#    o$  �   �%    b&  �
  z)  *   s4  	   �4     �4     �4  0   �4     �4     �4     �4     5     !5     )5     /5     D5     X5     n5  8   v5  d   �5  
   6  s   6  �   �6  y   _7  �   �7  V   g8  �   �8  �   }9  R   :     `:  <   �:  B   ;     `;     h;     v;     �;     �;  (   �;     �;     �;     �;     �;     �;     <  	   	<     <  Q   )<  3   {<     �<     �<  	   �<  
   �<     �<     �<  
   �<     �<  	   =  	   =  -   =  /   J=     z=  �   �=  3   >     S>     \>     l>     >     �>     �>     �>     �>     �>     �>     �>  )   ?     A?     Z?     j?     w?     ~?     �?  	   �?     �?     �?     �?     �?  Q   �?     K@     Q@  [   Y@  �   �@  p   RA  8   �A  >   �A     ;B        ,   V       [          >       M           K                 b   <          5                 Y   I       3   E   "            X      ]   
   B       *   4           %       (   Z       	   R                   ^       D   =   G   U   /   P       S   A       W   ;   _               9   @   a      J         O   $   0   ?   `          H      #              &          +          )   .               8      N   6      Q   1   '   2   :       7   L   !          -      C   \      F       T    
                                                Cette photo a été publiée <a href="https://doi.org/%(link)s" target="_blank">ici.</a>
                                             
                                                Un ajout localisé, sur %(count)s.
                                             
                                                %(count_located)s ajouts localisés, sur %(count)s.
                                             
                                    Nous avons trouvé un inventaire diatomique réalisé sur cette station le <span id="date-container"></span>. L'ajout provient-il de cet inventaire ?
                                     
                                Pour ajouter une nouvelle station :
                                <ul>
                                    <li>
                                        Commencez par indiquer des coordonnées. Pour ce faire, vous pouvez zoomer sur la carte et cliquer, ou bien directement
                                        entrer les coordonnées (format WGS84 / EPSG:4326) dans les champs Latitude et Longitude ci-dessous.
                                    </li>
                                    <li>
                                        Indiquez un nom pour votre station. Le format recommandé est : Le/la {nom de la rivière} à {nom de la commune}.
                                    </li>
                                </ul>
                                 
        <p>
            Cette section vous permet d'ajouter un groupe de photographies à la plateforme. Elle est pertinente pour les ajouts
            organisés de plusieurs diatomées.
        </p>
        <h2>Etape 1 : Verser les données</h2>
        <p>
            Vous trouverez ci-dessous un champ permettant de choisir un fichier compressé .zip. Ce fichier peut contenir autant
            de diatomées que vous le souhaitez, organisées comme vous le souhaitez. Un fichier .csv (séparateur : virgule, encodage : utf-8)
            donnera à l'application les informations nécessaires pour ajouter vos photos, grâce aux colonnes suivantes (toutes les autres colonnes seront ignorées) :
            <ul>
                <li><b>photo</b> : donne l'emplacement de la photo, relativement au fichier .csv ;</li>
                <li><b>taxon</b> : code OMNIDIA ou SANDRE du taxon ;</li>
                <li><b>health</b> : S pour Saine, T pour Tératologique, I pour Indéterminée ;</li>
                <li>scale : combien de micromètres représentent 100 pixels ;</li>
                <li>station : code SANDRE de la station de prélèvement ;</li>
                <li>sampling_date : date de prélèvement, au format "YYYY-MM-DD" ;</li>
                <li>photo_author : auteur à créditer de la photo ;</li>
                <li>photo_doi : DOI de la publication où cette photo est apparue pour la première fois.</li>
            </ul>
        </p>
        <h2>Etape 2 : Vérifier les ajouts</h2>
        <p>
            Si le fichier transmis est valide, vous verrez apparaître un récapitulatif des diatomées que vous vous apprêtez à ajouter.
            Vous pourrez y effectuer des corrections, compléter les manques, ajouter une station présente en base mais sans code SANDRE...
        </p>
        <p>
            En bas de ce tableau récapitulatif, vous trouverez deux boutons. Le bouton <b>Enregistrer</b> permet de sauvegarder vos modifications,
            sans ajouter les diatomées à la base de données. Le bouton <b>Soumettre</b> permet d'envoyer vos contributions : vos diatomées seront dès lors visibles sur le site.
            Par souci de lisibilité, vos photos sont groupées par centaine. Les deux boutons ne concernent que la centaine actuellement affichée.
        </p>
        <p>
            Vos photos sont sauvegardées : vous pouvez enregistrer vos modifications et y revenir plus tard, avant la soumission. Restez prudents : un redémarrage
            du serveur, ou autre souci technique, peut supprimer votre travail. Mieux vaut soumettre vos ajouts au fur et à mesure !
            Il n'est pas possible de transmettre un nouveau fichier tant que les diatomées précédentes n'ont pas été soumises ou supprimées.
        </p>
     AJOUT ALT Cropper AJOUT BUTTON Continuer AJOUT BUTTON Envoyer AJOUT BUTTON Non AJOUT BUTTON Nouvelle station AJOUT BUTTON Oui AJOUT BUTTON Retour AJOUT CHAMP Date de prélèvement AJOUT CHAMP ECHELLE AJOUT CHAMP Echelle AJOUT CHAMP Photo DOI AJOUT CHAMP Photo author AJOUT CHAMP Station label AJOUT CHAMP Statut térato AJOUT CHAMP Taxon AJOUT HELPER Collecte AJOUT HELPER Facteurs déformations AJOUT HELPER Fichier en cours AJOUT HELPER Propriété intellectuelle AJOUT HELPER Recadrage photo AJOUT HELPER Recherche station AJOUT HELPER Station AJOUT HELPER Sélection photo AJOUT HELPER Sélection taxon AJOUT HELPER Sélection échelle AJOUT MESSAGE Erreurs AJOUT MESSAGE Format non pris en charge AJOUT MSG Diatomée ajoutée AJOUT MSG Error in DOI AJOUT SECTION Echelle AJOUT SECTION Environnement AJOUT SECTION Identification AJOUT SECTION Photo AJOUT TITLE DELETE MSG Diatomée supprimée DIATOM CONTENT Ajoutée le DIATOM CONTENT Ajoutée par DIATOM CONTENT Prélevée le DIATOM CONTENT Revenir au taxon DIATOM CONTENT Signaler DIATOM CONTENT Statut terato DIATOM CONTENT Supprimer DIATOM CONTENT Utilisateur supprimé DIATOM CONTENT action irrréversible DIATOM CONTENT supprimer cette contribution ? DIATOM CONTENT sur la commune de DIATOM TITLE Diatom Diatoms Filename Health Inventory Latitude Locality Longitude MAP HELPER Zoom pour ajouts MAP HELPER Zoom pour stations MODIFY BUTTON Envoyer MODIFY HELPER Sélection photo MODIFY MSG Diatomée modifiée MODIFY TITLE Photo DOI Photo author Photo non disponible. River Sampling date Scale Station Station code Station name Stations TAXON BUTTON Taxons avec photos TAXON CONTENT No photo TAXON CONTENT Voir le détail TAXON INFO Alias TAXON INFO Auteur TAXON INFO Codes TAXON INFO Nb ajouts TAXON INFO Nom TAXON INFO Parent TAXON INFO Rang TAXON INFO Voir la carte des ajouts TAXON INFO Voir les synonymes TAXON MSG Redirection vers valide Taxon Thumbnail UPLOAD MSG For %(errors)s paths (total : %(total)s),                    no corresponding file was found UPLOAD MSG Found %(count)s files with invalid format.                 Accepted formats are .png, .jpg, .jpeg, .tif, .tiff                 and .bmp. UPLOAD MSG Found no file with a valid format. Accepted             formats are .png, .jpg, .jpeg, .tif, .tiff and .bmp. UPLOAD MSG More than one csv file provided during upload. UPLOAD MSG No csv file provided during upload. Upload date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
                                                Cette photo a d'abord été publiée <a href="https://doi.org/%(link)s" target="_blank">ici</a>.
                                             
                                                Un ajout localisé, sur %(count)s.
                                             
                                                %(count_located)s ajouts localisés, sur %(count)s.
                                             
                                    Nous avons trouvé un inventaire diatomique réalisé sur cette station le <span id="date-container"></span>. L'ajout provient-il de cet inventaire ?
                                     
                                Vous allez ajouter une nouvelle station :
                                <ul>
                                    <li>
                                        Commencez par indiquer des coordonnées. Pour ce faire, vous pouvez zoomer sur la carte et cliquer, ou bien directement
                                        entrer les coordonnées (format WGS84 / EPSG:4326) dans les champs Latitude et Longitude ci-dessous.
                                    </li>
                                    <li>
                                        Indiquez un nom pour votre station. Le format recommandé est : Le/la NOM_COURS_D_EAU à NOM_COMMUNE.
                                    </li>
                                </ul>
                                 
        <p>
            Cette section vous permet d'ajouter un groupe de photographies à la plateforme. Elle est pertinente pour les ajouts
            organisés de plusieurs diatomées.
        </p>
        <h2>Etape 1 : Verser les données</h2>
        <p>
            Vous trouverez ci-dessous un champ permettant de choisir un fichier compressé .zip. Ce fichier peut contenir autant
            de diatomées que vous le souhaitez, organisées comme vous le souhaitez. Un fichier .csv (séparateur : virgule, encodage : utf-8)
            donnera à l'application les informations nécessaires pour ajouter vos photos, grâce aux colonnes suivantes (toutes les autres colonnes seront ignorées) :
            <ul>
                <li><b>photo</b> : donne l'emplacement de la photo, relativement au fichier .csv ;</li>
                <li><b>taxon</b> : code OMNIDIA ou SANDRE du taxon ;</li>
                <li><b>health</b> : S pour Saine, T pour Tératologique, I pour Indéterminée ;</li>
                <li>scale : combien de micromètres représentent 100 pixels ;</li>
                <li>station : code SANDRE de la station de prélèvement ;</li>
                <li>sampling_date : date de prélèvement, au format "YYYY-MM-DD" ;</li>
                <li>photo_author : auteur à créditer de la photo ;</li>
                <li>photo_doi : DOI de la publication où cette photo est apparue pour la première fois.</li>
            </ul>
        </p>
        <h2>Etape 2 : Vérifier les ajouts</h2>
        <p>
            Si le fichier transmis est valide, vous verrez apparaître un récapitulatif des diatomées que vous vous apprêtez à ajouter.
            Vous pourrez y effectuer des corrections, compléter les manques, ajouter une station présente en base mais sans code SANDRE...
        </p>
        <p>
            En bas de ce tableau récapitulatif, vous trouverez deux boutons. Le bouton <b>Enregistrer</b> permet de sauvegarder vos modifications,
            sans ajouter les diatomées à la base de données. Le bouton <b>Soumettre</b> permet d'envoyer vos contributions : vos diatomées seront dès lors visibles sur le site.
            Par souci de lisibilité, vos photos sont groupées par centaine. Les deux boutons ne concernent que la centaine actuellement affichée.
        </p>
        <p>
            Vos photos sont sauvegardées : vous pouvez enregistrer vos modifications et y revenir plus tard, avant la soumission. Restez prudents : un redémarrage
            du serveur, ou autre souci technique, peut supprimer votre travail. Mieux vaut soumettre vos ajouts au fur et à mesure !
            Il n'est pas possible de transmettre un nouveau fichier tant que les diatomées précédentes n'ont pas été soumises ou supprimées.
        </p>
         Le recadreur d'image n'est pas disponible. Continuer Ajouter Non Vous ne trouvez pas votre station ? Ajoutez-la ! Oui Retour Date de prélèvement : Longueur en micromètres Echelle DOI : Auteur de la photo : Nom de la station : Cette diatomée est : Taxon : Précisez si possible la date de collecte de l'individu. (Facultatif) Cochez ci-contre les facteurs dont vous suspectez qu'ils ont pu causer la déformation. En cours : Indiquez l'auteur de la photo d'origine, et, si pertinent, le DOI de la première publication où elle est apparue. Vous êtes maintenant invité.e à recadrer l'image transmise. En déplaçant le cadre et en en modifiant les dimensions, entourez la diatomée d'intérêt au plus proche. Scroller vous permet de zoomer. La station de collecte peut être retrouvée en tapant son nom ou ses coordonnées, ou en utilisant la carte interactive. Précisez si possible le lieu de collecte de l'individu. Choisissez parmi les stations déjà répertoriées, ou enregistrez-en une nouvelle. Pour commencer, sélectionnez la photographie de diatomée que vous souhaitez ajouter. Identifiez votre ajout en indiquant son taxon. Commencez à taper pour afficher les suggestions. Les suggestions sont disponibles en tapant le nom complet, le code SANDRE ou le code OMNIDIA. Indiquez une échelle pour votre photo. Pour ce faire, dessinez un trait sur votre image, puis choisissez la longueur que ce trait représente. Une erreur est survenue pendant la validation du formulaire. Veuillez recommencer. Ce format d'image n'est actuellement pas pris en charge. Les formats disponibles sont : .jpg, .jpeg, .png, .tif, .tiff et .bmp. Merci ! Votre contribution a bien été ajoutée au recueil. Le DOI indiqué n'a pas pu être validé, et a donc été ignoré. Echelle Environnement Identification Photo Ajouter une diatomée La contribution a bien été supprimée. Ajoutée le par Prélevée le Revenir au taxon Signaler Statut Supprimer Utilisateur supprimé Cette action est irréversible. En cas d'erreur, vous devrez recommencer l'ajout. Voulez-vous vraiment supprimer cette contribution ? sur la commune de Détail Diatomée Diatomées Nom du fichier Statut Inventaire Latitude Localité Longitude Zoomez sur une région pour y voir les ajouts Zoomez sur une région pour y voir les stations Modifier La modification de la photo n'est pas autorisée. Si un problème concerne la photo, veuillez supprimer votre contribution, puis commencez un nouvel ajout. Vos modifications ont bien été prises en compte ! Modifier DOI de la photo Auteur de la photo Photo non disponible. Masse d'eau Date de prélèvement Echelle Station de prélèvement Code de la station Nom de la station Stations de prélèvement N'afficher que les taxons avec des photos Aucune photo disponible. Voir le détail Nom accepté Auteur Code (Omnidia, Sandre) Nombre d'ajouts :  Nom latin Nom du parent Rang Voir la carte des ajouts Voir les noms synonymisés Vous avez été redirigé(e) vers le nom actuellement accepté du taxon demandé. Taxon Aperçu Pour %(errors)s chemins (total : %(total)s), aucun fichier correspondant n'a été trouvé. %(count)s fichiers avec un format invalide ont été trouvés. Les formats 
acceptés sont .png, .jpg, .jpeg, .tif, .tiff and .bmp.
                         Aucun fichier trouvé avec un format valide. Les formats acceptés sont .png, .jpg, .jpeg, .tif, .tiff and .bmp. Plus d'un fichier .csv transmis lors du téléversement. Aucun fichier .csv n'a été transmis lors du téléversement. Date d'ajout 
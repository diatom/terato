from django.apps import AppConfig, apps

DEFAULT_FNAME = "John"
DEFAULT_LNAME = "Canari"
DEFAULT_EMAIL = "john.canari@here.com"
DEFAULT_PWD = "testpwd456"

PHOTO_BYTES = b"GIF87a\x01\x00\x01\x00\x80\x01\x00\x00\x00\x00ccc,\
    \x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02D\x01\x00"

THUMBNAIL_BYTES = b"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\
    \x01\x00\x00\x00\x01\x01\x03\x00\x00\x00%\xdbV\xca\x00\x00\x00\x06PLTE\
    \x00\x00\x00cccu\x9d\xc40\x00\x00\x00\nIDATx\x9cc`\x00\x00\x00\x02\x00\x01H\
    \xaf\xa4q\x00\x00\x00\x00IEND\xaeB`\x82"

ERROR_NOTHING_TO_TEST = "Configuration incomplète : pas de cas à tester.\
    Assurez-vous que la fonction setUp() génère les données utiles."


def setup_test_app(package, label=None):
    """
    Setup a Django test app for the provided package to allow test models
    tables to be created if the containing app has migrations.

    This function should be called from app.tests __init__ module and pass
    along __package__.

    See https://code.djangoproject.com/ticket/7835
    """

    app_config = AppConfig.create(package)
    app_config.apps = apps

    if label is None:
        containing_app_config = apps.get_containing_app_config(package)
        label = f"{containing_app_config.label}_tests"
    if label in apps.app_configs:
        raise ValueError(f"There's already an app registered with the '{label}' label.")
    app_config.label = label
    apps.app_configs[app_config.label] = app_config
    app_config.import_models()
    apps.clear_cache()

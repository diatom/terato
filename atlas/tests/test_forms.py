# class DiatomFormViewTests(TestCase):
#     def setUp(self):
#         self.user = create_user()
#         self.photo = create_photo_image()
#         self.taxon = create_taxon()
#         self.terato_status = Health.objects.all()[0]
#         self.station = create_station()
#         self.inventory_type = create_inventory_type()
#         self.inventory = create_inventory(self.inventory_type, self.station)
#         self.valid_data = {
#             "photo": self.photo,
#             "taxon": self.taxon.id,
#             "taxon_label": self.taxon.to_label(),
#             "terato_status": self.terato_status,
#             "date": self.inventory.inventory_date,
#             "station": self.station.station_id,
#             "station_label": self.station.station_name,
#             "inventory": self.inventory.inventory_id,
#         }


#     def test_POST_creates_Diatom_on_success(self):
#         self.client.login(username="testuser", password="testpassword")

#         response = self.client.post(reverse("atlas:diatom_upload"), self.valid_data)
#         new_photo = Photo.objects.last()
#         new_diatom = Diatom.objects.last()
#         self.assertEqual(new_photo.filename, self.photo.name)
#         self.assertEqual(new_photo.data_type, "GIF")
#         self.assertEqual(new_diatom.taxon, self.taxon)
#         self.assertEqual(new_diatom.photo, new_photo)
#         self.assertEqual(new_diatom.terato_status, self.terato_status)
#         self.assertEqual(new_diatom.user, self.user)

#     def test_POST_redirects_on_success(self):
#         self.client.login(username="testuser", password="testpassword")
#         photo = create_photo_image()
#         taxon = create_taxon()
#         terato_status = "T"
#         response = self.client.post(reverse("atlas:diatom_upload"), self.valid_data)
#         self.assertRedirects(response, reverse("atlas:success"))

#     def test_POST_invalid_photo_raises_error(self):
#         self.client.login(username="testuser", password="testpassword")
#         photo = create_photo_image(b"not an image", "myinvalidimage.jpg")
#         invalid_data = {**self.valid_data, "photo": photo}
#         response = self.client.post(reverse("atlas:diatom_upload"), invalid_data)

#         self.assertEqual(response.status_code, 200)
#         self.assertFormError(
#             response.context["form"],
#             "photo",
#             "Téléversez une image valide. Le fichier que vous avez
#              transféré n’est pas une image ou bien est corrompu.",
#         )

#     def test_POST_invalid_taxon_raises_error(self):
#         self.client.login(username="testuser", password="testpassword")
#         taxon = "invalid taxon"
#         invalid_data = {**self.valid_data, "taxon": taxon}
#         response = self.client.post(reverse("atlas:diatom_upload"), invalid_data)

#         self.assertEqual(response.status_code, 200)
#         self.assertFormError(
#             response.context["form"],
#             "taxon",
#             "Sélectionnez un choix valide. Ce choix ne fait pas partie de ceux
#               disponibles.",
#         )

#     def test_POST_invalid_terato_status_raises_error(self):
#         self.client.login(username="testuser", password="testpassword")
#         terato_status = "Invalid terato status"
#         invalid_data = {**self.valid_data, "terato_status": terato_status}
#         response = self.client.post(reverse("atlas:diatom_upload"), invalid_data)
#         self.assertEqual(response.status_code, 200)
#         self.assertFormError(
#             response.context["form"],
#             "terato_status",
#             "Sélectionnez un choix valide. Ce choix ne fait pas partie
#               de ceux disponibles.",
#         )

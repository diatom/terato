from django.contrib import admin, messages
from django.contrib.auth.models import Group
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from djatoms.admin import ExtendedModelAdmin, admin_site

from .forms import UserAdminChangeForm
from .models import User


class UserAdmin(ExtendedModelAdmin):

    form = UserAdminChangeForm

    list_display = (
        "last_name",
        "first_name",
        "is_staff",
        "is_superuser",
        "nb_contribs",
        "diatoms",
    )

    # We add is_superuser just to be able to use it in form rendering
    list_editable = ["is_staff", "is_superuser"]

    ordering = ("-is_superuser", "-is_staff", "last_name", "first_name")

    fields = [
        ("first_name", "last_name"),
        "email",
        ("is_staff", "is_superuser"),
        "last_login",
        "groups",
    ]
    readonly_fields = ["first_name", "last_name", "email", "last_login"]

    list_per_page = 20

    @admin.display(description=_("ADMIN USER Contrib. count"))
    def nb_contribs(self, obj):
        return obj.diatom_set(manager="objects").count()

    def diatoms(self, obj):
        link = reverse("admin:atlas_diatom_changelist")
        return mark_safe(
            '<a href="'
            + link
            + '?user__id__exact=%d">%s</a>'
            % (obj.id, _("ADMIN USER See list of diatoms"))
        )

    def save_form(self, request, form, change):
        """
        Making sure no one can play with superuser from admin interface
        """

        obj = form.save(commit=False)
        if form.initial and "is_superuser" in form.initial.keys():
            if form.initial["is_superuser"]:
                obj.is_staff = True
                obj.is_superuser = True
            if not form.initial["is_superuser"]:
                obj.is_superuser = False
        return obj

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        if obj and obj.is_superuser:
            return False
        return super().has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        if obj and obj.is_superuser:
            if request.POST:
                messages.error(
                    request,
                    _(
                        """ADMIN USER You cannot delete a superuser account.\n
                        Please renew your selection."""
                    ),
                )
            return False
        return super().has_delete_permission(request, obj)


admin_site.register(User, UserAdmin)
admin_site.register(Group, ExtendedModelAdmin)

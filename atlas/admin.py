from csp.decorators import csp_update
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect

from djatoms.admin import ExtendedModelAdmin, admin_site
from main.utils import refresh_materialized_view

from .bulk_upload import get_upload_queryset
from .forms import AdminStationForm, BaseDiatomForm
from .models import Diatom, Station

csrf_protect_m = method_decorator(csrf_protect)


class DiatomAdmin(ExtendedModelAdmin):

    list_display = (
        "taxon",
        "user",
        "health",
        "filename",
        "thumbnail_tag",
        "upload_date",
    )
    list_editable = ("health",)
    list_filter = ("user", "upload_date")
    ordering = ("-upload_date", "taxon")

    uploadable = True
    upload_list_display = (
        "photo",
        "filename",
        "taxon",
        "health",
        "scale",
        "station",
        "sampling_date",
        "user",
        "photo_author",
        "photo_doi",
    )
    upload_list_editable = (
        "id",
        "photo",
        "taxon",
        "health",
        "scale",
        "station",
        "sampling_date",
        "photo_author",
        "photo_doi",
    )
    upload_list_form = BaseDiatomForm

    fields = [
        ("taxon", "photo"),
        "user",
        "health",
        ("station", "sampling_date"),
        "scale",
        ("photo_author", "photo_doi"),
    ]
    form = BaseDiatomForm

    class Media:
        css = {"all": ["atlas/css/admin.css"]}

    def get_upload_queryset(self, request):
        return get_upload_queryset(self, request)

    def delete_view(self, request, object_id, extra_context=None):
        dv = super().delete_view(request, object_id, extra_context)
        if request.POST:
            refresh_materialized_view("terato.children_diatoms")
        return dv

    @method_decorator(
        csp_update(
            IMG_SRC=[
                "'self'",
                "data:",
            ],
        )
    )
    def changelist_view(self, request, extra_context=None):
        clv = super().changelist_view(request, extra_context)
        if request.method == "POST" and "_save" in request.POST:
            refresh_materialized_view("terato.children_diatoms")
        return clv

    @method_decorator(
        csp_update(
            IMG_SRC=[
                "'self'",
                "data:",
            ],
        )
    )
    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        cfv = super().changeform_view(request, object_id, form_url, extra_context)
        if request.method == "POST":
            refresh_materialized_view("terato.children_diatoms")
        return cfv


class StationAdmin(ExtendedModelAdmin):

    list_display = ("station_code", "station_name", "commune", "river")

    form = AdminStationForm


admin_site.register(Diatom, DiatomAdmin)
admin_site.register(Station, StationAdmin)

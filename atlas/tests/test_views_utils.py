from django.http import Http404
from django.test import RequestFactory

from tests import TestCase

from ..models import Diatom
from ..views import generate_image, generate_thumbnail


class GenerateImageViewTests(TestCase):
    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):
        self.request = RequestFactory()

    def test_existent_diatom_id_returns_image(self):
        """The view yields diatom photo when given existent diatom id"""

        for d in Diatom.objects.all():
            response = generate_image(self.request, d.id)
            self.assertEqual(response.content, d.photo.tobytes())
            self.assertIn("image/PNG", response._content_type_for_repr)

    def test_inexistent_diatom_raises_error(self):
        """The view raises 404 error when givent inexistent diatom id"""

        with self.assertRaises(Http404):
            generate_image(self.request, 9999)


class GenerateThumbnailViewTests(TestCase):
    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):
        self.request = RequestFactory()

    def test_existent_diatom_id_returns_image(self):
        """The view yields diatom photo when given existent diatom id"""

        for d in Diatom.objects.all():
            response = generate_thumbnail(self.request, d.id)
            self.assertEqual(response.content, d.photo_thumbnail.tobytes())
            self.assertIn("image/PNG", response._content_type_for_repr)

    def test_inexistent_diatom_raises_error(self):
        """The view raises 404 error when givent inexistent diatom id"""
        with self.assertRaises(Http404):
            generate_thumbnail(self.request, 9999)

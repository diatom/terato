from base64 import b64encode
from io import BytesIO

from django.forms.widgets import (
    CheckboxInput,
    ClearableFileInput,
    HiddenInput,
    MultiWidget,
    TextInput,
)
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _
from PIL import Image


class AutocompleteModelChoiceInput(MultiWidget):

    use_fieldset = False

    class Media:
        css = {
            "all": ["djatoms/css/autocomplete.css"],
        }
        js = ["djatoms/js/autocomplete.js"]

    def __init__(self, attrs=None):
        widgets = {
            "": HiddenInput,
            "label": TextInput(attrs={"class": "ui-autocomplete-input"}),
        }
        super().__init__(widgets, attrs)
        self.model = None

    def decompress(self, value):
        if value and self.model:
            # TODO : deal with when value does not return an instance
            instance = self.model.objects.get(pk=value)
            return [value, str(instance)]
        elif value:
            return [value, str(value)]
        else:
            return [None, None]

    def value_from_datadict(self, data, files, name):
        pk, label = super().value_from_datadict(data, files, name)
        return pk


class DisabledCheckboxInput(CheckboxInput):
    template_name = "djatoms/forms/widgets/checkbox_disabled.html"


class UploadableImageInput(ClearableFileInput):
    """
    A widget for an image that is uploadable, but once uploaded,
    is displayed and can't be modified
    """

    def is_initial(self, value):
        if value and isinstance(value, (bytes, memoryview)):
            return True
        else:
            return super().is_initial(value)

    def render(self, name, value, attrs=None, renderer=None):
        context = super().get_context(name, value, attrs)
        if self.is_initial(value):
            # TODO : check that value yields an image
            img_format = Image.open(BytesIO(value)).format
            return mark_safe(
                '<img id={} alt="{}" src="data: image/{}; base64, {}">'.format(
                    name,
                    _("Photo non disponible."),
                    img_format.lower(),
                    b64encode(value).decode("utf8"),
                )
            )
        return super()._render(self.template_name, context, renderer)

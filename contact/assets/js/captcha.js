import { generateSync } from "vanilla-captcha/dom";

$(function () {
  const options = {
    width: 240,
    height: 79,
    backgroundColor: "#FFF",
    font: "30px Arial",
    fontColor: "#777",
    lineColor: "#777",
    lineAmount: 10,
    lineWidth: 2,
  };

  function createCaptcha(init = true) {
    if (init === false) {
      $("#captcha-img").empty();
    }
    var { answer, captcha } = generateSync(6, options);
    const img = document.createElement("img");
    img.src = captcha;
    $("#captcha-img").append(img);
    return answer;
  }
  var answer = createCaptcha();

  $("#btn-refresh").on("click", function () {
    answer = createCaptcha(false);
  });

  $("#captcha-input").on("input", function () {
    if (answer == $(this).val()) {
      $("#captcha-input").removeClass("is-invalid");
      $("#captcha-input").addClass("is-valid");
      $("#btn-submit").removeClass("disabled");
    } else {
      $("#btn-submit").addClass("disabled");
      $("#captcha-input").removeClass("is-valid");
      $("#captcha-input").addClass("is-invalid");
    }
  });
});

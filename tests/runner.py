from types import MethodType

from django.apps import apps

# from django.apps import apps
from django.db import connections
from django.test.runner import DiscoverRunner


def prepare_database(self):
    self.connect()
    self.connection.cursor().execute(
        """
    CREATE SCHEMA django;
    ALTER SCHEMA django OWNER TO diatom_terato_admin;
    GRANT ALL ON SCHEMA django TO terato_admin;

    CREATE SCHEMA terato;
    ALTER SCHEMA terato OWNER TO diatom_terato_admin;
    GRANT ALL ON SCHEMA terato TO terato_admin;

    CREATE SCHEMA diatom;
    ALTER SCHEMA diatom OWNER TO diatom_terato_admin;
    GRANT ALL ON SCHEMA diatom TO terato_admin;
    """
    )


class PostgresSchemaTestRunner(DiscoverRunner):
    def setup_databases(self, **kwargs):
        # make sure schemas are created
        for connection_name in connections:
            connection = connections[connection_name]
            connection.prepare_database = MethodType(prepare_database, connection)

        # create the database using migrations
        ret = super().setup_databases(**kwargs)

        # modify tables' owners
        for connection_name in connections:
            connection = connections[connection_name]
            for table in [
                m._meta.db_table
                for c in apps.get_app_configs()
                for m in c.get_models()
                if c.name != "django.contrib.gis"
            ]:
                connection.cursor().execute(
                    "ALTER TABLE {} OWNER TO diatom_terato_admin;".format(
                        table.replace('"', "")
                    )
                )
        return ret

-- Fixing terato forms of var

update diatom.taxon a
set taxonomy_parent_id = b.taxon_id
from diatom.taxon b
where b.taxon_name = replace(a.taxon_name, ' abnormal form', '') and a.is_terato and a.taxon_name like '%var%' ;

-- Fixing a few official ids

update diatom.taxon a
set taxon_official_id = a.taxon_id
from diatom.taxon b
where a.taxon_official_id = b.taxon_id and a.is_taxon and not b.is_taxon;

-- Adding a deformation factor

insert into terato.defo_factor(defo_factor_name, defo_factor_name_fr, defo_factor_name_en, defo_factor_cat_id) values ('Développement en conditions de culture', 'Développement en conditions de culture', 'Culture growth conditions', 3);

-- Version
insert into terato.db_version(version_name, version_date) values ('24.1', '2024-11-26');

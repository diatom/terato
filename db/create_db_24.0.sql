SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


-- CREATE schemas

CREATE SCHEMA diatom;
ALTER SCHEMA diatom OWNER TO diatom_owner;

CREATE SCHEMA django;
ALTER SCHEMA django OWNER TO diatom_terato_admin;

-- *not* creating schema public, since initdb creates it
ALTER SCHEMA public OWNER TO postgres;

CREATE SCHEMA terato;
ALTER SCHEMA terato OWNER TO diatom_terato_admin;


CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';

SET default_tablespace = '';
SET default_table_access_method = heap;


--
-- CREATE tables and associated sequences
--

CREATE TABLE diatom.communes (
    id integer NOT NULL,
    geom public.geometry(MultiPolygon,4326),
    insee character varying(80),
    nom character varying(80),
    surf_ha numeric
);
ALTER TABLE diatom.communes OWNER TO diatom_owner;

CREATE SEQUENCE diatom.communes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.communes_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.communes_id_seq OWNED BY diatom.communes.id;
ALTER TABLE ONLY diatom.communes ALTER COLUMN id SET DEFAULT nextval('diatom.communes_id_seq'::regclass);


CREATE TABLE diatom.dataorigin (
    dataorigin_id integer NOT NULL,
    dataorigin_name character varying NOT NULL,
    dataorigin_url character varying,
    dataorigin_comment character varying
);
ALTER TABLE diatom.dataorigin OWNER TO diatom_owner;
COMMENT ON TABLE diatom.dataorigin IS 'List of origin of the data';

CREATE SEQUENCE diatom.dataorigin_dataorigin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.dataorigin_dataorigin_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.dataorigin_dataorigin_id_seq OWNED BY diatom.dataorigin.dataorigin_id;
ALTER TABLE ONLY diatom.dataorigin ALTER COLUMN dataorigin_id SET DEFAULT nextval('diatom.dataorigin_dataorigin_id_seq'::regclass);


CREATE TABLE diatom.identifier_type (
    identifier_type_id integer NOT NULL,
    identifier_type_name character varying NOT NULL
);
ALTER TABLE diatom.identifier_type OWNER TO diatom_owner;
COMMENT ON TABLE diatom.identifier_type IS 'List of types of identifiers';

CREATE SEQUENCE diatom.identifier_type_identifier_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.identifier_type_identifier_type_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.identifier_type_identifier_type_id_seq OWNED BY diatom.identifier_type.identifier_type_id;
ALTER TABLE ONLY diatom.identifier_type ALTER COLUMN identifier_type_id SET DEFAULT nextval('diatom.identifier_type_identifier_type_id_seq'::regclass);


CREATE TABLE diatom.inventory (
    inventory_id integer NOT NULL,
    inventory_date timestamp without time zone,
    inventory_year smallint,
    inventory_type_id integer NOT NULL,
    station_id integer NOT NULL,
    station_local_code character varying,
    dataorigin_id integer
);
ALTER TABLE diatom.inventory OWNER TO diatom_owner;
COMMENT ON TABLE diatom.inventory IS 'List of inventories of diatom';
COMMENT ON COLUMN diatom.inventory.inventory_date IS 'Date of the inventory';
COMMENT ON COLUMN diatom.inventory.inventory_year IS 'Year of the inventory';
COMMENT ON COLUMN diatom.inventory.station_local_code IS 'Local codification of the site';

CREATE SEQUENCE diatom.inventory_inventory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.inventory_inventory_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.inventory_inventory_id_seq OWNED BY diatom.inventory.inventory_id;
ALTER TABLE ONLY diatom.inventory ALTER COLUMN inventory_id SET DEFAULT nextval('diatom.inventory_inventory_id_seq'::regclass);


CREATE TABLE diatom.inventory_taxon (
    inventory_taxon_id integer NOT NULL,
    abundance integer,
    inventory_id integer NOT NULL,
    taxon_id integer NOT NULL
);
ALTER TABLE diatom.inventory_taxon OWNER TO diatom_owner;
COMMENT ON TABLE diatom.inventory_taxon IS 'List of taxa found in the inventory';
COMMENT ON COLUMN diatom.inventory_taxon.abundance IS 'Number of individus found';

CREATE SEQUENCE diatom.inventory_taxon_inventory_taxon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.inventory_taxon_inventory_taxon_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.inventory_taxon_inventory_taxon_id_seq OWNED BY diatom.inventory_taxon.inventory_taxon_id;
ALTER TABLE ONLY diatom.inventory_taxon ALTER COLUMN inventory_taxon_id SET DEFAULT nextval('diatom.inventory_taxon_inventory_taxon_id_seq'::regclass);


CREATE TABLE diatom.inventory_type (
    inventory_type_id integer NOT NULL,
    inventory_type_name character varying NOT NULL
);
ALTER TABLE diatom.inventory_type OWNER TO diatom_owner;


CREATE TABLE diatom.station (
    station_id integer NOT NULL,
    station_code character varying,
    station_name character varying,
    commune character varying,
    lon double precision,
    lat double precision,
    river character varying,
    geom public.geometry(Point,4326)
);
ALTER TABLE diatom.station OWNER TO diatom_owner;
COMMENT ON COLUMN diatom.station.station_code IS 'code of the station attributed by the SANDRE';
COMMENT ON COLUMN diatom.station.station_name IS 'Name of the station';
COMMENT ON COLUMN diatom.station.commune IS 'Commune of the station';
COMMENT ON COLUMN diatom.station.lon IS 'Longitude, in WGS84';
COMMENT ON COLUMN diatom.station.lat IS 'Latitude, in WGS84';
COMMENT ON COLUMN diatom.station.river IS 'Name of the river';
COMMENT ON COLUMN diatom.station.geom IS 'Geometric point of the station, in WGS84';

CREATE SEQUENCE diatom.station_station_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.station_station_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.station_station_id_seq OWNED BY diatom.station.station_id;
ALTER TABLE ONLY diatom.station ALTER COLUMN station_id SET DEFAULT nextval('diatom.station_station_id_seq'::regclass);


CREATE TABLE diatom.taxon (
    taxon_id integer NOT NULL,
    taxon_name character varying NOT NULL,
    taxon_official_id integer,
    taxonomy_parent_id integer,
    taxonomy_type_id integer,
    author character varying,
    bibliography character varying,
    comment character varying,
    is_terato boolean DEFAULT false,
    freshwater_diatom boolean DEFAULT true NOT NULL,
    is_taxon boolean DEFAULT true NOT NULL
);
ALTER TABLE diatom.taxon OWNER TO diatom_owner;
COMMENT ON COLUMN diatom.taxon.taxon_name IS 'Name of the taxon';
COMMENT ON COLUMN diatom.taxon.taxon_official_id IS 'Official id of the taxon. The current taxon is a synonym';
COMMENT ON COLUMN diatom.taxon.taxonomy_parent_id IS 'Id of the parent of the taxonomy';
COMMENT ON COLUMN diatom.taxon.bibliography IS 'Origin of the description of the taxon';
COMMENT ON COLUMN diatom.taxon.comment IS 'Precision on the type of taxon, or other';
COMMENT ON COLUMN diatom.taxon.is_terato IS 'Is the taxon a terato form?';
COMMENT ON COLUMN diatom.taxon.freshwater_diatom IS 'True if it is a diatom of fresh water';
COMMENT ON COLUMN diatom.taxon.is_taxon IS 'False if it is not a real taxon';

CREATE SEQUENCE diatom.taxon_taxon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE diatom.taxon_taxon_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.taxon_taxon_id_seq OWNED BY diatom.taxon.taxon_id;
ALTER TABLE ONLY diatom.taxon ALTER COLUMN taxon_id SET DEFAULT nextval('diatom.taxon_taxon_id_seq'::regclass);


CREATE TABLE diatom.taxon_identifier (
    taxon_identifier_id integer NOT NULL,
    taxon_id integer NOT NULL,
    identifier_type_id integer NOT NULL,
    code character varying NOT NULL
);
ALTER TABLE diatom.taxon_identifier OWNER TO diatom_owner;
COMMENT ON TABLE diatom.taxon_identifier IS 'List of codes used to identifier a taxon';

CREATE SEQUENCE diatom.taxon_identifier_taxon_identifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.taxon_identifier_taxon_identifier_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.taxon_identifier_taxon_identifier_id_seq OWNED BY diatom.taxon_identifier.taxon_identifier_id;
ALTER TABLE ONLY diatom.taxon_identifier ALTER COLUMN taxon_identifier_id SET DEFAULT nextval('diatom.taxon_identifier_taxon_identifier_id_seq'::regclass);


CREATE TABLE diatom.taxonomy_type (
    taxonomy_type_id integer NOT NULL,
    taxonomy_type_name character varying NOT NULL,
    taxonomy_type_name_fr character varying,
    taxonomy_type_name_en character varying
);
ALTER TABLE diatom.taxonomy_type OWNER TO diatom_owner;
COMMENT ON TABLE diatom.taxonomy_type IS 'Levels of taxonomy';

CREATE SEQUENCE diatom.taxonomy_type_taxonomy_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE diatom.taxonomy_type_taxonomy_type_id_seq OWNER TO diatom_owner;
ALTER SEQUENCE diatom.taxonomy_type_taxonomy_type_id_seq OWNED BY diatom.taxonomy_type.taxonomy_type_id;
ALTER TABLE ONLY diatom.taxonomy_type ALTER COLUMN taxonomy_type_id SET DEFAULT nextval('diatom.taxonomy_type_taxonomy_type_id_seq'::regclass);


CREATE TABLE django.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
ALTER TABLE django.auth_group OWNER TO diatom_terato_admin;
ALTER TABLE django.auth_group ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
ALTER TABLE django.auth_group_permissions OWNER TO diatom_terato_admin;
ALTER TABLE django.auth_group_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
ALTER TABLE django.auth_permission OWNER TO diatom_terato_admin;
ALTER TABLE django.auth_permission ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id bigint NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
ALTER TABLE django.django_admin_log OWNER TO diatom_terato_admin;
ALTER TABLE django.django_admin_log ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
ALTER TABLE django.django_content_type OWNER TO diatom_terato_admin;
ALTER TABLE django.django_content_type ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
ALTER TABLE django.django_migrations OWNER TO diatom_terato_admin;
ALTER TABLE django.django_migrations ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
ALTER TABLE django.django_session OWNER TO diatom_terato_admin;


CREATE TABLE django.django_site (
    id integer NOT NULL,
    domain character varying NOT NULL,
    name character varying NOT NULL
);
ALTER TABLE django.django_site OWNER TO diatom_terato_admin;
ALTER TABLE django.django_site ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.mailbox_message (
    id integer NOT NULL,
    subject character varying NOT NULL,
    message_id character varying NOT NULL,
    from_header character varying NOT NULL,
    to_header character varying NOT NULL,
    body text NOT NULL,
    encoded boolean NOT NULL,
    received timestamp with time zone NOT NULL,
    processed timestamp with time zone NOT NULL,
    read timestamp with time zone
);
ALTER TABLE django.mailbox_message OWNER TO diatom_terato_admin;
ALTER TABLE django.mailbox_message ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.mailbox_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.users_user (
    id bigint NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
ALTER TABLE django.users_user OWNER TO diatom_terato_admin;
ALTER TABLE django.users_user ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.users_user_groups (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    group_id integer NOT NULL
);
ALTER TABLE django.users_user_groups OWNER TO diatom_terato_admin;
ALTER TABLE django.users_user_groups ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.users_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE django.users_user_user_permissions (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    permission_id integer NOT NULL
);
ALTER TABLE django.users_user_user_permissions OWNER TO diatom_terato_admin;
ALTER TABLE django.users_user_user_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME django.users_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE terato.db_version (
    version_id integer NOT NULL,
    version_name character varying,
    version_date date NOT NULL
);
ALTER TABLE terato.db_version OWNER TO diatom_terato_admin;
COMMENT ON COLUMN terato.db_version.version_name IS 'Version name, format : 3.20.4';

CREATE SEQUENCE terato.db_version_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE terato.db_version_version_id_seq OWNER TO diatom_terato_admin;
ALTER SEQUENCE terato.db_version_version_id_seq OWNED BY terato.db_version.version_id;
ALTER TABLE ONLY terato.db_version ALTER COLUMN version_id SET DEFAULT nextval('terato.db_version_version_id_seq'::regclass);
INSERT INTO terato.db_version(version_name,version_date)  VALUES ('24.0','2024-06-05');


CREATE TABLE terato.defo_factor (
    defo_factor_id integer NOT NULL,
    defo_factor_name character varying NOT NULL,
    defo_factor_name_fr character varying NOT NULL,
    defo_factor_name_en character varying NOT NULL,
    defo_factor_cat_id integer NOT NULL
);
ALTER TABLE terato.defo_factor OWNER TO diatom_terato_admin;
COMMENT ON TABLE terato.defo_factor IS 'Suspected deformation factors for observed teratologies';
COMMENT ON COLUMN terato.defo_factor.defo_factor_name IS 'Name of deformation factor (fallback)';
COMMENT ON COLUMN terato.defo_factor.defo_factor_name_fr IS 'Name of deformation factor, in French';
COMMENT ON COLUMN terato.defo_factor.defo_factor_name_en IS 'Name of deformation factor, in English';

CREATE SEQUENCE terato.defo_factor_defo_factor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE terato.defo_factor_defo_factor_id_seq OWNER TO diatom_terato_admin;
ALTER SEQUENCE terato.defo_factor_defo_factor_id_seq OWNED BY terato.defo_factor.defo_factor_id;
ALTER TABLE ONLY terato.defo_factor ALTER COLUMN defo_factor_id SET DEFAULT nextval('terato.defo_factor_defo_factor_id_seq'::regclass);


CREATE TABLE terato.defo_factor_cat (
    defo_factor_cat_id integer NOT NULL,
    defo_factor_cat_name character varying NOT NULL,
    defo_factor_cat_name_fr character varying NOT NULL,
    defo_factor_cat_name_en character varying NOT NULL
);
ALTER TABLE terato.defo_factor_cat OWNER TO diatom_terato_admin;
COMMENT ON TABLE terato.defo_factor_cat IS 'Categories for suspected deformations of observed teratologies';
COMMENT ON COLUMN terato.defo_factor_cat.defo_factor_cat_name IS 'Name of deformation category (fallback)';
COMMENT ON COLUMN terato.defo_factor_cat.defo_factor_cat_name_fr IS 'Name of deformation category, in French';
COMMENT ON COLUMN terato.defo_factor_cat.defo_factor_cat_name_en IS 'Name of deformation category, in English';

CREATE SEQUENCE terato.defo_factor_cat_defo_factor_cat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE terato.defo_factor_cat_defo_factor_cat_id_seq OWNER TO diatom_terato_admin;
ALTER SEQUENCE terato.defo_factor_cat_defo_factor_cat_id_seq OWNED BY terato.defo_factor_cat.defo_factor_cat_id;
ALTER TABLE ONLY terato.defo_factor_cat ALTER COLUMN defo_factor_cat_id SET DEFAULT nextval('terato.defo_factor_cat_defo_factor_cat_id_seq'::regclass);


CREATE TABLE terato.diatom (
    diatom_id integer NOT NULL,
    taxon_id integer NOT NULL,
    user_id bigint NOT NULL,
    inventory_id integer,
    health_id integer NOT NULL,
    sampling_date timestamp with time zone,
    photo bytea NOT NULL,
    photo_thumbnail bytea NOT NULL,
    photo_filename character varying NOT NULL,
    upload_date timestamp with time zone NOT NULL,
    height integer NOT NULL,
    width integer NOT NULL,
    scale double precision,
    photo_author character varying,
    photo_doi character varying,
    station_id integer
);
ALTER TABLE terato.diatom OWNER TO diatom_terato_admin;
COMMENT ON COLUMN terato.diatom.sampling_date IS 'Sampling date';
COMMENT ON COLUMN terato.diatom.photo IS 'Entire image data';
COMMENT ON COLUMN terato.diatom.photo_thumbnail IS 'Thumbnail image data';
COMMENT ON COLUMN terato.diatom.photo_filename IS 'Original name of image file';
COMMENT ON COLUMN terato.diatom.upload_date IS 'Upload date';
COMMENT ON COLUMN terato.diatom.height IS 'Height of image, in pixels';
COMMENT ON COLUMN terato.diatom.width IS 'Width of image, in pixels';
COMMENT ON COLUMN terato.diatom.scale IS '100px represent this much micrometers';
COMMENT ON COLUMN terato.diatom.photo_author IS 'Original author of the photo';
COMMENT ON COLUMN terato.diatom.photo_doi IS 'DOI of the publication where the photo first appeared';

ALTER TABLE terato.diatom ALTER COLUMN diatom_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME terato.diatom_diatom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE terato.diatom_defo_factor (
    diatom_defo_factor_id integer NOT NULL,
    defo_factor_id integer NOT NULL,
    diatom_id integer NOT NULL
);
ALTER TABLE terato.diatom_defo_factor OWNER TO diatom_terato_admin;

CREATE SEQUENCE terato.diatom_defo_factor_diatom_defo_factor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE terato.diatom_defo_factor_diatom_defo_factor_id_seq OWNER TO diatom_terato_admin;
ALTER SEQUENCE terato.diatom_defo_factor_diatom_defo_factor_id_seq OWNED BY terato.diatom_defo_factor.diatom_defo_factor_id;
ALTER TABLE ONLY terato.diatom_defo_factor ALTER COLUMN diatom_defo_factor_id SET DEFAULT nextval('terato.diatom_defo_factor_diatom_defo_factor_id_seq'::regclass);


CREATE TABLE terato.health (
    health_id integer NOT NULL,
    health_abb character varying NOT NULL,
    health_full character varying NOT NULL,
    health_full_fr character varying NOT NULL,
    health_full_en character varying NOT NULL
);
ALTER TABLE terato.health OWNER TO diatom_terato_admin;
COMMENT ON TABLE terato.health IS 'Health status options for uploaded diatoms';
COMMENT ON COLUMN terato.health.health_abb IS 'One-letter abbreviation of health status';
COMMENT ON COLUMN terato.health.health_full IS 'Full name for health status (fallback)';
COMMENT ON COLUMN terato.health.health_full_fr IS 'Full name for health status, in French';
COMMENT ON COLUMN terato.health.health_full_en IS 'Full name for health status, in English';

CREATE SEQUENCE terato.health_health_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE terato.health_health_id_seq OWNER TO diatom_terato_admin;
ALTER SEQUENCE terato.health_health_id_seq OWNED BY terato.health.health_id;
ALTER TABLE ONLY terato.health ALTER COLUMN health_id SET DEFAULT nextval('terato.health_health_id_seq'::regclass);


CREATE MATERIALIZED VIEW IF NOT EXISTS terato.children_diatoms
 AS
 WITH RECURSIVE enfants(taxon_id, taxon_name, taxon_ancestor) AS (
         SELECT taxon.taxon_id,
            taxon.taxon_name,
            taxon.taxon_official_id AS taxon_ancestor
           FROM diatom.taxon
          WHERE taxon.freshwater_diatom IS TRUE AND taxon.is_taxon IS TRUE
        UNION
         SELECT b.taxon_id,
            b.taxon_name,
            ef_1.taxon_ancestor
           FROM enfants ef_1,
            diatom.taxon t
             RIGHT JOIN diatom.taxon b ON t.taxon_id = b.taxon_official_id
          WHERE t.taxonomy_parent_id = ef_1.taxon_id AND b.freshwater_diatom IS TRUE AND b.is_taxon IS TRUE
        )
 SELECT d.diatom_id,
    ef.taxon_name,
    ef.taxon_ancestor,
    d.health_id,
    d.upload_date
   FROM terato.diatom d
     JOIN enfants ef ON d.taxon_id = ef.taxon_id
 WITH DATA;

ALTER TABLE IF EXISTS terato.children_diatoms
  OWNER TO diatom_terato_admin;

-- Adding constraints

ALTER TABLE ONLY diatom.communes ADD CONSTRAINT communes_pk PRIMARY KEY (id);
ALTER TABLE ONLY diatom.dataorigin ADD CONSTRAINT dataorigin_pk PRIMARY KEY (dataorigin_id);
ALTER TABLE ONLY diatom.identifier_type ADD CONSTRAINT identifier_type_pk PRIMARY KEY (identifier_type_id);
ALTER TABLE ONLY diatom.inventory ADD CONSTRAINT inventory_pk PRIMARY KEY (inventory_id);
ALTER TABLE ONLY diatom.inventory_taxon ADD CONSTRAINT inventory_taxon_pk PRIMARY KEY (inventory_taxon_id);
ALTER TABLE ONLY diatom.inventory_type ADD CONSTRAINT inventory_type_pk PRIMARY KEY (inventory_type_id);
ALTER TABLE ONLY diatom.station ADD CONSTRAINT station_pk PRIMARY KEY (station_id);
ALTER TABLE ONLY diatom.taxon_identifier ADD CONSTRAINT taxon_identifier_pk PRIMARY KEY (taxon_identifier_id);
ALTER TABLE ONLY diatom.taxon ADD CONSTRAINT taxon_pk PRIMARY KEY (taxon_id);
ALTER TABLE ONLY diatom.taxonomy_type ADD CONSTRAINT taxonomy_type_pk PRIMARY KEY (taxonomy_type_id);

ALTER TABLE ONLY django.users_user_groups ADD CONSTRAINT atlas_user_groups_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.users_user_groups ADD CONSTRAINT atlas_user_groups_user_id_group_id_4224dbdb_uniq UNIQUE (user_id, group_id);
ALTER TABLE ONLY django.users_user ADD CONSTRAINT atlas_user_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.users_user_user_permissions ADD CONSTRAINT atlas_user_user_permissions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.users_user_user_permissions ADD CONSTRAINT atlas_user_user_permissions_user_id_permission_id_86aa8dad_uniq UNIQUE (user_id, permission_id);
ALTER TABLE ONLY django.auth_group ADD CONSTRAINT auth_group_name_key UNIQUE (name);
ALTER TABLE ONLY django.auth_group_permissions ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
ALTER TABLE ONLY django.auth_group_permissions ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.auth_group ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.auth_permission ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
ALTER TABLE ONLY django.auth_permission ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.django_admin_log ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.django_content_type ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
ALTER TABLE ONLY django.django_content_type ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.django_migrations ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.django_session ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
ALTER TABLE ONLY django.django_site ADD CONSTRAINT django_site_domain_uniq UNIQUE (domain);
ALTER TABLE ONLY django.django_site ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.mailbox_message ADD CONSTRAINT mailbox_message_pkey PRIMARY KEY (id);
ALTER TABLE ONLY django.users_user ADD CONSTRAINT users_user_email_u UNIQUE (email);

ALTER TABLE ONLY terato.defo_factor_cat ADD CONSTRAINT defo_factor_cat_pkey PRIMARY KEY (defo_factor_cat_id);
ALTER TABLE ONLY terato.defo_factor ADD CONSTRAINT defo_factor_pk PRIMARY KEY (defo_factor_id);
ALTER TABLE ONLY terato.diatom_defo_factor ADD CONSTRAINT diatom_defo_factor_pk PRIMARY KEY (diatom_defo_factor_id);
ALTER TABLE ONLY terato.diatom ADD CONSTRAINT diatom_pkey PRIMARY KEY (diatom_id);
ALTER TABLE ONLY terato.health ADD CONSTRAINT health_pkey PRIMARY KEY (health_id);
ALTER TABLE ONLY terato.db_version ADD CONSTRAINT version_name_uq UNIQUE (version_name);
ALTER TABLE ONLY terato.db_version ADD CONSTRAINT version_pk PRIMARY KEY (version_id);

ALTER TABLE ONLY diatom.inventory
    ADD CONSTRAINT dataorigin_fk FOREIGN KEY (dataorigin_id) REFERENCES diatom.dataorigin(dataorigin_id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY diatom.taxon_identifier
    ADD CONSTRAINT identifier_type_fk FOREIGN KEY (identifier_type_id) REFERENCES diatom.identifier_type(identifier_type_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY diatom.inventory_taxon
    ADD CONSTRAINT inventory_fk FOREIGN KEY (inventory_id) REFERENCES diatom.inventory(inventory_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY diatom.inventory
    ADD CONSTRAINT inventory_type_fk FOREIGN KEY (inventory_type_id) REFERENCES diatom.inventory_type(inventory_type_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY diatom.inventory
    ADD CONSTRAINT station_fk FOREIGN KEY (station_id) REFERENCES diatom.station(station_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY diatom.inventory_taxon
    ADD CONSTRAINT taxon_fk FOREIGN KEY (taxon_id) REFERENCES diatom.taxon(taxon_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY diatom.taxon_identifier
    ADD CONSTRAINT taxon_fk FOREIGN KEY (taxon_id) REFERENCES diatom.taxon(taxon_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY diatom.taxon
    ADD CONSTRAINT taxon_id_official_id_fk FOREIGN KEY (taxon_official_id) REFERENCES diatom.taxon(taxon_id);
ALTER TABLE ONLY diatom.taxon
    ADD CONSTRAINT taxonomy_parent_id_fk FOREIGN KEY (taxonomy_parent_id) REFERENCES diatom.taxon(taxon_id);
ALTER TABLE ONLY diatom.taxon
    ADD CONSTRAINT taxonomy_type_fk FOREIGN KEY (taxonomy_type_id) REFERENCES diatom.taxonomy_type(taxonomy_type_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY django.users_user_groups
    ADD CONSTRAINT atlas_user_groups_group_id_617d980c_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES django.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.users_user_groups
    ADD CONSTRAINT atlas_user_groups_user_id_e2a3bd75_fk_atlas_user_id FOREIGN KEY (user_id) REFERENCES django.users_user(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.users_user_user_permissions
    ADD CONSTRAINT atlas_user_user_perm_permission_id_92d7ea1c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES django.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.users_user_user_permissions
    ADD CONSTRAINT atlas_user_user_permissions_user_id_330c865b_fk_atlas_user_id FOREIGN KEY (user_id) REFERENCES django.users_user(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES django.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES django.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY django.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_atlas_user_id FOREIGN KEY (user_id) REFERENCES django.users_user(id) DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE ONLY terato.defo_factor
    ADD CONSTRAINT defo_factor_cat_fk FOREIGN KEY (defo_factor_cat_id) REFERENCES terato.defo_factor_cat(defo_factor_cat_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY terato.diatom_defo_factor
    ADD CONSTRAINT defo_factor_fk FOREIGN KEY (defo_factor_id) REFERENCES terato.defo_factor(defo_factor_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY terato.diatom_defo_factor
    ADD CONSTRAINT diatom_fk FOREIGN KEY (diatom_id) REFERENCES terato.diatom(diatom_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY terato.diatom
    ADD CONSTRAINT health_fk FOREIGN KEY (health_id) REFERENCES terato.health(health_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY terato.diatom
    ADD CONSTRAINT inventory_fk FOREIGN KEY (inventory_id) REFERENCES diatom.inventory(inventory_id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY terato.diatom
    ADD CONSTRAINT station_fk FOREIGN KEY (station_id) REFERENCES diatom.station(station_id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY terato.diatom
    ADD CONSTRAINT taxon_fk FOREIGN KEY (taxon_id) REFERENCES diatom.taxon(taxon_id);
ALTER TABLE ONLY terato.diatom
    ADD CONSTRAINT users_user_fk FOREIGN KEY (user_id) REFERENCES django.users_user(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


-- Create indexes
CREATE INDEX communes_geom_idx ON diatom.communes USING gist (geom);

CREATE INDEX station_geom_idx ON diatom.station USING gist (geom);
CREATE UNIQUE INDEX station_station_code_idx ON diatom.station USING btree (station_code);
CREATE UNIQUE INDEX station_station_id_idx ON diatom.station USING btree (station_id) ;
CREATE INDEX station_station_name_idx ON diatom.station USING btree (station_name) ;
CREATE INDEX station_commune_idx ON diatom.station USING btree (commune);

CREATE INDEX diatom_defo_factor_diatom_id_idx on terato.diatom_defo_factor USING btree (diatom_id);

CREATE INDEX inventory_inventory_date_idx on diatom.inventory USING btree (inventory_date) ;
CREATE INDEX inventory_station_id_idx on diatom.inventory USING btree (station_id) ;

CREATE INDEX taxon_identifier_type_code_idx ON diatom.taxon_identifier USING btree (code, identifier_type_id);

CREATE UNIQUE INDEX taxon_taxon_id_idx ON diatom.taxon USING btree (taxon_id);
CREATE INDEX taxon_taxon_official_id_idx ON diatom.taxon USING btree (taxon_official_id);
CREATE INDEX taxon_taxonomy_parent_id_idx ON diatom.taxon USING btree (taxonomy_parent_id);
CREATE INDEX taxon_taxon_name_idx ON diatom.taxon USING btree (taxon_name);

CREATE INDEX atlas_user_groups_group_id_617d980c ON django.users_user_groups USING btree (group_id) WITH (fillfactor='90');
CREATE INDEX atlas_user_groups_user_id_e2a3bd75 ON django.users_user_groups USING btree (user_id) WITH (fillfactor='90');
CREATE INDEX atlas_user_user_permissions_permission_id_92d7ea1c ON django.users_user_user_permissions USING btree (permission_id) WITH (fillfactor='90');
CREATE INDEX atlas_user_user_permissions_user_id_330c865b ON django.users_user_user_permissions USING btree (user_id) WITH (fillfactor='90');
CREATE INDEX auth_group_name_a6ea08ec_like ON django.auth_group USING btree (name) WITH (fillfactor='90');
CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON django.auth_group_permissions USING btree (group_id) WITH (fillfactor='90');
CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON django.auth_group_permissions USING btree (permission_id) WITH (fillfactor='90');
CREATE INDEX auth_permission_content_type_id_2f476e4b ON django.auth_permission USING btree (content_type_id) WITH (fillfactor='90');
CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django.django_admin_log USING btree (content_type_id) WITH (fillfactor='90');
CREATE INDEX django_admin_log_user_id_c564eba6 ON django.django_admin_log USING btree (user_id) WITH (fillfactor='90');
CREATE INDEX django_session_expire_date_a5c62663 ON django.django_session USING btree (expire_date) WITH (fillfactor='90');
CREATE INDEX django_session_session_key_c0390e0f_like ON django.django_session USING btree (session_key) WITH (fillfactor='90');

CREATE INDEX diatom_taxon_id_idx ON terato.diatom USING btree (taxon_id) WITH (fillfactor='90');
CREATE INDEX diatom_user_id_idx ON terato.diatom USING btree (user_id) WITH (fillfactor='90');

CREATE INDEX children_diatoms_ancestor_id_idx ON terato.children_diatoms USING btree (taxon_ancestor);
CREATE INDEX children_diatoms_health_id_idx ON terato.children_diatoms USING btree (health_id);
CREATE UNIQUE INDEX children_unique_idx ON terato.children_diatoms USING btree (diatom_id, taxon_ancestor);


-- Grants

GRANT USAGE ON SCHEMA diatom TO diatom_terato_admin;
REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT ALL ON TABLE diatom.communes TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.dataorigin TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.identifier_type TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.inventory TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.inventory_taxon TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.inventory_type TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.station TO diatom_terato_admin;
GRANT ALL ON SEQUENCE diatom.station_station_id_seq TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.taxon TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.taxon_identifier TO diatom_terato_admin;
GRANT ALL ON TABLE diatom.taxonomy_type TO diatom_terato_admin;

from datetime import date
from decimal import Decimal

from django import forms
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.forms import FileField
from django.utils.translation import gettext_lazy as _

from djatoms.fields import AutocompleteModelChoiceField, TaggedModelMultipleChoiceField
from djatoms.widgets import AutocompleteModelChoiceInput, UploadableImageInput

from .models import Commune, DefoFactor, Diatom, Health, Inventory, Station, Taxon


class StationWidget(AutocompleteModelChoiceInput):
    def value_from_datadict(self, data, files, name):
        pk = super().value_from_datadict(data, files, name)
        if pk != "-1":
            return pk
        else:
            return (data["station"], data["station_label"])


def create_station(form):
    station = Station()
    station.station_name = form.cleaned_data["station_label"]
    # TODO : check projection
    station.lon = form.cleaned_data["longitude"]
    station.lat = form.cleaned_data["latitude"]
    station.geom = Point(
        float(form.cleaned_data["longitude"]),
        float(form.cleaned_data["latitude"]),
        srid=4326,
    )

    communes = Commune.objects.filter(geom__contains=station.geom)
    if communes and len(communes) == 1:
        station.commune = list(communes)[0].name
    station.save()
    return station


class AdminStationForm(forms.ModelForm):
    class Meta:
        model = Station
        fields = ["station_code", "station_name", "commune", "river", "lon", "lat"]

    def save(self, commit=True):
        self.instance.geom = Point(
            float(self.cleaned_data["lon"]),
            float(self.cleaned_data["lat"]),
            srid=4326,
        )
        return super().save(commit)


class BaseDiatomForm(forms.ModelForm):
    class Meta:
        model = Diatom
        fields = [
            "photo",
            "taxon",
            "health",
            "sampling_date",
            "photo_author",
            "photo_doi",
            "scale",
            "station",
        ]

    photo = forms.ImageField(label="", widget=UploadableImageInput())
    taxon = AutocompleteModelChoiceField(
        queryset=Taxon.objects.all(), label=_("AJOUT CHAMP Taxon")
    )
    health = forms.ModelChoiceField(
        queryset=Health.objects.all(),
        label=_("AJOUT CHAMP Statut térato"),
    )
    sampling_date = forms.DateField(
        widget=forms.DateInput(
            attrs={"type": "date", "min": "1970-01-01", "max": date.today()},
            format="%Y-%m-%d",
        ),
        label=_("AJOUT CHAMP Date de prélèvement"),
        required=False,
    )
    station = AutocompleteModelChoiceField(
        queryset=Station.objects.all(),
        required=False,
        label=_("AJOUT CHAMP Station label"),
    )
    photo_author = forms.CharField(
        label=_("AJOUT CHAMP Photo author"),
        required=False,
    )
    photo_doi = forms.CharField(
        label=_("AJOUT CHAMP Photo DOI"),
        required=False,
    )
    scale = forms.DecimalField(
        label=_("AJOUT CHAMP Echelle"),
        required=False,
        min_value=Decimal(0.01),
        max_digits=16,
        decimal_places=10,
    )


class FullDiatomForm(BaseDiatomForm):
    inventory = forms.ModelChoiceField(
        queryset=Inventory.objects.all(), widget=forms.HiddenInput(), required=False
    )
    deformation_factors = TaggedModelMultipleChoiceField(
        queryset=DefoFactor.objects.all(),
        widget=forms.CheckboxSelectMultiple(),
        tag_field="category",
        required=False,
        label="",
    )
    station = AutocompleteModelChoiceField(
        queryset=Station.objects.all(),
        required=False,
        label=_("AJOUT CHAMP Station label"),
        widget=StationWidget,
    )
    latitude = forms.CharField(
        label="Latitude",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control form-control-sm"}),
    )
    longitude = forms.CharField(
        label="Longitude",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control form-control-sm"}),
    )
    scale = forms.DecimalField(
        required=False,
        min_value=Decimal(0),
        widget=forms.HiddenInput(),
        max_digits=16,
        decimal_places=10,
    )
    scale_human = forms.DecimalField(
        required=False,
        min_value=Decimal(0.01),
        initial=10,
        step_size=Decimal(0.01),
        label="",
        help_text=_("AJOUT CHAMP ECHELLE"),
    )

    def _clean_fields(self):
        create = False
        for name, bf in self._bound_items():
            field = bf.field
            value = bf.initial if field.disabled else bf.data
            try:
                if isinstance(field, FileField):
                    value = field.clean(value, bf.initial)
                else:
                    if name == "station" and isinstance(value, tuple):
                        self.cleaned_data["station_label"] = value[1]
                        create = True
                        continue
                    value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, "clean_%s" % name):
                    value = getattr(self, "clean_%s" % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self.add_error(name, e)

        if create:
            value = create_station(self)
            self.cleaned_data["station"] = value

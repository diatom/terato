from django.urls import path

from .views import ContactView, FlagView

app_name = "contact"
urlpatterns = [
    path("", ContactView.as_view(), name="contact"),
    path("flag/<int:pk>/", FlagView.as_view(), name="flag"),
]

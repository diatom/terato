# bundle static files
FROM node:18.17.0-slim as start
WORKDIR /usr/src/terato

COPY ./atlas/assets ./atlas/assets
COPY ./contact/assets ./contact/assets
COPY ./djatoms/assets ./djatoms/assets
COPY ./generic/assets ./generic/assets
COPY ./mailbox/assets ./mailbox/assets
COPY ./main/assets ./main/assets
COPY ./users/assets ./users/assets

COPY ./package.json .
COPY ./package-lock.json .
COPY ./webpack.config.js .
COPY ./.env .

RUN npm ci
RUN npx webpack

# pull Python base image
FROM python:3.10.12-slim as build
WORKDIR /usr/src/terato

ARG BUNDLE_DIR

COPY . .
COPY --from=start /usr/src/terato/${BUNDLE_DIR} ./${BUNDLE_DIR}
COPY --from=start /usr/src/terato/webpack-stats.json .

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONFAULTHANDLER 1

RUN pip install pipenv
RUN apt-get update && apt-get install -y --no-install-recommends gcc
RUN apt-get -y install libpq-dev
RUN apt-get -y install libgdal-dev
RUN apt-get install -y cron

# install dependencies
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --system --deploy

# collect static files
RUN python manage.py collectstatic --no-input

# set up cronjobs
RUN chmod +x delete_entries.sh
RUN crontab -u root crontab.txt
CMD ["cron", "-f"]

# delete assets and other unused files
RUN find . -name assets -exec rm -rf {} \; ; exit 0
RUN rm package.json
RUN rm package-lock.json
RUN rm crontab.txt

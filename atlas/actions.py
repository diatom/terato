from django.contrib.admin.actions import delete_selected as prev_ds
from django.contrib.admin.decorators import action
from django.utils.translation import gettext_lazy

from main.utils import refresh_materialized_view


@action(
    permissions=["delete"],
    description=gettext_lazy("Delete selected %(verbose_name_plural)s"),
)
def delete_selected(modeladmin, request, queryset):
    ds = prev_ds(modeladmin, request, queryset)
    if ds is None:
        refresh_materialized_view("terato.children_diatoms")
    return ds

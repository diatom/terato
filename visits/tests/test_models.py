from django.utils import timezone

from tests import TestCase

from ..models import Error


class ErrorTestCase(TestCase):
    def setUp(self):

        self.ua_string = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) \
            AppleWebKit/537.36 (KHTML, like Gecko) \
            Chrome/83.0.4103.116 Safari/537.36"

        self.error = Error(
            error_time=timezone.now(),
            path="/",
            error_code=302,
            traceback="Error in form",
            ua_string=self.ua_string,
        )

    def test_post_decoded(self):

        self.assertEqual(self.error.post_decoded, "-")

        POST = {"field1": "a", "field2": 2}
        self.error.posted = str(POST).encode()
        self.assertEqual(self.error.post_decoded, str(POST))

    def test_str(self):
        self.assertEqual(
            str(self.error), "Error 302 - {}".format(timezone.now().date())
        )

    def test_user_agent(self):
        self.assertEqual(
            str(self.error.user_agent), "PC / Mac OS X 10.15.5 / Chrome 83.0.4103"
        )
        self.assertEqual(self.error.device, "PC")
        self.assertEqual(self.error.os, "Mac OS X 10.15.5")
        self.assertEqual(self.error.browser, "Chrome 83.0.4103")

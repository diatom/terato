import copy

from django.contrib.admin.views.main import ChangeList
from django.core.exceptions import FieldDoesNotExist
from django.utils.translation import gettext


class UploadQuerySet(list):

    """
    Utility class that fakes the behaviour of a Django QuerySet.

    When a bulk upload takes place, the data we work on is not yet saved in
    the database, so QuerySets don't work. However, we still want to work
    with most of Django admin functionalities. That's the purpose of this class
    - give Django what it needs: an iterable with a few, minimal methods.
    """

    def __init__(self, list, model):
        super().__init__(list)
        self.ordered = True
        self.model = model

    def count(self):
        return len(self)

    def _clone(self):
        return copy.deepcopy(self)

    def filter(self, **kwargs):
        res = UploadQuerySet([], self.model)
        for arg, value in kwargs.items():
            if arg.endswith("__in"):
                attr = arg.split("__in")[0]
                for item in self:
                    if getattr(item, attr) in value:
                        res.append(item)
                    elif str(getattr(item, attr)) in value:
                        res.append(item)
        return res

    def get(self, **kwargs):
        for arg, value in kwargs.items():
            for item in self:
                if hasattr(item, arg):
                    if getattr(item, arg) == value:
                        return item


class UploadList(ChangeList):

    """
    Adapted version of ChangeList, to work with UploadQuerySets
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title = gettext("ADMIN Téléverser des données compressées")

    def get_queryset(self, request, exclude_parameters=None):

        qs = self.root_queryset
        if isinstance(qs, UploadQuerySet):
            return qs
        return super().get_queryset(request, exclude_parameters)

    def get_results(self, request):
        super().get_results(request)
        self.result_list = UploadQuerySet(self.result_list, self.model)


class DictQuerySet(dict):

    """
    Minimal patch to convert output of qs.values() to something
    Django's admin can work with.

    Useful to show aggregates instead of full tables in admin.
    """

    def __init__(self, model, *args, **kwargs):
        super(DictQuerySet, self).__init__(*args, **kwargs)
        self.__dict__ = self
        self.model = model
        self._meta = model._meta
        self.id = 1

    def serializable_value(self, field_name):
        try:
            field = self._meta.get_field(field_name)
        except FieldDoesNotExist:
            return getattr(self, field_name)
        return getattr(self, field.attname)


class AggregatedChangeList(ChangeList):
    def _get_deterministic_ordering(self, ordering):
        """
        Do not try to find total ordering from non-aggregated table.
        """
        return ordering

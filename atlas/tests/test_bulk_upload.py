import datetime

from django.contrib.auth import get_user_model
from django.contrib.messages import get_messages
from django.contrib.messages.storage.fallback import FallbackStorage
from django.core.exceptions import ObjectDoesNotExist
from django.test import RequestFactory
from django.utils.translation import gettext_lazy as _

from djatoms.admin import admin_site
from djatoms.admin.views import UploadQuerySet
from tests import TestCase
from tests.utils import DEFAULT_EMAIL

from ..admin import DiatomAdmin
from ..bulk_upload import get_upload_queryset
from ..models import Diatom, Health, Station, Taxon

User = get_user_model()


class GetUploadQuerySetTestCase(TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):
        self.request = RequestFactory()
        self.request.session = "session"
        self.request.FILES = {"file": "atlas/tests/folder_with_invalid_files.zip"}
        self.request._messages = FallbackStorage(self.request)
        self.request.user = User.objects.get(email=DEFAULT_EMAIL)
        self.request.COOKIES = {}

        self.model_admin = DiatomAdmin(Diatom, admin_site)

    def test_valid_folder_yields_queryset(self):
        qs = get_upload_queryset(self.model_admin, self.request)

        self.assertEqual(type(qs), UploadQuerySet)
        for d in qs:
            self.assertEqual(type(d), Diatom)

    def test_valid_folder_invalid_files_are_excluded(self):
        qs = get_upload_queryset(self.model_admin, self.request)

        self.assertEqual(len(qs), 5)
        for d in qs:
            self.assertIn(
                d.filename.split(".")[-1], ["png", "jpg", "jpeg", "tif", "tiff", "bmp"]
            )

    def test_valid_folder_invalid_images_yield_message(self):
        get_upload_queryset(self.model_admin, self.request)

        messages = [m for m in get_messages(self.request)]

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].level, 30)
        self.assertEqual(
            messages[0].message,
            _(
                "UPLOAD MSG Found %(count)s files with invalid format. \
                Accepted formats are .png, .jpg, .jpeg, .tif, .tiff \
                and .bmp."
            )
            % {"count": 1},
        )

    def test_valid_folder_diatoms_get_correct_data(self):

        qs = get_upload_queryset(self.model_admin, self.request)
        exceptions = 0

        for d in qs:

            if d.filename == "grid.jpeg":
                expected = {
                    "user": User.objects.get(pk=2),
                    "station": Station.objects.get(pk=2),
                    "scale": None,
                    "sampling_date": datetime.datetime(2015, 1, 1, 0, 0),
                    "photo_author": "",
                    "photo_doi": "10.1016/j.aquatox.2008.03.011",
                    "taxon": None,
                    "health": None,
                }

            elif d.filename == "grid.png":
                expected = {
                    "user": User.objects.get(pk=2),
                    "taxon": Taxon.objects.get(pk=6),
                    "health": Health.objects.get(pk=3),
                    "scale": 6,
                    "photo_author": "noone",
                    "photo_doi": "anything",
                    "station": None,
                }

            elif d.filename == "grid.tiff":
                expected = {
                    "user": User.objects.get(pk=2),
                    "taxon": Taxon.objects.get(pk=23),
                    "scale": 8,
                    "sampling_date": datetime.datetime(2017, 3, 2, 0, 0),
                    "photo_author": "",
                    "photo_doi": "",
                    "health": None,
                    "station": None,
                }

            elif d.filename == "grid.jpg":
                expected = {
                    "user": User.objects.get(pk=2),
                    "health": Health.objects.get(pk=2),
                    "scale": None,
                    "station": Station.objects.get(pk=3),
                    "photo_author": "",
                    "photo_doi": "",
                    "taxon": None,
                }

            elif d.filename == "grid.bmp":
                expected = {
                    "user": User.objects.get(pk=2),
                    "taxon": Taxon.objects.get(pk=26),
                    "health": Health.objects.get(pk=1),
                    "scale": 10,
                    "sampling_date": None,
                    "photo_author": "",
                    "photo_doi": "",
                    "station": None,
                }

            for k in expected:
                try:
                    self.assertEqual(
                        getattr(d, k),
                        expected[k],
                        msg="Error for diatom %s and field %s" % (d.filename, k),
                    )
                except Exception as e:
                    exceptions += 1
                    # print(e)
                    self.assertTrue(issubclass(type(e), ObjectDoesNotExist))

        self.assertEqual(exceptions, 4)

    def test_invalid_folder_no_csv(self):
        self.request.FILES = {"file": "atlas/tests/folder_no_csv.zip"}
        self.assertFalse(get_upload_queryset(self.model_admin, self.request))

        messages = [m for m in get_messages(self.request)]

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].level, 40)
        self.assertEqual(
            messages[0].message, _("UPLOAD MSG No csv file provided during upload.")
        )

    def test_invalid_folder_two_csvs(self):
        self.request.FILES = {"file": "atlas/tests/folder_two_csvs.zip"}
        self.assertFalse(get_upload_queryset(self.model_admin, self.request))

        messages = [m for m in get_messages(self.request)]

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].level, 40)
        self.assertEqual(
            messages[0].message,
            _("UPLOAD MSG More than one csv file provided during upload."),
        )

    def test_invalid_folder_no_image(self):
        self.request.FILES = {"file": "atlas/tests/folder_with_no_image.zip"}
        self.assertFalse(get_upload_queryset(self.model_admin, self.request))

        messages = [m for m in get_messages(self.request)]

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].level, 40)
        self.assertEqual(
            messages[0].message,
            _(
                "UPLOAD MSG Found no file with a valid format. Accepted \
            formats are .png, .jpg, .jpeg, .tif, .tiff and .bmp."
            ),
        )

    def test_csv_in_subfolder(self):
        self.request.FILES = {"file": "atlas/tests/folder_with_csv_not_at_root.zip"}
        qs = get_upload_queryset(self.model_admin, self.request)

        messages = [m for m in get_messages(self.request)]

        self.assertEqual(len(messages), 2)
        self.assertEqual(messages[1].level, 30)
        self.assertEqual(
            messages[1].message,
            _(
                "UPLOAD MSG For %(errors)s paths (total : %(total)s),\
                    no corresponding file was found"
            )
            % {"errors": 5, "total": 6},
        )
        self.assertEqual(len(qs), 1)

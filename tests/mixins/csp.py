from django.urls import NoReverseMatch, reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from .urls import UrlsMixin


class CSPTestMixin(UrlsMixin):

    """
    Mixin to test that csp headers are as expected
    """

    def _csp_test(self, name, expected, args=[]):

        name = name if ":" in name else self.app + ":" + name
        response = self.client.get(reverse(name, args=args))
        policy_list = sorted(response["Content-Security-Policy"].split("; "))

        policies = {}
        for policy in policy_list:
            p = policy.split(" ")
            policies[p[0]] = p[1:]

        msg = "Incorrect csp config for %s" % name
        self.assertEqual(policies, expected, msg)

    def _csp_test_all_other(self, exclude):

        expected = {"default-src": ["'self'"]}

        for n in self.url_names:
            if n in exclude:
                continue

            try:
                self._csp_test(n, expected)
            except NoReverseMatch:
                try:
                    self._csp_test(n, expected, [1])
                except NoReverseMatch:
                    uid = urlsafe_base64_encode(force_bytes(1))
                    self._csp_test(n, expected, [uid, "whatever"])

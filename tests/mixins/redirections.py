from django.urls import reverse

from .urls import UrlsMixin


class RedirectionTestMixin(UrlsMixin):

    """
    Mixin to be used with TestCase. Provides basic functionality for
    testing GET redirections.

    Inheritor test classes should define a self.expected dictionnary
    in their setUp method, with this format:
        (url_name,
            args for url_name) : (None or expected url_name,
                args for expected url_name)
    """

    def _get_names(self, asked, expected=""):
        """
        Get asked and expected name. If no semi-colon in provided name,
        assumes the name should be found in this space.
        This is useful in case of inter-apps redirections, e.g.
        atlas:diatom_upload -> users:logins
        """

        if ":" not in asked:
            self._asked_full = self.app + ":" + asked
            self._asked_name = asked
        else:
            self._asked_full = asked
            self._asked_name = asked.split(":")[1]

        if ":" not in expected:
            self._expected_full = self.app + ":" + expected
            self._expected_name = expected
        else:
            self._expected_full = expected
            self._expected_name = expected.split(":")[1]

    def base_redirection_test(
        self, asked, expected, asked_args=None, expected_args=None
    ):
        """
        Base test for redirections.
        """

        self._get_names(asked, expected)

        response = self.client.get(
            reverse(self._asked_full, args=asked_args), follow=True
        )
        self.assertEqual(
            response.status_code,
            200,
            msg="Response status code was not 200 for {}".format(self._asked_name),
        )

        url = reverse(self._expected_full, args=expected_args)
        if response.redirect_chain:
            if "?next=" in response.redirect_chain[0][0]:
                url = url + "?next=" + reverse(self._asked_full, args=asked_args)

        self.assertRedirects(
            response,
            url,
            msg_prefix="{} did not redirect to {}".format(
                self._asked_name, self._expected_name
            ),
        )

    def base_no_redirection_test(self, asked, asked_args=None, expected_code=200):
        """
        Base test for no redirections.
        """

        self._get_names(asked)

        response = self.client.get(
            reverse(self._asked_full, args=asked_args), follow=True
        )
        self.assertEqual(
            response.status_code,
            expected_code,
            msg="Response status code was not 200 for {}".format(self._asked_name),
        )
        if expected_code != 200:
            self.assertTemplateUsed(
                response,
                str(expected_code) + ".html",
                msg_prefix="{} did not use error template {}".format(
                    self._asked_name, expected_code
                ),
            )

    def test_expected_redirections(self):

        exp_redirects = {
            k: v for k, v in self.expected.items() if isinstance(v[0], str)
        }
        for a, e in exp_redirects.items():
            asked_args = list(a[1:])
            expected_args = list(e[1:])

            self.base_redirection_test(a[0], e[0], asked_args, expected_args)

    def test_expected_no_redirections(self):

        exp_no_redirects = {
            k: v for k, v in self.expected.items() if not isinstance(v[0], str)
        }
        for a, e in exp_no_redirects.items():
            asked_args = list(a[1:])
            if isinstance(e[0], int):
                self.base_no_redirection_test(a[0], asked_args, e[0])
            else:
                self.base_no_redirection_test(a[0], asked_args)

    def test_meta_all_urls_considered(self):

        tested = [u[0] for u in self.expected.keys()]
        for n in self.url_names:
            self.assertIn(
                n,
                tested,
                msg="Redirections for {} were not tested, please review \
              test config".format(
                    n
                ),
            )

from io import BytesIO

from csp.decorators import csp_update
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, UpdateView
from PIL import Image

from main.utils import refresh_materialized_view
from visits.views import ErrorFormViewMixin

from ..forms import FullDiatomForm
from ..models import Diatom, DiatomDefoFactor, Taxon
from .utils import check_doi


class DiatomFormViewMixin(ErrorFormViewMixin):
    model = Diatom
    form_class = FullDiatomForm

    def form_valid(self, form):

        if form.cleaned_data["photo_doi"] and not check_doi(
            form.cleaned_data["photo_doi"]
        ):
            form.cleaned_data["photo_doi"] = None
            messages.warning(self.request, _("AJOUT MSG Error in DOI"))

        taxon = form.cleaned_data["taxon"]
        if taxon.is_terato:
            taxon = Taxon.objects.get(pk=taxon.taxon_parent.id)

        self.object.taxon = taxon
        self.object.health = form.cleaned_data["health"]
        self.object.inventory = form.cleaned_data["inventory"]
        self.object.station = form.cleaned_data["station"]
        self.object.sampling_date = form.cleaned_data["sampling_date"]
        self.object.scale = form.cleaned_data["scale"]
        self.object.photo_author = form.cleaned_data["photo_author"]
        self.object.photo_doi = form.cleaned_data["photo_doi"]

        self.object.save()

        DiatomDefoFactor.objects.filter(diatom__id=self.object.id).delete()
        deformation_factors = form.cleaned_data["deformation_factors"]
        for factor in deformation_factors:
            DiatomDefoFactor.objects.create(
                deformation_factor=factor, diatom=self.object
            )

        refresh_materialized_view("terato.children_diatoms")


class UploadFormView(LoginRequiredMixin, DiatomFormViewMixin, CreateView):
    success_url = reverse_lazy("atlas:contrib:diatom_upload")
    template_name = "diatom_upload.html"

    def form_valid(self, form):
        self.object = Diatom()

        image = form.cleaned_data["photo"]
        binary = image.file.read()
        thumbnail = Image.open(image)
        thumbnail.thumbnail((250, 250))
        with BytesIO() as output:
            thumbnail.save(output, format="PNG")
            thumbnail = output.getvalue()
        # TODO: define thumbnail format and resolution somewhere else common

        self.object.photo = binary
        self.object.photo_thumbnail = thumbnail
        self.object.filename = image.name
        self.object.height = image.image.height
        self.object.width = image.image.width
        self.object.user = self.request.user

        super().form_valid(form)

        messages.success(self.request, _("AJOUT MSG Diatomée ajoutée"))
        return redirect(self.get_success_url())

    @method_decorator(
        csp_update(
            IMG_SRC=[
                "'self'",
                "blob:",
                "data:",
                "https://tile.openstreetmap.org",
                "https://*.google.com",
            ],
            CONNECT_SRC=["'self'", "blob:", "https://geolocation-db.com/json/"],
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ModifyFormView(LoginRequiredMixin, DiatomFormViewMixin, UpdateView):
    success_url = reverse_lazy("users:profile")
    template_name = "diatom_modify.html"

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if (not obj.user == self.request.user) and (not self.request.user.is_superuser):
            raise PermissionDenied
        return obj

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        if self.object.scale:
            scale_human = round(self.object.scale * self.object.width / 100, 2)
        else:
            scale_human = None

        kwargs.update(
            initial={
                "scale_human": scale_human,
                "deformation_factors": [
                    i.deformation_factor
                    for i in DiatomDefoFactor.objects.filter(diatom__id=self.object.id)
                ],
            }
        )
        return kwargs

    def form_valid(self, form):

        super().form_valid(form)

        messages.success(self.request, _("MODIFY MSG Diatomée modifiée"))
        return redirect(self.get_success_url())

    @method_decorator(
        csp_update(
            IMG_SRC=[
                "'self'",
                "data:",
                "https://tile.openstreetmap.org",
                "https://*.google.com",
            ],
            CONNECT_SRC=["'self'", "https://geolocation-db.com/json/"],
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

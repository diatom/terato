from django.views.csrf import csrf_failure


def custom_csrf_failure(request, reason=""):
    return csrf_failure(request, reason, template_name="403.html")

from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, ButtonHolder, Div, Field, Layout, Submit
from django import forms
from django.contrib.auth.forms import (
    AuthenticationForm,
    PasswordChangeForm,
    PasswordResetForm,
    SetPasswordForm,
    UserCreationForm,
)
from django.urls import reverse_lazy
from django.utils.html import mark_safe
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _

from djatoms.widgets import DisabledCheckboxInput

from .models import User


class UserCreationForm(UserCreationForm):
    usable_password = None

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "password1", "password2")

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.fields["email"].widget.attrs.pop("autofocus", None)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Div(Field("first_name"), css_class="col-6"),
                Div(Field("last_name"), css_class="col-6"),
                css_class="row",
            ),
            Field("email"),
            Field("password1"),
            Field("password2"),
            Field("cgu", value=0),
            ButtonHolder(
                Submit(
                    "submit",
                    _("REGISTER BUTTON Créer un compte"),
                    css_class="btn btn-primary disabled",
                    css_id="id-submit",
                ),
                css_class="text-center mb-3",
            ),
        )

    first_name = forms.CharField(max_length=50, label=_("Prénom"))
    last_name = forms.CharField(max_length=50, label=_("Nom"))
    email = forms.EmailField()
    cgu = forms.ChoiceField(
        choices=[(True, "")],
        widget=forms.CheckboxInput,
        label=mark_safe(
            format_lazy(
                "{read}<a href={link}>{name}</a>",
                read=_("REGISTER CHECK J'ai lu et j'accepte"),
                link=reverse_lazy("generic:cgu"),
                name=_("REGISTER CHECK CGU"),
            )
        ),
        required=False,
    )


class UsersFormMixin:

    button_text = ""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(*self.fields),
            ButtonHolder(Submit("submit", self.button_text), css_class="text-center"),
        )


class PasswordResetForm(UsersFormMixin, PasswordResetForm):
    button_text = _("PASSWORD BUTTON Envoyer le mail")


class SetPasswordForm(UsersFormMixin, SetPasswordForm):
    button_text = _("PASSWORD BUTTON Réinitialiser")


class PasswordChangeForm(UsersFormMixin, PasswordChangeForm):
    button_text = _("PASSWORD BUTTON Valider")


class AuthenticationForm(UsersFormMixin, AuthenticationForm):
    button_text = _("LOGIN BUTTON Se connecter")

    class Meta:
        model = User
        fields = ("email", "password")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"].widget.attrs.pop("autofocus", None)
        self.helper.form_show_errors = False


class DeleteProfileForm(forms.Form):
    class Meta:
        fields = "keep_diatom"

    keep_diatom = forms.ChoiceField(
        choices=[
            ("KEEP", _("DELETE CHOICE Conserver mes ajouts")),
            ("ANONYM", _("DELETE CHOICE Anonymiser mes ajouts")),
            ("DELETE", _("DELETE CHOICE Supprimer mes ajouts")),
        ],
        widget=forms.RadioSelect,
        label=_("DELETE MSG Choisissez une option pour vos contributions"),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(*self.fields),
            Div(
                FormActions(
                    HTML(
                        """
                    <a type="button" class="btn btn-secondary" href={}>{}</a>
                    """.format(
                            reverse_lazy("users:profile"), _("DELETE BUTTON Annuler")
                        )
                    ),
                    Submit(
                        "submit",
                        _("DELETE BUTTON Supprimer"),
                        css_class="btn btn-danger",
                    ),
                ),
                css_class="text-end",
            ),
        )


class UserAdminChangeForm(forms.ModelForm):

    fields = [
        "first_name",
        "last_name",
        "email",
        "is_staff",
        "is_superuser",
        "last_login",
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.initial and "is_superuser" in self.initial.keys():
            if self.initial["is_superuser"]:
                self.fields["is_staff"] = forms.BooleanField(
                    widget=DisabledCheckboxInput, required=False
                )
            else:
                self.fields["is_staff"] = forms.BooleanField(required=False)

    is_superuser = forms.BooleanField(widget=DisabledCheckboxInput, required=False)

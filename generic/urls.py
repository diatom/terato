from django.urls import path
from django.views.generic import TemplateView

app_name = "generic"
urlpatterns = [
    path("", TemplateView.as_view(template_name="home.html"), name="home"),
    path("cgu", TemplateView.as_view(template_name="cgu.html"), name="cgu"),
    path(
        "legal-notice", TemplateView.as_view(template_name="legals.html"), name="legals"
    ),
]

from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BTreeIndex, GistIndex
from django.utils.translation import gettext_lazy as _


class Station(models.Model):
    station_id = models.AutoField(primary_key=True)
    station_code = models.CharField(
        _("Station code"),
        unique=True,
        blank=True,
        null=True,
        db_comment="code of the station attributed by the SANDRE",
    )
    station_name = models.CharField(
        _("Station name"), blank=True, null=True, db_comment="Name of the station"
    )
    commune = models.CharField(
        _("Locality"), blank=True, null=True, db_comment="Commune of the station"
    )
    lon = models.FloatField(
        _("Longitude"), blank=True, null=True, db_comment="Longitude, in WGS84"
    )
    lat = models.FloatField(
        _("Latitude"), blank=True, null=True, db_comment="Latitude, in WGS84"
    )
    geom = models.PointField(
        blank=True, null=True, db_comment="Geometric point of the station, in in WGS84"
    )
    river = models.CharField(
        _("River"), blank=True, null=True, db_comment="Name of the river"
    )

    class Meta:
        db_table = 'diatom"."station'
        indexes = [
            GistIndex(fields=["geom"], name="station_geom_idx"),
            BTreeIndex(fields=["station_code"], name="station_station_code_idx"),
            BTreeIndex(fields=["station_id"], name="station_station_id_idx"),
            BTreeIndex(fields=["station_name"], name="station_station_name_idx"),
            BTreeIndex(fields=["commune"], name="station_commune_idx"),
        ]
        managed = False
        verbose_name = _("Station")
        verbose_name_plural = _("Stations")

    def __str__(self):
        return self.station_name


class StationProjection(models.Model):
    id = models.AutoField(primary_key=True, db_column="station_projection_id")
    x = models.FloatField()
    y = models.FloatField()
    station = models.ForeignKey(
        "Station", on_delete=models.CASCADE, db_column="station_id"
    )
    projection = models.ForeignKey(
        "Projection", on_delete=models.CASCADE, db_column="projection_id"
    )

    class Meta:
        db_table = 'diatom"."station_projection'
        managed = False


class Projection(models.Model):
    id = models.AutoField(primary_key=True, db_column="projection_id")
    projection_name = models.CharField()
    epsg = models.SmallIntegerField(null=True)
    projection_code = models.CharField()

    class Meta:
        db_table = 'diatom"."projection'
        managed = False

    def __str__(self):
        return self.projection_name


class Commune(models.Model):
    id = models.AutoField(primary_key=True)
    insee = models.CharField()
    name = models.CharField(db_column="nom")
    geom = models.PolygonField()

    class Meta:
        db_table = 'diatom"."communes'
        managed = False

        indexes = [GistIndex(fields=["geom"], name="communes_geom_idx")]

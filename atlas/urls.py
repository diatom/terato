from django.urls import include, path

from . import views

contrib_patterns = [
    path(
        "diatom/delete/<int:pk>/",
        views.DiatomDeleteView.as_view(),
        name="diatom_delete",
    ),
    path(
        "diatom/modify/<int:pk>/", views.ModifyFormView.as_view(), name="diatom_modify"
    ),
    path("diatom/upload/", views.UploadFormView.as_view(), name="diatom_upload"),
]

consult_patterns = [
    path("diatom/<int:pk>/", views.DiatomDetailView.as_view(), name="detail_diatom"),
    path("taxons/", views.TaxonListView.as_view(), name="taxons"),
    path("taxons/<int:pk>/", views.TaxonDetailView.as_view(), name="detail_taxon"),
]

api_patterns = [
    path("generate_image/<int:diatom_id>", views.generate_image, name="generate_image"),
    path(
        "generate_thumbnail/<int:diatom_id>",
        views.generate_thumbnail,
        name="generate_thumbnail",
    ),
    path("search/diatom", views.search_diatom, name="diatom_search"),
    path("search/inventory", views.search_inventory, name="inventory_search"),
    path("search/station", views.search_station, name="station_search"),
    path("search/taxon", views.search_taxon, name="taxon_search"),
    path(
        "stations_geojson",
        views.StationsLayerView.as_view(),
        name="stations_data",
    ),
    path(
        "contribs_geojson",
        views.ContributionsLayerView.as_view(),
        name="contribs_data",
    ),
    path("taxons_json/", views.TaxonJson.as_view(), name="taxons_json"),
]

app_name = "atlas"
urlpatterns = [
    path("", include((contrib_patterns, app_name), namespace="contrib")),
    path("", include((consult_patterns, app_name), namespace="consult")),
    path("", include((api_patterns, app_name), namespace="api")),
]

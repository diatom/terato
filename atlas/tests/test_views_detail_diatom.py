# class DiatomDetailViewTests(TestCase):
#     def setUp(self):
#         self.user = create_user()
#         self.terato_status = Health.objects.all()[0]
#         self.diatom = create_diatom(
#             create_taxon(), create_photo_object(), self.user, self.terato_status
#         )

#     def test_GET_url(self):
#         """The view is accessible at the correct URL"""
#         response = self.client.get(f"/diatom/{self.diatom.id}/")
#         self.assertEqual(response.status_code, 200)

#     def test_GET_name(self):
#         """The view is accessible using its name"""
#         response = self.client.get(
#             reverse("atlas:diatom_detail", args=[self.diatom.id])
#         )
#         self.assertEqual(response.status_code, 200)

from django.test import TestCase

from atlas.models import Diatom
from tests import RedirectionTestMixin
from tests.utils import DEFAULT_EMAIL, DEFAULT_PWD


class TestLoggedOutRedirects(RedirectionTestMixin, TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):
        self.expected = {("contact",): (None,)}

        for d in Diatom.objects.all():
            self.expected[("flag", d.pk)] = ("atlas:consult:detail_diatom", d.pk)


class TestLoggedInRedirects(RedirectionTestMixin, TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):

        self.expected = {("contact",): (None,)}

        for d in Diatom.objects.all():
            self.expected[("flag", d.pk)] = ("atlas:consult:detail_diatom", d.pk)

        self.client.login(username=DEFAULT_EMAIL, password=DEFAULT_PWD)

    def tearDown(self):
        self.client.logout()

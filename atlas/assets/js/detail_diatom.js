import "../css/base_detail.css";
import "../css/detail_diatom.css";
import { prepareAutocomplete } from "./base_detail";

$(function () {
  prepareAutocomplete();

  const scaleWidth = JSON.parse(
    document.getElementById("scale-width").textContent
  );
  const baseWidth = JSON.parse(
    document.getElementById("img-width").textContent
  );
  const baseHeight = JSON.parse(
    document.getElementById("img-height").textContent
  );

  function resizeScale() {
    var image = document.querySelector(".rounded.detail-image");
    var ratio = Math.min(
      image.offsetHeight / baseHeight,
      image.offsetWidth / baseWidth
    );
    document.getElementById("scale-container").style.width =
      scaleWidth * ratio + "px";
  }
  setTimeout(function () {
    if (document.getElementById("scale")) {
      resizeScale();
      window.addEventListener("resize", resizeScale, true);
    }
  }, 50);
});

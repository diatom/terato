from django.test import RequestFactory
from django.urls import reverse

from djatoms.admin import admin_site
from djatoms.admin.views.main import DictQuerySet
from tests import TestCase

from ..admin import RequestAdmin
from ..models import Request


class RequestAdminTestCase(TestCase):
    def setUp(self):
        for i in range(5):
            self.client.get(reverse("generic:home"))

        self.admin = RequestAdmin(Request, admin_site)
        self.request = RequestFactory()

    def test_admin_queryset(self):

        qs = self.admin.get_queryset(self.request)
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs.last()["num_visited"], 5)

    def test_admin_num_visited(self):
        obj = self.admin.get_queryset(self.request).last()
        self.assertEqual(self.admin.num_visited(DictQuerySet(Request, obj)), 5)

from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BTreeIndex


class DefoFactor(models.Model):
    id = models.AutoField(primary_key=True, db_column="defo_factor_id")
    name = models.CharField(
        db_column="defo_factor_name", db_comment="Name of deformation factor (fallback)"
    )
    category = models.ForeignKey(
        "DefoFactorCategory", models.DO_NOTHING, db_column="defo_factor_cat_id"
    )

    class Meta:
        managed = False
        db_table = 'terato"."defo_factor'
        db_table_comment = "Suspected deformation factors for observed teratologies"

    def __str__(self):
        return self.name


class DefoFactorCategory(models.Model):
    id = models.AutoField(primary_key=True, db_column="defo_factor_cat_id")
    category_name = models.CharField(
        db_column="defo_factor_cat_name",
        db_comment="Name of deformation category (fallback)",
    )

    class Meta:
        managed = False
        db_table = 'terato"."defo_factor_cat'
        db_table_comment = (
            "Categories for suspected deformations of observed teratologies"
        )

    def __str__(self):
        return self.category_name


# Many to many relationship between Diatom and Inventory
class DiatomDefoFactor(models.Model):
    id = models.AutoField(primary_key=True, db_column="diatom_defo_factor_id")
    deformation_factor = models.ForeignKey(
        "DefoFactor", models.DO_NOTHING, db_column="defo_factor_id"
    )
    diatom = models.ForeignKey("Diatom", models.CASCADE, db_column="diatom_id")

    class Meta:
        managed = False
        db_table = 'terato"."diatom_defo_factor'

        indexes = [
            BTreeIndex(fields=["diatom"], name="diatom_defo_factor_diatom_id_idx")
        ]

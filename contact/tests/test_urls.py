import random

from django.test import SimpleTestCase
from django.urls import resolve, reverse

from ..views import ContactView, FlagView


class TestUrls(SimpleTestCase):
    def test_home_url_resolves(self):
        url = reverse("contact:contact")
        self.assertEqual(resolve(url).func.view_class, ContactView)

    def test_flag_url_resolves(self):
        url = reverse("contact:flag", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func.view_class, FlagView)

from django.contrib.gis.db import models


class DbVersion(models.Model):
    version_id = models.AutoField(primary_key=True)
    version_name = models.CharField(
        unique=True,
        blank=True,
        null=True,
        db_comment="Version name, format : 2024.20.4",
    )
    version_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'terato"."db_version'

from functools import update_wrapper
from unittest.mock import MagicMock, Mock, PropertyMock, patch

from django.contrib.admin.options import TO_FIELD_VAR
from django.contrib.admin.utils import model_ngettext
from django.contrib.auth import get_user_model
from django.forms import modelform_factory
from django.http import Http404
from django.test import RequestFactory, override_settings
from django.urls import path, reverse
from django.utils.translation import gettext as _
from django.utils.translation import ngettext

from tests import TestCase

from ..admin.forms import UploadModelFormMixin
from .admin import (
    TestModelAdmin,
    TestModelUploadForm,
    TestNotUploadableMA,
    TestUploadableMA,
    site,
    site_not_uploadable,
    site_uploadable,
)
from .models import TestModel

User = get_user_model()


def assert_not_called_with(mock, *args, **kwargs):
    try:
        mock.assert_any_call(*args, **kwargs)
    except AssertionError:
        return
    raise AssertionError(
        "Expected %s to not have been called."
        % mock._format_mock_call_signature(args, kwargs)
    )


@override_settings(ROOT_URLCONF="djatoms.tests.urls")
class NotUploadableExtendedModelAdminTestCase(TestCase):

    """
    If ExtendedModelAdmin is not uploadable, it should behave
    in most cases like the Django-provided ModelAdmin
    """

    fixtures = ["test_model", "users"]

    def setUp(self):

        self.base_ma = TestModelAdmin(TestModel, site)
        self.extended_ma = TestNotUploadableMA(TestModel, site_not_uploadable)

        self.request = RequestFactory()
        self.request.user = User.objects.get(email="any@mail.com")
        self.request.COOKIES = {}
        self.request.method = "GET"

        d = {TO_FIELD_VAR: None}
        self.request.GET = MagicMock()
        self.request.GET.get.side_effect = d.get
        self.request.POST = Mock()
        self.request.META = MagicMock()
        self.request.resolver_match = Mock()

        self.patcher = patch(
            "djatoms.admin.options.ExtendedModelAdmin.uploading",
            new_callable=PropertyMock,
        )

        self.addCleanup(self.patcher.stop)
        self.mock_uploading = self.patcher.start()
        self.mock_uploading.return_value = False

    def tearDown(self):
        # checks that self.uploading is never set to True
        assert_not_called_with(self.mock_uploading, True)

        self.patcher.stop()

    def test_init_upload_list_form(self):
        self.assertFalse(
            issubclass(self.extended_ma.upload_list_form, UploadModelFormMixin)
        )
        self.assertTrue(
            issubclass(self.extended_ma._upload_list_form, UploadModelFormMixin)
        )

    def test_init_creates_attributes(self):
        for attr in vars(self.base_ma):
            self.assertTrue(hasattr(self.extended_ma, attr))

        for attr in [
            "uploadable",
            "upload_list_display",
            "upload_list_editable",
            "upload_list_template",
            "upload_form",
            "upload_list_form",
            "uploading",
            "_upload_queryset",
            "_upload_list_form",
            "_upload_list_editable",
            "_upload_list_display",
            "_upload_list_display_links",
            "upload_view",
            "uploadlist_view",
            "_upload_get_changelist",
            "_upload_get_queryset",
            "get_upload_queryset",
            "_upload_get_changelist_form",
            "_upload_save_form",
            "_upload_message_user",
        ]:
            self.assertTrue(hasattr(self.extended_ma, attr))
            self.assertFalse(hasattr(self.base_ma, attr))

    def test_get_urls(self):
        self.assertGreater(len(self.base_ma.get_urls()), 0)
        self.assertTrue(self.base_ma.get_urls(), self.extended_ma.get_urls())

    def test_upload_view(self):
        with self.assertRaises(Http404):
            self.extended_ma.upload_view(self.request)

    @patch("django.contrib.admin.ModelAdmin.get_changelist")
    def test_upload_get_changelist(self, mock_command):
        self.extended_ma.get_changelist()
        self.assertTrue(mock_command.call_count, 1)

    @patch("django.contrib.admin.ModelAdmin.get_queryset")
    def test_get_queryset(self, mock_command):
        self.extended_ma.get_queryset(self.request)
        self.assertTrue(mock_command.call_count, 1)

    def test_get_upload_queryset(self):
        # make sure error is raised when get_upload_queryset is not defined
        with self.assertRaises(NotImplementedError):
            self.extended_ma.get_upload_queryset(self.request)

    @patch("django.contrib.admin.ModelAdmin.get_changelist_form")
    def test_get_change_list_form(self, mock_command):
        self.extended_ma.get_changelist_form(self.request)
        self.assertTrue(mock_command.call_count, 1)

    def test_uploadlist_view(self):
        with self.assertRaises(Http404):
            self.extended_ma.uploadlist_view(self.request)

    def test_changelist_view(self):
        extended_clv = self.extended_ma.changelist_view(self.request)
        base_clv = self.base_ma.changelist_view(self.request)
        base_clv.template_name.insert(2, "djatoms/admin/change_list.html")

        self.assertEqual(base_clv.template_name, extended_clv.template_name)

    @patch("django.contrib.admin.ModelAdmin.save_form")
    def test_save_form(self, mock_command):
        form = modelform_factory(TestModel, fields=["name"])
        self.extended_ma.save_form(self.request, form, False)
        self.assertEqual(mock_command.call_count, 1)

    @patch("django.contrib.admin.ModelAdmin.message_user")
    def test_message_user(self, mock_command):
        self.extended_ma.message_user(self.request, "test message")
        self.assertEqual(mock_command.call_count, 1)


@override_settings(ROOT_URLCONF="djatoms.tests.urls")
class UploadableExtendedModelAdminTestCase(TestCase):

    """
    If ExtendedModelAdmin is uploadable, it should behave
    like the Django-provided ModelAdmin when uploading is False
    """

    fixtures = ["test_model", "users"]

    def setUp(self):

        self.base_ma = TestModelAdmin(TestModel, site)
        self.extended_ma = TestUploadableMA(TestModel, site_uploadable)

        self.request = RequestFactory()
        self.request.user = User.objects.get(email="any@mail.com")
        self.request.COOKIES = {}
        self.request.method = "GET"

        d = {TO_FIELD_VAR: None}
        self.request.GET = MagicMock()
        self.request.GET.get.side_effect = d.get
        self.request.POST = Mock()
        self.request.META = MagicMock()
        self.request.resolver_match = Mock()

    def test_init_upload_list_form(self):
        self.assertFalse(
            issubclass(self.extended_ma.upload_list_form, UploadModelFormMixin)
        )
        self.assertTrue(
            issubclass(self.extended_ma._upload_list_form, UploadModelFormMixin)
        )

    def test_init_creates_attributes(self):
        for attr in vars(self.base_ma):
            self.assertTrue(hasattr(self.extended_ma, attr))

        for attr in [
            "uploadable",
            "upload_list_display",
            "upload_list_editable",
            "upload_list_template",
            "upload_form",
            "upload_list_form",
            "uploading",
            "_upload_queryset",
            "_upload_list_form",
            "_upload_list_editable",
            "_upload_list_display",
            "_upload_list_display_links",
            "upload_view",
            "uploadlist_view",
            "_upload_get_changelist",
            "_upload_get_queryset",
            "get_upload_queryset",
            "_upload_get_changelist_form",
            "_upload_save_form",
            "_upload_message_user",
        ]:
            self.assertTrue(hasattr(self.extended_ma, attr))
            self.assertFalse(hasattr(self.base_ma, attr))

    def test_get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.extended_ma.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self.extended_ma
            return update_wrapper(wrapper, view)

        self.assertGreater(len(self.base_ma.get_urls()), 0)
        self.assertTrue(
            [
                path(
                    "upload/",
                    wrap(self.extended_ma.upload_view),
                    name="djatoms_tests_testmodel_uploadlist",
                )
            ]
            + self.base_ma.get_urls(),
            self.extended_ma.get_urls(),
        )

    def test_upload_view_GET_context(self):
        self.request.FILES = {}
        response = self.extended_ma.upload_view(self.request)

        expected_context = {
            **site_uploadable.each_context(self.request),
            "opts": TestModel._meta,
            "form": TestModelUploadForm,
            "actions_on_bottom": True,
            "init": True,
        }

        self.assertEqual(response.context_data.keys(), expected_context.keys())
        self.assertEqual(response.context_data["opts"], expected_context["opts"])
        self.assertEqual(
            type(response.context_data["form"]()), type(expected_context["form"]())
        )
        self.assertEqual(
            response.context_data["actions_on_bottom"],
            expected_context["actions_on_bottom"],
        )
        self.assertEqual(response.context_data["init"], expected_context["init"])

    def test_upload_view_GET_templates(self):
        self.request.FILES = {}
        response = self.extended_ma.upload_view(self.request)

        templates = [
            "admin/djatoms_tests/testmodel/upload_list.html",
            "admin/djatoms_tests/upload_list.html",
            "djatoms/admin/upload_list.html",
        ]
        self.assertEqual(response.template_name, templates)

    @patch("djatoms.admin.ExtendedModelAdmin.uploadlist_view")
    def test_upload_view_POST(self, mock_command):
        self.request.method = "POST"
        with open("djatoms/tests/upload_file_correct.csv", "rb") as file:
            self.request.FILES = {"file": file}
        self.extended_ma.upload_view(self.request)
        self.assertTrue(mock_command.call_count, 1)

    @patch("django.contrib.admin.ModelAdmin.get_changelist")
    def test_upload_get_changelist(self, mock_command):
        self.extended_ma.get_changelist()
        self.assertTrue(mock_command.call_count, 1)

    @patch("django.contrib.admin.ModelAdmin.get_queryset")
    def test_get_queryset(self, mock_command):
        self.extended_ma.get_queryset(self.request)
        self.assertTrue(mock_command.call_count, 1)

    def test_get_upload_queryset(self):
        with open("djatoms/tests/upload_file_correct.csv", "rb") as file:
            self.request.FILES = {"file": file}
            qs = self.extended_ma.get_upload_queryset(self.request)
        self.assertEqual(len(qs), 3)

    @patch("django.contrib.admin.ModelAdmin.get_changelist_form")
    def test_get_change_list_form(self, mock_command):
        self.extended_ma.get_changelist_form(self.request)
        self.assertTrue(mock_command.call_count, 1)

    def test_changelist_view(self):
        extended_clv = self.extended_ma.changelist_view(self.request)
        base_clv = self.base_ma.changelist_view(self.request)
        base_clv.template_name.insert(2, "djatoms/admin/change_list.html")

        self.assertEqual(base_clv.template_name, extended_clv.template_name)

    @patch("django.contrib.admin.ModelAdmin.save_form")
    def test_save_form(self, mock_command):
        form = modelform_factory(TestModel, fields=["name"])
        self.extended_ma.save_form(self.request, form, False)
        self.assertEqual(mock_command.call_count, 1)

    @patch("django.contrib.admin.ModelAdmin.message_user")
    def test_message_user(self, mock_command):
        self.extended_ma.message_user(self.request, "test message")
        self.assertEqual(mock_command.call_count, 1)


@override_settings(ROOT_URLCONF="djatoms.tests.urls")
class UploadingExtendedModelAdminTestCase(TestCase):
    """
    Testing ExtendedModelAdmin uploading process
    """

    fixtures = ["test_model", "users"]

    def setUp(self):
        # self.extended_ma = TestUploadableMA(TestModel, site_uploadable)
        self.client.login(username="any@mail.com", password="50meP4ssw0rd")
        self.url = reverse("admin_uploadable:djatoms_tests_testmodel_uploadlist")
        self.clean_upload_qs = False

    def correct_file_upload(self):
        with open("djatoms/tests/upload_file_correct.csv", "r") as file:

            return self.client.post(self.url, data={"file": file})

    def test_GET_upload_view(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "djatoms/admin/upload_list.html")
        self.assertNotContains(response, '<table id="result_list">')
        self.assertIn("init", response.context)

    def test_POST_with_empty_file(self):
        with open("djatoms/tests/upload_file_empty.csv", "r") as file:
            response = self.client.post(self.url, data={"file": file}, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "djatoms/admin/upload_list.html")
        self.assertNotContains(response, '<table id="result_list">')
        self.assertIn("init", response.context)

    def test_POST_valid_file_creates_queryset(self):
        response = self.correct_file_upload()
        self.clean_upload_qs = True

        extended_ma = response.context["cl"].model_admin
        self.assertEqual(len(extended_ma._upload_queryset), 3)
        self.assertEqual(extended_ma._upload_queryset[0].name, "Un")
        self.assertEqual(extended_ma._upload_queryset[1].name, "Deux")
        self.assertEqual(extended_ma._upload_queryset[2].name, "Trois")

    def test_POST_valid_file_sets_uploading_state(self):
        response = self.correct_file_upload()
        self.clean_upload_qs = True

        extended_ma = response.context["cl"].model_admin
        self.assertTrue(extended_ma.uploading)

    def test_POST_valid_shows_change_table(self):
        response = self.correct_file_upload()
        self.clean_upload_qs = True

        self.assertTemplateUsed(response, "djatoms/admin/upload_list.html")
        self.assertContains(response, '<table id="result_list">')

    def test_quitting_and_coming_back_does_not_reset_upload_queryset(self):
        self.correct_file_upload()
        self.clean_upload_qs = True
        # get something else
        self.client.get(reverse("admin_uploadable:djatoms_tests_testmodel_changelist"))

        # come back
        response = self.client.get(
            reverse("admin_uploadable:djatoms_tests_testmodel_uploadlist")
        )
        self.extended_ma = response.context["cl"].model_admin
        self.assertEqual(len(self.extended_ma._upload_queryset), 3)
        self.assertEqual(self.extended_ma._upload_queryset[0].name, "Un")
        self.assertEqual(self.extended_ma._upload_queryset[1].name, "Deux")
        self.assertEqual(self.extended_ma._upload_queryset[2].name, "Trois")

    def test_POST_save_without_modifs(self):
        response = self.correct_file_upload()
        data = {
            "form-TOTAL_FORMS": "3",
            "form-INITIAL_FORMS": "3",
            "form-MIN_NUM_FORMS": "0",
            "form-MAX_NUM_FORMS": "1000",
            "action": [""],
            "form-0-id": ["1"],
            "form-0-name": ["Un"],
            "form-1-id": ["2"],
            "form-1-name": ["Deux"],
            "form-2-id": ["3"],
            "form-2-name": ["Trois"],
            "_save": ["Soumettre"],
        }
        response = self.client.post(self.url, data=data, follow=True)

        self.assertEqual(TestModel.objects.count(), 8)
        self.assertEqual(TestModel.objects.last().name, "Trois")

        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]),
            ngettext(
                "%(count)s %(name)s was added successfully.",
                "%(count)s %(name)s were added successfully.",
                3,
            )
            % {
                "count": 3,
                "name": "test models",
            },
        )

    def test_POST_save_with_modifs(self):
        self.correct_file_upload()
        data = {
            "form-TOTAL_FORMS": "3",
            "form-INITIAL_FORMS": "3",
            "form-MIN_NUM_FORMS": "0",
            "form-MAX_NUM_FORMS": "1000",
            "action": [""],
            "form-0-id": ["1"],
            "form-0-name": ["Un"],
            "form-1-id": ["2"],
            "form-1-name": ["Deux"],
            "form-2-id": ["3"],
            "form-2-name": ["Quatre"],
            "_save": ["Soumettre"],
        }
        response = self.client.post(self.url, data=data, follow=True)
        self.assertEqual(TestModel.objects.count(), 8)
        self.assertEqual(TestModel.objects.last().name, "Quatre")

        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]),
            ngettext(
                "%(count)s %(name)s was added successfully.",
                "%(count)s %(name)s were added successfully.",
                3,
            )
            % {
                "count": 3,
                "name": "test models",
            },
        )

    def test_POST_save_tmp_with_modifs(self):
        self.correct_file_upload()
        self.clean_upload_qs = True
        data = {
            "form-TOTAL_FORMS": "3",
            "form-INITIAL_FORMS": "3",
            "form-MIN_NUM_FORMS": "0",
            "form-MAX_NUM_FORMS": "1000",
            "action": [""],
            "form-0-id": ["1"],
            "form-0-name": ["Un"],
            "form-1-id": ["2"],
            "form-1-name": ["Deux"],
            "form-2-id": ["3"],
            "form-2-name": ["Quatre"],
            "_save_tmp": ["Enregistrer"],
        }
        response = self.client.post(self.url, data=data, follow=True)

        self.assertEqual(TestModel.objects.count(), 5)
        self.assertEqual(TestModel.objects.last().name, "e")

        self.extended_ma = response.context["cl"].model_admin
        self.assertEqual(len(self.extended_ma._upload_queryset), 3)
        self.assertEqual(self.extended_ma._upload_queryset[0].name, "Un")
        self.assertEqual(self.extended_ma._upload_queryset[1].name, "Deux")
        self.assertEqual(self.extended_ma._upload_queryset[2].name, "Quatre")

        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]),
            ngettext(
                "%(count)s %(name)s was modified successfully.",
                "%(count)s %(name)s were modified successfully.",
                3,
            )
            % {
                "count": 3,
                "name": "test models",
            },
        )

    def test_POST_delete_item(self):
        self.correct_file_upload()
        self.clean_upload_qs = True

        data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "action": ["delete_selected"],
            "select_across": 0,
            "index": 0,
            "_selected_action": [2],
            "form-0-id": ["1"],
            "form-0-name": ["Un"],
            "form-1-id": ["2"],
            "form-1-name": ["Deux"],
            "form-2-id": ["3"],
            "form-2-name": ["Trois"],
        }
        response = self.client.post(self.url, data=data, follow=True)

        self.assertEqual(TestModel.objects.count(), 5)
        self.assertEqual(TestModel.objects.last().name, "e")

        self.extended_ma = response.context["cl"].model_admin
        self.assertEqual(len(self.extended_ma._upload_queryset), 2)
        self.assertEqual(self.extended_ma._upload_queryset[0].name, "Un")
        self.assertEqual(self.extended_ma._upload_queryset[1].name, "Trois")

        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]),
            _("Successfully deleted %(count)d %(items)s.")
            % {
                "count": 1,
                "items": model_ngettext(self.extended_ma.opts, 1),
            },
        )

    def tearDown(self):
        # if test ends before upload is finished, reset it
        if self.clean_upload_qs:
            self.client.post(
                self.url,
                data={
                    "form-TOTAL_FORMS": 3,
                    "form-INITIAL_FORMS": 3,
                    "form-MIN_NUM_FORMS": 0,
                    "form-MAX_NUM_FORMS": 1000,
                    "action": ["delete_selected"],
                    "select_across": 0,
                    "index": 0,
                    "_selected_action": [1, 2, 3],
                    "form-0-id": ["1"],
                    "form-0-name": ["Un"],
                    "form-1-id": ["2"],
                    "form-1-name": ["Deux"],
                    "form-2-id": ["3"],
                    "form-2-name": ["Trois"],
                },
            )

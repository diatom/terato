import inspect
import os

from django.urls.resolvers import URLPattern


class UrlsMixin:

    """
    Mixin to be used with TestCase. At initialization, adds a .url_names
    to the test class. Can be used to test that all app URLs have been
    tested by the class
    """

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        app_path = self._get_file()
        self.app = str(app_path).split("/")[-3]

        try:
            mod = __import__(self.app + ".urls", fromlist=["urlpatterns"])
        except ModuleNotFoundError:
            # will happen if an app has no urls
            raise ModuleNotFoundError(
                "This app has no URL config. No redirection test can be performed."
            )

        self.url_names = [
            u.name for u in getattr(mod, "urlpatterns") if isinstance(u, URLPattern)
        ]

    def _get_file(self):
        """
        Mandatory so as for inheritor classes to be correctly located
        in their files, and not in this one
        """
        return os.path.abspath(inspect.getfile(self.__class__))

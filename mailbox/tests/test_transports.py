from mailbox.tests.base import EmailMessageTestCase, get_email_as_text
from unittest import mock

from ..transports import ImapTransport

FAKE_UID_SEARCH_ANSWER = (
    "OK",
    [
        b"18 19 20 21 22 23 24 25 26 27 28 29 "
        + b"30 31 32 33 34 35 36 37 38 39 40 41 42 43 44"
    ],
)
FAKE_UID_FETCH_SIZES = (
    "OK",
    [b"1 (UID 18 RFC822.SIZE 58070000000)", b"2 (UID 19 RFC822.SIZE 2593)"],
)
FAKE_UID_FETCH_MSG = (
    "OK",
    [
        (b"1 (UID 18 RFC822 {5807}", get_email_as_text("generic_message.eml")),
    ],
)
FAKE_UID_COPY_MSG = ("OK", [b"[COPYUID 1 2 2] (Success)"])
FAKE_LIST_ARCHIVE_FOLDERS_ANSWERS = (
    "OK",
    [b'(\\HasNoChildren \\All) "/" "[Gmail]/All Mail"'],
)


class TestImapTransport(EmailMessageTestCase):
    def setUp(self):
        def imap_server_uid_method(*args):
            cmd = args[0]
            arg2 = args[2]
            if cmd == "search":
                return FAKE_UID_SEARCH_ANSWER
            if cmd == "copy":
                return FAKE_UID_COPY_MSG
            if cmd == "fetch":
                if arg2 == "(RFC822.SIZE)":
                    return FAKE_UID_FETCH_SIZES
                if arg2 == "(RFC822)":
                    return FAKE_UID_FETCH_MSG

        def imap_server_list_method(pattern=None):
            return FAKE_LIST_ARCHIVE_FOLDERS_ANSWERS

        self.imap_server = mock.Mock()
        self.imap_server.uid = imap_server_uid_method
        self.imap_server.list = imap_server_list_method
        super().setUp()
        self.arbitrary_hostname = "one.two.three"
        self.arbitrary_port = 100
        self.ssl = False
        self.transport = ImapTransport(self.arbitrary_hostname, self.arbitrary_port)
        self.transport.server = self.imap_server

    def test_get_email_message(self):
        actual_messages = list(self.transport.get_message())
        self.assertEqual(len(actual_messages), 27)
        actual_message = actual_messages[0]
        expected_message = self._get_email_object("generic_message.eml")
        self.assertEqual(expected_message, actual_message)

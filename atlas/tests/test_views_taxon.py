from django.contrib.messages import get_messages
from django.contrib.messages.storage.fallback import FallbackStorage
from django.db.models import F
from django.test import RequestFactory
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from modeltranslation.utils import get_language

from tests import TestCase

from ..models import Diatom, DiatomChildren, Health, Taxon, TaxonomicRank
from ..views import TaxonDetailView, TaxonJson


class TaxonListViewTests(TestCase):
    def test_response_template(self):
        response = self.client.get(reverse("atlas:consult:taxons"))
        self.assertTemplateUsed(response, "taxon_list.html")


class TaxonJsonTests(TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):
        self.factory = RequestFactory()
        self.view = TaxonJson()

    def test_response(self):
        response = self.client.get(reverse("atlas:api:taxons_json"))
        self.assertEqual(response.status_code, 200)

    def test_get_initial_queryset(self):
        request = self.factory.get(reverse("atlas:api:taxons_json"))
        self.view.setup(request)

        qs = self.view.get_initial_queryset()
        self.assertEqual(len(qs), Taxon.objects.count())

        for t in Taxon.objects.all():
            f = qs.get(pk=t.id)
            self.assertEqual(f, t)
            self.assertEqual(f.diatom_count, t.diatom_set.count())
            self.assertEqual(
                f.codes_tostring,
                ", ".join(str(code) for code in t.taxoncode_set.order_by("-code"))
                or None,
            )

    def test_get_context_data(self):
        request = self.factory.get(reverse("atlas:api:taxons_json") + "?draw=1")
        self.view.setup(request)

        context = self.view.get_context_data()

        self.assertIn("ranks", context)
        self.assertEqual(
            context["ranks"],
            [rank.rank for rank in TaxonomicRank.objects.filter(id__gte=4)],
        )

    def test_filter_queryset_switch(self):
        request = self.factory.get(reverse("atlas:api:taxons_json") + "?switch=true")
        self.view.setup(request)

        qs = self.view.get_initial_queryset()
        qs = self.view.filter_queryset(qs)

        self.assertEqual(
            set(qs),
            set(
                Taxon.objects.filter(
                    id__in=(Diatom.objects.values_list("taxon", flat=True))
                )
            ),
        )

    def test_filter_queryset_not_switch(self):
        request = self.factory.get(reverse("atlas:api:taxons_json"))
        self.view.setup(request)

        qs = self.view.get_initial_queryset()
        qs = self.view.filter_queryset(qs)

        self.assertEqual(
            set(qs),
            set(Taxon.objects.all()),
        )

    def test_get_columns_translated(self):
        request = self.factory.get(reverse("atlas:api:taxons_json"))
        self.view.setup(request)

        columns = self.view.get_columns()

        for col in self.view.translated_columns:
            self.assertIn(col + "_" + get_language(), columns)


class TaxonDetailViewTests(TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):
        self.request = RequestFactory()
        self.request.session = "session"
        self.request._messages = FallbackStorage(self.request)
        self.request.COOKIES = {}

    def test_response_template(self):
        response = self.client.get(reverse("atlas:consult:taxons"))
        self.assertTemplateUsed(response, "taxon_list.html")

    def test_get_object_terato(self):

        for t in Taxon.objects.filter(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.get_object()

            self.assertEqual(self.view._object, t.taxon_parent)

    def test_get_object_aliased(self):

        for t in Taxon.objects.exclude(taxon_alias__id=F("id")).exclude(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.get_object()

            self.assertEqual(self.view._object, t.taxon_alias)

    def test_get_object_valid(self):

        for t in Taxon.objects.filter(taxon_alias__id=F("id")).exclude(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.get_object()

            self.assertEqual(self.view._object, t.taxon_alias)

    def test_render_to_response_terato(self):
        for t in Taxon.objects.filter(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.object = self.view.get_object()

            context = self.view.get_context_data(object=self.view.object)
            response = self.view.render_to_response(context)

            self.assertEqual(response.status_code, 302)
            self.assertEqual(
                response.url,
                reverse("atlas:consult:detail_taxon", args=[t.taxon_parent.pk]),
            )

    def test_render_to_response_aliased(self):
        for t in Taxon.objects.exclude(taxon_alias__id=F("id")).exclude(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.object = self.view.get_object()

            context = self.view.get_context_data(object=self.view.object)
            response = self.view.render_to_response(context)

            self.assertEqual(response.status_code, 302)
            self.assertEqual(
                response.url,
                reverse("atlas:consult:detail_taxon", args=[t.taxon_alias.pk]),
            )

    def test_render_to_response_aliased_sends_message(self):
        for t in Taxon.objects.exclude(taxon_alias__id=F("id")).exclude(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.object = self.view.get_object()

            context = self.view.get_context_data(object=self.view.object)
            self.view.render_to_response(context)

            messages = [m for m in get_messages(self.view.request)]
            self.assertGreaterEqual(len(messages), 1)
            self.assertEqual(messages[0].level, 20)
            self.assertEqual(
                messages[0].message, _("TAXON MSG Redirection vers valide")
            )

    def test_render_to_response_valid(self):

        for t in Taxon.objects.filter(taxon_alias__id=F("id")).exclude(is_terato=True):
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.object = self.view.get_object()

            context = self.view.get_context_data(object=self.view.object)
            response = self.view.render_to_response(context)

            self.assertEqual(response.status_code, 200)

    def test_get_context_data(self):

        for t in Taxon.objects.all():
            self.view = TaxonDetailView()
            self.view.setup(self.request, pk=t.pk)
            self.view.object = self.view.get_object()

            context = self.view.get_context_data(object=self.view.object)

            health = {}
            for h in Health.objects.all():
                health[h] = (
                    DiatomChildren.objects.filter(ancestor=self.view.object.id)
                    .filter(health=h.id)
                    .order_by("-upload_date")[:10]
                    .values_list("diatom", "taxon")
                )

                self.assertEqual(set(context["health"][h]), set(health[h]))

            self.assertEqual(
                context["count"],
                len(
                    DiatomChildren.objects.filter(
                        ancestor=self.view.object.id
                    ).values_list("ancestor")
                ),
            )

            self.assertEqual(
                context["count_located"],
                len(
                    DiatomChildren.objects.filter(ancestor=self.view.object.id)
                    .exclude(diatom__station__isnull=True)
                    .values_list("ancestor")
                ),
            )

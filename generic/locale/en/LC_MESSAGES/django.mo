��    I      d  a   �      0  �   1    �  �  �  �   �  $  �  E  �  7  	  �  A  �  �  �   �  �  A  �  �  z   �  !  :    \  �   n  h  b  x  �  �  D   =  !#  4  _$  �   �,  �   -  �   �-  t   7.     �.     �.     �.     �.     /      /     </     Q/  	   j/  .   t/  $   �/  &   �/     �/     �/     0  @   (0  /   i0  .   �0  "   �0     �0     1     "1      ;1  !   \1     ~1  #   �1  $   �1  #   �1  #   2     /2     M2     \2     q2     �2     �2  !   �2  "   �2     �2     3     43  &   P3     w3  ,   �3     �3     �3     �3  &   �3  B  !4  �   d5  �  06  }  !8  �   �:    p;  s  t<  �   �>  �  �?  i  ]A  �   �B  �  �C  �  E  z   �F    NG  �  `H  �   7J  g  K  I  �L  �  �M  �   mP  �  fQ  g   �X  l   bY  x   �Y  Y   HZ  	   �Z     �Z  	   �Z  	   �Z  	   �Z     �Z  	   �Z     �Z     �Z     [     2[     H[  
   [[     f[     y[  =   �[  B   �[  B   	\  *   L\  (   w\  �   �\  {   U]  q   �]  F   C^  d   �^  �  �^  �   �`  �  �a  �   }d  �  he  1   i     Hi     _i  	   gi  	   qi  %   {i     �i  
   �i  -   �i  8   �i     %j     3j     @j     Zj     aj     pj     xj               8   I   .   -                                         E   G                   2                 7      )         5       @              =       !   ;   3   ?       1   %       A       	   &   /   0          <   B   (   >      ,      '   4           $              +      #   6             F   
   9   C   "   *   D                    :   H        
                    <li>SIREN : 180070039</li>
                    <li>Code APE : 7219Z</li>
                    <li>Numéro de TVA intracommunautaire : FR 57 1800700039</li>
                     
                    Autres crédits :
                    <ul>
                        <li>
                            L'apparence de ce site utilise une version customisée de <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>.
                        </li>
                        <li>
                            Ce site utilise les fonctionnalités de <a href="https://www.djangoproject.com/" target="_blank">Django</a>.
                        </li>
                    </ul>
                     
                    Ce site a pour vocation de permettre à la communauté scientifique étudiant les diatomées de partager des photos de diatomées, déformées ou non, ainsi que les
                    conditions environnementales associées à ces observations. Les objectifs sont :
                    <ul>
                        <li>
                            Constituer une iconographie de référence, accessible à tous, pouvant servir d'appui à identifier une diatomée comme déformée ou non ;
                        </li>
                        <li>Utiliser les données collectées pour étudier statistiquement les déformations des diatomées.</li>
                    </ul>
                     
                    Ce site n'emploie pas de cookies, outre ceux strictement nécessaires à son fonctionnement pour la connexion des utilisateurs et la sécurité de leurs contributions.
                     
                    Cette page concerne les conditions d'utilisation du site Internet.
                    Elle précise les droits et devoirs respectifs des utilisateurs du site Internet et de l'éditeur et hébergeur de ce dernier, défini comme l'unité EABX d'INRAE.
                     
                    Crédits des icônes :
                    <ul>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/france" title="france icônes">Drapeau de la France créé par Freepik - Flaticon</a>
                        </li>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/royaume-uni" title="royaume-uni icônes">Drapeau du Royaume-Uni créé par Freepik - Flaticon</a>
                        </li>
                    </ul>
                     
                    Des données personnelles sont recueillies auprès des utilisateurs remplissant le formulaire d'inscription. Les informations relatives au traitement de ces données personnelles sont disponibles dans nos <a href="%(link_cgu)s">conditions générales d'utilisation</a>.
                     
                    Deux catégories d'utilisateurs sont à distinguer, en fonction de leur inscription ou non. Les utilisateurs non inscrits ne peuvent que consulter le site,
                    sans possibilité d'y ajouter du contenu. Leurs droits et devoirs, ainsi que les droits et devoirs de l'éditeur envers eux, se limitent aux principes élémentaires régissant l'Internet et la propriété intellectuelle.
                     
                    En vertu de ce principe de confiance, l'éditeur et INRAE déclinent toute responsabilité en cas d'ajout de contenu inexact, inapproprié ou illégal. Néanmoins, afin de limiter les désagréments pour les autres utilisateurs, l'éditeur s'engage à assurer de façon régulière la modification ou la suppression, le cas échéant, de tels contenus.
                     
                    Hormis le contenu des contributions, l'éditeur se réserve tout droit de propriété intellectuelle sur le contenu actuel et à venir de ce site.
                     
                    INRAE est un établissement public à caractère scientifique et technologique.
                    Il a son siège au 147, rue de l’université – 75338 Paris cedex 07.
                    Ses statuts sont publiés dans le Code rural et de la pêche maritime (articles R831-1 et suivants).
                    Il est représenté par son président-directeur général, M. Philippe MAUGUIN.
                     
                    L'ajout de données sur la plateforme ne modifie en rien la propriété intellectuelle de ces données, qui restent donc la propriété du contributeur, de son institution ou du tiers dont il a obtenu la permission d'effectuer l'ajout. La photographie est ajoutée par le contributeur sous la licence CC-BY-NC, qui en autorise le partage et l'utilisation à des fins non commerciales, sous condition d'attribution.
                     
                    L'hébergement est assuré par l'unité EABX, 50 avenue de Verdun, 33612 Cestas.
                     
                    La plateforme Terato fonctionne sur un principe de confiance. Les utilisateurs s'engagent à n'ajouter que des informations à leurs connaissances exactes. En cas d'erreur, la possibilité de modifier ou de supprimer leurs ajouts leur est ouverte.
                     
                    La possibilité de contribuer à la plateforme est garantie à tous les utilisateurs inscrits dès lors qu'ils sont connectés. L'éditeur s'engage à mettre tout en oeuvre pour assurer le maintien et la sécurité du site.
                    Néanmoins, des interruptions de service pourront avoir lieu en cas de mise à jour, de panne ou de force majeure. Si une interruption prévisible devait s'étendre au-delà de 48 heures, les utilisateurs inscrits en seront prévenus par mail.
                     
                    Le site Plateforme Terato est édité par l'unité EABX (Ecosystèmes aquatiques et changements globaux) d'INRAE, l'Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement.
                     
                    Les données personnelles collectées ne font l'objet d'aucun autre usage, et ne sont transmises à aucun tiers. Elles sont conservées pour la durée totale de leur inscription. A tout moment, les utilisateurs ont la possibilité de voir leurs données rectifiées ou effacées en contactant l'administrateur du site.
                     
                    Les photos ajoutées par les contributeurs de la plateforme, ainsi que les informations attenantes, relèvent de leur responsabilité exclusive.
                    INRAE ne saurait être tenu responsable de tout contenu erroné ou inapproprié, comme le précisent nos <a href="%(link_cgu)s">conditions générales d'utilisation</a>.
                     
                    Les utilisateurs s'engagent à n'ajouter que des photos de diatomées dont ils, ou l'institution qu'ils représentent et à laquelle ils déclarent appartenir, détiennent la propriété intellectuelle, ou dont ils obtenu du détenteur de la propriété intellectuelle la permission expresse d'effectuer cet ajout.
                    L'éditeur et INRAE ne seraient en aucun cas être tenus responsables si un contributeur venait à ajouter des données sans en avoir le droit ou l'autorisation. En cas de litige, néanmoins, l'éditeur s'engage à supprimer de bonne foi les données concernées, à la demande du contributeur ou du véritable détenteur de la propriété intellectuelle.
                     
                    Les utilisateurs sont considérés comme inscrits après remplissage du <a href="%(register_link)s">formulaire d'inscription</a>, au terme duquel une case à cocher obligatoirement vaut pour
                    acceptation des présentes conditions générales d'utilisation.
                     
                    Lors de l'inscription, les utilisateurs renseignent leurs noms, prénoms et adresse mail. Ils peuvent également renseigner l'institution à laquelle ils appartiennent. Ces données personnelles ne sont utilisées que pour les finalités suivantes, dont la base légale est précisée entre parenthèses :
                    <ul>
                        <li>
                            Le nom, prénom et institution d'appartenance sont affichés sur chaque contribution. Cette identification est reconnue nécessaire à garantir la qualité scientifique de la plateforme (intérêt légitime) ;
                        </li>
                        <li>
                            L'adresse mail est utilisée pour la connexion, de même nécessaire à la qualité scientifique de la plateforme (intérêt légitime) ;
                        </li>
                        <li>
                            L'adresse mail pourra aussi être utilisée pour contacter les utilisateurs dans les cas suivants (consentement) :
                            <ul>
                                <li>Pour les prévenir d'une interruption de service de plus de 48 heures ;</li>
                                <li>
                                    Pour leur communiquer, à leur demande, un lien de renouvellement de leur mot de passe, en cas d'oubli de ce dernier ;
                                </li>
                                <li>Pour leur signaler un manquement ou abus dans une de leurs contributions ;</li>
                                <li>
                                    Pour leur proposer de devenir modérateur du site, s'ils remplissent les conditions évoquées dans la section Règlement des contributions.
                                </li>
                                <li>
                                    Pour leur signaler une modification des présentes conditions générales d'utilisation.
                                </li>
                            </ul>
                        </li>
                    </ul>
                     
    La page demandée n'existe pas, ou a été déplacée. Vérifiez l'URL, ou reprenez votre navigation depuis l'accueil.
     
    Une de vos actions a généré une mauvaise requête vers notre serveur. Réessayez, ou reprenez votre navigation depuis l'accueil.
     
    Une erreur est survenue de notre côté. Nous la corrigerons dès que possible. Dans l'attente, vous pouvez revenir à la page d'accueil.
     
    Vous n'avez pas accès à cette ressource. Vérifiez l'URL, ou reprenez votre navigation depuis l'accueil.
     400 TITLE Erreur 400 400 TITLE Requête invalide 403 TITLE Accès refusé 403 TITLE Erreur 403 404 TITLE Erreur 404 404 TITLE Page non trouvée 500 TITLE Erreur 500 500 TITLE Erreur interne CGU TITLE CGU TITLE Inscription et données personnelles CGU TITLE Propriété intellectuelle CGU TITLE Règlement des contributions HEADER Ajouter HEADER Consulter HOME ALT Photo diatomée HOME CHART Cause probable des tératologies dans la littérature HOME CHART Probabilité d'occurrence Fragilaria HOME CHART Probabilité d'occurrence Nitzschia HOME CHART Source causes probables HOME CHART Source probabilités HOME CONTENT Bienvenue HOME CONTENT Financement HOME CONTENT Objectifs Collecter HOME CONTENT Objectifs Constituer HOME CONTENT Objectifs Utiliser HOME CONTENT Pourquoi tératologies HOME CONTENT Que sont les diatomées HOME CONTENT Terato bioindication 1 HOME CONTENT Terato bioindication 2 HOME CONTENT Terato typologie HOME NAME Home HOME TITLE Bienvenue HOME TITLE Financement HOME TITLE Objectifs HOME TITLE Plus d’infos HOME TITLE Pourquoi tératologies HOME TITLE Que sont les diatomées HOME TITLE Références HOME TITLE Terato bioindication HOME TITLE Terato typologie LEGAL CONTENT Numéro de téléphone : LEGAL TITLE LEGAL TITLE Cookies et données personnelles LEGAL TITLE Crédits LEGAL TITLE Editeur du site LEGAL TITLE Hébergement LEGAL TITLE Propriété intellectuelle Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                    <li>SIREN: 180070039</li>
                    <li>APE (business sector) code: 7219Z</li>
                    <li>Intracommunity VAT number: FR 57 1800700039</li>
                     
                    Other credit :
                    <ul>
                        <li>
                            This website's style uses a customized version of <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>.
                        </li>
                        <li>
                            This website is empowered by <a href="https://www.djangoproject.com/" target="_blank">Django</a>.
                        </li>
                    </ul>
                     
                    The purpose of this site is to enable the scientific community studying diatoms to share photos of diatoms, distorted or not, as well as the
                    environmental conditions associated with these observations. Its goals are :
                    <ul>
                        <li>
                            To create a reference iconography, available to all, that can be used to help identify a diatom as deformed or not ;
                        </li>
                        <li>To use the collected data to study diatom deformations statistically.</li>
                    </ul>
                     
                    This website does not use cookies, apart from those strictly necessary for its operation, to enable users to log in and to ensure the security of their contributions.
                     
                    This page describes the conditions of use of this website.
                    It sets out the respective rights and obligations of the users of the website and of its publisher and host, defined as INRAE's EABX unit.
                     
                    Credit for icons :
                    <ul>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/france"
                               title="france icônes">French flag created by Freepik - Flaticon</a>
                        </li>
                        <li>
                            <a href="https://www.flaticon.com/fr/icones-gratuites/royaume-uni"
                               title="royaume-uni icônes">United Kingdom flag created by Freepik - Flaticon</a>
                        </li>
                    </ul>
                     
                    Personal data is collected from users who complete the registration form. Information on the processing of this personal data is available in our <a href="%(link_cgu)s">general terms of use</a>.
                     
                    There are two categories of users, depending on whether or not they are registered. Non-registered users can only consult the site,
                    without being able to add content. Their rights and duties, and the publisher's rights and duties towards them, are limited to the basic principles governing the Internet and intellectual property.
                     
                    By virtue of this principle of trust, the publisher and INRAE decline all responsibility in the event of the addition of inaccurate, inappropriate or illegal content. Nevertheless, in order to limit inconvenience to other users, the publisher undertakes to ensure that any such content is regularly modified or removed.
                     
                    With the exception of the content of contributions, the publisher reserves all intellectual property rights over the current and future content of this site.
                     
                    INRAE is a public scientific and technological establishment.
                    Its head office is at 147, rue de l’université – 75338 Paris cedex 07 - France.
                    Its statutes are published in the Code rural et de la pêche maritime (articles R831-1 et seq.).
                    It is represented by its CEO, M. Philippe MAUGUIN.
                     
                    Contributing data to the platform in no way alters the intellectual property of this data, which therefore remains the property of the contributor, their institution or the third party from whom they have obtained permission to add it. The photograph is added by the contributor under the CC-BY-NC licence, which authorises its sharing and use for non-commercial purposes, subject to attribution.
                     
                    Hosting is provided by the EABX unit, 50 avenue de Verdun, 33612 Cestas, France.
                     
                    The Teratotheca operates on a principle of trust. Users undertake only to add information that is accurate to the best of their knowledge. In the event of an error, they have the option of modifying or deleting their contributions.
                     
                    All registered users are guaranteed the opportunity to contribute to the platform as long as they're logged in. The publisher undertakes to do its utmost to ensure the maintenance and security of the site.
                    Nevertheless, service interruptions may occur in the event of updates, breakdowns or force majeure. Should a predictable interruption last more than 48 hours, registered users will be notified by email.
                     
                    The Teratotheca website is published by the EABX unit (Aquatic Ecosystems and Global Change) of INRAE, the French National Research Institute for Agriculture, Food and the Environment.
                     
The personal data collected is not used for any other purpose and is not shared with any third party. It is kept for the entire duration of the user's registration. Users may have their data corrected or deleted at any time by contacting the site administrator. The link to the contact form is always available at the bottom of the page.
                     
                    The photos added by the platform's contributors, as well as the related information, are their exclusive responsibility. 
                    INRAE cannot be held responsible for any erroneous or inappropriate content, as specified in our <a href="%(link_cgu)s">general terms of use</a>.
                     
                    Users undertake to add only photos of diatoms for which they, or the institution they represent and to which they declare themselves to belong, hold the intellectual property, or for which they have obtained the express permission of the holder of the intellectual property to add such photos.
                    The publisher and INRAE cannot be held responsible if a contributor adds data without having the right or authorisation to do so. In the event of a dispute, however, the publisher undertakes to remove the data concerned in good faith, at the request of the contributor or the true owner of the intellectual property.
                     
                    Users are deemed registered once they have filled in the <a href="%(register_link)s">registration form</a>, at the end of which they must tick a box to accept
                    these general terms of use.
                     
                    At registration time, users enter their last name, first name and email adress. They may also indicate the institution to which they belong. This personal data is only used for the following purposes, the legal basis for which is specified between brackets:
                    <ul>
                        <li>
                            The surname, first name and institution to which they belong are displayed on each contribution. This identification is deemed necessary to guarantee the scientific quality of the platform (legitimate interest);
                        </li>
                        <li>
                            The email adress is used for logging purposes, which is also necessary for the scientific quality of the platform (legitimate interest);
                        </li>
                        <li>
                            The email address may also be used to contact users in the following cases (consent):
                            <ul>
                                <li>To notify them of a service interruption of more than 48 hours;</li>
                                <li>
                                    To send them, at their request, a link to renew their password if they have forgotten it;
                                </li>
                                <li>To inform them of a breach or abuse in one of their contributions;</li>
                                <li>
                                    To offer them the opportunity to become a site moderator, if they meet the conditions set out in the Contribution rules section;
                                </li>
                                <li>
                                    To inform them of any changes to these general terms of use.
                                </li>
                            </ul>
                        </li>
                    </ul>
                     
The requested page does not exist, or has been moved. Check the URL, or start anew from the home page. 
One of your actions has generated a bad request to our server. Try again, or start anew from the home page. 
An error has occurred on our side. It will be dealt with as soon as possible. For now, you can return to the home page. 
You don't have access to this resource. Check the URL, or start anew from the home page. Error 400 Bad request Forbidden Error 403 Error 404 Page not found Error 500 Internal error General terms of use Registration and personal data Intellectual property Contribution rules Contribute View contributions Diatom picture Probable origin of teratologies indicated in published papers Presence probability of normal (FCAP) and abnormal (FCAT) forms of Presence probability of normal (NPAL) and abnormal (NPTR) forms of Source: adapted from Falasco et al. (2021) Source: figures from Coste et al. (2009) This interactive, collaborative interface aims to document diatom teratologies, by collecting pictures of deformed specimens and describing the associated environmental conditions. This project is funded by the Office Français de la Biodiversité (OFB) through the Aquaref 2022-2024 programme (Theme B). <b>Collect</b> images of deformed diatoms, together with their environmental data, using a collaborative approach <b>Create</b> a reference iconography for identifying deformed diatoms <b>Use</b> this dataset to establish correlations between deformations and associated stress factors Various studies indicate that frustule deformations in diatoms are characteristic of toxic stress. In the corpus of publications mentioning deformed diatoms (from <a href='https://doi.org/10.1016/j.ecolind.2017.06.048' target='_blank'>Lavoie et al. 2017</a> and <a href='https://doi.org/10.1007/s10750-021-04540-x' target='_blank'>Falasco et al. 2021</a>), the presence of toxic contaminants (red bars in the graph) is indeed preferentially mentioned as the probable cause of the appearance of teratologies. Diatoms, unicellular microscopic algae that are ubiquitous in aquatic environments, are used as bioindicators of water quality in rivers and lakes. Diatom species can be identified by the shape and ornamentation of their siliceous skeleton, the frustule. Given the observed increase in the frequency of anomalies under conditions of toxic exposure, some diatom indices consider the relative abundance of teratological diatoms as a downgrading criterion for the quality of the environment (e.g. IBD, <a href='https://doi.org/10.1016/j.ecolind.2008.06.003' target='_blank'>Coste et al. 2009</a>, graph below), by attributing a specific profile to deformed specimens. If the percentage of deformations is not taken into account, the index scores may be overestimated (<a href='https://doi.org/10.1007/978-3-030-39212-3_4' target='_blank'>Olenici et al. 2020</a>) and thus overestimate the quality of the environment. Despite their relevance, deformations are generally poorly documented, mainly because of the lack of documentation on existing abnormal shapes and the difficulty for operators to draw the line between normality and subtle deformation. Deformations involve alterations to the contour of individuals (loss of symmetry in particular) or to the ornamentation of the frustule. The tendency to deform, and the ability to display one or the other of the types of frustule anomalies, seem to depend on the species (<a href='https://doi.org/10.1007/s10750-021-04540-x' target='_blank'>Falasco et al. 2021</a>). Thus, <a href='https://doi.org/10.1016/j.ecolind.2017.06.048' target='_blank'>Lavoie et al. (2017)</a> suggested considering the taxonomic dimension in addition to the frequency of teratology in all species when diagnosing toxic pollution. Recent morphometric analysis work also suggests that the severity of deformations could reflect the intensity of the level of toxic pollution (<a href='https://doi.org/10.1007/s10646-017-1830-3' target='_blank'>Olenici et al. 2017</a>, <a href='https://doi.org/10.1080/23818107.2018.1474800' target='_blank'>Cerisier et al. 2019</a>). Teratotheca: references for teratological diatoms Welcome on Teratotheca Funding Our goals More info Why take an interest in teratologies? What are diatoms? References Bioindication and consideration of teratology Teratology: a question of species, typology and severity Phone number: Legal notice Cookies and personal data Credit Website editor Hosting Intellectual property 
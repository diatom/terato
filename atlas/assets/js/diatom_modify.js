import "../css/base_diatom.css";
import "leaflet/dist/leaflet.css";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";

import { preparePage, drawOnImage } from "./base_diatom";

$(function () {
  var coords = { startX: 0, startY: 0, endX: 0, endY: 0 };
  var first = true;
  var calc = { lenX: null, lenY: null, ratio: null };

  preparePage(calc);

  $('div[id^="col-"]').each(function () {
    $(this).addClass("c-border-success");
  });

  $("#photo").addClass("img-fluid");

  // Logic for step 2

  function sendCropper() {
    var image_scale = new Image();
    image_scale.setAttribute("src", $("#photo").attr("src"));
    image_scale.setAttribute("id", "test-id");

    drawOnImage(image_scale, first, coords, calc);
  }

  if ($("#known-station-switch").is(":checked")) {
    $("#known-station-switch").trigger("click");
  }

  $("#step-2-tab").on("click", sendCropper);

  if ($("#id_inventory").val()) {
    $("#radioInventoryYes").trigger("click");
    $("#id_station").trigger("change");
  }

  if ($("input[name=taxon_label]").val().includes("abnormal form")) {
    $("#id_health option[value='']").prop("disabled", true);
    $("#id_health option[value='1']").prop("disabled", true);
    $("#id_health option[value='3']").prop("disabled", true);
  }
});

��    O      �  k         �  �   �  |   r  k   �     [     u  g   �     �     	     %	  #   =	  "   a	  "   �	     �	     �	     �	  7   �	     -
     :
     F
  
   L
  	   W
     a
     j
     w
     �
     �
      �
     �
  %        .     M  
   Y  	   d     n     r     �     �  +   �     �            #   8     \     z     �     �     �  !   �          %     =     O     _      s     �     �     �     �     �      �       #   ,     P     o     �     �     �     �  #   �          )     G     a     ~     �     �     �     �  A  �  �     z   �  f   9     �     �  ^   �     '     B  	   J  w   T  v   �  f   C  B   �  .   �  <     L   Y     �     �     �     �     �     �     �       /        @  :   N     �  h   �       	        (     <     @     D     T     c     k  l   �     �  x   �  =   n  *   �     �  �   �     }     �     �     �     �     �     �  	   �                    1     9     @     H  $   P     u  4   �     �     �  w   �     _  _   g  8   �        %        2     A     O     `     u     �     �     G   B             J   *            ;      D      0       K       ,   1             )   #   4   	   3       @              %       ?           >   /   8      &   .                       5       "       :           +   7       !             F      <                9       $       =   M                     (          N       I   O   L   2      A             E   C   6      -   
   '       H        
                                    Vos contributions apparaîtront ici, dès lors que vous aurez <a href="%(link_upload)s">ajouté une diatomée</a>.
                                 
        You're receiving this email because you requested a password reset for your user account at %(site_name)s.
         
        You're receiving this email because you requested an user account creation at %(domain)s.
         ADMIN USER Contrib. count ADMIN USER See list of diatoms ADMIN USER You cannot delete a superuser account.

                        Please renew your selection. Account activation DELETE BUTTON Annuler DELETE BUTTON Supprimer DELETE CHOICE Anonymiser mes ajouts DELETE CHOICE Conserver mes ajouts DELETE CHOICE Supprimer mes ajouts DELETE MSG Account deleted DELETE MSG Account inactive DELETE MSG All deleted DELETE MSG Choisissez une option pour vos contributions DELETE Title Date joined Email First name Is active Is staff Is superuser LOGIN BUTTON Se connecter LOGIN MSG Connexion requise LOGIN MSG Créer un compte LOGIN MSG Identifiants invalides LOGIN MSG Mot de passe oublié LOGIN MSG Niveau d'accès insuffisant LOGIN MSG Pas encore de compte LOGIN Title Last login Last name Nom PASSWORD BUTTON Envoyer le mail PASSWORD BUTTON Réinitialiser PASSWORD BUTTON Valider PASSWORD MSG Demander une réinitialisation PASSWORD MSG Email sent PASSWORD MSG Hi PASSWORD MSG Lien invalide PASSWORD MSG Lien réinitialisation PASSWORD MSG Password changed PASSWORD MSG Thanks for using PASSWORD MSG enter your mail PASSWORD TITLE Change password PASSWORD TITLE Email Sent PASSWORD TITLE Forgotten Password PASSWORD TITLE Reset PROFILE MSG Changer mdp PROFILE MSG Email PROFILE MSG Nom PROFILE MSG Prénom PROFILE MSG Supprimer mon compte PROFILE TAB Profil PROFILE TAB Vos contributions PROFILE TITLE Bonjour PROFILE TITLE Profil Prénom REGISTER BUTTON Créer un compte REGISTER CHECK CGU REGISTER CHECK J'ai lu et j'accepte REGISTER MSG Account activated REGISTER MSG Connectez-vous REGISTER MSG Déjà un compte REGISTER MSG Email sent REGISTER MSG Hi REGISTER MSG Lien invalide REGISTER MSG Lien réinitialisation REGISTER MSG Recommencer REGISTER MSG Thanks for using REGISTER TITLE Email Sent REGISTER TITLE Lien invalide REGISTER Title The %(domain)s team The %(site_name)s team User Users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
                                    Vos contributions apparaîtront ici, dès lors que vous aurez <a href="%(link_upload)s">ajouté une diatomée</a>.
                                 
Nous vous envoyons ce mail car vous avez demandé à réinitialiser votre mot de passe sur notre site %(site_name)s.
     
Nous vous envoyons ce mail car vous avez demandé à créer un compte sur notre site %(domain)s.
     Nombre de contributions Voir ses ajouts Vous ne pouvez pas supprimer un compte superutilisateur. Veuillez renouveler votre sélection. Activation de votre compte Annuler Confirmer Anonymiser vos ajouts : votre compte sera supprimé, vos contributions seront désormais affichées de façon anonyme ; Conserver vos ajouts : votre compte sera conservé et rendu inactif, votre nom restant associé à vos contributions ; Supprimer vos ajouts : votre compte sera supprimé, ainsi que l'ensemble des contributions associées. Votre compte a été supprimé, et vos contributions anonymisées. Votre compte a été désactivé avec succès. Votre compte et vos contributions ont bien été supprimés. Vous vous apprêtez à supprimer votre compte. Veuillez choisir une option : Supprimer mon compte Date d'inscription Email Prénom Compte actif Equipier Superutilisateur Se connecter Vous devez vous connecter pour voir cette page. Créez-en un. Votre adresse email ou votre mot de passe sont incorrects. Mot de passe oublié Votre compte n'a pas accès à cette page. Pour continuer, connectez-vous avec un compte y ayant accès. Pas encore de compte ? Connexion Dernière connexion Nom Nom Envoyer le mail Réinitialiser Valider demander un nouveau. Nous vous avons envoyé un email pour réinitialiser votre mot de passe. Vous devriez le recevoir sous peu ! Bonjour Ce lien de réinitialisation du mot de passe est invalide, possiblement parce qu'il a déjà été utilisé. Veuillez en Suivez le lien suivant pour choisir un nouveau mot de passe : Votre mot de passe a bien été modifié ! Merci de votre participation ! Entrez votre adresse mail ci-dessous. si un compte actif y est associé, nous y enverrons un lien pour réinitialiser le mot de passe. Changer le mot de passe Mail envoyé ! Mot de passe oublié Réinitialisation Changer mon mot de passe Adresse mail : Nom : Prénom : Supprimer mon compte Profil Vos contributions Bonjour Profil Prénom Valider conditions générales d'utilisation J'ai lu et j'accepte les  La validation de votre compte a réussi. Bienvenue ! Connectez-vous. Déjà un compte ? Nous vous avons envoyé un email contenant un lien de validation. Cliquez sur ce lien pour finaliser votre inscription. Bonjour Ce lien de validation est invalide, possiblement parce qu'il a déjà été utilisé. Veuillez  Suivez le lien suivant pour terminer votre inscription : recommencer Merci et à bientôt sur notre site ! Mail envoyé ! Lien invalide Créer un compte L'équipe %(domain)s L'équipe %(site_name)s Utilisateur Utilisateurs 
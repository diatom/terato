from django.contrib import messages
from django.contrib.admin.actions import delete_selected as ds
from django.contrib.admin.decorators import action
from django.contrib.admin.utils import model_ngettext
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy


@action(
    permissions=["delete"],
    description=gettext_lazy("Delete selected %(verbose_name_plural)s"),
)
def delete_selected(modeladmin, request, queryset):

    """
    Delete selected objects from upload list. No confirmation will be asked.
    """

    if modeladmin.uploading:
        n = len(queryset)
        for obj in queryset:
            modeladmin._upload_queryset.remove(obj)
        if not modeladmin._upload_queryset:
            modeladmin.uploading = False
        modeladmin.message_user(
            request,
            _("Successfully deleted %(count)d %(items)s.")
            % {"count": n, "items": model_ngettext(modeladmin.opts, n)},
            messages.SUCCESS,
        )
        return None

    else:
        return ds(modeladmin, request, queryset)

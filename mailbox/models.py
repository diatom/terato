import base64
import email
import logging
from email.message import Message as EmailMessage
from email.utils import parseaddr

from django.contrib import admin
from django.db import models
from django.utils.translation import gettext_lazy as _

from .utils import get_body_from_message

logger = logging.getLogger(__name__)


class UnreadMessageManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(read=None)


class Message(models.Model):

    subject = models.CharField(_("Subject"), max_length=255)
    message_id = models.CharField(_("Message ID"), max_length=255)

    from_header = models.CharField(
        _("From header"),
        max_length=255,
    )

    to_header = models.TextField(
        _("To header"),
    )

    body = models.TextField(
        _("Body"),
    )

    encoded = models.BooleanField(
        _("Encoded"),
        default=False,
        help_text=_("True if the e-mail body is Base64 encoded"),
    )

    received = models.DateTimeField(_("Received"))
    processed = models.DateTimeField(_("Processed"), auto_now_add=True)

    read = models.DateTimeField(
        _("Read"),
        default=None,
        blank=True,
        null=True,
    )

    objects = models.Manager()
    unread_messages = UnreadMessageManager()

    @property
    def address(self):
        """Property allowing one to get the relevant address(es).

        In earlier versions of this library, the model had an `address` field
        storing the e-mail address from which a message was received.  During
        later refactorings, it became clear that perhaps storing sent messages
        would also be useful, so the address field was replaced with two
        separate fields.

        """
        addresses = []
        addresses = self.to_addresses + self.from_address
        return addresses

    @property
    def from_address(self):
        """Returns the address (as a list) from which this message was received

        .. note::

           This was once (and probably should be) a string rather than a list,
           but in a pull request received long, long ago it was changed;
           presumably to make the interface identical to that of
           `to_addresses`.

        """
        if self.from_header:
            return [parseaddr(self.from_header)[1].lower()]
        else:
            return []

    @property
    def to_addresses(self):
        """Returns a list of addresses to which this message was sent."""
        addresses = []
        for address in self.to_header.split(","):
            if address:
                addresses.append(parseaddr(address)[1].lower())
        return addresses

    @property
    @admin.display(description=_("Content"))
    def content(self):
        """
        Returns the message body matching content type 'text/html',
        or 'text/plain' if not available.
        """

        text = (
            get_body_from_message(self.get_email_object(), "text", "html")
            .replace("\n", "")
            .strip()
        )

        if text != "":
            return text

        else:
            return (
                get_body_from_message(self.get_email_object(), "text", "plain")
                .replace("=\n", "")
                .strip()
            )

    def _rehydrate(self, msg):
        new = EmailMessage()

        if msg.is_multipart():
            for header, value in msg.items():
                new[header] = value
            for part in msg.get_payload():
                new.attach(self._rehydrate(part))
        else:
            for header, value in msg.items():
                new[header] = value
            new.set_payload(msg.get_payload())
        return new

    def get_body(self):
        """Returns the `body` field of this record.

        This will automatically base64-decode the message contents
        if they are encoded as such.

        """
        if self.encoded:
            return base64.b64decode(self.body.encode("ascii"))
        return self.body.encode("utf-8")

    def set_body(self, body):
        """Set the `body` field of this record.

        This will automatically base64-encode the message contents to
        circumvent a limitation in earlier versions of Django in which
        no fields existed for storing arbitrary bytes.

        """
        self.encoded = True
        self.body = base64.b64encode(body.encode("utf-8")).decode("ascii")

    def get_email_object(self):
        """Returns an `email.message.EmailMessage` instance representing the
        contents of this message and all attachments.

        See [email.message.EmailMessage]_ for more information as to what methods
        and properties are available on `email.message.EmailMessage` instances.

        .. note::

           Depending upon the storage methods in use (specifically --
           whether ``DJANGO_MAILBOX_STORE_ORIGINAL_MESSAGE`` is set
           to ``True``, this may either create a "rehydrated" message
           using stored attachments, or read the message contents stored
           on-disk.

        .. [email.message.EmailMessage] Python's `email.message.EmailMessage` docs
           (https://docs.python.org/3/library/email.message.html)

        """
        if not hasattr(self, "_email_object"):  # Cache fill
            body = self.get_body()

            flat = email.message_from_bytes(body)
            self._email_object = self._rehydrate(flat)
        return self._email_object

    def __str__(self):
        return self.subject

    def previous(self):
        return self.get_previous_by_received()

    def next(self):
        return self.get_next_by_received()

    class Meta:
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")

import "../css/autocomplete.css";
import "jquery-ui/ui/widgets/autocomplete";

const lang = $("html").attr("lang");

function ajaxSource(request, response, url) {
  jQuery.ajax({
    url: url,
    data: "term=" + encodeURIComponent(request.term),
    dataType: "json",
    success: function (results) {
      if (results.length > 10) {
        var crop = results.slice(0, 10);
        crop.push({ id: -1, label: "10 / " + results.length });
        response(crop);
      } else {
        response(results);
      }
    },
  });
}

$(function () {
  // Autocomplete

  $(".ui-autocomplete-input").each(function () {
    var labelRef = "input[name=" + $(this).attr("name") + "]";
    var hiddenRef =
      "input[name=" + $(this).attr("name").replace("_label", "") + "]";
    var model = $(hiddenRef).attr("name").split("-").slice(-1);

    var search_url = "/" + lang + "/search/" + model;

    $(labelRef).autocomplete({
      source: function (request, response) {
        ajaxSource(request, response, search_url);
      },
      minLength: 1,
      delay: 100,
      select: function (event, ui) {
        $(labelRef).val(ui.item.label);
        $(hiddenRef).val(ui.item.value);
        event.preventDefault();
      },
      response: function (event, ui) {
        if (ui.content.length == 1) {
          $(hiddenRef).val(ui.content[0].value);
          $(labelRef).on("focusout.complete", () => {
            $(this).val(ui.content[0].label);
          });
        } else {
          $(hiddenRef).val("");
          $(labelRef).off("focusout.complete");
        }
      },
    });
  });
});

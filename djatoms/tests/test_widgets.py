from base64 import b64encode

from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _

from tests import TestCase, WidgetTestMixin
from tests.utils import PHOTO_BYTES

from ..widgets import (
    AutocompleteModelChoiceInput,
    DisabledCheckboxInput,
    UploadableImageInput,
)
from .models import TestModel


class AutocompleteModelChoiceInputTest(WidgetTestMixin, TestCase):

    fixtures = ["test_model"]

    def setUp(self):
        self.widget = AutocompleteModelChoiceInput()

    def test_render(self):
        self.check_html(
            self.widget,
            "model",
            "",
            html=(
                '<input name="model" type="hidden">'
                '<input class="ui-autocomplete-input" name="model_label" type="text">'
            ),
        )

    def test_value_from_datadict(self):
        self.assertEqual(
            self.widget.value_from_datadict(
                {"model": 1, "model_label": "Un"}, None, "model"
            ),
            1,
        )

    def test_decompress_no_value(self):
        self.assertEqual(self.widget.decompress([]), [None, None])

    def test_decompress_no_model(self):
        self.assertEqual(self.widget.decompress("whatever"), ["whatever", "whatever"])

    def test_decompress_model(self):
        self.widget.model = TestModel
        self.assertEqual(
            self.widget.decompress(4), [4, str(TestModel.objects.get(pk=4))]
        )


class DisabledCheckboxInputTest(WidgetTestMixin, TestCase):

    widget = DisabledCheckboxInput()

    def test_render(self):
        self.check_html(
            self.widget,
            "is_cool",
            "foo",
            html='<input checked type="checkbox" name="is_cool" value="foo" disabled>',
        )


class FakeFieldFile:
    """
    Quacks like a FieldFile (has a .url and string representation), but
    doesn't require us to care about storages etc.
    """

    url = "something"

    def __str__(self):
        return self.url


class UploadableImageInputTest(WidgetTestMixin, TestCase):
    def setUp(self):
        self.widget = UploadableImageInput()

    def test_render_not_uploaded(self):
        self.check_html(
            self.widget, "myfile", None, html='<input type="file" name="myfile">'
        )

    def test_render_already_uploaded(self):
        self.check_html(
            self.widget,
            "myfile",
            PHOTO_BYTES,
            html=mark_safe(
                '<img id={} alt="{}" src="data: image/{}; base64, {}">'.format(
                    "myfile",
                    _("Photo non disponible."),
                    "gif",
                    b64encode(PHOTO_BYTES).decode("utf-8"),
                )
            ),
        )

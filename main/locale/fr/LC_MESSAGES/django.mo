��          �            h  +   i  )   �     �     �     �     	          )     :     N     `     s     �     �  A  �  $   �  "        *     <     P     ]  	   s     }     �  
   �     �     �     �  3   C                                    
                                 	           FOOTER Conditions générales d'utilisation FOOTER Contacter l'administrateur du site FOOTER Mentions légales FOOTER Voir le code source HEADER Administration HEADER Ajouter HEADER Connexion HEADER Consulter HEADER Déconnexion HEADER Mon Compte HEADER Nom du site HEADER Profil META Description META Keywords Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Conditions générales d'utilisation Contacter l'administrateur du site Mentions légales Voir le code source Accès admin Ajouter une diatomée Connexion Consulter le recueil Déconnexion Mon compte La Tératothèque Profil La Tératothèque est un recueil collaboratif de photographies de diatomées déformées, porté par la recherche scientifique. diatomée, tératologie, déformation, image, INRAE 
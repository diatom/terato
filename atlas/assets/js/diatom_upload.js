import "../css/base_diatom.css";
import "leaflet/dist/leaflet.css";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";
import "cropperjs/dist/cropper.css";

import Cropper from "cropperjs";
import UTIF from "utif";
import {
  validDisplay,
  invalidDisplay,
  preparePage,
  drawOnImage,
} from "./base_diatom";

$(function () {
  var coords = { startX: 0, startY: 0, endX: 0, endY: 0 };
  var first = false;
  var calc = { lenX: null, lenY: null, ratio: null };

  preparePage(calc);

  // Make sure to empty form when reloading the page
  $("#form").trigger("reset");

  $('button[id^="step-"]').each(function () {
    if (!$(this).hasClass("active")) {
      $(this).addClass("disabled");
    }
  });

  $('button[id^="btn-next-"]').each(function () {
    $(this).addClass("disabled");
  });

  $('div[id^="col-"]').each(function () {
    $(this).addClass("c-border-danger");
  });

  // Logic for step 1

  var cropper;

  // when photo is chosen, open it and create the cropper
  $("#crop-row").hide();
  $("#id_photo").on("change", function (e) {
    var imgsrc = URL.createObjectURL(e.target.files[0]);
    $("#filename").text(e.target.value.split("\\").pop());
    $("#filename-par").removeClass("d-none");
    var fileExt = e.target.value.split(".").pop();

    if (
      ["png", "jpg", "jpeg", "tif", "tiff", "bmp"].includes(
        fileExt.toLowerCase()
      )
    ) {
      $("#format-not-valid").attr("hidden", true);
      validDisplay($("#col-photo"));

      if (["tif", "tiff"].includes(fileExt)) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", imgsrc);
        xhr.responseType = "arraybuffer";
        xhr.onload = function (e) {
          var ifds = UTIF.decode(e.target.response);
          UTIF.decodeImage(e.target.response, ifds[0]);
          var rgba = UTIF.toRGBA8(ifds[0]);

          var cnv = document.createElement("canvas");
          cnv.width = ifds[0].width;
          cnv.height = ifds[0].height;
          var ctx = cnv.getContext("2d");
          var imgd = ctx.createImageData(ifds[0].width, ifds[0].height);
          for (var i = 0; i < rgba.length; i++) {
            imgd.data[i] = rgba[i];
          }
          ctx.putImageData(imgd, 0, 0);
          imgsrc = cnv.toDataURL();
          makeCropper(imgsrc);
        };
        xhr.send();
      } else {
        makeCropper(imgsrc);
      }
    } else {
      $("#format-not-valid").removeAttr("hidden");
      $("#crop-row").hide();
      invalidDisplay($("#col-photo"));
      $("#filename-par").addClass("d-none");
      if (cropper) {
        cropper.destroy();
      }
    }
  });

  // Crop selected image
  function makeCropper(src) {
    if (src) {
      $("#crop-row").show();
      var image = document.getElementById("imagecan");

      image.setAttribute("src", src);
      if (cropper) {
        cropper.destroy();
      }
      cropper = new Cropper(image, {
        aspectRatio: NaN,
        initialAspectRatio: 1 / 1,
        viewMode: 1,
        dragMode: "move",
      });

      function sendCropper() {
        const photo = document.getElementById("id_photo");
        const fileName = photo.value.split(/(\\|\/)/g).pop();

        cropper.getCroppedCanvas().toBlob((blob) => {
          const myFile = new File([blob], fileName);

          const dataTransfer = new DataTransfer();
          dataTransfer.items.add(myFile);

          photo.files = dataTransfer.files;
        }, true);

        var image_scale = new Image();
        image_scale.setAttribute("src", src);
        image_scale.setAttribute("id", "test-id");

        drawOnImage(image_scale, first, coords, calc);
      }
      $("#step-2-tab").on("click", sendCropper);

      // if coming back to step-1, make sure to resize the cropper
      $("#step-1-tab").on("click", () => {
        if (cropper) {
          cropper.resize();
        }
      });
    }
  }

  // If coming back to step 1 and changing photo, make sure user reselects a scale
  $("#id_photo").on("change", () => {
    coords = { startX: 0, startY: 0, endX: 0, endY: 0 };
    $("#id_scale").val(null);
    invalidDisplay($("#col-scale"));
  });
});

import logging
from mailbox.models import Message
from mailbox.utils import convert_header_to_unicode

from django.contrib import admin, messages
from django.contrib.admin.utils import unquote
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext

from djatoms.admin import ExtendedModelAdmin, admin_site

from .mailbox import Mailbox

logger = logging.getLogger(__name__)


class MessageAdmin(ExtendedModelAdmin):

    list_display = (
        "subject",
        "received",
        "read",
    )
    ordering = ["-received"]
    list_filter = ("received",)
    exclude = ("message_id", "body", "encoded", "to_header", "subject")
    readonly_fields = (
        "from_header",
        "received",
        "read",
        "content",
    )
    actions = ["mark_read", "mark_unread"]

    # change_list_template = "admin/mailbox/change_list.html"

    @admin.action(description=_("ADMIN Mark as unread"))
    def mark_unread(self, request, queryset):
        updated = queryset.update(read=None)
        self.message_user(
            request,
            ngettext(
                "%d message marked as unread",
                "%d messages marked as unread",
                updated,
            )
            % updated,
            messages.SUCCESS,
        )

    @admin.action(description=_("ADMIN Mark as read"))
    def mark_read(self, request, queryset):
        updated = queryset.update(read=timezone.now())
        self.message_user(
            request,
            ngettext(
                "%d message marked as read",
                "%d messages marked as read",
                updated,
            )
            % updated,
            messages.SUCCESS,
        )

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        box = Mailbox()
        for msg in box.get_new_mail():
            pass
        return super().get_queryset(request)

    def change_view(self, request, object_id, form_url="", extra_context=None):
        obj = self.get_object(request, unquote(object_id))
        if not obj.read:
            obj.read = timezone.now()
            obj.save()
        extra_context = {"title": _("ADMIN Messages")}
        return super().change_view(request, object_id, form_url, extra_context)

    def changelist_view(self, request, extra_context=None):
        extra_context = {"title": _("ADMIN Boîte de réception")}
        return super().changelist_view(request, extra_context)

    def subject(self, msg):
        return convert_header_to_unicode(msg.subject)

    def envelope_headers(self, msg):
        email = msg.get_email_object()
        return "\n".join([("{}: {}".format(h, v)) for h, v in email.items()])


admin_site.register(Message, MessageAdmin)

import json

from django.test import RequestFactory

from tests import TestCase

from ..models import DiatomChildren, Health, Taxon
from ..views import search_diatom, search_inventory, search_station, search_taxon


class SearchTaxonViewTests(TestCase):

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
    ]

    def setUp(self):
        self.request = RequestFactory()

    def test_search_no_taxon_no_pk(self):
        # Should return an empty json
        self.request.GET = {}
        response = search_taxon(self.request)
        self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_no_matching_taxon_no_pk(self):
        # Should return an empty json
        self.request.GET = {"term": "whatever"}
        response = search_taxon(self.request)
        self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_matching_taxon_no_pk(self):
        # Should return all valid taxa with name or code starting with term
        self.request.GET = {"term": "cymatopleura elliptica"}
        response = search_taxon(self.request)

        expected = [
            {"label": "Cymatopleura elliptica, CELL, 7249", "value": 21},
            {"label": "Cymatopleura elliptica var. angulata, CEAG, 38063", "value": 22},
        ]

        self.assertJSONEqual(response.content.decode("utf-8"), expected)

    def test_search_matching_taxon_but_not_valid(self):
        # Should not return invalid taxa
        self.request.GET = {"term": "chrombio"}
        response = search_taxon(self.request)

        self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_matching_code_no_pk(self):
        # Should return all valid taxa with name or code starting with term
        self.request.GET = {"term": "SBR"}
        response = search_taxon(self.request)

        expected = [
            {"label": "Surirella brebissonii, SBRE, 8491", "value": 16},
            {"label": "Surirella brebissonii abnormal form, SBRT, 17985", "value": 17},
        ]

        self.assertJSONEqual(response.content.decode("utf-8"), expected)

    def test_search_pk_and_digit(self):
        # Should return taxon indicated by pk
        self.request.GET = {"term": "7", "pk": True}
        response = search_taxon(self.request)

        expected = [{"label": "Cymbelgeia floweri, CFLW", "value": 7}]

        self.assertJSONEqual(response.content.decode("utf-8"), expected)

    def test_search_pk_and_not_digit(self):
        # should ignore pk and behave like a normal search
        self.request.GET = {"term": "Fra", "pk": True}
        response_1 = search_taxon(self.request).content.decode("utf-8")

        self.request.GET = {"term": "Fra"}
        response_2 = search_taxon(self.request).content.decode("utf-8")

        self.assertJSONEqual(response_1, response_2)


class SearchStationViewTests(TestCase):
    fixtures = ["station"]

    def setUp(self):
        self.request = RequestFactory()

    def test_search_no_station_no_pk(self):
        # Should return an empty json
        self.request.GET = {}
        response = search_station(self.request)
        self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_no_matching_station_no_pk(self):
        # Should return an empty json
        self.request.GET = {"term": "whatever"}
        response = search_station(self.request)
        self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_matching_station_no_pk(self):
        # Should return all valid stations with name or commune containing term
        self.request.GET = {"term": "Sla"}
        response = search_station(self.request)

        expected = [
            {"label": "LA SLACK À RINXENT (62), Rinxent", "value": 4},
        ]

        self.assertJSONEqual(response.content.decode("utf-8"), expected)

    def test_search_matching_commune_no_pk(self):
        # Should return all valid stations with name or commune containing term
        self.request.GET = {"term": "Brime"}
        response = search_station(self.request)

        expected = [
            {"label": "LA CANCHE À BRIMEUX (62), Brimeux", "value": 1},
        ]

        self.assertJSONEqual(response.content.decode("utf-8"), expected)

    def test_search_pk_and_digit(self):
        # Should return station indicated by pk
        self.request.GET = {"term": "5", "pk": True}
        response = search_station(self.request)

        expected = [
            {
                "label": "MARQUE À SAINGHIN EN MELANTOIS (59), Sainghin-en-Mélantois",
                "value": 5,
            }
        ]

        self.assertJSONEqual(response.content.decode("utf-8"), expected)

    def test_search_pk_and_not_digit(self):
        # should ignore pk and behave like a normal search
        self.request.GET = {"term": "La S", "pk": True}
        response_1 = search_station(self.request).content.decode("utf-8")

        self.request.GET = {"term": "La S"}
        response_2 = search_station(self.request).content.decode("utf-8")

        self.assertJSONEqual(response_1, response_2)


class SearchDiatomViewTests(TestCase):

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "station",
        "defo_factor_cat",
        "defo_factor",
        "diatom",
    ]

    def setUp(self):
        self.request = RequestFactory()

    def test_search_no_station_or_no_taxon(self):
        # Should return an empty json
        for d in [{}, {"taxon": 1}, {"health": 1}]:
            self.request.GET = d
            response = search_diatom(self.request)
            self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_no_matching_taxon_or_health(self):

        for d in [{"taxon": 9999, "health": 1}, {"taxon": 1, "health": 9999}]:

            self.request.GET = d
            response = search_diatom(self.request)
            self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_no_page_given(self):
        for t in Taxon.objects.all():
            for h in Health.objects.all():

                self.request.GET = {"taxon": t.id, "health": h.id}
                response = search_diatom(self.request)

                expected = (
                    DiatomChildren.objects.filter(ancestor=t.id)
                    .filter(health=h.id)
                    .order_by("-upload_date")
                    .values_list("diatom", "taxon")[0:10]
                )
                results = [{"id": obj[0], "taxon": obj[1]} for obj in expected]

                self.assertEqual(
                    sorted(
                        json.loads(response.content.decode("utf-8")),
                        key=lambda x: x["id"],
                    ),
                    sorted(results, key=lambda x: x["id"]),
                )

    def test_search_invalid_page_given(self):

        for t in Taxon.objects.all():
            for h in Health.objects.all():

                self.request.GET = {"taxon": t.id, "health": h.id, "page": "whatever"}
                response = search_diatom(self.request)

                expected = (
                    DiatomChildren.objects.filter(ancestor=t.id)
                    .filter(health=h.id)
                    .order_by("-upload_date")
                    .values_list("diatom", "taxon")[0:10]
                )
                results = [{"id": obj[0], "taxon": obj[1]} for obj in expected]

                self.assertEqual(
                    sorted(
                        json.loads(response.content.decode("utf-8")),
                        key=lambda x: x["id"],
                    ),
                    sorted(results, key=lambda x: x["id"]),
                )

    def test_search_positive_page_given(self):

        for t in Taxon.objects.all():
            for h in Health.objects.all():

                self.request.GET = {"taxon": t.id, "health": h.id, "page": 2}
                response = search_diatom(self.request)

                expected = (
                    DiatomChildren.objects.filter(ancestor=t.id)
                    .filter(health=h.id)
                    .order_by("-upload_date")
                    .values_list("diatom", "taxon")[10:20]
                )
                results = [{"id": obj[0], "taxon": obj[1]} for obj in expected]

                self.assertEqual(
                    sorted(
                        json.loads(response.content.decode("utf-8")),
                        key=lambda x: x["id"],
                    ),
                    sorted(results, key=lambda x: x["id"]),
                )

    def test_search_negative_page_given(self):

        for t in Taxon.objects.all():
            for h in Health.objects.all():

                self.request.GET = {"taxon": t.id, "health": h.id, "page": -2}
                response = search_diatom(self.request)

                expected = (
                    DiatomChildren.objects.filter(ancestor=t.id)
                    .filter(health=h.id)
                    .order_by("-upload_date")
                    .values_list("diatom", "taxon")[10:20]
                )
                results = [{"id": obj[0], "taxon": obj[1]} for obj in expected]

                self.assertEqual(
                    sorted(
                        json.loads(response.content.decode("utf-8")),
                        key=lambda x: x["id"],
                    ),
                    sorted(results, key=lambda x: x["id"]),
                )


class SearchInventoryViewTests(TestCase):

    fixtures = ["station", "inventory_type", "inventory"]

    def setUp(self):
        self.request = RequestFactory()

    def test_search_no_date_no_station(self):

        for r in [{}, {"date": "2023-05-12"}, {"station": 4}]:
            self.request.GET = r
            response = search_inventory(self.request)
            self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_invalid_date(self):

        self.request.GET = {"date": "2023-05-34", "station": 1}
        response = search_inventory(self.request)
        self.assertJSONEqual(response.content.decode("utf-8"), "[]")

    def test_search_match_closest_above(self):
        self.request.GET = {"date": "2019-07-10", "station": 1}
        response = search_inventory(self.request)
        self.assertJSONEqual(
            response.content.decode("utf-8"), {"date": "2019-07-11", "inventory": 1}
        )

    def test_search_match_closest_below(self):
        self.request.GET = {"date": "2019-07-05", "station": 1}
        response = search_inventory(self.request)
        self.assertJSONEqual(
            response.content.decode("utf-8"), {"date": "2019-07-04", "inventory": 2}
        )

    def test_search_no_match(self):
        self.request.GET = {"date": "2021-07-05", "station": 1}
        response = search_inventory(self.request)
        self.assertJSONEqual(response.content.decode("utf-8"), {"inventory": -1})

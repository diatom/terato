import "../css/home.css";
import Chart from "chart.js/auto";

/* global gettext */

(async function () {
  const style = getComputedStyle(document.body);
  const theme = {
    primary: style.getPropertyValue("--bs-primary"),
    danger: style.getPropertyValue("--bs-danger"),
    primarySubtle: style.getPropertyValue("--bs-primary-bg-subtle"),
    dangerSubtle: style.getPropertyValue("--bs-danger-bg-subtle"),
  };

  const dataCauses = [
    {
      cause: gettext("HOME CHART pH"),
      count: 5,
      color: theme.primary,
      hoverColor: theme.primarySubtle,
    },
    {
      cause: gettext("HOME CHART Eutrophisation"),
      count: 5,
      color: theme.primary,
      hoverColor: theme.primarySubtle,
    },
    {
      cause: gettext("HOME CHART Pesticides"),
      count: 6,
      color: theme.danger,
      hoverColor: theme.dangerSubtle,
    },
    {
      cause: gettext("HOME CHART Autres"),
      count: 10,
      color: theme.primary,
      hoverColor: theme.primarySubtle,
    },
    {
      cause: gettext("HOME CHART Drainage minier acide"),
      count: 10,
      color: theme.danger,
      hoverColor: theme.dangerSubtle,
    },
    {
      cause: gettext("HOME CHART Stress physique"),
      count: 15,
      color: theme.primary,
      hoverColor: theme.primarySubtle,
    },
    {
      cause: gettext("HOME CHART Cond. artificielles"),
      count: 22,
      color: theme.primary,
      hoverColor: theme.primarySubtle,
    },
    {
      cause: gettext("HOME CHART Inconnu"),
      count: 31,
      color: theme.primary,
      hoverColor: theme.primarySubtle,
    },
    {
      cause: gettext("HOME CHART Multiple"),
      count: 32,
      color: theme.danger,
      hoverColor: theme.dangerSubtle,
    },
    {
      cause: gettext("HOME CHART Métaux"),
      count: 56,
      color: theme.danger,
      hoverColor: theme.dangerSubtle,
    },
  ];

  const getOrCreateLegendList = (id) => {
    const legendContainer = document.getElementById(id);
    let listContainer = legendContainer.querySelector("ul");

    if (!listContainer) {
      listContainer = document.createElement("ul");
      listContainer.style.display = "flex";
      listContainer.style.flexDirection = "row";
      listContainer.style.margin = 0;
      listContainer.style.paddingLeft = "6px";
      listContainer.style.justifyContent = "center";

      legendContainer.appendChild(listContainer);
    }

    return listContainer;
  };

  const htmlLegendPlugin = {
    id: "htmlLegend",
    afterUpdate(chart, args, options) {
      const ul = getOrCreateLegendList(options.containerID);

      // Remove old legend items
      while (ul.firstChild) {
        ul.firstChild.remove();
      }

      const items = [
        {
          fillStyle: theme.danger,
          fontColor: "#666",
          hidden: false,
          text: gettext("HOME CHART Contaminants toxiques"),
        },
        {
          fillStyle: theme.primary,
          fontColor: "#666",
          hidden: false,
          text: gettext("HOME CHART Autres causes"),
        },
      ];

      items.forEach((item) => {
        const li = document.createElement("li");
        li.style.alignItems = "center";
        li.style.display = "flex";
        li.style.flexDirection = "row";
        li.style.marginLeft = "10px";

        // Color box
        const boxSpan = document.createElement("span");
        boxSpan.style.background = item.fillStyle;
        boxSpan.style.borderColor = item.strokeStyle;
        boxSpan.style.borderWidth = item.lineWidth + "px";
        boxSpan.style.display = "inline-block";
        boxSpan.style.flexShrink = 0;
        boxSpan.style.height = "15px";
        boxSpan.style.marginRight = "10px";
        boxSpan.style.width = "15px";

        // Text
        const textContainer = document.createElement("p");
        textContainer.style.color = item.fontColor;
        textContainer.style.margin = 0;
        textContainer.style.padding = 0;
        textContainer.style.textDecoration = item.hidden ? "line-through" : "";
        const text = document.createTextNode(item.text);
        textContainer.appendChild(text);

        li.appendChild(boxSpan);
        li.appendChild(textContainer);
        ul.appendChild(li);
      });
    },
  };

  new Chart(document.getElementById("terato_causes"), {
    type: "bar",
    options: {
      animation: false,
      // indexAxis: 'y',
      plugins: {
        htmlLegend: {
          containerID: "legend-container",
        },
        tooltip: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
      },
      scales: {
        y: {
          title: {
            display: true,
            text: gettext("HOME CHART Nombre de publications"),
          },
        },
      },
      maintainAspectRatio: false,
    },
    data: {
      labels: dataCauses.map((row) => row.cause),
      datasets: [
        {
          data: dataCauses.map((row) => row.count),
          backgroundColor: dataCauses.map((row) => row.color),
          hoverBackgroundColor: dataCauses.map((row) => row.hoverColor),
        },
      ],
    },
    plugins: [htmlLegendPlugin],
  });

  const dataClasses = [
    {
      name: "C1",
      values: {
        EOMI: 0.004,
        EOMT: 0.846,
        FCAP: 0.002,
        FCAT: 0.846,
        NPAL: 0.239,
        NPTR: 0.846,
      },
    },
    {
      name: "C2",
      values: {
        EOMI: 0.028,
        EOMT: 0.11,
        FCAP: 0.004,
        FCAT: 0.11,
        NPAL: 0.215,
        NPTR: 0.11,
      },
    },
    {
      name: "C3",
      values: {
        EOMI: 0.349,
        EOMT: 0.022,
        FCAP: 0.051,
        FCAT: 0.022,
        NPAL: 0.195,
        NPTR: 0.022,
      },
    },
    {
      name: "C4",
      values: {
        EOMI: 0.307,
        EOMT: 0.018,
        FCAP: 0.145,
        FCAT: 0.018,
        NPAL: 0.191,
        NPTR: 0.018,
      },
    },
    {
      name: "C5",
      values: {
        EOMI: 0.224,
        EOMT: 0.003,
        FCAP: 0.268,
        FCAT: 0.003,
        NPAL: 0.134,
        NPTR: 0.003,
      },
    },
    {
      name: "C6",
      values: {
        EOMI: 0.082,
        EOMT: 0.0,
        FCAP: 0.301,
        FCAT: 0.0,
        NPAL: 0.024,
        NPTR: 0.0,
      },
    },
    {
      name: "C7",
      values: {
        EOMI: 0.004,
        EOMT: 0.0,
        FCAP: 0.23,
        FCAT: 0.0,
        NPAL: 0.001,
        NPTR: 0.0,
      },
    },
  ];

  new Chart(document.getElementById("classes_EOM"), {
    type: "line",
    options: {
      animation: false,
      locale: gettext("HOME CHART Locale"),
      plugins: {
        tooltip: {
          enabled: true,
        },
        title: {
          display: false,
        },
        legend: {
          labels: {
            boxHeight: 0,
          },
        },
      },
      scales: {
        y: {
          title: {
            display: true,
            text: gettext("HOME CHART Probabilité d'occurence"),
          },
          min: 0,
          max: 1,
        },
        x: {
          title: {
            display: true,
            text: gettext("HOME CHART Qualité de l'eau"),
          },
        },
      },
      maintainAspectRatio: false,
    },
    data: {
      labels: dataClasses.map((row) => row.name),
      datasets: [
        {
          data: dataClasses.map((row) => row.values.NPAL),
          label: "NPAL",
          borderColor: theme.primary,
          tension: 0.1,
        },
        {
          data: dataClasses.map((row) => row.values.NPTR),
          label: "NPTR",
          borderColor: theme.danger,
          tension: 0.1,
        },
      ],
    },
  });

  new Chart(document.getElementById("classes_FCA"), {
    type: "line",
    options: {
      animation: false,
      locale: gettext("HOME CHART Locale"),
      plugins: {
        tooltip: {
          enabled: true,
        },
        title: {
          display: false,
        },
        legend: {
          labels: {
            boxHeight: 0,
          },
        },
      },
      scales: {
        y: {
          title: {
            display: true,
            text: gettext("HOME CHART Probabilité d'occurence"),
          },
          min: 0,
          max: 1,
        },
        x: {
          title: {
            display: true,
            text: gettext("HOME CHART Qualité de l'eau"),
          },
        },
      },
      maintainAspectRatio: false,
    },
    data: {
      labels: dataClasses.map((row) => row.name),
      datasets: [
        {
          data: dataClasses.map((row) => row.values.FCAP),
          label: "FCAP",
          borderColor: theme.primary,
          tension: 0.1,
        },
        {
          data: dataClasses.map((row) => row.values.FCAT),
          label: "FCAT",
          borderColor: theme.danger,
          tension: 0.1,
        },
      ],
    },
  });
})();

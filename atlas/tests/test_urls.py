import random

from django.test import SimpleTestCase
from django.urls import resolve, reverse

from ..views import (
    ContributionsLayerView,
    DiatomDeleteView,
    DiatomDetailView,
    ModifyFormView,
    StationsLayerView,
    TaxonDetailView,
    TaxonJson,
    TaxonListView,
    UploadFormView,
    generate_image,
    generate_thumbnail,
    search_diatom,
    search_inventory,
    search_station,
    search_taxon,
)


class TestUrls(SimpleTestCase):
    def test_detail_diatom_url_resolves(self):
        url = reverse("atlas:consult:detail_diatom", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func.view_class, DiatomDetailView)

    def test_diatom_delete_url_resolves(self):
        url = reverse("atlas:contrib:diatom_delete", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func.view_class, DiatomDeleteView)

    def test_diatom_modify_url_resolves(self):
        url = reverse("atlas:contrib:diatom_modify", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func.view_class, ModifyFormView)

    def test_diatom_upload_url_resolves(self):
        url = reverse("atlas:contrib:diatom_upload")
        self.assertEqual(resolve(url).func.view_class, UploadFormView)

    def test_generate_image_url_resolves(self):
        url = reverse("atlas:api:generate_image", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func, generate_image)

    def test_generate_thumbnail_url_resolves(self):
        url = reverse("atlas:api:generate_thumbnail", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func, generate_thumbnail)

    def test_search_diatom_url_resolves(self):
        url = reverse("atlas:api:diatom_search")
        self.assertEqual(resolve(url).func, search_diatom)

    def test_search_inventory_url_resolves(self):
        url = reverse("atlas:api:inventory_search")
        self.assertEqual(resolve(url).func, search_inventory)

    def test_search_station_url_resolves(self):
        url = reverse("atlas:api:station_search")
        self.assertEqual(resolve(url).func, search_station)

    def test_search_taxon_url_resolves(self):
        url = reverse("atlas:api:taxon_search")
        self.assertEqual(resolve(url).func, search_taxon)

    def test_stations_geojson_url_resolves(self):
        url = reverse("atlas:api:stations_data")
        self.assertEqual(resolve(url).func.view_class, StationsLayerView)

    def test_contribs_geojson_url_resolves(self):
        url = reverse("atlas:api:contribs_data")
        self.assertEqual(resolve(url).func.view_class, ContributionsLayerView)

    def test_taxons_url_resolves(self):
        url = reverse("atlas:consult:taxons")
        self.assertEqual(resolve(url).func.view_class, TaxonListView)

    def test_taxon_detail_url_resolves(self):
        url = reverse("atlas:consult:detail_taxon", args=[random.randint(1, 100000)])
        self.assertEqual(resolve(url).func.view_class, TaxonDetailView)

    def test_taxons_json_url_resolves(self):
        url = reverse("atlas:api:taxons_json")
        self.assertEqual(resolve(url).func.view_class, TaxonJson)

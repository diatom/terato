from django.core.management import call_command
from django.db import ProgrammingError, connection
from django.test import TestCase


class TestTefreshMV(TestCase):
    @classmethod
    def setUpTestData(cls):
        # using random means that every time the view is refreshed,
        # we get new data
        with connection.cursor() as cursor:
            cursor.execute(
                """
                CREATE MATERIALIZED VIEW fake_mv AS
                    SELECT random() * num.num::double precision AS num
                    FROM generate_series(1,10) num(num)
                WITH DATA;
                CREATE UNIQUE INDEX fake_idx ON fake_mv USING btree(num);
                """
            )

    def test_command_refreshes_data(self):
        with connection.cursor() as cursor:

            # select data
            cursor.execute("SELECT * FROM fake_mv;")
            orig_data = cursor.fetchall()

            # refresh
            call_command("refresh_mv", "fake_mv")

            # select new data
            cursor.execute("SELECT * FROM fake_mv;")
            fresh_data = cursor.fetchall()

            # compare. Since we use random, we should have different values
            self.assertNotEqual(orig_data, fresh_data)

    def test_command_with_inexistent_table(self):

        with self.assertRaises(ProgrammingError):
            call_command("refresh_mv", "inexistent_table")

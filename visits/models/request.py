from django.contrib.postgres.indexes import BTreeIndex
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class Request(models.Model):
    """
    Record of a request made to the website.

    This is used for tracking and reporting - knowing the volume of visitors
    to the site, and being able to report on someone's interaction with the site.

    We record minimal info required to identify user sessions, plus changes in
    device. This is useful in identifying suspicious activity and support issues.
    """

    id = models.AutoField(primary_key=True)

    request_time = models.DateTimeField(
        _("Date"),
        default=timezone.now,
    )
    path = models.CharField(_("Path"), max_length=100)
    session = models.CharField(
        max_length=40,
        verbose_name=_("Session key"),
        help_text=_("Django session identifier"),
    )

    class Meta:
        get_latest_by = "date"
        managed = False
        verbose_name = _("Request")
        verbose_name_plural = _("Requests")

        indexes = [
            BTreeIndex(
                fields=["request_time", "session"], name="request_session_time_idx"
            ),
        ]

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.test import TestCase
from django.test.client import Client
from django.urls import resolve, reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from ..views import (
    DeleteProfileView,
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
    ProfileView,
    RegistrationConfirmView,
    RegistrationDoneView,
    RegistrationView,
)

User = get_user_model()


class TestUserUrls(TestCase):

    fixtures = ["users"]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user("john", "testpwd")

    def test_login_url_resolves(self):
        url = reverse("users:login")
        self.assertEqual(resolve(url).func.view_class, LoginView)

    def test_logout_url_resolves(self):
        url = reverse("users:logout")
        self.assertEqual(resolve(url).func.view_class, LogoutView)

    def test_profile_url_resolves(self):
        url = reverse("users:profile")
        self.assertEqual(resolve(url).func.view_class, ProfileView)

    def test_register_url_resolves(self):
        url = reverse("users:register")
        self.assertEqual(resolve(url).func.view_class, RegistrationView)

    def test_register_done_url_resolves(self):
        url = reverse("users:register_done")
        self.assertEqual(resolve(url).func.view_class, RegistrationDoneView)

    def test_register_confirm_url_resolves(self):
        uid = urlsafe_base64_encode(force_bytes(self.user.pk))
        token = default_token_generator.make_token(self.user)
        url = reverse("users:register_confirm", args=[uid, token])
        self.assertEqual(resolve(url).func.view_class, RegistrationConfirmView)

    def test_register_password_reset_url_resolves(self):
        url = reverse("users:password_reset")
        self.assertEqual(resolve(url).func.view_class, PasswordResetView)

    def test_register_password_reset_done_url_resolves(self):
        url = reverse("users:password_reset_done")
        self.assertEqual(resolve(url).func.view_class, PasswordResetDoneView)

    def test_password_reset_confirm_url_resolves(self):
        uid = urlsafe_base64_encode(force_bytes(self.user.pk))
        token = default_token_generator.make_token(self.user)
        url = reverse("users:password_reset_confirm", args=[uid, token])
        self.assertEqual(resolve(url).func.view_class, PasswordResetConfirmView)

    def test_password_change_url_resolves(self):
        url = reverse("users:password_change")
        self.assertEqual(resolve(url).func.view_class, PasswordChangeView)

    def test_delete_url_resolves(self):
        url = reverse("users:delete")
        self.assertEqual(resolve(url).func.view_class, DeleteProfileView)

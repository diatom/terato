from django.core.exceptions import EmptyResultSet
from django.db.models import F, Q

from atlas.models import Diatom
from tests import RedirectionTestMixin, TestCase
from tests.utils import DEFAULT_EMAIL, DEFAULT_PWD, ERROR_NOTHING_TO_TEST

from ..models import Taxon


class TestLoggedOutRedirects(RedirectionTestMixin, TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):

        self.expected = {
            ("atlas:contrib:diatom_upload",): ("users:login",),
            ("atlas:consult:taxons",): (None,),
            ("atlas:api:diatom_search",): (None,),
            ("atlas:api:inventory_search",): (None,),
            ("atlas:api:station_search",): (None,),
            ("atlas:api:taxon_search",): (None,),
            ("atlas:api:stations_data",): (None,),
            ("atlas:api:contribs_data",): (None,),
            ("atlas:api:taxons_json",): (None,),
        }

        for d in Diatom.objects.all():
            self.expected[("atlas:consult:detail_diatom", d.pk)] = (None,)
            self.expected[("atlas:contrib:diatom_modify", d.pk)] = ("users:login",)
            self.expected[("atlas:contrib:diatom_delete", d.pk)] = ("users:login",)
            self.expected[("atlas:api:generate_image", d.pk)] = (None,)
            self.expected[("atlas:api:generate_thumbnail", d.pk)] = (None,)

        for t in Taxon.objects.all():
            self.expected[("atlas:consult:detail_taxon", t.pk)] = (None,)


class TestLoggedInRedirects(RedirectionTestMixin, TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):

        self.expected = {
            ("atlas:contrib:diatom_upload",): (None,),
            ("atlas:consult:taxons",): (None,),
            ("atlas:api:diatom_search",): (None,),
            ("atlas:api:inventory_search",): (None,),
            ("atlas:api:station_search",): (None,),
            ("atlas:api:taxon_search",): (None,),
            ("atlas:api:stations_data",): (None,),
            ("atlas:api:contribs_data",): (None,),
            ("atlas:api:taxons_json",): (None,),
        }

        for d in Diatom.objects.filter(user__email=DEFAULT_EMAIL):
            self.expected[("atlas:consult:detail_diatom", d.pk)] = (None,)
            self.expected[("atlas:contrib:diatom_modify", d.pk)] = (None,)
            self.expected[("atlas:contrib:diatom_delete", d.pk)] = (None,)
            self.expected[("atlas:api:generate_image", d.pk)] = (None,)
            self.expected[("atlas:api:generate_thumbnail", d.pk)] = (None,)

        for t in Taxon.objects.all():
            self.expected[("atlas:consult:detail_taxon", t.pk)] = (None,)

        self.client.login(username=DEFAULT_EMAIL, password=DEFAULT_PWD)

    def tearDown(self):
        self.client.logout()


class TestTaxaRedirections(RedirectionTestMixin, TestCase):
    fixtures = ["taxonomy_type", "taxon"]

    def setUp(self):

        self.expected = {}
        querysets = [
            # valid and not aliased
            Taxon.objects.exclude(Q(is_terato=True)).filter(Q(taxon_alias__id=F("id"))),
            # aliased
            Taxon.objects.filter(~Q(taxon_alias__id=F("id"))),
            # terato with valid parent
            Taxon.objects.filter(
                Q(is_terato=True) & Q(taxon_parent__taxon_alias=F("taxon_parent"))
            ),
            # terato with aliased parent
            Taxon.objects.filter(
                Q(is_terato=True) & ~Q(taxon_parent__taxon_alias=F("taxon_parent"))
            ),
            # improper taxa
            Taxon.objects.filter(Q(is_taxon=False) | Q(freshwater_diatom=False)),
        ]

        for cases in querysets[:-1]:
            if len(cases) == 0:
                raise EmptyResultSet(ERROR_NOTHING_TO_TEST)
        if len(querysets[-1]) > 0:
            raise ValueError(
                "Some invalid taxa are still out with Taxon objects manager"
            )

        for c in querysets[0]:
            self.expected[("atlas:consult:detail_taxon", c.pk)] = (None,)
        for c in querysets[1]:
            self.expected[("atlas:consult:detail_taxon", c.pk)] = (
                "atlas:consult:detail_taxon",
                c.taxon_alias.pk,
            )
        for c in querysets[2]:
            self.expected[("atlas:consult:detail_taxon", c.pk)] = (
                "atlas:consult:detail_taxon",
                c.taxon_parent.pk,
            )
        for c in querysets[3]:
            self.expected[("atlas:consult:detail_taxon", c.pk)] = (
                "atlas:consult:detail_taxon",
                c.taxon_parent.taxon_alias.pk,
            )
        for c in querysets[4]:
            self.expected[("atlas:consult:detail_diatom", c.pk)] = (404,)

    def test_meta_all_urls_considered(self):
        pass

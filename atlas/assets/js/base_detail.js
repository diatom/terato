import "jquery-ui/ui/widgets/autocomplete";

export function prepareAutocomplete() {
  const link = JSON.parse(
    document.getElementById("taxon-url").textContent
  ).slice(0, -2);

  var lang = window.location.href.split("/")[3];

  $("#search-input").autocomplete({
    source: function (request, response) {
      jQuery.ajax({
        url: "/" + lang + "/search/taxon",
        data: "term=" + encodeURIComponent(request.term),
        dataType: "json",
        success: function (results) {
          if (results.length > 10) {
            var crop = results.slice(0, 10);
            crop.push({ value: -1, label: "10 / " + results.length });
            response(crop);
          } else {
            response(results);
          }
        },
      });
    },
    minLength: 3,
    select: function (event, ui) {
      window.location.href = link + ui.item.value;
      event.preventDefault();
    },
  });

  $("#search-delete").on("click", () => {
    $("#search-input").val("").trigger("keyup");
  });
  $("#search-input").on("keyup", function () {
    if ($("#search-input").val() !== "") {
      $("#search-delete")
        .removeClass("btn-outline-secondary")
        .addClass("btn-secondary");
    } else {
      $("#search-delete")
        .removeClass("btn-secondary")
        .addClass("btn-outline-secondary");
    }
  });
}

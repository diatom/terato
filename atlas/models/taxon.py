from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BTreeIndex
from django.db import connection
from django.db.models import F, Q
from django.utils.functional import cached_property

from .codes import TaxonCode

qValid = Q(freshwater_diatom=True) & Q(is_taxon=True)
qTerato = Q(is_terato=True)
qUnaliased = Q(taxon_alias__id=F("id"))


class TaxonManager(models.Manager):
    def get_queryset(self):
        s = super().get_queryset()
        return s.filter(qValid)


class Taxon(models.Model):

    objects = TaxonManager()

    id = models.AutoField(primary_key=True, db_column="taxon_id")
    taxon_name = models.CharField(db_comment="Name of the taxon")
    taxon_alias = models.ForeignKey(
        "self",
        models.DO_NOTHING,
        related_name="alias",
        blank=True,
        null=True,
        db_comment="Official id of the taxon. The current taxon is a synonym",
        db_column="taxon_official_id",
    )
    taxon_parent = models.ForeignKey(
        "self",
        models.DO_NOTHING,
        related_name="children",
        blank=True,
        null=True,
        db_comment="Id of the parent of the taxonomy",
        db_column="taxonomy_parent_id",
    )
    author_name = models.CharField(blank=True, null=True, db_column="author")
    reference = models.CharField(
        blank=True,
        null=True,
        db_comment="Origin of the description of the taxon",
        db_column="bibliography",
    )
    comment = models.CharField(
        blank=True, null=True, db_comment="Precision on the type of taxon, or other"
    )
    taxonomic_rank = models.ForeignKey(
        "TaxonomicRank",
        models.DO_NOTHING,
        blank=True,
        null=True,
        db_column="taxonomy_type_id",
    )
    is_terato = models.BooleanField(
        blank=True, null=True, default=False, db_comment="Is the taxon a terato form?"
    )
    freshwater_diatom = models.BooleanField(
        blank=True,
        null=True,
        default=True,
        db_comment="True if it is a diatom of fresh water",
    )
    is_taxon = models.BooleanField(
        blank=True,
        null=True,
        default=True,
        db_comment="False if it is not a real taxon",
    )

    class Meta:
        db_table = 'diatom"."taxon'
        managed = False
        indexes = [
            BTreeIndex(fields=["id"], name="taxon_taxon_id_idx"),
            BTreeIndex(fields=["taxon_alias"], name="taxon_taxon_official_id_idx"),
            BTreeIndex(fields=["taxon_parent"], name="taxon_taxonomy_parent_id_idx"),
            BTreeIndex(fields=["taxon_name"], name="taxon_taxon_name_idx"),
        ]

    def __str__(self):
        return self.taxon_name

    @cached_property
    def get_parent_taxonomy(self):
        query = """
            with recursive parents(taxon_id, taxon_name,
                taxonomy_parent_id, taxonomy_type_id) as (
                select taxon_id, taxon_name, taxonomy_parent_id, taxonomy_type_id
                from diatom.taxon where taxon_id=%s
                    and freshwater_diatom is true and is_taxon is true
                union
                    select t.taxon_id, t.taxon_name, t.taxonomy_parent_id,
                    t.taxonomy_type_id
                    from parents p
                    join diatom.taxon t on p.taxonomy_parent_id = t.taxon_id
                    where t.freshwater_diatom is true and t.is_taxon is true
            )
            select p.taxon_id, p.taxon_name, p.taxonomy_parent_id, p.taxonomy_type_id,
            t.taxonomy_type_id as parent_taxonomy_type_id
            from parents p
            join parents t on p.taxonomy_parent_id = t.taxon_id
            where p.taxon_id != %s and p.taxonomy_type_id >= 4
            order by taxonomy_type_id
            """

        with connection.cursor() as cursor:
            cursor.execute(query, [self.id, self.id])
            columns = [col[0] for col in cursor.description]
            res = [dict(zip(columns, row)) for row in cursor.fetchall()]
        return res

    @cached_property
    def get_direct_children(self):
        return (
            Taxon.objects.filter(taxon_parent=self.id)
            .filter(qUnaliased & ~qTerato)
            .filter(
                Q(taxonomic_rank__id__gte=8)
                | Q(id__in=Taxon.objects.values_list("taxon_parent", flat=True))
            )
            .filter(taxonomic_rank=self.taxonomic_rank.id + 1)
        )

    @cached_property
    def get_indirect_children(self):
        return (
            Taxon.objects.filter(taxon_parent=self.id)
            .filter(qUnaliased & ~qTerato)
            .filter(
                Q(taxonomic_rank__id__gte=8)
                | Q(id__in=Taxon.objects.values_list("taxon_parent", flat=True))
            )
            .filter(taxonomic_rank__gt=self.taxonomic_rank.id + 1)
        )

    @cached_property
    def get_aliases_with_codes(self):
        synonymes = Taxon.objects.raw(
            """
            select a.taxon_id, a.taxon_name, a.author,
            b.code as omnidia , c.code as sandre
            from diatom.taxon a
            left outer join diatom.taxon_identifier b
            on a.taxon_id = b.taxon_id and b.identifier_type_id = 1
            left outer join diatom.taxon_identifier c
            on a.taxon_id = c.taxon_id and c.identifier_type_id = 2
            where a.taxon_official_id=%s and a.taxon_id!=a.taxon_official_id
            and a.freshwater_diatom is true and a.is_taxon is true
            """,
            [self.id],
        )
        return synonymes

    @cached_property
    def get_codes(self):
        c = {}
        for co in (
            TaxonCode.objects.filter(taxon_id=self.id)
            .select_related("code_type")
            .order_by("code_type__code_type_name")
        ):
            c.setdefault(co.code_type.code_type_name, []).append(co.code)
        return c

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase


class TestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        User = get_user_model()

        for user in User.objects.all():
            user.set_password(user.password)
            user.save()

    def _fixture_setup(self):
        super()._fixture_setup()

        if self.fixtures and "diatom" in self.fixtures:
            call_command("refresh_mv", "terato.children_diatoms")

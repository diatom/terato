from django.contrib.auth import get_user_model
from django.contrib.messages import get_messages
from django.contrib.messages.storage.fallback import FallbackStorage
from django.core.exceptions import PermissionDenied
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView

from tests import TestCase
from tests.utils import DEFAULT_EMAIL, PHOTO_BYTES, THUMBNAIL_BYTES

from ..models import Diatom, DiatomDefoFactor
from ..views.upload import DiatomFormViewMixin, ModifyFormView, UploadFormView

User = get_user_model()

# config

data = {
    "taxon": 17,
    "health": 1,
    "inventory": "",
    "station": 2,
    "sampling_date": "2024-08-28",
    "scale": 8.73,
    "photo_author": "",
    "photo_doi": "pas_un_vrai_doi",
    "deformation_factors": ["4", "8"],
}

fixtures = [
    "taxonomy_type",
    "taxon",
    "identifier_type",
    "taxon_identifier",
    "health",
    "users",
    "station",
    "defo_factor_cat",
    "defo_factor",
]


class FormViewClassForTesting(DiatomFormViewMixin, FormView):
    def __init__(self):
        # the mixin doesn't create self.object, so let's
        self.object = Diatom()
        self.object.photo = PHOTO_BYTES
        self.object.thumbnail = THUMBNAIL_BYTES
        self.object.filename = "a_file.png"
        self.object.height = 250
        self.object.width = 250
        self.object.user = User.objects.get(email=DEFAULT_EMAIL)

        self.request = RequestFactory().get("/some/url")
        self.request.session = "session"
        self.request._messages = FallbackStorage(self.request)


class TestDiatomFormViewMixin(TestCase):

    fixtures = fixtures

    def setUp(self):

        self.view = FormViewClassForTesting()
        self.form = self.view.form_class(data)
        self.form.full_clean()
        self.view.form_valid(self.form)

    def test_form_valid_creates_objects(self):
        self.assertEqual(1, Diatom.objects.count())
        self.assertEqual(2, DiatomDefoFactor.objects.count())

    def test_form_valid_rejects_invalid_doi(self):

        self.assertEqual(Diatom.objects.get(filename="a_file.png").photo_doi, None)
        messages = [m for m in get_messages(self.view.request)]
        self.assertGreaterEqual(len(messages), 1)
        self.assertEqual(messages[0].level, 30)
        self.assertEqual(messages[0].message, _("AJOUT MSG Error in DOI"))

    def test_form_valid_corrects_terato_taxon(self):
        self.assertEqual(Diatom.objects.get(filename="a_file.png").taxon.id, 16)


class TestUploadFormView(TestCase):

    fixtures = fixtures

    def setUp(self):

        self.view = UploadFormView()
        self.request = RequestFactory().get(reverse("atlas:contrib:diatom_upload"))
        self.request.session = "session"
        self.request._messages = FallbackStorage(self.request)
        self.request.user = User.objects.get(email=DEFAULT_EMAIL)
        self.view.setup(self.request)

        with open("atlas/tests/grid.png", "rb") as img:
            files = {"photo": SimpleUploadedFile(img.name, img.read())}
            form = self.view.form_class(data, files)

        form.full_clean()
        self.view.form_valid(form)

    def test_form_valid_creates_objects(self):
        self.assertEqual(1, Diatom.objects.count())
        self.assertEqual(2, DiatomDefoFactor.objects.count())

    def test_form_valid_sends_message(self):

        messages = [m for m in get_messages(self.view.request)]
        self.assertGreaterEqual(len(messages), 1)
        self.assertEqual(messages[1].level, 25)
        self.assertEqual(messages[1].message, _("AJOUT MSG Diatomée ajoutée"))


class TestModifyFormView(TestCase):

    fixtures = fixtures + ["diatom"]

    def setUp(self):

        self.view = ModifyFormView()
        self.request = RequestFactory()
        self.request.session = "session"
        self.request._messages = FallbackStorage(self.request)
        self.request.user = User.objects.get(email=DEFAULT_EMAIL)
        self.request.method = "GET"
        self.request.COOKIES = {}

    def test_get_object_without_rights(self):
        for d in Diatom.objects.exclude(user=self.request.user):
            self.request.get(reverse("atlas:contrib:diatom_modify", args=[d.pk]))
            self.view.setup(self.request, pk=d.pk)
            self.assertRaises(PermissionDenied, self.view.get_object)

    def test_get_object_with_rights(self):
        for d in Diatom.objects.filter(user=self.request.user):
            self.request.get(reverse("atlas:contrib:diatom_modify", args=[d.pk]))
            self.view.setup(self.request, pk=d.pk)

            self.assertEqual(d, self.view.get_object())

    def test_get_form_kwargs_scale_human(self):
        for d in Diatom.objects.filter(user=self.request.user):
            self.request.get(reverse("atlas:contrib:diatom_modify", args=[d.pk]))
            self.view.setup(self.request, pk=d.pk)
            self.view.object = self.view.get_object()

            if d.scale:
                self.assertEqual(
                    self.view.get_form_kwargs()["initial"]["scale_human"],
                    round(d.scale * d.width / 100, 2),
                )
            else:
                self.assertIsNone(self.view.get_form_kwargs()["initial"]["scale_human"])

    def test_get_form_kwargs_deformation_factors(self):
        for d in Diatom.objects.filter(user=self.request.user):
            self.request.get(reverse("atlas:contrib:diatom_modify", args=[d.pk]))
            self.view.setup(self.request, pk=d.pk)
            self.view.object = self.view.get_object()

            self.assertEqual(
                self.view.get_form_kwargs()["initial"]["deformation_factors"],
                [
                    i.deformation_factor
                    for i in DiatomDefoFactor.objects.filter(
                        diatom__id=self.view.object.id
                    )
                ],
            )

    def test_form_valid_sends_message(self):
        for d in Diatom.objects.filter(user=self.request.user):
            self.request.get(reverse("atlas:contrib:diatom_modify", args=[d.pk]))
            self.view.setup(self.request, pk=d.pk)
            self.view.object = self.view.get_object()

            form = self.view.form_class(data)
            form.full_clean()
            self.view.form_valid(form)

            messages = [m for m in get_messages(self.view.request)]
            self.assertGreaterEqual(len(messages), 1)
            self.assertEqual(messages[1].level, 25)
            self.assertEqual(messages[1].message, _("MODIFY MSG Diatomée modifiée"))

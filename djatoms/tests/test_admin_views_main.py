from django.contrib.auth import get_user_model
from django.test import RequestFactory, override_settings
from django.urls import reverse
from django.utils.translation import gettext as _

from djatoms.admin.views.main import UploadList, UploadQuerySet
from tests import TestCase

from .admin import TestUploadableMA, site_uploadable
from .models import TestModel

User = get_user_model()


class UploadQuerySetTestCase(TestCase):
    """
    We want UploadQuerySet to behave like Django's QuerySet
    """

    fixtures = ["test_model"]

    def setUp(self):
        self.uploaded = UploadQuerySet([], TestModel)

        for i in TestModel.objects.all():
            self.uploaded.append(i)

    def test_upload_queryset_init_attributes(self):
        self.assertTrue(self.uploaded.ordered)
        self.assertEqual(self.uploaded.model, TestModel)

    def test_upload_queryset_count(self):

        self.assertGreater(self.uploaded.count(), 0)
        self.assertEqual(self.uploaded.count(), TestModel.objects.count())

    def test_upload_queryset_filter(self):

        self.assertGreater(
            len([x for x in self.uploaded.filter(pk__in=[3, "4", "6"])]), 0
        )
        self.assertEqual(
            [x for x in self.uploaded.filter(pk__in=[3, "4", "6"])],
            [x for x in TestModel.objects.filter(pk__in=[3, "4", "6"])],
        )

    def test_upload_queryset_get(self):
        for i in TestModel.objects.all():
            self.assertEqual(self.uploaded.get(pk=i.pk), TestModel.objects.get(pk=i.pk))
            self.assertEqual(
                self.uploaded.get(name=i.name), TestModel.objects.get(name=i.name)
            )

    def test_upload_queryset_clone(self):
        self.assertEqual(self.uploaded, self.uploaded._clone())


@override_settings(ROOT_URLCONF="djatoms.tests.urls")
class UploadListTestCase(TestCase):

    fixtures = ["test_model", "users"]

    def setUp(self):
        m = TestUploadableMA(TestModel, site_uploadable)
        # print(site._registry)
        m.uploading = True

        with open("djatoms/tests/upload_file_correct.csv", "r") as file:
            self.request = RequestFactory().post(
                reverse("admin_uploadable:djatoms_tests_testmodel_uploadlist"),
                data={"file": file},
            )
        self.request.user = User.objects.get(email="any@mail.com")

        self.uploadlist = UploadList(
            self.request,
            TestModel,
            m._upload_list_display,
            m._upload_list_display_links,
            m.get_list_filter(self.request),
            m.date_hierarchy,
            m.get_search_fields(self.request),
            m.get_list_select_related(self.request),
            m.list_per_page,
            m.list_max_show_all,
            m._upload_list_editable,
            m,
            m.get_sortable_by(self.request),
            m.search_help_text,
        )
        self.uploadlist.root_queryset = m._upload_get_queryset(self.request)
        # m.get_changelist_instance(self.request)

    def test_upload_list_title(self):
        self.assertEqual(
            self.uploadlist.title, _("ADMIN Téléverser des données compressées")
        )

    def test_upload_list_get_queryset(self):
        self.assertEqual(
            self.uploadlist.root_queryset, self.uploadlist.get_queryset(self.request)
        )
        self.assertEqual(len(self.uploadlist.root_queryset), 3)

    def test_upload_list_get_results(self):
        self.assertTrue(isinstance(self.uploadlist.result_list, UploadQuerySet))

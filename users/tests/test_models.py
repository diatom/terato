from tests import TestCase
from tests.utils import DEFAULT_EMAIL, DEFAULT_FNAME, DEFAULT_LNAME, DEFAULT_PWD

from ..models import User


class UserTestCase(TestCase):
    def test_create_user_without_email_raises_error(self):
        self.assertRaises(ValueError, User.objects.create_user, None, DEFAULT_PWD)

    def test_create_user(self):

        u = User.objects.create_user(
            DEFAULT_EMAIL,
            DEFAULT_PWD,
            first_name=DEFAULT_FNAME,
            last_name=DEFAULT_LNAME,
        )

        self.assertEqual(u.email, DEFAULT_EMAIL)
        self.assertEqual(u.first_name, DEFAULT_FNAME)
        self.assertEqual(u.last_name, DEFAULT_LNAME)
        self.assertTrue(u.check_password(DEFAULT_PWD))
        self.assertFalse(u.is_superuser)
        self.assertFalse(u.is_staff)
        self.assertTrue(u.is_active)

    def test_create_superuser(self):

        u = User.objects.create_superuser(
            DEFAULT_EMAIL,
            DEFAULT_PWD,
            first_name=DEFAULT_FNAME,
            last_name=DEFAULT_LNAME,
        )

        self.assertEqual(u.email, DEFAULT_EMAIL)
        self.assertEqual(u.first_name, DEFAULT_FNAME)
        self.assertEqual(u.last_name, DEFAULT_LNAME)
        self.assertTrue(u.check_password(DEFAULT_PWD))
        self.assertTrue(u.is_superuser)
        self.assertTrue(u.is_staff)
        self.assertTrue(u.is_active)

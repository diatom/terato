import "../css/base_detail.css";
import "../css/detail_taxon.css";

import L from "leaflet";
import { prepareAutocomplete } from "./base_detail";
import { Carousel } from "bootstrap";
import { createMap } from "./leaflet";

/* global gettext */

$(function () {
  prepareAutocomplete();

  const link = JSON.parse(
    document.getElementById("diatom-url").textContent
  ).slice(0, -2);
  const geo_tick = JSON.parse(document.getElementById("geotick").textContent);

  const lang = window.location.href.split("/")[3];
  const taxon = window.location.href.split("/")[5];

  function loadLink(el) {
    var displayed = el.querySelectorAll(".carousel-item.active");
    if (displayed) {
      if (displayed[0] && displayed[0].querySelector("img")) {
        var taxon_link = el.parentNode.querySelector(".taxon-link");
        var id = displayed[0].querySelector("img").src.split("/").slice(-1);
        taxon_link.href = link + id;
      }
    }
  }

  function loadMore(e) {
    if (!e.target.finished) {
      var did = e.target.id.replace("carousel", "");
      var limit = e.target.querySelectorAll(".carousel-item").length - 2;

      var to_include = null;
      var reverse = false;

      if (e.from == 1 && e.direction == "right") {
        to_include = e.target.loaded[0] - 1;
        e.target.loaded[0] = to_include;
        reverse = true;
      } else if (e.from == limit && e.direction == "left") {
        to_include = e.target.loaded[1] + 1;
        e.target.loaded[1] = to_include;
        reverse = false;
      }
      if (to_include) {
        $.ajax({
          url:
            "/" +
            lang +
            "/search/diatom?taxon=" +
            taxon +
            "&health=" +
            did +
            "&page=" +
            to_include,
          dataType: "json",
          success: function (results) {
            fillCarousel(e.target, results, reverse);
          },
        });
      }
    }
  }

  function cleanLoaded(e) {
    loadLink(e.target);
  }

  function fillCarousel(carousel, results, reverse = false) {
    var inner = carousel.querySelector(".carousel-inner");

    for (var d of results) {
      if (d.id != carousel.leftMost && d.id != carousel.rightMost) {
        var el = document.createElement("div");
        el.classList = `carousel-item`;
        el.id = d.id;
        el.innerHTML = `
          <img class="card-img-top crsl-card-image rounded"
            src="/${lang}/generate_thumbnail/${d.id}"
            alt=""
            loading="lazy"/>
          <div class="crsl-card-image-caption">
              <p class="mb-1">${d.taxon}</p>
          </div>
        `;
        if (reverse) {
          inner.insertBefore(el, inner.firstChild);
          carousel.leftMost = d.id;
        } else {
          inner.appendChild(el);
          carousel.rightMost = d.id;
        }
      } else {
        carousel.finished = true;
        break;
      }
    }
  }

  $("div[id^=carousel]").each(function () {
    var carousel = $(this);

    var nodes = carousel[0].querySelectorAll(".carousel-item");
    if (nodes.length) {
      new Carousel(carousel);
      carousel[0].leftMost = parseInt(nodes[0].id);
      carousel[0].rightMost = parseInt(nodes[nodes.length - 1].id);
    }

    carousel[0].loaded = [0, 1];
    carousel[0].finished = false;

    loadLink(carousel[0]);

    carousel[0].addEventListener("slide.bs.carousel", loadMore);
    carousel[0].addEventListener("slid.bs.carousel", cleanLoaded);
  });

  if (document.getElementById("data-url")) {
    function pointToLayer(feature, latlng) {
      var div = document.createElement("div");
      if (feature.properties.diatom__count) {
        div.innerHTML += `<b>${gettext("AJOUT CARTE Nombre d'ajouts")}</b> : `;
        div.innerHTML += feature.properties.diatom__count;
        div.innerHTML += "<br>";
      }
      div.innerHTML += `<b>${gettext("AJOUT CARTE Station name")}</b> : `;
      div.innerHTML += feature.properties.station_name;
      div.innerHTML += "<br>";
      if (feature.properties.commune) {
        div.innerHTML += `<b>${gettext("AJOUT CARTE Commune")}</b> : `;
        div.innerHTML += feature.properties.commune;
        div.innerHTML += "<br>";
      }
      if (feature.properties.station_code) {
        div.innerHTML += `<b>${gettext("AJOUT CARTE Code Sandre")}</b> : `;
        div.innerHTML += feature.properties.station_code;
        div.innerHTML += "<br>";
      }

      var html =
        "<img src='" + geo_tick + "' class='base-color' height=32 width=32>";
      html +=
        "<span class=number-label>" +
        feature.properties.diatom__count +
        "</span>";
      var icon = L.divIcon({
        className: "custom-icon d-flex justify-content-center",
        html: html,
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -25],
      });

      var m = new L.Marker(latlng, { icon: icon });
      m.bindPopup(div);
      return m;
    }

    var map = createMap(taxon, pointToLayer, 6, 5);

    var myCollapsible = document.getElementById("mapCollapse");
    myCollapsible.addEventListener("shown.bs.collapse", function () {
      map.invalidateSize(true);
    });
  }
});

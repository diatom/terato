from csp.decorators import csp_update
from django.contrib import messages
from django.contrib.postgres.aggregates import StringAgg
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, TemplateView
from django_datatables_view.base_datatable_view import BaseDatatableView
from modeltranslation.utils import get_language

from ..models import DiatomChildren, Health, Taxon, TaxonomicRank


class TaxonListView(TemplateView):
    template_name = "taxon_list.html"


class TaxonJson(BaseDatatableView):
    model = Taxon

    # define the columns that will be returned
    columns = [
        "id",
        "taxon_name",
        "author_name",
        "codes_tostring",
        "taxon_parent.taxon_name",
        "taxon_alias.taxon_name",
        "taxonomic_rank.rank",
        "diatom_count",
    ]

    # define column names that will be used in sorting
    order_columns = [
        "id",
        "taxon_name",
        "author_name",
        "codes_tostring",
        "taxon_parent.taxon_name",
        "taxon_alias.taxon_name",
        "taxonomic_rank.rank",
        "diatom_count",
    ]

    # set max limit of records returned
    max_display_length = 500

    # indicate which column(s) refer to model translation
    translated_columns = ["taxonomic_rank.rank"]

    def get_context_data(self, *args, **kwargs):
        ret = super().get_context_data(*args, **kwargs)
        if int(self._querydict.get("draw", 0)) == 1:
            ret["ranks"] = [
                rank.rank for rank in TaxonomicRank.objects.filter(id__gte=4)
            ]
        return ret

    def get_filter_method(self):
        """Returns preferred filter method"""
        return self.FILTER_ICONTAINS

    def get_initial_queryset(self):
        return (
            self.model.objects.annotate(diatom_count=Count("diatom", distinct=True))
            .annotate(
                codes_tostring=StringAgg(
                    "taxoncode__code",
                    delimiter=", ",
                    ordering="-taxoncode__code",
                    distinct=True,
                )
            )
            .select_related("taxon_parent", "taxon_alias", "taxonomic_rank")
        )

    def get_columns(self):
        columns = super().get_columns()

        if self.translated_columns:
            for col in self.translated_columns:
                for c in columns:
                    if c.startswith(col):
                        index = columns.index(c)
                        columns[index] = col + "_" + get_language()

        return columns

    def filter_queryset(self, qs):

        qs = super().filter_queryset(qs)

        switch = self._querydict.get("switch", None)
        if switch == "true":
            qs = qs.filter(diatom_count__gt=0)
        return qs


class TaxonDetailView(DetailView):
    model = Taxon
    template_name = "detail_taxon.html"

    def __init__(self):
        super().__init__()
        self._object = None
        self._orig_obj = None

    def get_object(self):

        if not self._object:
            self._orig_obj = super().get_object()
            if self._orig_obj.is_terato:
                self._object = self._orig_obj.taxon_parent
            elif self._orig_obj.taxon_alias != self._orig_obj:
                self._object = self._orig_obj.taxon_alias
            else:
                self._object = self._orig_obj

        return self._object

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.select_related("taxonomic_rank", "taxon_alias")

    def render_to_response(self, context, **response_kwargs):
        if self._orig_obj == self._object:
            return super().render_to_response(context, **response_kwargs)
        elif self._object == self._orig_obj.taxon_alias:
            messages.info(self.request, _("TAXON MSG Redirection vers valide"))
            return HttpResponseRedirect(
                reverse("atlas:consult:detail_taxon", args=[self._object.id])
            )
        elif self._object == self._orig_obj.taxon_parent:
            return HttpResponseRedirect(
                reverse("atlas:consult:detail_taxon", args=[self._object.id])
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        qs = DiatomChildren.objects.filter(ancestor=self._object.id)

        context["health"] = {}
        for h in Health.objects.all():
            context["health"][h] = (
                qs.filter(health=h.id)
                .order_by("-upload_date")[:10]
                .values_list("diatom", "taxon")
            )
        context["count"] = qs.count()
        context["count_located"] = qs.exclude(diatom__station__isnull=True).count()
        return context

    @method_decorator(
        csp_update(
            IMG_SRC=[
                "'self'",
                "data:",
                "https://tile.openstreetmap.org",
                "https://*.google.com",
            ]
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

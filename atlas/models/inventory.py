from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BTreeIndex


class Inventory(models.Model):
    inventory_id = models.AutoField(primary_key=True)
    inventory_date = models.DateTimeField(
        blank=True, null=True, db_comment="Date of the inventory"
    )
    inventory_year = models.SmallIntegerField(
        blank=True, null=True, db_comment="Year of the inventory"
    )
    inventory_type = models.ForeignKey("InventoryType", models.DO_NOTHING)
    station_local_code = models.CharField(
        blank=True, null=True, db_comment="Local codification of the site"
    )
    station = models.ForeignKey("Station", models.DO_NOTHING)

    class Meta:
        db_table = 'diatom"."inventory'
        db_table_comment = "List of inventories of diatom"
        managed = False

        indexes = [
            BTreeIndex(fields=["inventory_date"], name="inventory_inventory_date_idx"),
            BTreeIndex(fields=["station"], name="inventory_station_id_idx"),
        ]


class InventoryType(models.Model):
    inventory_type_id = models.IntegerField(primary_key=True)
    inventory_type_name = models.CharField()

    class Meta:
        db_table = 'diatom"."inventory_type'
        managed = False


class InventoryTaxon(models.Model):
    inventory_taxon_id = models.AutoField(primary_key=True)
    abundance = models.IntegerField(
        blank=True, null=True, db_comment="Number of individus found"
    )
    taxon = models.ForeignKey("Taxon", models.DO_NOTHING)
    inventory = models.ForeignKey("Inventory", models.DO_NOTHING)

    class Meta:
        db_table = 'diatom"."inventory_taxon'
        db_table_comment = "List of taxa found in the inventory"
        managed = False

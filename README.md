# Teratotheca

[![Coverage Status](./coverage-badge.svg?dummy=8484744)](https://coverage.readthedocs.io/en/7.6.0/)

This web app aims to document diatom teratologies and allow its users to upload teratologies pictures.

# New in version 24.1.0

<ul>
    <li>Ability to delete one's account;</li>
    <li>Display map of contributions, per taxon;</li>
    <li>Improved test suites using RequestFactory;</li>
    <li>Taxa data fixes.</li>
</ul>

# Installation

Clone the repository, then go in:

```bash
git clone https://forgemia.inra.fr/diatom/terato.git
cd terato
```

## Setup: Python environment

Python 3.10 is recommended. If you have another version installed, pipenv will install Python 3.10 inside the virtual environment.

Run the following commands to install the virtual environment.

<b>WARNING</b>: installing Python package `psycopg2` may need manual installation of pre-requisites: https://www.psycopg.org/docs/install.html#build-prerequisites

```bash
pipenv install
```
Launch the virtual environment with
```bash
pipenv shell
```

## Setup: Django

At startup, Django checks for settings, in our case in [/main/settings.py](/main/settings.py). However, a few, sensitive settings are defined as environment variables. It is up to you to define them for your installation.

To do so, start by creating the `.env` file in project root. We provide `.env.example` as a template.

To generate a `SECRET_KEY`, run in a Python shell:

```python
from django.core.management.utils import get_random_secret_key
print(get_random_secret_key())
```

## Setup: Postgresql database

Make sure to install PostgreSQL 14+. Then, create your database on PostgreSQL.

```SQL
CREATE DATABASE database_name;
\c database_name ;
CREATE EXTENSION postgis ;
```

Create your user(s) and grant necessary permissions.

```SQL
CREATE USER user WITH ENCRYPTED PASSWORD 'password';
ALTER ROLE user SET client_encoding TO 'utf8';
ALTER ROLE user SET default_transaction_isolation TO 'read committed';
ALTER ROLE user SET timezone TO 'UTC';
ALTER ROLE user CREATEDB;
```

Grant privileges and ownership on your DB to your user(s).

```SQL
GRANT ALL PRIVILEGES ON DATABASE database_name TO user;
ALTER DATABASE database_name OWNER TO user;
ALTER SCHEMA public OWNER TO user;
ALTER TABLE spatial_ref_sys OWNER TO user;
```

We had to work with a legacy database: as such, we chose to not let Django manage our tables in the `atlas` app. That's why all models in atlas are marked as unmanaged (`managed=False`). To recreate the corresponding database structure, please run: <code>psql -U user -d database_name -f db/create_db_24.0.sql</code>.

We also provide, in [db/populate_db_24.0](db/populate_db_24.0), scripts to populate the most necessary tables in the database. If used, those scripts should be run in this order:
<ol>
<li>populate_utils.sql</li>
<li>populate_taxon.sql</li>
<li>populate_taxon_identifier.sql</li>
<li>populate_station.sql</li>
</ol>

Finally, fake migrations:
```bash
python manage.py migrate --fake
```

## Setup: django-admin

To access the site admin, you will need to create a superuser:
```bash
python manage.py createsuperuser
```

## Setup: Webpack

Webpack is an asset bundler: given a set of .js, .css, .sass... files with module dependencies, Webpack resolves them and provides optimized files to serve.

To install Webpack, make sure a recent version of Node.js (v18+) is installed. Then, install all necessary modules by running :

```bash
npm ci
```

Then, create the webpack bundles in the folder `STATIC_URL` :
```bash
npx webpack
```

## Setup: pre-commit

A pre-commit hook has been defined to ensure code cleanliness and consistency. Have a look at [.pre-commit-config.yaml](.pre-commit-config.yaml) to understand which hooks are used, and at [.eslintrc.yml](.eslintrc.yml) and [pyproject.toml](pyproject.toml) for more rules.

Only ESLint needs to be configured by running :

```bash
npx eslint
```

# Development

For <b>development</b> purposes only, Django provides an utility to run the project - don't forget to reactive the virtual environment:

```bash
python manage.py runserver
```

You may also want Webpack to recreate the static files each time they are modified:
```bash
npx webpack --watch
```

# Serving

For <b>deployment</b>, please refer to the Django guidelines :
<ul>
<li>[docs.djangoproject.com/en/dev/howto/deployment/](https://docs.djangoproject.com/en/5.0/howto/deployment/)</li>
<li>[docs.djangoproject.com/en/dev/howto/static-files/deployment/](https://docs.djangoproject.com/en/5.0/howto/static-files/deployment/)</li>
</ul>

As for us, we chose to deploy using Docker and Apache; the detailed steps are described in deploiementApache.md

# Testing & coverage report :

```bash
coverage run manage.py test
coverage html
```

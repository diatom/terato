from django.test import SimpleTestCase
from django.urls import resolve, reverse
from django.views.generic import TemplateView


class TestUrls(SimpleTestCase):
    def test_home_url_resolves(self):
        url = reverse("generic:home")
        self.assertEqual(resolve(url).func.view_class, TemplateView)

    def test_cgu_url_resolves(self):
        url = reverse("generic:cgu")
        self.assertEqual(resolve(url).func.view_class, TemplateView)

    def test_legal_notice_url_resolves(self):
        url = reverse("generic:legals")
        self.assertEqual(resolve(url).func.view_class, TemplateView)

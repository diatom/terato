from django.contrib.gis.db import models


class TaxonomicRank(models.Model):
    id = models.AutoField(primary_key=True, db_column="taxonomy_type_id")
    rank = models.CharField(db_column="taxonomy_type_name")

    class Meta:
        db_table = 'diatom"."taxonomy_type'
        db_table_comment = "Levels of taxonomy"
        managed = False

    def __str__(self):
        return self.rank

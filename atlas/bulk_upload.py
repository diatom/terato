import csv
import zipfile
from datetime import datetime
from io import BytesIO

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.utils.dateparse import parse_date
from django.utils.translation import gettext_lazy as _
from PIL import Image, UnidentifiedImageError

from djatoms.admin.views import UploadQuerySet
from users.models import User

from .models import Diatom, Health, Station, TaxonCode


def check_uploads(modeladmin, request):

    uploaded = zipfile.ZipFile(request.FILES["file"])

    csvs = []
    ignored = 0
    pictures = 0
    msgs = []

    for n in uploaded.namelist():
        if n.lower().endswith(".csv"):
            csvs.append(n)
        elif any(
            [
                n.lower().endswith(ext)
                for ext in ["png", "jpg", "jpeg", "tif", "tiff", "bmp"]
            ]
        ):
            pictures += 1
        elif not n.endswith("/"):
            ignored += 1

    uploaded.close()

    # Raise error if no unique csv
    if not csvs:
        msgs.append(_("UPLOAD MSG No csv file provided during upload."))
    elif len(csvs) > 1:
        msgs.append(_("UPLOAD MSG More than one csv file provided during upload."))

    # Raise error if no pictures
    if not pictures:
        msgs.append(
            _(
                "UPLOAD MSG Found no file with a valid format. Accepted \
            formats are .png, .jpg, .jpeg, .tif, .tiff and .bmp."
            )
        )
    if msgs:
        for msg in msgs:
            modeladmin.message_user(request, msg, messages.ERROR)
        return None

    # Raise warning if some files are not pictures
    if ignored:
        msg = (
            _(
                "UPLOAD MSG Found %(count)s files with invalid format. \
                Accepted formats are .png, .jpg, .jpeg, .tif, .tiff \
                and .bmp."
            )
            % {"count": ignored}
        )
        modeladmin.message_user(request, msg, messages.WARNING)

    return csvs[0]


def get_upload_queryset(modeladmin, request):

    csvfile = check_uploads(modeladmin, request)

    if not csvfile:
        return Diatom.objects.none()

    uploaded = zipfile.ZipFile(request.FILES["file"])
    root = "/".join(csvfile.split("/")[:-1])
    if root:
        root += "/"
    reader = csv.DictReader(uploaded.read(csvfile).decode("utf-8").splitlines())

    queryset = UploadQuerySet([], model=modeladmin.model)

    count = 1
    total = errs = 0

    if Diatom.objects.count():
        base_id = Diatom.objects.all().latest("id").id

    for row in reader:
        total += 1

        path = root + row["photo"]

        try:
            image = Image.open(uploaded.open(path))
            if not image.format.lower() in ["png", "jpg", "jpeg", "tif", "tiff", "bmp"]:
                continue

            output_format = image.format
            if any([image.format.lower() == x for x in ["bmp", "tif", "tiff"]]):
                output_format = "PNG"

            thumbnail = Image.open(uploaded.open(path))
            thumbnail.thumbnail((250, 250))

            with BytesIO() as output:
                image.save(output, format=output_format)
                binary = output.getvalue()
                thumbnail.save(output, format="PNG")
                thumbnail = output.getvalue()

        except KeyError:
            errs += 1
            continue
        except UnidentifiedImageError:
            continue

        d = Diatom(
            id=base_id + count,
            photo=binary,
            photo_thumbnail=thumbnail,
            height=image.height,
            width=image.width,
        )

        ops = [
            ("photo", "filename", lambda x: x.split("/")[-1], None),
            ("user", "user", lambda x: User.objects.get(email=x), request.user),
            ("taxon", "taxon", lambda x: TaxonCode.objects.get(code=x).taxon, None),
            ("health", "health", lambda x: Health.objects.get(health_abb=x), None),
            ("station", "station", lambda x: Station.objects.get(station_code=x), None),
            ("photo_author", "photo_author", lambda x: x, None),
            ("photo_doi", "photo_doi", lambda x: x, None),
            ("scale", "scale", lambda x: float(x), None),
            (
                "sampling_date",
                "sampling_date",
                lambda x: datetime.combine(parse_date(x), datetime.min.time()),
                None,
            ),
        ]

        for col, field, func, err in ops:
            try:
                setattr(d, field, func(row[col]))
            except (ObjectDoesNotExist, KeyError, ValueError, TypeError):
                setattr(d, field, err)

        queryset.append(d)
        count += 1

    if errs:
        msg = (
            _(
                "UPLOAD MSG For %(errors)s paths (total : %(total)s),\
                    no corresponding file was found"
            )
            % {"errors": errs, "total": total}
        )
        modeladmin.message_user(request, msg, messages.WARNING)

    uploaded.close()
    return queryset

from tests import CSPTestMixin, TestCase
from tests.utils import DEFAULT_EMAIL, DEFAULT_PWD


class CSPTestCase(CSPTestMixin, TestCase):

    fixtures = [
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "station",
        "defo_factor_cat",
        "defo_factor",
        "diatom",
    ]

    def setUp(self):
        self.client.login(username=DEFAULT_EMAIL, password=DEFAULT_PWD)

    def test_csp_diatom_upload(self):

        expected = {
            "connect-src": ["'self'", "blob:"],
            "img-src": [
                "'self'",
                "blob:",
                "data:",
                "https://tile.openstreetmap.org",
                "https://*.google.com",
            ],
            "default-src": ["'self'"],
        }

        self._csp_test("atlas:contrib:diatom_upload", expected)

    def test_csp_diatom_modify(self):

        expected = {
            "img-src": [
                "'self'",
                "data:",
                "https://tile.openstreetmap.org",
                "https://*.google.com",
            ],
            "default-src": ["'self'"],
        }

        self._csp_test("atlas:contrib:diatom_modify", expected, [1])

    def test_csp_taxon_detail(self):
        expected = {
            "img-src": [
                "'self'",
                "data:",
                "https://tile.openstreetmap.org",
                "https://*.google.com",
            ],
            "default-src": ["'self'"],
        }

        self._csp_test("atlas:consult:detail_taxon", expected, [1])

    def test_csp_all_other(self):

        self._csp_test_all_other(["diatom_upload", "diatom_modify", "detail_taxon"])

from django.forms import CharField, EmailField, Form
from django.template import Context, Template
from django.test import SimpleTestCase, TestCase

from ..forms import UserCreationForm, UsersFormMixin

# See tests for base auth forms :
# https://github.com/django/django/blob/main/tests/auth_tests/test_forms.py
# The following tests only test overridden funcitonalities


class UserCreationFormTestCase(TestCase):

    form_class = UserCreationForm

    def test_form_fields(self):
        form = self.form_class()
        self.assertEqual(
            list(form.fields.keys()),
            ["first_name", "last_name", "email", "password1", "password2", "cgu"],
        )

    def test_form_with_validated(self):
        data = {
            "first_name": "John",
            "last_name": "Canari",
            "email": "john.canari@here.com",
            "password1": "testpwd456",
            "password2": "testpwd456",
            "cgu": 1,
        }

        form = self.form_class(data=data)
        form.is_valid()
        self.assertTrue(form.is_valid())

    def test_form_layout_has_all_fields(self):
        form = self.form_class()
        rendered_fields = [t.name for t in form.helper.layout.get_field_names()]
        self.assertEqual(list(form.fields.keys()), rendered_fields)

    def test_form_layout_renders_cgu(self):
        form = self.form_class()
        self.assertEqual(
            "J'ai lu et j'accepte les <a href=/fr/cgu>\
conditions générales d'utilisation</a>",
            form.fields["cgu"].label,
        )


class TestingFormClass(UsersFormMixin, Form):

    button_text = "text_for_button"

    char = CharField()
    email = EmailField()


class UsersFormMixinTest(SimpleTestCase):
    def test_form_layout_has_all_fields(self):

        form = TestingFormClass()
        rendered_fields = [t.name for t in form.helper.layout.get_field_names()]
        self.assertEqual(list(form.fields.keys()), rendered_fields)

    def test_form_layout_has_custom_button(self):
        form = TestingFormClass()

        template = Template(
            """
            {% load crispy_forms_tags %}
            {% crispy form %}
            """
        )
        c = Context({"form": form})
        html = template.render(c)

        self.assertEqual(html.count('value="{}"'.format(form.button_text)), 1)

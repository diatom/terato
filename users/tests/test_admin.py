from django.contrib.admin.utils import lookup_field
from django.contrib.auth import get_user_model
from django.urls import reverse

from atlas.models import Diatom
from tests import TestCase
from tests.utils import DEFAULT_PWD

User = get_user_model()


class UserAdminTestCase(TestCase):

    fixtures = [
        "station",
        "taxonomy_type",
        "taxon",
        "identifier_type",
        "taxon_identifier",
        "health",
        "users",
        "diatom",
    ]

    def setUp(self):

        User.objects.create_superuser("admin@admin.fr", DEFAULT_PWD)

        self.client.login(username="admin@admin.fr", password=DEFAULT_PWD)

    def test_user_admin_shows_contrib_number(self):

        response = self.client.get(reverse("admin:users_user_changelist"))

        cl = response.context["cl"]

        for result in cl.result_list:
            f, attr, value = lookup_field(
                "nb_contribs", result, model_admin=cl.model_admin
            )

            self.assertEqual(
                value, len(Diatom.objects.filter(user__email=result.email))
            )

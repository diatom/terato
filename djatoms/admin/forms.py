from django import forms
from django.core.exceptions import ValidationError


class BaseUploadForm(forms.Form):

    file = forms.FileField(
        widget=forms.FileInput(
            attrs={
                "accept": "application/zip"
                # , application/x-7z-compressed,
                # application/x-tar,
                # application/x-rar-compressed"
            }
        ),
    )


class UploadModelFormMixin:

    """
    Mixin used to build the forms passed to uploadlist_view.
    Overwrites _clean_fields method to avoid checking the ids
    against the database, since the objects do not exist in the
    database yet.
    """

    def _clean_fields(self):
        for name, bf in self._bound_items():
            field = bf.field
            value = bf.initial if field.disabled else bf.data

            if name == "id":
                # do not validate ids against database
                self.cleaned_data[name] = int(value)
                continue
            try:
                if isinstance(field, forms.FileField):
                    value = field.clean(value, bf.initial)
                else:
                    value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, "clean_%s" % name):
                    value = getattr(self, "clean_%s" % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self.add_error(name, e)

    def _get_validation_exclusions(self):
        # do not validate ids uniqueness
        exclude = super()._get_validation_exclusions()
        exclude.add("id")
        return exclude

    def has_changed(self):
        """Always act as if data has changed"""
        return True

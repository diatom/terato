��    M      �  g   �      �  �   �  |   B  k   �  g   +     �     �     �  #   �  "   �  "   	     >	     Y	     u	  7   �	     �	     �	     �	  
   �	  	   �	     �	     
     
     (
     D
      _
     �
  %   �
     �
     �
  
   �
  	   �
          	     )     H  +   `     �     �     �  #   �     �          /     L     k  !   �     �     �     �     �     �      
     +     >     \     r     �      �     �  #   �     �          "     @     X     h  #   �     �     �     �     �          $     8     O     T  B  Z  �   �  p   $  k   �  C        E     X     _  n   g  t   �  a   K  A   �  /   �  >     >   ^     �     �     �  
   �     �     �  	   �     �     �       6   $     [  h   n     �     �  
   �  	   �  	   	  
             $     +  a   >     �  V   �  :   �     5     U  ~   n     �     �     	          "     5     C     P     \     n     v     �     �  
   �     �     �     �  6   �       &     V   >     �  T   �  1   �       '   +     S     _     l     u     �     �     �             )                  C   .              E         ;   !   >   <   9                 B   3   "      K   D       ?       5   4   I      M                  F   	         L           -   '   @      0       %         (   J   1              H   +   7   A   /             =       
                &   8            6                  ,             :   $          #   *   2      G    
                                    Vos contributions apparaîtront ici, dès lors que vous aurez <a href="%(link_upload)s">ajouté une diatomée</a>.
                                 
        You're receiving this email because you requested a password reset for your user account at %(site_name)s.
         
        You're receiving this email because you requested an user account creation at %(domain)s.
         ADMIN USER You cannot delete a superuser account.

                        Please renew your selection. Account activation DELETE BUTTON Annuler DELETE BUTTON Supprimer DELETE CHOICE Anonymiser mes ajouts DELETE CHOICE Conserver mes ajouts DELETE CHOICE Supprimer mes ajouts DELETE MSG Account deleted DELETE MSG Account inactive DELETE MSG All deleted DELETE MSG Choisissez une option pour vos contributions DELETE Title Date joined Email First name Is active Is staff Is superuser LOGIN BUTTON Se connecter LOGIN MSG Connexion requise LOGIN MSG Créer un compte LOGIN MSG Identifiants invalides LOGIN MSG Mot de passe oublié LOGIN MSG Niveau d'accès insuffisant LOGIN MSG Pas encore de compte LOGIN Title Last login Last name Nom PASSWORD BUTTON Envoyer le mail PASSWORD BUTTON Réinitialiser PASSWORD BUTTON Valider PASSWORD MSG Demander une réinitialisation PASSWORD MSG Email sent PASSWORD MSG Hi PASSWORD MSG Lien invalide PASSWORD MSG Lien réinitialisation PASSWORD MSG Password changed PASSWORD MSG Thanks for using PASSWORD MSG enter your mail PASSWORD TITLE Change password PASSWORD TITLE Email Sent PASSWORD TITLE Forgotten Password PASSWORD TITLE Reset PROFILE MSG Changer mdp PROFILE MSG Email PROFILE MSG Nom PROFILE MSG Prénom PROFILE MSG Supprimer mon compte PROFILE TAB Profil PROFILE TAB Vos contributions PROFILE TITLE Bonjour PROFILE TITLE Profil Prénom REGISTER BUTTON Créer un compte REGISTER CHECK CGU REGISTER CHECK J'ai lu et j'accepte REGISTER MSG Account activated REGISTER MSG Connectez-vous REGISTER MSG Déjà un compte REGISTER MSG Email sent REGISTER MSG Hi REGISTER MSG Lien invalide REGISTER MSG Lien réinitialisation REGISTER MSG Recommencer REGISTER MSG Thanks for using REGISTER TITLE Email Sent REGISTER TITLE Lien invalide REGISTER Title The %(domain)s team The %(site_name)s team User Users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Your uploads will be displayed here, as soon as you'll <a href="%(link_upload)s">contribute</a>.
                                     
You're receiving this email because you requested a password reset for your user account at %(site_name)s.
     
        You're receiving this email because you requested an user account creation at %(domain)s.
         You cannot delete a superuser account. Please renew your selection. Account activation Cancel Confirm Anonymise your contributions: your account will be deleted, your contributions will now be shown as anonymous; Keep your contributions: your account will be kept and made inactive, your name still shown with your contributions; Delete your contributions: your account will be deleted, as well as all associated contributions. Your account has been deleted, and your contributions anonymised. Your account has successfully been deactivated. Your account and contributions have successfully been deleted. You are about to delete your account. Please choose an option: Delete my account Date joined Email First name Active account Staff member Superuser Log in Please login to see this page. Create one. Your email or password didn't match. Please try again. Forgotten password Your account doesn't have access to this page. To proceed, please login with an account that has access. Don't have an account? Login Last login Last name Last Name Send email Reset Change request a new one. We've emailed you instructions for resetting your password. You should receive the email shortly! Hi The password reset link was invalid, possibly because it has already been used. Please Please go to the following page and choose a new password: Your password has been changed! Thanks for contributing! Enter your email address below. If it is associated with an active account, we'll send you instructions for setting a new one. Change password Email sent! Forgotten password Reset Change my password Email adress: Family name: First name: Delete my account Profile Your uploads Welcome Profile First Name Register general terms of use I have read and I accept the  Your account has successfully been validated. Welcome! login If you already have an account, please We've emailed you a validation link. Follow this link to end the registration process. Hi The registration link was invalid, possibly because it has already been used. Please Please follow this link to end your registration: start again Thanks and see you soon on our website! Email sent! Invalid link Register The %(domain)s team The %(site_name)s team User Users 
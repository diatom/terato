from csp.decorators import csp_update
from django.contrib import messages
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import FormView

from .forms import ContactForm, FlagForm


class ContactView(FormView):

    template_name = "contact.html"
    form_class = ContactForm
    success_url = reverse_lazy("contact:contact")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        if self.request.user and self.request.user.is_authenticated:
            user = self.request.user
            kwargs.update(
                initial={
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "email": user.email,
                }
            )
        return kwargs

    def form_valid(self, form):

        email = EmailMessage(
            subject="[contact] Nouvelle demande de {}".format(
                (
                    form.cleaned_data["first_name"]
                    + " "
                    + form.cleaned_data["last_name"]
                ),
            ),
            body=form.cleaned_data["message"],
            from_email="web-terato@inrae.fr",
            to=["web-terato@inrae.fr"],
            reply_to=[form.cleaned_data["email"]],
        )

        email.send()

        messages.success(self.request, _("CONTACT MSG Mail envoyé"))
        return redirect(self.get_success_url())

    @method_decorator(
        csp_update(
            IMG_SRC=[
                "'self'",
                "data:",
            ],
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class FlagView(FormView):

    template_name = "flag.html"
    form_class = FlagForm
    success_url = reverse_lazy("users:profile")

    def form_valid(self, form):

        link = reverse_lazy(
            "atlas:consult:detail_diatom", args=[form.cleaned_data["diatom"].id]
        )
        raison = dict(form.fields["flag"].choices)[form.cleaned_data["flag"]]
        message = render_to_string(
            "flag_email.html",
            {
                "user": self.request.user,
                "link": link,
                "raison": raison,
                "message": form.cleaned_data["message"],
            },
        )

        email = EmailMessage(
            subject="[signal] Nouveau signalement",
            body=message,
            from_email="web-terato@inrae.fr",
            to=["web-terato@inrae.fr"],
            reply_to=[self.request.user.email],
        )

        email.send()

        messages.success(self.request, _("CONTACT MSG Mail envoyé"))
        return redirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        return redirect(
            reverse_lazy("atlas:consult:detail_diatom", args=[kwargs["pk"]])
        )

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from visits.models import Error, Request

REQUEST_DELETION_DAYS = getattr(settings, "REQUEST_DELETION_DAYS", 90)
ERROR_DELETION_DAYS = getattr(settings, "ERROR_DELETION_DAYS", 365)


class Command(BaseCommand):
    help = "Deletes old requests and errors, according to settings"

    def handle(self, *args, **options):
        request_past = timezone.datetime.today() - timezone.timedelta(days=90)
        Request.objects.filter(request_time__lte=request_past).delete()

        error_past = timezone.datetime.today() - timezone.timedelta(days=365)
        Error.objects.filter(error_time__lte=error_past).delete()

        # success message
        self.stdout.write("Successfully removed all old requests and errors.")

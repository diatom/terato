from django.db.models.fields.related import RelatedField
from django.forms.models import ModelChoiceField, ModelMultipleChoiceField

from .widgets import AutocompleteModelChoiceInput


class TaggedModelFieldMixin:

    """
    A mixin to organize model choices by tag

    queryset: objects to display as options
    tag_field: model field containing the tag for each object
    """

    def __init__(self, queryset, tag_field, **kwargs):
        self.tag_field = tag_field
        super().__init__(queryset, **kwargs)

    def _get_choices(self):

        choices = []

        _tag_field = getattr(self.queryset.model, self.tag_field).field

        if issubclass(type(_tag_field), RelatedField):
            categories = _tag_field.related_model.objects.all()
        else:
            # Untested
            categories = self.queryset.values_list(self.tag_field, flat=True)
        for cat in categories:
            choice = []
            sub_queryset = self.queryset.filter(**{self.tag_field: cat})
            for obj in sub_queryset:
                choice.append((obj.pk, obj))
            choices.append((cat, choice))

        return choices

    # For compatibility with django-modeltranslation
    def _set_queryset(self, queryset):
        self._queryset = None if queryset is None else queryset.all()
        self.widget.choices = self._get_choices

    queryset = property(ModelMultipleChoiceField._get_queryset, _set_queryset)


class TaggedModelChoiceField(TaggedModelFieldMixin, ModelChoiceField):
    pass


class TaggedModelMultipleChoiceField(TaggedModelFieldMixin, ModelMultipleChoiceField):
    pass


class AutocompleteModelChoiceField(ModelChoiceField):
    widget = AutocompleteModelChoiceInput

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.widget.model = self.queryset.model

from django.urls import path

from .views import (
    DeleteProfileView,
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
    ProfileView,
    RegistrationConfirmView,
    RegistrationDoneView,
    RegistrationView,
)

app_name = "users"

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("profile/", ProfileView.as_view(), name="profile"),
    path("register/", RegistrationView.as_view(), name="register"),
    path("register/done", RegistrationDoneView.as_view(), name="register_done"),
    path(
        "register/confirm/<uidb64>/<token>/",
        RegistrationConfirmView.as_view(),
        name="register_confirm",
    ),
    path(
        "password_reset/",
        PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "password_reset/done/",
        PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path("password_change/", PasswordChangeView.as_view(), name="password_change"),
    path("delete/", DeleteProfileView.as_view(), name="delete"),
]

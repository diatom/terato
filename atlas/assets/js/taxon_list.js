import "datatables.net-responsive-bs5";
import "datatables.net-fixedheader-dt";
import language_fr from "datatables.net-plugins/i18n/fr-FR.mjs";
import language_en from "datatables.net-plugins/i18n/en-GB.mjs";

import "datatables.net-bs5/css/dataTables.bootstrap5.css";
import "datatables.net-responsive-bs5/css/responsive.bootstrap5.css";
import "datatables.net-fixedheader-dt/css/fixedHeader.dataTables.css";
import "../css/taxon_list.css";

/* global gettext */

$(document).ready(function () {
  const x_path = JSON.parse(document.getElementById("xpath").textContent);

  // get language
  var lang = window.location.href.split("/")[3];
  var lang_param = lang == "en" ? language_en : language_fr;

  // initialize params dict
  var myData = { switch: false };

  // initialize datatable
  var dt = $("#table_taxon").DataTable({
    ajax: {
      url: "/" + lang + "/taxons_json/",
      data: function (d) {
        return $.extend(d, myData);
      },
    },
    responsive: true,
    processing: true,
    serverSide: true,
    fixedHeader: {
      headerOffset: $("#mainNav").outerHeight(false),
    },
    language: lang_param,
    columnDefs: [
      {
        target: 0,
        visible: false,
        searchable: false,
      },
      {
        target: 4,
        visible: false,
        searchable: false,
      },
      {
        target: 7,
        visible: false,
        searchable: false,
      },
    ],
    order: [[1, "asc"]],
    pagingType: "simple_numbers",
    initComplete: function (settings, json) {
      // add custom search fields, with button to reset search
      function replaceTemplate(id_base, ph) {
        var template = `
          <div class="input-group input-group-sm">${`<input id="search-${id_base}"
              class="form-control form-control-sm" type="search"
              placeholder="${ph}">`}
            <button class="btn btn-outline-secondary" type="button" id="delete-${id_base}">
              <img src="${x_path}">
            </button>
          </div>
        `;
        return template;
      }
      $("#table_taxon thead").append("<tr id='search-columns'></tr>");
      dt.columns().every(function () {
        if (this.visible() && !this.header().classList.contains("dtr-hidden")) {
          var title = this.header().innerText;
          var id_base = title.toLowerCase().replaceAll(/[,()\s]/g, "");
          var ph =
            gettext("TAXONS FORM Rerchercher un") + " " + title.toLowerCase();
          $("#search-columns").append(
            "<th>" + replaceTemplate(id_base, ph) + "</th>"
          );
        }
      });

      $(".dt-search").each(function () {
        var id_base = "global";
        var ph = $(this).text().replace(":", "");
        // copy datatables' instance of search field to make sure
        // every datatables property gets transmitted
        var input = $(this).find("input").detach();

        $(this).empty();
        $(this).append(replaceTemplate(id_base, ph));

        $("#search-" + id_base).replaceWith(input);
        input.attr("id", "search-" + id_base);
        input.attr("placeholder", ph);
      });

      // activate delete buttons
      $('[id^="search-"]').each(function () {
        var base = $(this).attr("id").replace("search-", "");
        $("#delete-" + base).on("click", () => {
          $(this).val("").trigger("keyup");
        });
        $(this).on("keyup", function () {
          if ($(this).val() !== "") {
            $("#delete-" + base)
              .removeClass("btn-outline-secondary")
              .addClass("btn-secondary");
          } else {
            $("#delete-" + base)
              .removeClass("btn-secondary")
              .addClass("btn-outline-secondary");
          }
        });
      });

      // Apply the individual column text search
      this.api()
        .columns(".ics-text")
        .every(function () {
          var column = this;
          var col = this.header()
            .firstChild.innerText.toLowerCase()
            .replaceAll(/[,()\s]/g, "");

          $("#search-" + col).on("keyup change clear", function () {
            if (column.search() !== this.value) {
              column.search(this.value).draw();
            }
          });
        });
      // Apply the individual column select search
      this.api()
        .columns(".ics-select-rank")
        .every(function () {
          var column = this;
          var col = this.header()
            .firstChild.innerText.toLowerCase()
            .replaceAll(/[,()\s]/g, "");

          $("#search-" + col).each(function () {
            var select = $(
              '<select class="form-select form-select-sm"><option value=""></option></select>'
            )
              .appendTo($(this).parent().parent().empty())
              .on("change", function () {
                column.search($(this).val()).draw();
              });

            var ranks = json["ranks"];
            ranks.forEach(function (e) {
              select.append('<option value="' + e + '">' + e + "</option>");
            });
          });
        });
    },
  });

  $("#table_taxon tbody").on("click", "tr", function () {
    var currentRow;
    if (this.classList.contains("child")) {
      currentRow = dt.row($(this).attr("data-dt-row")).data();
      window.location.href = currentRow[0];
    } else if (
      getComputedStyle(this.firstChild, "::before").content == "none"
    ) {
      currentRow = dt.row(this).data();
      window.location.href = currentRow[0];
    }
  });

  $("#taxons-photos-switch").prop("checked", false);
  $("#taxons-photos-switch").on("click", function () {
    myData.switch = !myData.switch;
    dt.ajax.reload();
  });
});

import logging
from datetime import datetime
from email.message import Message as EmailMessage
from mailbox import utils

from .models import Message
from .transports import ImapTransport

logger = logging.getLogger(__name__)


class Mailbox:
    def get_connection(self):
        """Returns the transport instance for this mailbox."""

        settings = utils.get_settings()
        conn = ImapTransport(settings["host"], settings["port"])
        conn.connect(settings["username"], settings["password"])
        return conn

    def process_incoming_message(self, message):
        """Process a message incoming to this mailbox."""
        msg = self._process_message(message)
        if msg is None:
            return None
        msg.outgoing = False
        msg.save()

        # message_received.send(sender=self, message=msg)

        return msg

    def record_outgoing_message(self, message):
        """Record an outgoing message associated with this mailbox."""
        msg = self._process_message(message)
        if msg is None:
            return None
        msg.outgoing = True
        msg.save()
        return msg

    def _get_dehydrated_message(self, msg, record):
        settings = utils.get_settings()

        new = EmailMessage()
        if msg.is_multipart():
            for header, value in msg.items():
                new[header] = value
            for part in msg.get_payload():
                new.attach(self._get_dehydrated_message(part, record))
        elif (
            settings["strip_unallowed_mimetypes"]
            and not msg.get_content_type() in settings["allowed_mimetypes"]
        ):
            for header, value in msg.items():
                new[header] = value
            # Delete header, otherwise when attempting to  deserialize the
            # payload, it will be expecting a body for this.
            del new["Content-Transfer-Encoding"]
            new[
                settings["altered_message_header"]
            ] = "Stripped; Content type %s not allowed" % (msg.get_content_type())
            new.set_payload("")
        elif (msg.get_content_type() not in settings["text_stored_mimetypes"]) or (
            "attachment" in msg.get("Content-Disposition", "")
        ):
            placeholder = EmailMessage()
            placeholder[settings["attachment_interpolation_header"]] = "true"
            new = placeholder
        else:
            content_charset = msg.get_content_charset()
            if not content_charset:
                content_charset = "ascii"
            try:
                # Make sure that the payload can be properly decoded in the
                # defined charset, if it can't, let's mash some things
                # inside the payload :-\
                msg.get_payload(decode=True).decode(content_charset)
            except LookupError:
                logger.warning(
                    "Unknown encoding %s; interpreting as ASCII!", content_charset
                )
                msg.set_payload(msg.get_payload(decode=True).decode("ascii", "ignore"))
            except ValueError:
                logger.warning(
                    "Decoding error encountered; interpreting %s as ASCII!",
                    content_charset,
                )
                msg.set_payload(msg.get_payload(decode=True).decode("ascii", "ignore"))
            new = msg
        return new

    def _process_message(self, message):
        msg = Message()
        msg._email_object = message

        if "subject" in message:
            msg.subject = utils.convert_header_to_unicode(message["subject"])[0:255]
        if "message-id" in message:
            msg.message_id = message["message-id"][0:255].strip()
        if (
            "[contact]" in msg.subject or "[signal]" in msg.subject
        ) and "Reply-To" in message:
            msg.from_header = utils.convert_header_to_unicode(message["Reply-To"])
        elif "from" in message:
            msg.from_header = utils.convert_header_to_unicode(message["from"])
        if "to" in message:
            msg.to_header = utils.convert_header_to_unicode(message["to"])
        elif "Delivered-To" in message:
            msg.to_header = utils.convert_header_to_unicode(message["Delivered-To"])
        if "date" in message:
            raw_date = utils.convert_header_to_unicode(message["date"])
            try:
                msg.received = datetime.strptime(raw_date, "%a, %d %b %Y %H:%M:%S %z")
            except ValueError:
                msg.received = datetime.now()
        else:
            msg.received = datetime.now()

        msg.save()
        message = self._get_dehydrated_message(message, msg)
        try:
            body = message.as_string()
        except KeyError as exc:
            # email.message.replace_header may raise 'KeyError' if the header
            # 'content-transfer-encoding' is missing
            logger.warning(
                "Failed to parse message: %s",
                exc,
            )
            return None
        msg.set_body(body)
        if message["in-reply-to"]:
            try:
                msg.in_reply_to = Message.objects.filter(
                    message_id=message["in-reply-to"].strip()
                )[0]
            except IndexError:
                pass
        msg.save()
        return msg

    def get_new_mail(self, condition=None):
        """Connect to this transport and fetch new messages."""

        connection = self.get_connection()
        if not connection:
            return
        for message in connection.get_message(condition):
            msg = self.process_incoming_message(message)
            if msg is not None:
                yield msg

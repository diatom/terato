"""
Django settings for main project.
"""

import os
import sys
from pathlib import Path
from urllib.request import unquote

import environ
from django.contrib.messages import constants as message_constants
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from generic.views import custom_csrf_failure

VERSION = "25.0.1"

env = environ.Env()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# reading .env file
environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("SECRET_KEY")

TESTING = sys.argv[1:2] == ["test"]
DEBUG = False if TESTING else env.bool("DEBUG")

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")

# Application definition

INSTALLED_APPS = [
    "djatoms.admin.apps.AdminConfig",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.postgres",
    "django.contrib.gis",
    "django.contrib.humanize",
    "crispy_forms",
    "crispy_bootstrap5",
    "modeltranslation",
    "webpack_loader",
]

my_apps = [
    "atlas",
    "contact",
    "generic",
    "mailbox",
    "main",
    "users",
    "visits",
    "djatoms",
]

INSTALLED_APPS += my_apps

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "visits.middleware.VisitMiddleware",
]

if not DEBUG:
    MIDDLEWARE.append("csp.middleware.CSPMiddleware")

if DEBUG and not TESTING:
    INTERNAL_IPS = [
        "127.0.0.1",
    ]
    INSTALLED_APPS.append("debug_toolbar")
    MIDDLEWARE.insert(0, "main.middleware.NonHtmlDebugToolbarMiddleware")
    MIDDLEWARE.insert(0, "debug_toolbar.middleware.DebugToolbarMiddleware")

ROOT_URLCONF = "main.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "main/templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "main.wsgi.application"


# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "OPTIONS": {"options": "-c search_path=" + env("SEARCH_PATH")},
        "NAME": env("DATABASE_NAME"),
        "USER": env("DATABASE_USER"),
        "PASSWORD": env("DATABASE_PASSWORD"),
        "HOST": env("DATABASE_HOST"),
        "PORT": env("DATABASE_PORT"),
    }
}

# Model to use to represent a User
# https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
AUTH_USER_MODEL = "users.User"

# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation."
        "UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]

LOGIN_URL = reverse_lazy("users:login")
LOGIN_REDIRECT_URL = reverse_lazy("users:profile")
LOGOUT_REDIRECT_URL = "/"

# Messages : Bootstrap style

MESSAGE_TAGS = {
    message_constants.DEBUG: "alert alert-primary alert-dismissible",
    message_constants.INFO: "alert alert-info alert-dismissible",
    message_constants.SUCCESS: "alert alert-success alert-dismissible",
    message_constants.WARNING: "alert alert-warning alert-dismissible",
    message_constants.ERROR: "alert alert-danger alert-dismissible",
}

# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = "fr"

LANGUAGES = [
    ("fr", _("French")),
    ("en", _("English")),
]

LANGUAGE_COOKIE_SECURE = True

LOCALE_PATHS = [os.path.join(BASE_DIR, app + "/locale") for app in my_apps]

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = env("STATIC_URL")
STATIC_ROOT = env("STATIC_ROOT")
STATICFILES_DIRS = (os.path.join(BASE_DIR, env("WEBPACK_BUNDLE_DIR")),)

# https://github.com/django-webpack/django-webpack-loader
WEBPACK_LOADER = {}
for app in my_apps:
    WEBPACK_LOADER[app.upper()] = {
        "CACHE": not DEBUG,
        "BUNDLE_DIR_NAME": app,
        "STATS_FILE": os.path.join(BASE_DIR, "webpack-stats.json"),
        "POLL_INTERVAL": 0.1,
        "IGNORE": [r".+\.hot-update.js", r".+\.map"],
    }

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Form default template
# https://docs.djangoproject.com/en/4.2/releases/4.1/#forms
FORM_RENDERER = "django.forms.renderers.DjangoTemplates"


# Custom test runner for DB schema handling
# https://stackoverflow.com/questions/31346655/django-and-postgresql-testing-schema
TEST_RUNNER = "tests.runner.PostgresSchemaTestRunner"

FIXTURE_DIRS = ["tests/fixtures"]

# Crispy forms
# https://github.com/django-crispy-forms/crispy-bootstrap5
# https://django-crispy-forms.readthedocs.io/en/latest/index.html
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap5"
CRISPY_TEMPLATE_PACK = "bootstrap5"


# EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
# EMAIL_FILE_PATH = BASE_DIR / "sent_emails"

DEFAULT_FROM_EMAIL = env("DEFAULT_FROM_EMAIL")
EMAIL_HOST_USER = env("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = unquote(env("EMAIL_HOST_PASSWORD"))
EMAIL_HOST = env("EMAIL_HOST_SEND")
EMAIL_PORT = env.int("EMAIL_PORT_SEND")
EMAIL_USE_TLS = env.bool("EMAIL_USE_TLS")

DJANGO_MAILBOX_EMAIL_HOST = env("EMAIL_HOST_GET")
DJANGO_MAILBOX_EMAIL_PORT = env("EMAIL_PORT_GET")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "{levelname} {message}",
            "style": "{",
        }
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
        },
        "file": {
            "level": "WARNING",
            "class": "logging.FileHandler",
            "filename": BASE_DIR / "main/errors.log",
            "formatter": "simple",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["console", "file"],
            "level": "INFO",
            "propagate": True,
        },
    },
}

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True
CSRF_FAILURE_VIEW = custom_csrf_failure
CSRF_TRUSTED_ORIGINS = ["https://" + a for a in ALLOWED_HOSTS]

SECURE_HSTS_SECONDS = 15768000
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

DATA_UPLOAD_MAX_NUMBER_FIELDS = 2000

REQUEST_DELETION_DAYS = env("REQUEST_DELETION_DAYS") or None
ERROR_DELETION_DAYS = env("ERROR_DELETION_DAYS") or None

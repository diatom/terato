from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from django.urls import resolve
from django.utils import timezone

from .models import Request
from .views import register_error


def IGNORE_REQUEST(request):
    if getattr(settings, "IGNORE_REQUEST", None):
        return settings.IGNORE_REQUEST

    if (
        any([x in resolve(request.path).namespace for x in ["api", "admin"]])
        or resolve(request.path).url_name == "javascript-catalog"
    ):
        return True

    return False


class VisitMiddleware:
    """Middleware to record user visits."""

    def __init__(self, get_response):
        self.get_response = get_response

        apps = getattr(settings, "INSTALLED_APPS")
        if "django.contrib.sessions" not in apps:
            raise MiddlewareNotUsed(
                "Cannot use visit middleware without the sessions app"
            )

        middlewares = getattr(settings, "MIDDLEWARE")
        if "django.contrib.sessions.middleware.SessionMiddleware" not in middlewares:
            raise MiddlewareNotUsed(
                "Cannot use visit middleware without session middleware"
            )

    def __call__(self, request):

        # make sure there's a session key, even for anonymous users
        if not request.session or not request.session.session_key:
            request.session.save()

        # do not count hidden requests
        if not IGNORE_REQUEST(request):
            r = Request(
                request_time=timezone.now(),
                path=request.path,
                session=request.session.session_key,
            )
            r.save()

        response = self.get_response(request)

        if response.status_code >= 400:
            register_error(request, response)

        return response

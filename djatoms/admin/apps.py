from django.contrib.admin.apps import AdminConfig


class AdminConfig(AdminConfig):
    default_site = "djatoms.admin.admin.MyAdminSite"
    # name = "djatoms.admin"

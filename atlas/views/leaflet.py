from django.db.models import Count

from djatoms.views import LeafletLayerView

from ..models import Station


class StationsLayerView(LeafletLayerView):

    model = Station
    properties = ["station_id", "station_code", "commune", "station_name"]

    def get_queryset(self):

        sel = self.request.GET.get("arg")
        if not sel:
            sel = -1

        qs = Station.objects.filter(pk=sel)
        if all([x in self.request.GET for x in ("x0", "x1", "y0", "y1")]):
            qs |= super().get_queryset()
        return qs


class ContributionsLayerView(LeafletLayerView):

    model = Station
    properties = [
        "station_id",
        "station_code",
        "commune",
        "station_name",
        "diatom__count",
    ]

    def get_queryset(self):

        taxon = self.request.GET.get("arg")

        if taxon:
            qs = Station.objects.filter(diatom__children__ancestor=taxon).annotate(
                diatom__count=Count("diatom__children__diatom")
            )
            qs &= super().get_queryset()
            return qs
        return Station.objects.none()

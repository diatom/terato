"""
The code of this app is a stripped-down and adapted version of Django-mailbox,
a Python package available online : https://django-mailbox.readthedocs.io/en/latest/.

See copy of the license.
"""
default_app_config = "mailbox.apps.MailBoxConfig"

import email
import imaplib
import logging
from email.errors import MessageParseError

from django.conf import settings


class EmailTransport:
    def get_email_from_bytes(self, contents):
        message = email.message_from_bytes(contents)

        return message


# By default, imaplib will raise an exception if it encounters more
# than 10k bytes; sometimes users attempt to consume mailboxes that
# have a more, and modern computers are skookum-enough to handle just
# a *few* more messages without causing any sort of problem.
imaplib._MAXLINE = 1000000


logger = logging.getLogger(__name__)


class ImapTransport(EmailTransport):
    def __init__(self, hostname, port=None):
        self.integration_testing_subject = getattr(
            settings, "DJANGO_MAILBOX_INTEGRATION_TESTING_SUBJECT", None
        )
        self.hostname = hostname
        self.port = port

        self.transport = imaplib.IMAP4_SSL
        if not self.port:
            self.port = 993

    def connect(self, username, password):
        self.server = self.transport(self.hostname, self.port)
        typ, msg = self.server.login(username, password)

        self.server.select()

    def _get_all_message_ids(self):
        # Fetch all the message uids
        response, message_ids = self.server.uid("search", None, "ALL")
        message_id_string = message_ids[0].strip()
        # Usually `message_id_string` will be a list of space-separated
        # ids; we must make sure that it isn't an empty string before
        # splitting into individual UIDs.
        if message_id_string:
            return message_id_string.decode().split(" ")
        return []

    def get_message(self, condition=None):
        message_ids = self._get_all_message_ids()

        if not message_ids:
            return

        for uid in message_ids:
            try:
                typ, msg_contents = self.server.uid("fetch", uid, "(RFC822)")
                if not msg_contents:
                    continue
                try:
                    message = self.get_email_from_bytes(msg_contents[0][1])
                except TypeError:
                    # This happens if another thread/process deletes the
                    # message between our generating the ID list and our
                    # processing it here.
                    continue

                if condition and not condition(message):
                    continue

                yield message
            except MessageParseError:
                continue

            self.server.uid("store", uid, "+FLAGS", "(\\Deleted)")
        self.server.expunge()
        return

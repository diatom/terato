import ast

from django import forms
from django.test import RequestFactory
from django.views.generic import FormView

from tests.testcases import TestCase

from ..models import Error
from ..views import ErrorFormViewMixin


class ErrorForm(forms.Form):
    field1 = forms.CharField()
    field2 = forms.IntegerField()
    password = forms.CharField()


class ErrorFormView(ErrorFormViewMixin, FormView):
    form_class = ErrorForm
    template_name = "home.html"


data = {
    "field1": 21,
    "field2": "ijtol",
    "password": "password",
    "csrfmiddlewaretoken": "token",
}


class ErrorFormViewMixinTestCase(TestCase):
    def setUp(self):
        self.request = RequestFactory()
        self.request.POST = data
        self.request.path = "/"
        self.request.session = self.client.session
        self.request.session["session_key"] = "key"
        self.request.headers = {"User-Agent": ""}

        self.view = ErrorFormView()
        self.view.setup(self.request)

        self.form = self.view.form_class(data)
        self.form.full_clean()

    def test_form_invalid_creates_error(self):

        self.assertEqual(0, Error.objects.count())
        self.view.form_invalid(self.form)
        self.assertEqual(1, Error.objects.count())

        e = Error.objects.last()

        self.assertEqual(e.path, "/")
        self.assertEqual(e.error_code, 302)
        self.assertEqual(e.traceback, str(self.form.errors.as_data()))
        self.assertEqual(e.ua_string, "")

    def test_form_invalid_posted_data(self):

        self.view.form_invalid(self.form)
        e = Error.objects.last()

        decoded = ast.literal_eval(e.post_decoded)

        self.assertNotIn("password", decoded)
        self.assertNotIn("csrfmiddlewaretoken", decoded)
        self.assertEqual(data["field1"], decoded["field1"])
        self.assertEqual(data["field2"], decoded["field2"])

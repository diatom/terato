from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .actions import delete_selected


class MyAdminSite(admin.AdminSite):
    site_header = _("ADMIN Tératothèque - Admin")

    def __init__(self, name="admin"):
        super().__init__()
        self.disable_action("delete_selected")
        self.add_action(delete_selected)


admin_site = MyAdminSite(name="admin")

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DeleteView, DetailView

from contact.forms import FlagForm
from main.utils import refresh_materialized_view

from ..models import Diatom, DiatomDefoFactor


class DiatomDetailView(DetailView):
    model = Diatom
    template_name = "detail_diatom.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["flag_form"] = FlagForm(
            initial={
                "diatom": self.object,
            }
        )
        return context


class DiatomDeleteView(LoginRequiredMixin, DeleteView):
    model = Diatom
    success_url = reverse_lazy("users:profile")

    def form_valid(self, form):
        DiatomDefoFactor.objects.filter(diatom=self.object).delete()
        self.object.delete()
        refresh_materialized_view("terato.children_diatoms")
        messages.success(self.request, _("DELETE MSG Diatomée supprimée"))
        return redirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        return redirect(
            reverse_lazy("atlas:consult:detail_diatom", args=[kwargs["pk"]])
        )

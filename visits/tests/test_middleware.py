from unittest import mock

from django.core.exceptions import MiddlewareNotUsed
from django.test import Client, modify_settings, override_settings
from django.urls import reverse
from django.utils import timezone

from tests import TestCase

from ..middleware import VisitMiddleware
from ..models import Error, Request

home = reverse("generic:home")


class UserVisitMiddlewareTest(TestCase):
    def test_request_record(self):

        self.client.get(home)
        self.assertEqual(Request.objects.count(), 1)
        r = Request.objects.last()
        self.assertEqual(r.request_time.date(), timezone.now().date())
        self.assertEqual(r.path, home)

    def test_n_visited_hidden_pages(self):
        self.client.get(reverse("atlas:api:diatom_search"), follow=True)

        self.assertEqual(Request.objects.count(), 0)

    @override_settings(IGNORE_REQUEST=home)
    def test_ignore_in_settings(self):
        self.client.get(home, follow=True)
        self.assertEqual(Request.objects.count(), 0)

    @modify_settings(INSTALLED_APPS={"remove": "django.contrib.sessions"})
    def test_sessions_app_not_used(self):
        with self.assertRaises(MiddlewareNotUsed):
            VisitMiddleware(mock.Mock())

    @modify_settings(
        MIDDLEWARE={"remove": "django.contrib.sessions.middleware.SessionMiddleware"}
    )
    def test_sessions_middleware_not_used(self):
        with self.assertRaises(MiddlewareNotUsed):
            VisitMiddleware(mock.Mock())

    def test_error_report(self):

        # create a csrf error in the login form
        data = {
            "email": "whatever",
            "password": "revetahw",
            "csrfmiddlewaretoken": "obviouslyfalse",
        }
        csrf_client = Client(enforce_csrf_checks=True)
        csrf_client.post(reverse("users:login"), data=data)

        self.assertEqual(Error.objects.count(), 1)
        e = Error.objects.last()
        self.assertEqual(e.path, reverse("users:login"))
        self.assertEqual(e.error_code, 403)

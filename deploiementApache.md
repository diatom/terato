---
title: "TERATO - déploiement de l'application avec un serveur Apache"
author:
    - Pierre VILLEFOURCEIX-GIMENEZ
    - Éric Quinton
date: 06/12/2023
lang: fr
---

# Déployer l'application TERATO avec Apache

## Présentation

L'application a été créée avec *Python/Django*. Elle fonctionne avec une base de données Postgresql, et est déployée dans un serveur Apache.

La configuration présentée ici a été mise au point dans un ordinateur fonctionnant avec une distribution Linux Manjaro (dérivée de Arch). Elle est déployée dans le sous-dossier `/var/www/terato`.

Dans un contexte de site web, un environnement virtuel (*venv*) est créé pour installer les dépendances Python nécessaires. L'application est exécutée en mode CGI.

Le compte web utilisé par le serveur Apache doit disposer des droits de lecture dans l'ensemble des dossiers utilisés par l'application, dont l'environnement virtuel. Dans les commandes ci-dessous, le compte web est considéré comme membre du groupe Linux des dossiers.

## Installer l'application

### Installer Docker

Installez docker et docker-compose en suivant les instructions présentes ici : [Install the Compose plugin | Docker Docs](https://docs.docker.com/compose/install/linux/#install-using-the-repository)

### Installer et configurer l'application dans le dossier web/terato

```bash
cd /var/www
git clone https://gitlab.irstea.fr/terato/terato.git -b main
cd terato
```

### Paramétrer le fichier spécifique à l'application (.env)

Recopiez le fichier *main/.env.example* :

```bash
cp .env.example .env
```

Puis éditez-le avec votre outil favori :

```ini
SECRET_KEY=""

DATABASE_NAME=name_of_the_database
DATABASE_USER=login_for_app
DATABASE_PASSWORD="associated_password"
DATABASE_HOST="server_name"
DATABASE_PORT=5432
SEARCH_PATH="django,terato,diatom,public"
SEARCH_PATH="django,terato,diatom,public"

# Set to False for production
DEBUG=True

ALLOWED_HOSTS="terato.local"
STATIC_ROOT="static"
STATIC_URL="assets/"
```

Pour renseigner le champ `SECRET_KEY`, lancez la commande suivante et recopiez le résultat dans le fichier `.env` :

```bash
python -c "from django.core.management.utils import get_random_secret_key;print(get_random_secret_key())"
```

La variable `ALLOWED_HOSTS` doit contenir le DNS de votre site web.

<u>Attention : </u> Toute modification du fichier `.env` impose de recréer le container Docker.

### Créer le container docker

```bash
cd /var/www/terato
docker-compose --env-file .env build
docker-compose up -d
docker cp terato_app:/usr/src/terato/static /home/equinton/web/terato/static/
```

<u>Attention : </u> Dans les chemins ci-desssus, bien spécifier les chemins complets correspondants à la variable d'environnement STATIC_ROOT.

### Créer le fichier vhosts pour Apache

Créez le fichier `terato.conf`, à positionner dans le dossier `/etc/httpd/conf/vhosts` (Arch, Manjaro) ou dans le dossier `/etc/apache2/sites-available` (Debian, Ubuntu) :

```apacheconf
<VirtualHost *:80>
    ServerName terato.local
    ServerPath /terato.local
    RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>
<VirtualHost *:443>
    ServerName terato.local:8000
    ServerPath /terato.local
    SSLEngine on
    SSLCertificateFile    /etc/ssl/certs/server.crt
    SSLCertificateKeyFile /etc/ssl/private/server.key
    # Uniquement pour les certificats gérés localement
    SSLCACertificateFile /etc/ssl/certs/cacert.crt
    RequestHeader set X-FORWARDED-PROTOCOL ssl
    RequestHeader set X-FORWARDED-SSL on
    RequestHeader set X-FORWARDED-PROTO "https"

    DocumentRoot /home/equinton/web/terato
   <Proxy *>
    require all granted
   </Proxy>
   # Remplacer assets par la valeur de STATIC_URL
   ProxyPass /assets/ !
   # Remplacer assets par STATIC_URL et le chemin par
   # STATIC_ROOT (chemin complet)
   Alias /assets/ /home/equinton/web/terato/static/
   ProxyPreserveHost On
   ProxyPass / http://localhost:8000/
   ProxyPassReverse / http://127.0.0.1:8000/
   LogLevel info
   # De nouveau remplacer par STATIC_ROOT
   <directory /home/equinton/web/terato/static/>
      Require all granted
   </directory>
</VirtualHost>
```

Avec Debian ou Ubuntu, activez le *vhost* créé :

```bash
a2ensite terato
```

Puis redémarrez le serveur web :

```bash
# Arch, Manjaro
systemctl restart httpd
# Debian, Ubuntu
systemctl restart apache2
```

## Mettre à jour l'application

Voici l'ensemble des opérations à réaliser :
Voici l'ensemble des opérations à réaliser :

```bash
cd /var/www/terato
# Récupérer la nouvelle version
git restore .
git restore .
git pull origin main

# Reconstruire le container
docker container stop terato_app
docker-compose --env-file .env build
docker-compose up -d
sudo rm -r /home/equinton/web/terato/static
docker cp terato_app:/usr/src/terato/static /home/equinton/web/terato/static/

```

La nouvelle version est alors disponible.

from tests import CSPTestMixin, TestCase


class CSPTestCase(CSPTestMixin, TestCase):
    def test_csp_contact(self):
        expected = {
            "img-src": [
                "'self'",
                "data:",
            ],
            "default-src": ["'self'"],
        }

        self._csp_test("contact", expected)

    def test_csp_all_other(self):
        self._csp_test_all_other(["contact"])

import csv

from django import forms
from django.contrib import admin

from djatoms.admin import ExtendedModelAdmin
from djatoms.admin.actions import delete_selected
from djatoms.admin.views.main import UploadQuerySet

from .models import TestModel

site = admin.AdminSite(name="admin")
site_uploadable = admin.AdminSite(name="admin_uploadable")
site_not_uploadable = admin.AdminSite(name="admin_not_uploadable")
site_uploadable.disable_action("delete_selected")
site_uploadable.add_action(delete_selected)


class TestModelUploadForm(forms.Form):

    file = forms.FileField(
        widget=forms.FileInput(attrs={"accept": ".csv"}),
    )


def get_upload_queryset(modeladmin, request):
    queryset = UploadQuerySet([], model=modeladmin.model)

    uploaded = request.FILES["file"].read().decode("utf-8").split("\n")
    reader = csv.DictReader(uploaded)

    for row in reader:
        t = TestModel(name=row["name"])
        queryset.append(t)
    return queryset


list_display = ("id", "name")
list_editable = ("name",)
list_display_links = ("id",)


class TestModelAdmin(admin.ModelAdmin):
    list_display = list_display
    list_editable = list_editable
    list_display_links = list_display_links


class TestExtendedModelAdmin(ExtendedModelAdmin):

    list_display = list_display
    list_editable = list_editable
    list_display_links = list_display_links

    upload_list_display = ("name",)
    upload_list_editable = ("name",)
    upload_form = TestModelUploadForm


class TestUploadableMA(TestExtendedModelAdmin):
    uploadable = True

    def get_upload_queryset(self, request):
        return get_upload_queryset(self, request)


class TestNotUploadableMA(TestExtendedModelAdmin):
    uploadable = False


site.register(TestModel, TestModelAdmin)
site_uploadable.register(TestModel, TestUploadableMA)
site_not_uploadable.register(TestModel, TestNotUploadableMA)

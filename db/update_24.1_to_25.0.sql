-- Create model Request

CREATE TABLE django.visits_request (
    id bigint NOT NULL,
    request_time timestamp with time zone NOT NULL,
    "path" varchar(100) NOT NULL,
    session character(40) NOT NULL
);

ALTER TABLE django.visits_request OWNER TO diatom_terato_admin;

CREATE SEQUENCE django.visits_request_id_seq
	INCREMENT BY 1
	MINVALUE -9223372036854775808
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
ALTER SEQUENCE django.visits_request_id_seq OWNER TO diatom_terato_admin;

ALTER TABLE django.visits_request ALTER COLUMN id TYPE bigint;
ALTER TABLE django.visits_request ALTER COLUMN id SET DEFAULT nextval('django.visits_request_id_seq'::regclass);
ALTER TABLE django.visits_request ADD CONSTRAINT visits_request_pk PRIMARY KEY (id);

CREATE INDEX "request_session_time_idx" ON django.visits_request USING btree ("request_time", "session");

-- Create model Error

CREATE TABLE django.visits_error (
    id integer NOT NULL,
    error_time timestamp with time zone NOT NULL,
    "path" varchar(100) NOT NULL,
    error_code smallint NOT NULL,
    traceback text NOT NULL,
    posted bytea NULL,
    ua_string varchar(250) NOT NULL
);

ALTER TABLE django.visits_error OWNER TO diatom_terato_admin;

CREATE SEQUENCE django.visits_error_id_seq
	INCREMENT BY 1
	MINVALUE -2147483648
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

ALTER SEQUENCE django.visits_error_id_seq OWNER TO diatom_terato_admin;

ALTER TABLE django.visits_error ALTER COLUMN id TYPE integer;
ALTER TABLE django.visits_error ALTER COLUMN id SET DEFAULT nextval('django.visits_error_id_seq'::regclass);
ALTER TABLE django.visits_error ADD CONSTRAINT visits_error_pk PRIMARY KEY (id);

COMMENT ON COLUMN "django"."visits_error"."ua_string" IS 'Client User-Agent HTTP header';

CREATE INDEX "error_error_time_idx" ON django.visits_error USING btree ("error_time");

-- Version
insert into terato.db_version(version_name, version_date) values ('25.0', '2025-01-24');

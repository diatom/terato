from django.core.exceptions import EmptyResultSet
from django.db.models import QuerySet


class ModelTestMixin:
    """
    Mixin to be used with TestCase. Provides basic functionality for
    testing models.

    Assumes that models are tested upon some data, provided via
    fixtures or otherwise.

    Inheritor test classes should provide :
        - model: model class to be tested
        - name_field: expected field used in __str__ method
    """

    model = None
    name_field = None

    def __setattr__(self, attr, value):
        if attr == "queryset":
            if issubclass(type(value), QuerySet):
                if len(value) == 0:
                    raise EmptyResultSet(
                        "Test failed because queryset to test is empty"
                    )

            elif value is not None:
                raise TypeError(
                    "Case 'queryset' property should be of a type \
                    inheriting from django.db.models.query.QuerySet, not '{}'".format(
                        type(value)
                    )
                )

        super().__setattr__(attr, value)

    def setUp(self):

        super().setUp()
        self.queryset = self.model.objects.all()

    def test_object_str_is_name_field(self):

        if self.name_field is not None:
            for obj in self.queryset:
                self.assertEqual(str(obj), getattr(obj, self.name_field))
        else:
            raise TypeError("name_field must be an appropriate string")

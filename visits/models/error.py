import user_agents
from django.contrib import admin
from django.contrib.postgres.indexes import BTreeIndex
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class Error(models.Model):
    """
    Record of an error that occurred during a visit.

    Used for being able to reproduce errors.
    """

    id = models.AutoField(primary_key=True)
    error_time = models.DateTimeField(
        _("Timestamp"),
        default=timezone.now,
    )
    path = models.CharField(_("Path"), max_length=100)
    error_code = models.IntegerField(_("Error code"))
    traceback = models.TextField(_("Traceback"))
    posted = models.BinaryField(null=True)

    ua_string = models.CharField(
        _("User agent"),
        help_text=_("Client User-Agent HTTP header"),
        blank=True,
        db_comment="Client User-Agent HTTP header",
        max_length=250,
    )

    @property
    @admin.display(description=_("User agent"))
    def user_agent(self):
        """Return UserAgent object from the raw user_agent string."""
        return user_agents.parsers.parse(self.ua_string)

    @property
    @admin.display(description=_("Browser"))
    def browser(self):
        return self.user_agent.get_browser()

    @property
    @admin.display(description=_("Device type"))
    def device(self):
        return self.user_agent.get_device()

    @property
    @admin.display(description=_("Operating System"))
    def os(self):
        return self.user_agent.get_os()

    @property
    @admin.display(description=_("POST decoded"))
    def post_decoded(self):
        if self.posted:
            if isinstance(self.posted, memoryview):
                return self.posted.tobytes().decode()
            else:
                return self.posted.decode()
        else:
            return "-"

    def __str__(self):
        return f"Error {self.error_code} - {self.error_time.date()}"

    class Meta:
        get_latest_by = "error_time"
        managed = False
        verbose_name = _("Error")
        verbose_name_plural = _("Errors")

        indexes = [
            BTreeIndex(fields=["error_time"], name="error_error_time_idx"),
        ]

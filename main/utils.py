import subprocess


def refresh_materialized_view(name):

    args = ["python", "manage.py", "refresh_mv", name]
    subprocess.Popen(args, start_new_session=True)
